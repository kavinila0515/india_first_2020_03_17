
/*
 Animation Flow
 PageByPage
 */

window.stage.screens = [];

for(var i=0; i<window.common_screens.length; i++ )
{
    window.stage.screens.push(window.common_screens[i]);
}
updatevalues();
var customer,dob,gender,email,mob,id,nom,annul,add;

function updatevalues()
{
if(window.up_CUSTOMER_NAME!='')
{
  window.customer=window.up_CUSTOMER_NAME;
  console.log('First Condition');
}
else
{
  console.log('2nd Condition');
    window.customer=window.p_CUSTOMER_NAME;
    

}
if (window.up_DOB_PH!='')
   {
   window.dob=window.up_DOB_PH;
}
else {
  window.dob=window.p_DOB_PH;
}
if (window.up_GENDER!='')
   {
   window.gender=window.up_GENDER;
}
else {
  window.gender=window.p_GENDER;
}
if (window.up_EMAIL!='')
   {
   window.email=window.up_EMAIL;
}
else {
  window.email=window.p_EMAIL;
}
if (window.up_MOBILE_NUMBER!='')
   {
   window.mob=window.up_MOBILE_NUMBER;
}
else {
  window.mob=window.p_MOBILE_NUMBER;
}
if (window.up_IDPROF!='')
   {
   window.id=window.up_IDPROF;
}
else {
  window.id=window.p_IDPROF;
}
if (window.up_NOMINEE_NAME!='')
   {
   window.nom=window.up_NOMINEE_NAME;
}
else {
  window.nom=window.p_NOMINEE_NAME;
}
if (window.up_ANNUALINCOME!='')
   {
   window.annul=window.up_ANNUALINCOME;
}
else {
  window.annul=window.p_ANNUALINCOME;
}
if (window.up_address!='' && window.up_address!=window.p_address)
   {
    //window.up_address=window.add1+window.add2+window.add3;
  //  window.up_address=window.res;
   window.add=window.up_address;
   console.log("updated address call");
}

else 
{

  //window.p_address=window.add1+window.add2+window.add3;
  window.add=window.p_address;
   console.log("address call");
}

}

address_mode(window.add);
check_add();

var numericType='indian';

//alert(customer);
var screens_eng = [
    {
        "sprite_animations": [
            {"sprite": "bar_01","x": 0,"y": 0,"loop": false,"timing": 0,"delay": 0,"toTopObj":1, "anchor":[0,0]},
            {"sprite": "logo_01","x": 20,"y": 35,"loop": false,"timing": 0,"delay": 0.1,"toTopObj":2, "anchor":[0,0]}
        ],
        "text_animations": [
            {"text": [{"content":"Pre Issuance Verification Call "}], "sx": -1000,"sy": 50,"x": 210 ,"y": 190,"size": 28,"weight":"bold","color":"#FFFFFF","tween_type": "Elastic.easeOut","timing": 200,"delay":0.2, "anchor":[0,0] }
        ],
        "functions": [
            {"fn": "SetBGTile('bg_02')", "delay": 0 },
            {"fn": "langAssetsRest()", "delay": 0 },
            {"fn": "getGeoLocationText()", "delay": 0 },
            {"fn": "webCamCreate()", "delay": 0 },
            {"fn": "initCamOnly()", "delay": 0.5 }
        ],
        "name": "Assets Loading",
        "timing": -1,
        "index": 2
    },
    {
        "sprite_animations": [
            {"sprite": "bar_01","x": 0,"y": 0,"loop": false,"timing": 0,"delay": 0,"toTopObj":1, "anchor":[0,0]},
            {"sprite": "logo_01","x": 10,"y": 33,"loop": false,"timing": 0,"delay": 0.1,"toTopObj":2,"anchor":[0,0]},
            {"sprite": "icn_01","x": 20,"y": 150,"loop": false,"timing": 0,"delay": 0, "toTopObj":1, "anchor":[0,0]},
            {"sprite": "bar_02","x": 300,"y": 250,"loop": false,"timing": 0,"delay": 0, "toTopObj":1, "anchor":[0.5,0]},
            {"sprite": "btn_02","x": 300,"y": 732,"loop": false,"timing": 0,"delay": 3, "toTopObj":1, "anchor":[0.5,0],"onClickFn":"goToPage(4),yesno_update('proceed','Welcome Screen')"},//4
            {"sprite": "btn_sp_1","x": 540,"y": 112,"loop": false,"timing": 0,"delay": 2, "toTopObj":1, "anchor":[0,0], "onClickFn":"audioReplay()"},

            {"sprite": "welcome_anim","x": 210,"y": 500,"loop": false,"timing": 30,"delay": 3, "toTopObj":1, "anchor":[0,0]}

        ],
        "text_animations": [
            {"text": [{"content":"Pre Issuance Verification Call "}], "sx": -1000,"sy": 50,"x": 190 ,"y": 50,"size": 28,"weight":"bold","color":"#003869","tween_type": "Elastic.easeOut","timing": 200,"delay":0.2, "anchor":[0,0]},

            {"text": [
                    {"content":"కేసులో మీరు మళ్ళీ వినాలనుకుంటే, ఇక్కడ క్లిక్ చేయండి ---->"}

                ], "sx": -1000,"sy": 120,"x": 238 ,"y": 120,"size": 12,"weight":"bold","color":"#003869","tween_type": "Elastic.easeOut","timing": 200,"delay":2, "anchor":[0,0],"wordWrap": true, "wordWrapWidth":520,"align":"right"}, //,"fontFamily":"Whitney Medium"
           {"text": [
             {"content":"స్వాగతం, "},
             {"content":"$var.window.p_CUSTOMER_NAME"},
               ], "sx": -1000,"sy": 150,"x": 102 ,"y": 160,"size": 20,"weight":"bold","color":"#003869","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },
           {"text": [
             {"content":"ప్రతిపాదన సంఖ్య: "},
             {"content":"$var.window.p_PROPOSAL_NUMBER"}
           ], "sx": -1000,"sy": 155,"x": 225 ,"y": 195,"size": 20,"weight":"bold","color":"#003869","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0.5,0] },

            {"text": [
            {"content":"మీ ప్రతిపాదన సంఖ్య యొక్క ప్రీ-జారీ ధృవీకరణ కాల్‌కు స్వాగతం. "},
            {"content":"$var.window.p_PROPOSAL_NUMBER"},
            {"content":". మీ ప్రతిపాదనను ప్రాసెస్ చేయడానికి వ్యక్తిగత మరియు ప్రణాళిక వివరాలు, రుణ ప్రశ్నలు, వైద్య ప్రశ్నలు & ఉత్పత్తి ప్రయోజనాలు మొదలైన ముఖ్యమైన వివరాలను అర్థం చేసుకోవడానికి మరియు నిర్ధారించడానికి ఈ ప్రక్రియ మీకు సహాయం చేస్తుంది."},
            ], "sx": -1000,"sy": 245,"x": 50 ,"y": 290,"size": 20,"lineSpacing":-1,"color":"#545252","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5,"wordWrap": true, "wordWrapWidth":520,"align":"left", "anchor":[0,0],"weight":"normal" },


            {"text": [{"content":"కొనసాగండి"}], "sx": -1000,"sy": 750,"x": 300 ,"y": 750,"size": 24,"weight":"bold","color":"#FFFFFF","tween_type": "Elastic.easeOut","timing": 200,"delay":3, "anchor":[0.5,0] }

        ],
        "sound_list": [
            {
                   "sound": ["welcome1_audio","$var.alphanumeric_window.p_PROPOSAL_NUMBER","welcome2_audio"]

            }
        ],
        "functions": [

            {"fn": "SetBGTile('bg_02')", "delay": 0 },
           {"fn": "cameraAccessError()", "delay" : 0.5 },
            {"fn": "captureImage()", "delay": 1.5 }


        ],
        "name": "Welcome Screen",
        "timing": -1,
        "index": 3
    },

  {
     "sprite_animations": [
            {"sprite": "bar_01","x": 0,"y": 0,"loop": false,"timing": 0,"delay": 0,"toTopObj":1, "anchor":[0,0]},
            {"sprite": "logo_01","x": 10,"y": 33,"loop": false,"timing": 0,"delay": 0.1,"toTopObj":2, "anchor":[0,0]},
            {"sprite": "bar_03","x": 300,"y": 180,"loop": false,"timing": 0,"delay": 0, "toTopObj":1, "anchor":[0.5,0]},
            {"sprite": "box_18","x": 300,"y": 223,"loop": false,"timing": 0,"delay": 0, "toTopObj":1, "anchor":[0.5,0]},
         //   {"sprite": "icn_02","x": 33,"y": 343,"loop": false,"timing": 0,"delay": 0, "toTopObj":1, "anchor":[0,0]},

            {"sprite": "btn_03_2","x": 150,"y": 740,"loop": false,"timing": 0,"delay": 4, "toTopObj":1, "anchor":[0.5,0],"onClickFn":"goToScreen(7,'cPerDet',1),yesno_update('Agree','Personal Details - Show')"},
            {"sprite": "btn_03_3","x": 450,"y": 740,"loop": false,"timing": 0,"delay": 4, "toTopObj":1, "anchor":[0.5,0],"onClickFn":"goToPage(5),yesno_update('Disagree','Personal Details - Show'),specLoad('tel')"},
            {"sprite": "btn_sp_1","x": 540,"y": 112,"loop": false,"timing": 0,"delay": 2, "toTopObj":1, "anchor":[0,0], "onClickFn":"audioReplay()"}
        ],
        "text_animations": [
           {"text": [{"content":"Pre Issuance Verification Call "}], "sx": -1000,"sy": 50,"x": 190 ,"y": 50,"size": 28,"weight":"bold","color":"#003869","tween_type": "Elastic.easeOut","timing": 200,"delay":0.2, "anchor":[0,0] },
            {"text": [
                    {"content":"పప్రతిపాదన సంఖ్య: "},
                    {"content":"$var.window.p_PROPOSAL_NUMBER"}
                ], "sx": -1000,"sy": 120,"x": 40 ,"y": 150,"size": 22,"weight":"bold","color":"#235291","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },
             {"text": [
                    {"content":"కేసులో మీరు మళ్ళీ వినాలనుకుంటే, ఇక్కడ క్లిక్ చేయండి ---->"}
                              ], "sx": -1000,"sy": 120,"x": 238 ,"y": 120,"size": 12,"weight":"bold","color":"#235291","tween_type": "Elastic.easeOut","timing": 200,"delay":2, "anchor":[0,0],"wordWrap": true, "wordWrapWidth":520,"align":"right" },
            {"text": [{"content":"వ్యక్తిగత & సంప్రదింపు వివరాలు"}], "sx": -1000,"sy": 120,"x": 300 ,"y": 181,"size": 26,"weight":"bold","color":"#FFFFFF","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0.5,0] },
            {"text": [{"content":"మా రికార్డ్స్ ప్రకారం మీ వ్యక్తిగత వివరాలు "}], "sx": -1000,"sy": 120,"x": 80 ,"y": 235,"size": 18,"weight":"bold","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },

            {"text": [{"content":"పేరు"}], "sx": -1000,"sy": 120,"x": 80 ,"y": 270,"size": 18,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },
            {"text": [{"content":":"}], "sx": -1000,"sy": 120,"x": 310 ,"y": 270,"size": 18,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },
            {"text": [{"content":window.customer}], "sx": -1000,"sy": 120,"x": 325 ,"y": 270,"size": 18,"weight":"bold","color":"#0098DE","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },


            {"text": [{"content":"పుట్టిన తేది"}], "sx": -1000,"sy": 120,"x": 80 ,"y": 310,"size": 18,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },
            {"text": [{"content":":"}], "sx": -1000,"sy": 120,"x": 310 ,"y": 310,"size": 18,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },
            {"text": [{"content":window.dob}], "sx": -1000,"sy": 120,"x": 325 ,"y": 310,"size": 18,"weight":"bold","color":"#0098DE","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },


            {"text": [{"content":"జెండర్"}], "sx": -1000,"sy": 120,"x": 80 ,"y": 350,"size": 18,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },
            {"text": [{"content":":"}], "sx": -1000,"sy": 120,"x": 310 ,"y": 350,"size": 18,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },
            {"text": [{"content":window.gender}], "sx": -1000,"sy": 120,"x": 325 ,"y": 350,"size": 18,"weight":"bold","color":"#0098DE","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },


            {"text": [{"content":"ఇమెయిల్ ఐడి"}], "sx": -1000,"sy": 120,"x": 80 ,"y":390,"size": 18,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },
            {"text": [{"content":":"}], "sx": -1000,"sy": 120,"x": 310 ,"y": 390,"size": 18,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },
            {"text": [{"content":window.email}], "sx": -1000,"sy": 120,"x": 325 ,"y": 390,"size": 18,"weight":"bold","color":"#0098DE","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },


            {"text": [{"content":"మొబైల్ నం."}], "sx": -1000,"sy": 120,"x": 80 ,"y": 430,"size": 18,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },
            {"text": [{"content":":"}], "sx": -1000,"sy": 120,"x": 310 ,"y": 430,"size": 18,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },
            {"text": [{"content":window.mob}], "sx": -1000,"sy": 120,"x": 325 ,"y": 430,"size": 18,"weight":"bold","color":"#0098DE","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },

            // {"text": [{"content":"ఐడి కార్డ్ ప్రూఫ్ రకం"}], "sx": -1000,"sy": 120,"x": 80 ,"y": 470,"size": 18,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },
            // {"text": [{"content":":"}], "sx": -1000,"sy": 120,"x": 310 ,"y": 470,"size": 18,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },
            // {"text": [{"content":id}], "sx": -1000,"sy": 120,"x": 325 ,"y": 470,"size": 18,"weight":"bold","color":"#0098DE","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },

            {"text": [{"content":"నామినీ పేరు & సంబంధం"}], "sx": -1000,"sy": 120,"x": 80 ,"y": 470,"size": 18,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },
            {"text": [{"content":":"}], "sx": -1000,"sy": 120,"x": 310 ,"y": 470,"size": 18,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },
            {"text": [{"content":window.nom}], "sx": -1000,"sy": 120,"x": 325 ,"y": 470,"size": 18,"weight":"bold","color":"#0098DE","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },

            {"text": [{"content":"వార్షిక ఆదాయం"}], "sx": -1000,"sy": 120,"x": 80 ,"y": 510,"size": 18,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },
            {"text": [{"content":":"}], "sx": -1000,"sy": 120,"x": 310 ,"y": 510,"size": 18,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },
            {"text": [{"content":window.annul}], "sx": -1000,"sy": 120,"x": 325 ,"y": 510,"size": 18,"weight":"bold","color":"#0098DE","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },


            {"text": [{"content":"చిరునామా"}], "sx": -1000,"sy": 120,"x": 80 ,"y": 550,"size": 18,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },
            {"text": [{"content":":"}], "sx": -1000,"sy": 120,"x": 310 ,"y": 550,"size": 18,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },
            {"text": [
                       {"content":window.add}

            ], "sx": -1000,"sy": 120,"x": 325 ,"y": 550,"size": 18,"weight":"bold","color":"#0098DE","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0],"wordWrap":true,"wordWrapWidth":250,"align":"left","lineSpacing":-6},

                                   
            {"text": [{"content":'తెరపై ప్రదర్శించబడే మొత్తం సమాచారం సరైనది అయితే, దయచేసి "అంగీకరిస్తున్నాను" పై క్లిక్ చేయండి, లేకపోతే "అంగీకరించలేదు" క్లిక్ చేయండి'}], "sx": -1000,"sy": 120,"x": 300 ,"y": 670,"size": 17,"weight":"bold","color":"#235291","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0.5,0],"wordWrap":true,"wordWrapWidth":500 },
         //   {"text": [{"content":'Please click on "Agree", else click "Disagree"'}], "sx": -1000,"sy": 120,"x": 300 ,"y": 698,"size": 18,"weight":"bold","color":"#235291","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0.5,0] },

            {"text": [{"content":'అంగీకరిస్తున్నారు'}], "sx": -1000,"sy": 120,"x": 150 ,"y": 754,"size": 19,"weight":"bold","color":"#FFFFFF","tween_type": "Elastic.easeOut","timing": 200,"delay":4, "anchor":[0.5,0] },
            {"text": [{"content":'విభేదిస్తున్నారు'}], "sx": -1000,"sy": 120,"x": 450 ,"y": 754,"size": 19,"weight":"bold","color":"#FFFFFF","tween_type": "Elastic.easeOut","timing": 200,"delay":4, "anchor":[0.5,0] },
        ],
        "sound_list": [
            {
                "sound": ["personal_details_audio"]
            }
        ],
        "functions": [
            {"fn": "SetBGTile('bg_02')", "delay": 0 },
             {"fn": "cameraAccessError()", "delay" : 0.5 },
             {"fn": "captureImage()", "delay": 1.5 }
        ],
        "name": "Personal Details - Show",
        "timing": -1,
        "index": 4
    },


    {
        "sprite_animations": [
            {"sprite": "bar_01","x": 0,"y": 0,"loop": false,"timing": 0,"delay": 0,"toTopObj":1, "anchor":[0,0]},
            {"sprite": "logo_01","x": 10,"y": 33,"loop": false,"timing": 0,"delay": 0.1,"toTopObj":2, "anchor":[0,0]},
            {"sprite": "bar_03","x": 300,"y": 180,"loop": false,"timing": 0,"delay": 0, "toTopObj":1, "anchor":[0.5,0]},
            {"sprite": "box_02","x": 300,"y": 223,"loop": false,"timing": 0,"delay": 0, "toTopObj":1, "anchor":[0.5,0]},

            {"sprite": "btn_03_2","x": 150,"y": 454,"loop": false,"timing": 0,"delay": 2, "toTopObj":1, "anchor":[0.5,0],"onClickFn":"goToPage(6)"},

            {"sprite": "btn_03_3","x": 450,"y": 454,"loop": false,"timing": 0,"delay": 2, "toTopObj":1, "anchor":[0.5,0],"onClickFn":"goToPageBack(4)"},

            {"sprite": "btn_sp_1","x": 540,"y": 112,"loop": false,"timing": 0,"delay": 2, "toTopObj":1, "anchor":[0,0], "onClickFn":"audioReplay()"}
        ],
          "text_animations": [
           {"text": [{"content":"Pre Issuance Verification Call "}], "sx": -1000,"sy": 50,"x": 190 ,"y": 50,"size": 28,"weight":"bold","color":"#003869","tween_type": "Elastic.easeOut","timing": 200,"delay":0.2, "anchor":[0,0] },
            {"text": [
                    {"content":"పప్రతిపాదన సంఖ్య: "},
                    {"content":"$var.window.p_PROPOSAL_NUMBER"}
                ], "sx": -1000,"sy": 120,"x": 40 ,"y": 150,"size": 22,"weight":"bold","color":"#235291","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },
             {"text": [
                    {"content":"కేసులో మీరు మళ్ళీ వినాలనుకుంటే, ఇక్కడ క్లిక్ చేయండి ---->"}
                              ], "sx": -1000,"sy": 120,"x": 238 ,"y": 120,"size": 12,"weight":"bold","color":"#235291","tween_type": "Elastic.easeOut","timing": 200,"delay":2, "anchor":[0,0],"wordWrap": true, "wordWrapWidth":520,"align":"right" },
            {"text": [{"content":"వ్యక్తిగత & సంప్రదింపు వివరాలు"}], "sx": -1000,"sy": 120,"x": 300 ,"y": 181,"size": 26,"weight":"bold","color":"#FFFFFF","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0.5,0] },

            {"text": [{"content":"ఈ స్క్రీన్‌పై ఉన్న వివరాలతో మీరు విభేదించాలనుకుంటున్నారా."}], "sx": -1000,"sy": 120,"x": 65 ,"y": 280,"size": 35,"weight":"bold","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0],"wordWrap": true, "wordWrapWidth":520,"align":"center" },

            {"text": [{"content":'అవును'}], "sx": -1000,"sy": 120,"x": 150 ,"y": 468,"size": 22,"weight":"bold","color":"#FFFFFF","tween_type": "Elastic.easeOut","timing": 200,"delay":2, "anchor":[0.5,0] },
            {"text": [{"content":'లేదు'}], "sx": -1000,"sy": 120,"x": 450 ,"y": 468,"size": 22,"weight":"bold","color":"#FFFFFF","tween_type": "Elastic.easeOut","timing": 200,"delay":2, "anchor":[0.5,0] },
        ],
        "functions": [

            {"fn": "SetBGTile('bg_02')", "delay": 0 }

        ],

         "sound_list": [
            {
               "sound": ["personal_yesno_audio"]
            }
        ],

        "name": "Personal YesNo - Confirm",
        "timing": -1,
        "index": 5
    },


 {
     "sprite_animations": [
            {"sprite": "bar_01","x": 0,"y": 0,"loop": false,"timing": 0,"delay": 0,"toTopObj":1, "anchor":[0,0]},
            {"sprite": "logo_01","x": 10,"y": 33,"loop": false,"timing": 0,"delay": 0.1,"toTopObj":2, "anchor":[0,0]},
            {"sprite": "bar_03","x": 300,"y": 180,"loop": false,"timing": 0,"delay": 0, "toTopObj":1, "anchor":[0.5,0]},
            {"sprite": "box_18","x": 300,"y": 223,"loop": false,"timing": 0,"delay": 0, "toTopObj":1, "anchor":[0.5,0]},
         //   {"sprite": "icn_02","x": 35,"y": 343,"loop": false,"timing": 0,"delay": 0, "toTopObj":1, "anchor":[0,0]},
            {"sprite": "btn_02","x": 300,"y": 743,"loop": false,"timing": 0,"delay": 4.5, "toTopObj":1, "anchor":[0.5,0],"onClickFn":"editGoToPage(17,240,216,'ePerDet','cPerDet'),yesno_update('Update','Personal Details - edit')"},
            {"sprite": "btn_sp_1","x": 540,"y": 112,"loop": false,"timing": 0,"delay": 2, "toTopObj":1, "anchor":[0,0], "onClickFn":"audioReplay()"}
        ],
        "text_animations": [
           {"text": [{"content":"Pre Issuance Verification Call "}], "sx": -1000,"sy": 50,"x": 190 ,"y": 50,"size": 28,"weight":"bold","color":"#003869","tween_type": "Elastic.easeOut","timing": 200,"delay":0.2, "anchor":[0,0] },
            {"text": [
                    {"content":"పప్రతిపాదన సంఖ్య: "},
                    {"content":"$var.window.p_PROPOSAL_NUMBER"}
                ], "sx": -1000,"sy": 120,"x": 40 ,"y": 150,"size": 22,"weight":"bold","color":"#235291","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },
             {"text": [
                    {"content":"కేసులో మీరు మళ్ళీ వినాలనుకుంటే, ఇక్కడ క్లిక్ చేయండి ---->"}
                              ], "sx": -1000,"sy": 120,"x": 238 ,"y": 120,"size": 12,"weight":"bold","color":"#235291","tween_type": "Elastic.easeOut","timing": 200,"delay":2, "anchor":[0,0],"wordWrap": true, "wordWrapWidth":520,"align":"right" },
            {"text": [{"content":"వ్యక్తిగత & సంప్రదింపు వివరాలు"}], "sx": -1000,"sy": 120,"x": 300 ,"y": 181,"size": 26,"weight":"bold","color":"#FFFFFF","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0.5,0] },
            {"text": [{"content":"మా రికార్డ్స్ ప్రకారం మీ వ్యక్తిగత వివరాలు "}], "sx": -1000,"sy": 120,"x": 80 ,"y": 235,"size": 18,"weight":"bold","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },

            {"text": [{"content":"పేరు"}], "sx": -1000,"sy": 120,"x": 80 ,"y": 270,"size": 18,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },
            {"text": [{"content":":"}], "sx": -1000,"sy": 120,"x": 310 ,"y": 270,"size": 18,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },

            {"text": [{"content":"పుట్టిన తేది"}], "sx": -1000,"sy": 120,"x": 80 ,"y": 310,"size": 18,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },
            {"text": [{"content":":"}], "sx": -1000,"sy": 120,"x": 310 ,"y": 310,"size": 18,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },

            {"text": [{"content":"జెండర్"}], "sx": -1000,"sy": 120,"x": 80 ,"y": 350,"size": 18,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },
            {"text": [{"content":":"}], "sx": -1000,"sy": 120,"x": 310 ,"y": 350,"size": 18,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },

            {"text": [{"content":"ఇమెయిల్ ఐడి"}], "sx": -1000,"sy": 120,"x": 80 ,"y":390,"size": 18,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },
            {"text": [{"content":":"}], "sx": -1000,"sy": 120,"x": 305 ,"y": 390,"size": 18,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },

            {"text": [{"content":"మొబైల్ నం."}], "sx": -1000,"sy": 120,"x": 80 ,"y": 430,"size": 18,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },
            {"text": [{"content":":"}], "sx": -1000,"sy": 120,"x": 310 ,"y": 430,"size": 18,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },

            // {"text": [{"content":"ఐడి కార్డ్ ప్రూఫ్ రకం"}], "sx": -1000,"sy": 120,"x": 80 ,"y": 470,"size": 18,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },
            // {"text": [{"content":":"}], "sx": -1000,"sy": 120,"x": 310 ,"y": 470,"size": 18,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },

            {"text": [{"content":"నామినీ పేరు & సంబంధం"}], "sx": -1000,"sy": 120,"x": 80 ,"y": 470,"size": 18,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },
            {"text": [{"content":":"}], "sx": -1000,"sy": 120,"x": 310 ,"y": 470,"size": 16,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },

            {"text": [{"content":"వార్షిక ఆదాయం"}], "sx": -1000,"sy": 120,"x": 80 ,"y": 510,"size": 18,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },
            {"text": [{"content":":"}], "sx": -1000,"sy": 120,"x": 310 ,"y": 510,"size": 18,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },


            {"text": [{"content":"చిరునామా"}], "sx": -1000,"sy": 120,"x": 80 ,"y": 550,"size": 18,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },
           // {"text": [{"content":":"}], "sx": -1000,"sy": 120,"x": 310 ,"y": 550,"size": 18,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },


            {"text": [{"content":'దయచేసి "పసుపు" అని గుర్తు పెట్టబడిన పెట్టెల్లో అవసరమైన దిద్దుబాట్లు చేయండి, ఆపై మరింత కొనసాగడానికి "సేవ్ అండ్ ప్రొసీడ్" పై క్లిక్ చేయండి.'}], "sx": -1000,"sy": 120,"x": 300 ,"y": 666,"size": 17,"weight":"bold","color":"#235291","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0.5,0],"wordWrap":true,"wordWrapWidth":510 },

            {"text": [{"content":'సేవ్ & కొనసాగండి'}], "sx": -1000,"sy": 120,"x": 300 ,"y": 764,"size": 19,"weight":"bold","color":"#FFFFFF","tween_type": "Elastic.easeOut","timing": 200,"delay":4.5, "anchor":[0.5,0] },
        ],

        "input_animations": [
            {"text": [{"content":window.customer}], "placeHolder":" ", "key":"in_name", "sx": -1000,"sy": 120,"x": 325 ,"y": 270,"size": 18,"weight":"bold","width":250,"backgroundColor":"#e9e579","fill":"#2961ab","tween_type": "Elastic.easeOut","timing": 200,"delay":1, "anchor":[0,0] },
            {"text": [{"content":window.dob}], "placeHolder":" ", "key":"in_dob", "sx": -1000,"sy": 120,"x": 325 ,"y": 310,"size": 18,"weight":"bold","width":250,"backgroundColor":"#e9e579","fill":"#2961ab","tween_type": "Elastic.easeOut","timing": 200,"delay":1, "anchor":[0,0] },
            {"text": [{"content":window.gender}], "placeHolder":" ", "key":"in_gender", "sx": -1000,"sy": 120,"x": 325 ,"y": 350,"size": 18,"weight":"bold","width":250,"backgroundColor":"#e9e579","fill":"#2961ab","tween_type": "Elastic.easeOut","timing": 200,"delay":1, "anchor":[0,0] },
            {"text": [{"content":window.email}], "placeHolder":" ", "key":"in_email", "sx": -1000,"sy": 120,"x": 325 ,"y": 390,"size": 18,"weight":"bold","width":250,"backgroundColor":"#e9e579","fill":"#2961ab","tween_type": "Elastic.easeOut","timing": 200,"delay":1, "anchor":[0,0] },
            {"text": [{"content":window.mob}], "placeHolder":" ", "key":"in_mob","sx": -1000,"sy": 120,"x": 325 ,"y": 430,"size": 18,"weight":"bold","width":250,"backgroundColor":"#e9e579","fill":"#2961ab","tween_type": "Elastic.easeOut","timing": 200,"delay":1, "anchor":[0,0] },
          //  {"text": [{"content":id}], "placeHolder":" ", "key":"in_id", "sx": -1000,"sy": 120,"x": 325 ,"y": 470,"size": 18,"weight":"bold","width":250,"backgroundColor":"#e9e579","fill":"#2961ab","tween_type": "Elastic.easeOut","timing": 200,"delay":1, "anchor":[0,0] },
            {"text": [{"content":window.nom}], "placeHolder":" ", "key":"in_nom", "sx": -1000,"sy": 120,"x": 325 ,"y": 470,"size": 18,"weight":"bold","width":250,"backgroundColor":"#e9e579","fill":"#2961ab","tween_type": "Elastic.easeOut","timing": 200,"delay":1, "anchor":[0,0] },
            {"text": [{"content":window.annul}], "placeHolder":" ", "key":"in_annul", "sx": -1000,"sy": 120,"x": 325 ,"y": 510,"size": 18,"weight":"bold","width":250,"backgroundColor":"#e9e579","fill":"#2961ab","tween_type": "Elastic.easeOut","timing": 200,"delay":1, "anchor":[0,0] },
            // {"text": [
            //            {"content":window.add}

            //  ], "placeHolder":" ", "key":"in_add", "sx": -1000,"sy": 120,"x": 325 ,"y": 550,"size": 18,"weight":"bold","width":250,"backgroundColor":"#e9e579","fill":"#2961ab","tween_type": "Elastic.easeOut","timing": 200,"delay":1, "anchor":[0,0],"height":100 }
             {"text": [ {"content":window.add1[0]+", "+window.add1[1]}], "placeHolder":" ", "key":"in_add1", "sx": -1000,"sy": 120,"x": 80 ,"y": 570,"size": 18,"weight":"bold","width":495,"backgroundColor":"#e9e579","fill":"#2961ab","tween_type": "Elastic.easeOut","timing": 200,"delay":1, "anchor":[0,0] },

             {"text": [
                       {"content":window.add1[2]+", "+window.add1[3]}

             ], "placeHolder":" ", "key":"in_add2", "sx": -1000,"sy": 120,"x": 80 ,"y": 590,"size": 18,"weight":"bold","width":495,"backgroundColor":"#e9e579","fill":"#2961ab","tween_type": "Elastic.easeOut","timing": 200,"delay":1, "anchor":[0,0] },
          
           {"text":[
                      {"content":window.add1[4]+", "+window.add1[5]}

             ], "placeHolder":" ", "key":"in_add3", "sx": -1000,"sy": 120,"x": 80 ,"y": 610,"size": 18,"weight":"bold","width":495,"backgroundColor":"#e9e579","fill":"#2961ab","tween_type": "Elastic.easeOut","timing": 200,"delay":1, "anchor":[0,0] },
  
        {"text": [
                      {"content":window.add1[6]+", "+window.add1[7]}

             ], "placeHolder":" ", "key":"in_add4", "sx": -1000,"sy": 120,"x": 80 ,"y": 630,"size": 18,"weight":"bold","width":495,"backgroundColor":"#e9e579","fill":"#2961ab","tween_type": "Elastic.easeOut","timing": 200,"delay":1, "anchor":[0,0] },


        
        ],

        "sound_list": [
            {
                "sound": ["personal_disagree_audio"]
            }
        ],
        "functions": [
            {"fn": "SetBGTile('bg_02')", "delay": 0 },
             {"fn": "cameraAccessError()", "delay" : 0.5 },
             {"fn": "captureImage()", "delay": 1.5 }
        ],
        "name": "Personal Details - Disagree",
        "timing": -1,
        "index": 6
    },

{
          "sprite_animations": [
             {"sprite": "bar_01","x": 0,"y": 0,"loop": false,"timing": 0,"delay": 0,"toTopObj":1, "anchor":[0,0]},
            {"sprite": "logo_01","x": 10,"y": 33,"loop": false,"timing": 0,"delay": 0.1,"toTopObj":2, "anchor":[0,0]},
            {"sprite": "bar_03","x": 300,"y": 180,"loop": false,"timing": 0,"delay": 0, "toTopObj":1, "anchor":[0.5,0]},
            {"sprite": "box_18","x": 300,"y": 223,"loop": false,"timing": 0,"delay": 0, "toTopObj":1, "anchor":[0.5,0]},
           {"sprite": "loan_icon","x": 300,"y": 240,"loop": false,"timing": 0,"delay": 0.5, "toTopObj":1, "anchor":[0.5,0]},


            {"sprite": "btn_03_2","x": 150,"y": 674,"loop": false,"timing": 0,"delay": 2, "toTopObj":1, "anchor":[0.5,0],"onClickFn":"goToPage(8),yesno_update('Yes','Loan Details - Confirm')"},

            {"sprite": "btn_03_3","x": 450,"y": 674,"loop": false,"timing": 0,"delay": 2, "toTopObj":1, "anchor":[0.5,0],"onClickFn":"goToPageBack(8),yesno_update('No','Loan Details - Confirm')"},

            {"sprite": "btn_sp_1","x": 540,"y": 112,"loop": false,"timing": 0,"delay": 2, "toTopObj":1, "anchor":[0,0], "onClickFn":"audioReplay()"}
        ],
          "text_animations": [
           {"text": [{"content":"Pre Issuance Verification Call "}], "sx": -1000,"sy": 50,"x": 190 ,"y": 50,"size": 28,"weight":"bold","color":"#003869","tween_type": "Elastic.easeOut","timing": 200,"delay":0.2, "anchor":[0,0] },
            {"text": [
                    {"content":"పప్రతిపాదన సంఖ్య: "},
                    {"content":"$var.window.p_PROPOSAL_NUMBER"}
                ], "sx": -1000,"sy": 120,"x": 40 ,"y": 150,"size": 22,"weight":"bold","color":"#235291","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },
             {"text": [
                    {"content":"కేసులో మీరు మళ్ళీ వినాలనుకుంటే, ఇక్కడ క్లిక్ చేయండి ---->"}
                              ], "sx": -1000,"sy": 120,"x": 238 ,"y": 120,"size": 12,"weight":"bold","color":"#235291","tween_type": "Elastic.easeOut","timing": 200,"delay":2, "anchor":[0,0],"wordWrap": true, "wordWrapWidth":520,"align":"right" },
            {"text": [{"content":"లోన్ ప్రశ్న"}], "sx": -1000,"sy": 120,"x": 300 ,"y": 181,"size": 26,"weight":"bold","color":"#FFFFFF","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0.5,0] },

            {"text": [
            {"content":"పాలసీ విశిష్టతలన్నిటినీ మీకు కరెక్టుగా వివరించినట్లుగా మేము నమ్ముతున్నాము. మీరు చెల్లించిన డబ్బు పాలసీ ప్రీమియంకి మాత్రమే మరియు ఏదైనా ఇతర పాలసీకి మీకు ఏ విధమైన బోనస్, లోన్, మొబైల్ టవర్ ఇన్స్టలేషన్ లేదా రిఫండ్ ఏదీ మీకు వాగ్దానం చేయబడలేదు. దయచేసి ఇలాంటి తప్పుడు వాగ్దానాలను నమ్మకండి మరియు ఇలాంటి ఏదైనా ఆందోళనను మాకు వెంటనే తెలియజేయండి. మీకు ఇలాంటి వాగ్దానం చేయలేదనే విషయం మేము ధృవీకరించుకోవచ్చా.\n\nమున్ముందు ఈ విషయంలో ఇలాంటి ఫిర్యాదు దేనినీ ఇండియాఫస్ట్ పరిగణనలోకి తీసుకోదని దయచేసి గమనించండి."}], "sx": -1000,"sy": 120,"x": 45 ,"y": 360,"size": 19,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0],"wordWrap": true, "wordWrapWidth":520,"align":"center" },

//{"text": [{"content":"Please note that IndiaFirst may not consider any such complaint in the matter on a later date."}], "sx": -1000,"sy": 120,"x": 45 ,"y": 570,"size": 20,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0],"wordWrap": true, "wordWrapWidth":520,"align":"center" },


            {"text": [{"content":'అవును'}], "sx": -1000,"sy": 120,"x": 150 ,"y": 688,"size": 22,"weight":"bold","color":"#FFFFFF","tween_type": "Elastic.easeOut","timing": 200,"delay":2, "anchor":[0.5,0] },
            {"text": [{"content":'లేదు'}], "sx": -1000,"sy": 120,"x": 450 ,"y": 688,"size": 22,"weight":"bold","color":"#FFFFFF","tween_type": "Elastic.easeOut","timing": 200,"delay":2, "anchor":[0.5,0] },
        ],
        "functions": [

            {"fn": "SetBGTile('bg_02')", "delay": 0 },
            {"fn": "cameraAccessError()", "delay" : 0.5 },
            {"fn": "captureImage()", "delay": 1.5 }


        ],
         "sound_list": [
            {
                 "sound": ["loan_details"]
            }
        ],
        "name": "Loan Details - Confirm",
        "timing": -1,
        "index": 7
      },

   {
        "sprite_animations": [
            {"sprite": "bar_01","x": 0,"y": 0,"loop": false,"timing": 0,"delay": 0,"toTopObj":1, "anchor":[0,0]},
            {"sprite": "logo_01","x": 10,"y": 33,"loop": false,"timing": 0,"delay": 0.1,"toTopObj":2, "anchor":[0,0]},
            {"sprite": "bar_03","x": 300,"y": 180,"loop": false,"timing": 0,"delay": 0, "toTopObj":1, "anchor":[0.5,0]},
            {"sprite": "box_22","x": 300,"y": 223,"loop": false,"timing": 0,"delay": 0, "toTopObj":1, "anchor":[0.5,0]},
           {"sprite": "box_073","x": 30,"y": 640,"loop": false,"timing": 0,"delay": 0, "toTopObj":1, "anchor":[0,0]},
            {"sprite": "btn_03_2","x": 150,"y": 740,"loop": false,"timing": 0,"delay": 4, "toTopObj":1, "anchor":[0.5,0],"onClickFn":"goToScreen(13,'cPerDet',1),yesno_update('Agree','Plan Details')"},
            {"sprite": "btn_03_3","x": 450,"y": 740,"loop": false,"timing": 0,"delay": 4, "toTopObj":1, "anchor":[0.5,0],"onClickFn":"goToPage(9),yesno_update('Disagree','Plan Details')"},
            {"sprite": "btn_sp_1","x": 540,"y": 112,"loop": false,"timing": 0,"delay": 2, "toTopObj":1, "anchor":[0,0], "onClickFn":"audioReplay()"}
        ],
        "text_animations": [
           {"text": [{"content":"Pre Issuance Verification Call "}], "sx": -1000,"sy": 50,"x": 190 ,"y": 50,"size": 28,"weight":"bold","color":"#003869","tween_type": "Elastic.easeOut","timing": 200,"delay":0.2, "anchor":[0,0] },
            {"text": [
                    {"content":"పప్రతిపాదన సంఖ్య: "},
                    {"content":"$var.window.p_PROPOSAL_NUMBER"}
                ], "sx": -1000,"sy": 120,"x": 40 ,"y": 150,"size": 22,"weight":"bold","color":"#235291","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },
             {"text": [
                    {"content":"కేసులో మీరు మళ్ళీ వినాలనుకుంటే, ఇక్కడ క్లిక్ చేయండి ---->"}
                              ], "sx": -1000,"sy": 120,"x": 238 ,"y": 120,"size": 12,"weight":"bold","color":"#235291","tween_type": "Elastic.easeOut","timing": 200,"delay":2, "anchor":[0,0],"wordWrap": true, "wordWrapWidth":520,"align":"right" },
            {"text": [{"content":"ప్లాన్ వివరాలు"}], "sx": -1000,"sy": 120,"x": 300 ,"y": 181,"size": 26,"weight":"bold","color":"#FFFFFF","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0.5,0] },
            {"text": [{"content":"మా రికార్డుల ప్రకారం మీ ప్రణాళిక వివరాలు "}], "sx": -1000,"sy": 120,"x": 100 ,"y": 230,"size": 18,"weight":"bold","color":"#3b3a3a ","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },

            {"text": [{"content":"ప్రణాళిక పేరు"}], "sx": -1000,"sy": 120,"x": 104 ,"y": 270,"size": 18,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },
            {"text": [{"content":":"}], "sx": -1000,"sy": 120,"x": 296 ,"y": 270,"size": 18,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },
            
            {"text": [{"content":"$var.window.p_planname"}], "sx": -1000,"sy": 120,"x": 310 ,"y": 270,"size": 18,"weight":"bold","color":"#0098DE","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },


          {"text": [{"content":"ప్రణాళిక రకం"}], "sx": -1000,"sy": 120,"x": 104 ,"y": 320,"size": 18,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },
            {"text": [{"content":":"}], "sx": -1000,"sy": 120,"x": 296 ,"y": 320,"size": 18,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },

            {"text": [{"content":"నాన్ లింక్డ్ పార్టిసిపేటింగ్ ఎండోమెంట్ పెన్షన్ ప్లాన్"}], "sx": -1000,"sy": 120,"x": 310 ,"y": 310,"size": 18,"weight":"bold","color":"#0098DE","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0],"wordWrap":true,"wordWrapWidth":260,"lineSpacing":-5,"align":"left" },


            {"text": [{"content":"ప్రీమియం మొత్తం"}], "sx": -1000,"sy": 120,"x": 104 ,"y": 374,"size": 18,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },
            {"text": [{"content":":"}], "sx": -1000,"sy": 120,"x": 296 ,"y": 374,"size": 18,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },
            {"text": [
            {"content":"Rs. "},
            {"content":"$var.window.p_premiumamount","enableNumericType":true}
            ], "sx": -1000,"sy": 120,"x": 310 ,"y": 374,"size": 18,"weight":"bold","color":"#0098DE","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },


            {"text": [{"content":"చెల్లింపు పౌన .పున్యం"}], "sx": -1000,"sy": 120,"x": 104 ,"y":434,"size": 18,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },
            {"text": [{"content":":"}], "sx": -1000,"sy": 120,"x": 296 ,"y": 434,"size": 18,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },
            {"text": [{"content":"$var.window.p_paymentFre"}], "sx": -1000,"sy": 120,"x": 310 ,"y": 434,"size": 18,"weight":"bold","color":"#0098DE","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },


            {"text": [{"content":"ప్రీమియం చెల్లింపు పదం"}], "sx": -1000,"sy": 120,"x": 104 ,"y": 494,"size": 18,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },
            {"text": [{"content":":"}], "sx": -1000,"sy": 120,"x": 296 ,"y": 494,"size": 18,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },
            {"text": [
            {"content":"$var.window.p_payingTerm"},
            {"content":" years"}
            ], "sx": -1000,"sy": 120,"x": 310 ,"y": 494,"size": 18,"weight":"bold","color":"#0098DE","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },


            {"text": [{"content":"పాలసీ టర్మ్"}], "sx": -1000,"sy": 120,"x": 104 ,"y": 554,"size": 18,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },
            {"text": [{"content":":"}], "sx": -1000,"sy": 120,"x": 296 ,"y": 554,"size": 18,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },
            {"text": [
            {"content":"$var.window.p_policyTerm"},
            {"content":" years"}
            ], "sx": -1000,"sy": 120,"x": 310 ,"y": 554,"size": 18,"weight":"bold","color":"#0098DE","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },

            {"text": [{"content":"మొత్తం హామీ"}], "sx": -1000,"sy": 120,"x": 104 ,"y": 605,"size": 18,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },
            {"text": [{"content":":"}], "sx": -1000,"sy": 120,"x": 296 ,"y": 605,"size": 18,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },
            {"text": [
            {"content":"Rs. "},
            {"content":"$var.window.p_Assuredsum","enableNumericType":true}
            ], "sx": -1000,"sy": 120,"x": 310 ,"y": 605,"size": 18,"weight":"bold","color":"#0098DE","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },

 
            {"text": [{"content":"ఇండియా ఫస్ట్ మహా జీవన్ ప్లాన్ దీర్ఘకాలిక ప్రణాళిక, ఇక్కడ మీరు "},
                      {"content":"$var.window.p_payingTerm"},
                      {"content":" సంవత్సరాల పదం ముగిసే వరకు చెల్లించినట్లయితే మాత్రమే గరిష్ట ప్రయోజనాలను పొందవచ్చు."},

            ], "sx": -1000,"sy": 120,"x": 300 ,"y": 645,"size": 14,"weight":"bold","color":"#0098DE","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0.5,0],"wordWrap":true,"wordWrapWidth":550,"lineSpacing":-5 },
 
           
            {"text": [{"content":'తెరపై ప్రదర్శించబడే మొత్తం సమాచారం సరైనది అయితే, దయచేసి "అంగీకరిస్తున్నాను" పై క్లిక్ చేయండి, లేకపోతే "అంగీకరించలేదు" క్లిక్ చేయండి'}], "sx": -1000,"sy": 120,"x": 300 ,"y": 700,"size": 16,"weight":"bold","color":"#235291","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0.5,0],"wordWrap":true,"wordWrapWidth":520,"lineSpacing":-5 },
         //   {"text": [{"content":'Please click on "Agree", else click "Disagree"'}], "sx": -1000,"sy": 120,"x": 300 ,"y": 698,"size": 18,"weight":"bold","color":"#235291","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0.5,0] },


            {"text": [{"content":'అంగీకరిస్తున్నారు'}], "sx": -1000,"sy": 120,"x": 150 ,"y": 754,"size": 19,"weight":"bold","color":"#FFFFFF","tween_type": "Elastic.easeOut","timing": 200,"delay":4, "anchor":[0.5,0] },
            {"text": [{"content":'విభేదిస్తున్నారు'}], "sx": -1000,"sy": 120,"x": 450 ,"y": 754,"size": 19,"weight":"bold","color":"#FFFFFF","tween_type": "Elastic.easeOut","timing": 200,"delay":4, "anchor":[0.5,0] },
        ],
        "sound_list": [
            {
                     "sound":["startplan","plan_name","maha_jeevan_plan","plantype","non_linked","premium_amount","$var.currency_window.p_premiumamount","payment_frequency","$var.window.p_paymentFre","paying_term","$var.number_window.p_payingTerm","years","policy_term","$var.number_window.p_policyTerm","years","sum_assured","$var.currency_window.p_Assuredsum"]

            }
        ],
        "functions": [

            {"fn": "SetBGTile('bg_02')", "delay": 0 },
            {"fn": "cameraAccessError()", "delay" : 0.5 },
            {"fn": "captureImage()", "delay": 1.5 }

        ],
        "name": "Plan Details",
        "timing": -1,
        "index": 8
    },


    {
       "sprite_animations": [
             {"sprite": "bar_01","x": 0,"y": 0,"loop": false,"timing": 0,"delay": 0,"toTopObj":1, "anchor":[0,0]},
            {"sprite": "logo_01","x": 10,"y": 33,"loop": false,"timing": 0,"delay": 0.1,"toTopObj":2, "anchor":[0,0]},
            {"sprite": "bar_03","x": 300,"y": 180,"loop": false,"timing": 0,"delay": 0, "toTopObj":1, "anchor":[0.5,0]},
            {"sprite": "box_02","x": 300,"y": 223,"loop": false,"timing": 0,"delay": 0, "toTopObj":1, "anchor":[0.5,0]},

            {"sprite": "btn_03_2","x": 150,"y": 454,"loop": false,"timing": 0,"delay": 2, "toTopObj":1, "anchor":[0.5,0],"onClickFn":"goToPage(10)"},

            {"sprite": "btn_03_3","x": 450,"y": 454,"loop": false,"timing": 0,"delay": 2, "toTopObj":1, "anchor":[0.5,0],"onClickFn":"goToPageBack(8)"},

            {"sprite": "btn_sp_1","x": 540,"y": 112,"loop": false,"timing": 0,"delay": 2, "toTopObj":1, "anchor":[0,0], "onClickFn":"audioReplay()"}
        ],
          "text_animations": [
           {"text": [{"content":"Pre Issuance Verification Call "}], "sx": -1000,"sy": 50,"x": 190 ,"y": 50,"size": 28,"weight":"bold","color":"#003869","tween_type": "Elastic.easeOut","timing": 200,"delay":0.2, "anchor":[0,0] },
            {"text": [
                    {"content":"పప్రతిపాదన సంఖ్య: "},
                    {"content":"$var.window.p_PROPOSAL_NUMBER"}
                ], "sx": -1000,"sy": 120,"x": 40 ,"y": 150,"size": 22,"weight":"bold","color":"#235291","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },
             {"text": [
                    {"content":"కేసులో మీరు మళ్ళీ వినాలనుకుంటే, ఇక్కడ క్లిక్ చేయండి ---->"}
                              ], "sx": -1000,"sy": 120,"x": 238 ,"y": 120,"size": 12,"weight":"bold","color":"#235291","tween_type": "Elastic.easeOut","timing": 200,"delay":2, "anchor":[0,0],"wordWrap": true, "wordWrapWidth":520,"align":"right" },
            {"text": [{"content":"ప్లాన్ వివరాలు"}], "sx": -1000,"sy": 120,"x": 300 ,"y": 181,"size": 26,"weight":"bold","color":"#FFFFFF","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0.5,0] },
            {"text": [{"content":"ఈ స్క్రీన్‌పై ఉన్న వివరాలతో మీరు విభేదించాలనుకుంటున్నారా."}], "sx": -1000,"sy": 120,"x": 50 ,"y": 280,"size": 35,"weight":"bold","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0],"wordWrap": true, "wordWrapWidth":520,"align":"center" },

            {"text": [{"content":'అవును'}], "sx": -1000,"sy": 120,"x": 150 ,"y": 468,"size": 22,"weight":"bold","color":"#FFFFFF","tween_type": "Elastic.easeOut","timing": 200,"delay":2, "anchor":[0.5,0] },
            {"text": [{"content":'లేదు'}], "sx": -1000,"sy": 120,"x": 450 ,"y": 468,"size": 22,"weight":"bold","color":"#FFFFFF","tween_type": "Elastic.easeOut","timing": 200,"delay":2, "anchor":[0.5,0] },
        ],
        "functions": [

            {"fn": "SetBGTile('bg_02')", "delay": 0 }

        ],

          "sound_list": [
            {
                "sound": ["plan_yesno_audio"]
            }
        ],

        "name": "Plan Yesno - Confirm",
        "timing": -1,
        "index": 9
    },

     {
        "sprite_animations": [
             {"sprite": "bar_01","x": 0,"y": 0,"loop": false,"timing": 0,"delay": 0,"toTopObj":1, "anchor":[0,0]},
            {"sprite": "logo_01","x": 10,"y": 33,"loop": false,"timing": 0,"delay": 0.1,"toTopObj":2, "anchor":[0,0]},
            {"sprite": "bar_03","x": 300,"y": 180,"loop": false,"timing": 0,"delay": 0, "toTopObj":1, "anchor":[0.5,0]},
            {"sprite": "box_18","x": 300,"y": 223,"loop": false,"timing": 0,"delay": 0, "toTopObj":1, "anchor":[0.5,0]},
            {"sprite": "btn_02","x": 300,"y": 743,"loop": false,"timing": 0,"delay": 4.5, "toTopObj":1, "anchor":[0.5,0],"onClickFn":"editGoToPage(16,240,660,'ePerDet','cPerDet')"},
            {"sprite": "btn_sp_1","x": 540,"y": 112,"loop": false,"timing": 0,"delay": 2, "toTopObj":1, "anchor":[0,0], "onClickFn":"audioReplay()"}
        ],
        "text_animations": [
           {"text": [{"content":"Pre Issuance Verification Call "}], "sx": -1000,"sy": 50,"x": 190 ,"y": 50,"size": 28,"weight":"bold","color":"#003869","tween_type": "Elastic.easeOut","timing": 200,"delay":0.2, "anchor":[0,0] },
            {"text": [
                    {"content":"పప్రతిపాదన సంఖ్య: "},
                    {"content":"$var.window.p_PROPOSAL_NUMBER"}
                ], "sx": -1000,"sy": 120,"x": 40 ,"y": 150,"size": 22,"weight":"bold","color":"#235291","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },
             {"text": [
                    {"content":"కేసులో మీరు మళ్ళీ వినాలనుకుంటే, ఇక్కడ క్లిక్ చేయండి ---->"}
                              ], "sx": -1000,"sy": 120,"x": 238 ,"y": 120,"size": 12,"weight":"bold","color":"#235291","tween_type": "Elastic.easeOut","timing": 200,"delay":2, "anchor":[0,0],"wordWrap": true, "wordWrapWidth":520,"align":"right" },
            {"text": [{"content":"ప్లాన్ వివరాలు"}], "sx": -1000,"sy": 120,"x": 300 ,"y": 181,"size": 26,"weight":"bold","color":"#FFFFFF","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0.5,0] },
          //  {"text": [{"content":"మా రికార్డ్స్ ప్రకారం మీ వ్యక్తిగత వివరాలు "}], "sx": -1000,"sy": 120,"x": 100 ,"y": 240,"size": 18,"weight":"bold","color":"#a3a5a6","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },

            {"text": [{"content":"ప్రణాళిక పేరు"}], "sx": -1000,"sy": 120,"x": 104 ,"y": 250,"size": 18,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },
            {"text": [{"content":":"}], "sx": -1000,"sy": 120,"x": 296 ,"y": 250,"size": 18,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },
           {"text": [{"content":"$var.window.p_planname"}], "sx": -1000,"sy": 120,"x": 310 ,"y": 250,"size": 18,"weight":"bold","color":"#0098DE","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },

          
            {"text": [{"content":"ప్రణాళిక రకం"}], "sx": -1000,"sy": 120,"x": 104 ,"y": 310,"size": 18,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },
            {"text": [{"content":":"}], "sx": -1000,"sy": 120,"x": 296 ,"y": 310,"size": 18,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },

            {"text": [{"content":"నాన్ లింక్డ్ పార్టిసిపేటింగ్ ఎండోమెంట్ పెన్షన్ ప్లాన్"}], "sx": -1000,"sy": 120,"x": 310 ,"y": 300,"size": 18,"weight":"bold","color":"#0098DE","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0],"wordWrap":true,"wordWrapWidth":260,"lineSpacing":-5,"align":"left" },


            {"text": [{"content":"ప్రీమియం మొత్తం"}], "sx": -1000,"sy": 120,"x": 104 ,"y": 370,"size": 18,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },
            {"text": [{"content":":"}], "sx": -1000,"sy": 120,"x": 296 ,"y": 370,"size": 18,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },
            {"text": [
            {"content":"Rs. "},
            {"content":"$var.window.p_premiumamount","enableNumericType":true}
            ], "sx": -1000,"sy": 120,"x": 310 ,"y": 370,"size": 18,"weight":"bold","color":"#0098DE","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },


            {"text": [{"content":"చెల్లింపు పౌన .పున్యం"}], "sx": -1000,"sy": 120,"x": 104 ,"y": 430,"size": 18,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },
            {"text": [{"content":":"}], "sx": -1000,"sy": 120,"x": 296 ,"y": 430,"size": 18,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },
            {"text": [{"content":"$var.window.p_paymentFre"}], "sx": -1000,"sy": 120,"x": 310 ,"y": 430,"size": 18,"weight":"bold","color":"#0098DE","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },


            {"text": [{"content":"ప్రీమియం చెల్లింపు పదం"}], "sx": -1000,"sy": 120,"x": 104 ,"y": 490,"size": 18,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },
            {"text": [{"content":":"}], "sx": -1000,"sy": 120,"x": 296 ,"y": 490,"size": 18,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },
            {"text": [
            {"content":"$var.window.p_payingTerm"},
            {"content":" years"}
            ], "sx": -1000,"sy": 120,"x": 310 ,"y": 490,"size": 18,"weight":"bold","color":"#0098DE","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },


            {"text": [{"content":"పాలసీ టర్మ్"}], "sx": -1000,"sy": 120,"x": 104 ,"y": 550,"size": 18,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },
            {"text": [{"content":":"}], "sx": -1000,"sy": 120,"x": 296 ,"y": 550,"size": 18,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },
            {"text": [
            {"content":"$var.window.p_policyTerm"},
            {"content":" years"}
            ], "sx": -1000,"sy": 120,"x": 310 ,"y": 550,"size": 18,"weight":"bold","color":"#0098DE","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },

            {"text": [{"content":"మొత్తం హామీ"}], "sx": -1000,"sy": 120,"x": 104 ,"y": 610,"size": 18,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },
            {"text": [{"content":":"}], "sx": -1000,"sy": 120,"x": 296 ,"y": 610,"size": 18,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },
            {"text": [
            {"content":"Rs. "},
            {"content":"$var.window.p_Assuredsum","enableNumericType":true}
            ], "sx": -1000,"sy": 120,"x": 310 ,"y": 610,"size": 18,"weight":"bold","color":"#0098DE","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },

         //   {"text": [{"content":"Equity "}], "sx": -1000,"sy": 120,"x": 104 ,"y": 488,"size": 16,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },
         //   {"text": [{"content":":"}], "sx": -1000,"sy": 120,"x": 296 ,"y": 488,"size": 16,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },
           {"text": [
                    {"content":'పైన ఇచ్చిన పెట్టెపై మీ అసమ్మతిని నమోదు చేసి, "సేవ్ చేసి ముందుకు సాగండి" క్లిక్ చేయండి'}
                     ], "sx": -1000,"sy": 120,"x": 300 ,"y": 700,"size": 17,"weight":"bold","color":"#235291","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0.5,0],"wordWrap":true,"wordWrapWidth":520 },
            {"text": [{"content":'సేవ్ & కొనసాగండి'}], "sx": -1000,"sy": 120,"x": 300 ,"y": 764,"size": 19,"weight":"bold","color":"#FFFFFF","tween_type": "Elastic.easeOut","timing": 200,"delay":4.5, "anchor":[0.5,0] },
        ],
         "input_animations": [
          {"text": [{"content":""}], "key":"in_disagreement", "sx": -1000,"sy": 120,"x": 20 ,"y": 670,"size": 20,"weight":"bold","width":558,"tween_type": "Elastic.easeOut","timing": 200,"delay":1,"backgroundColor":"#e9e9e9","placeHolder":"మీ అసమ్మతిని నమోదు చేయండి ...", "anchor":[0,0] }
        ],
        "sound_list": [
            {
                "sound": ["plan_disagree_audio"]
            }
        ],
        "functions": [

            {"fn": "SetBGTile('bg_02')", "delay": 0 },
             {"fn": "cameraAccessError()", "delay" : 0.5 },
            {"fn": "captureImage()", "delay": 1 }

        ],
        "name": "Plan Details - Disagree",
        "timing": -1,
        "index": 10
    },


    {
         "sprite_animations": [
            {"sprite": "bar_01","x": 0,"y": 0,"loop": false,"timing": 0,"delay": 0,"toTopObj":1, "anchor":[0,0]},
            {"sprite": "logo_01","x": 10,"y": 33,"loop": false,"timing": 0,"delay": 0.1,"toTopObj":2, "anchor":[0,0]},
            {"sprite": "bar_03","x": 300,"y": 180,"loop": false,"timing": 0,"delay": 0, "toTopObj":1, "anchor":[0.5,0]},
            {"sprite": "box_23","x": 300,"y": 223,"loop": false,"timing": 0,"delay": 0, "toTopObj":1, "anchor":[0.5,0]},
             {"sprite": "medicon","x": 300,"y": 250,"loop": false,"timing": 0,"delay": 0.5, "toTopObj":1, "anchor":[0.5,0]},

            {"sprite": "btn_03_2","x": 150,"y": 604,"loop": false,"timing": 0,"delay": 2, "toTopObj":1, "anchor":[0.5,0],"onClickFn":"goToPage(14),yesno_update('Agree','Medical Questions')"},

            {"sprite": "btn_03_3","x": 450,"y": 604,"loop": false,"timing": 0,"delay": 2, "toTopObj":1, "anchor":[0.5,0],"onClickFn":"goToPageBack(12),yesno_update('Disgree','Medical Questions')"},

            {"sprite": "btn_sp_1","x": 540,"y": 112,"loop": false,"timing": 0,"delay": 2, "toTopObj":1, "anchor":[0,0], "onClickFn":"audioReplay()"}

        ],
          "text_animations": [
           {"text": [{"content":"Pre Issuance Verification Call "}], "sx": -1000,"sy": 50,"x": 190 ,"y": 50,"size": 28,"weight":"bold","color":"#003869","tween_type": "Elastic.easeOut","timing": 200,"delay":0.2, "anchor":[0,0] },
            {"text": [
                    {"content":"పప్రతిపాదన సంఖ్య: "},
                    {"content":"$var.window.p_PROPOSAL_NUMBER"}
                ], "sx": -1000,"sy": 120,"x": 40 ,"y": 150,"size": 22,"weight":"bold","color":"#235291","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },
             {"text": [
                    {"content":"కేసులో మీరు మళ్ళీ వినాలనుకుంటే, ఇక్కడ క్లిక్ చేయండి ---->"}
                              ], "sx": -1000,"sy": 120,"x": 238 ,"y": 120,"size": 12,"weight":"bold","color":"#235291","tween_type": "Elastic.easeOut","timing": 200,"delay":2, "anchor":[0,0],"wordWrap": true, "wordWrapWidth":520,"align":"right" },
            {"text": [{"content":"వైద్య ప్రశ్నలు"}], "sx": -1000,"sy": 120,"x": 300 ,"y": 181,"size": 26,"weight":"bold","color":"#FFFFFF","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0.5,0] },

            {"text": [{"content":"ప్రొపోసల్లోని అన్ని వైద్య ప్రశ్నలను మీరు సరిగ్గా చదివి మరియు సమాధానాలు అందించారని మరియు అన్ని వైద్య వివరాలను/చికిత్స చరిత్రను (ఏదైనా ఉంటే) వెల్లడించారని ధృవీకరించవలసిందిగా మేము కోరుతున్నాము"}], "sx": -1000,"sy": 215,"x": 45 ,"y": 380,"size": 19,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5,"wordWrap": true, "wordWrapWidth":520,"align":"center", "anchor":[0,0] },
            {"text": [{"content":"[ఏదైనా ప్రతికూల వైద్య చరిత్ర ఉండి వెల్లడించని పక్షంలో భవిష్యత్తులో క్లెయిం తిరస్కరించడం జరగవచ్చు]."}], "sx": -1000,"sy": 295,"x": 45 ,"y": 500,"size": 19,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5,"wordWrap": true, "wordWrapWidth":520,"align":"center", "anchor":[0,0] },

          //  {"text": [{"content":"There were a set of medical questions in the application form, which you have answered as “No” which means that you are not under any medication and you are in good health."}], "sx": -1000,"sy": 120,"x": 50 ,"y": 390,"size": 22,"weight":"bold","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0],"wordWrap": true, "wordWrapWidth":520,"align":"center" },

            {"text": [{"content":'అంగీకరిస్తున్నారు'}], "sx": -1000,"sy": 120,"x": 150 ,"y": 618,"size": 19,"weight":"bold","color":"#FFFFFF","tween_type": "Elastic.easeOut","timing": 200,"delay":2, "anchor":[0.5,0] },
            {"text": [{"content":'విభేదిస్తున్నారు'}], "sx": -1000,"sy": 120,"x": 450 ,"y": 618,"size": 19,"weight":"bold","color":"#FFFFFF","tween_type": "Elastic.easeOut","timing": 200,"delay":2, "anchor":[0.5,0] },
        ],
        "functions": [

            {"fn": "SetBGTile('bg_02')", "delay": 0 },
            {"fn": "cameraAccessError()", "delay" : 0.5 },
            {"fn": "captureImage()", "delay": 1.5 }

        ],
         "sound_list": [
            {
               "sound": ["medical_audio"]
            }
        ],
        "name": "Medical Questions - Confirm",
        "timing": -1,
        "index": 11
    },


 {

          "sprite_animations": [
           {"sprite": "bar_01","x": 0,"y": 0,"loop": false,"timing": 0,"delay": 0,"toTopObj":1, "anchor":[0,0]},
            {"sprite": "logo_01","x": 10,"y": 33,"loop": false,"timing": 0,"delay": 0.1,"toTopObj":2, "anchor":[0,0]},
            {"sprite": "bar_03","x": 300,"y": 180,"loop": false,"timing": 0,"delay": 0, "toTopObj":1, "anchor":[0.5,0]},
            {"sprite": "box_06","x": 300,"y": 223,"loop": false,"timing": 0,"delay": 0, "toTopObj":1, "anchor":[0.5,0]},
            {"sprite": "illness_icon","x": 300,"y": 240,"loop": false,"timing": 0,"delay": 0.5, "toTopObj":1, "anchor":[0.5,0]},
            {"sprite": "btn_02","x": 300,"y": 743,"loop": false,"timing": 0,"delay": 4.5, "toTopObj":1, "anchor":[0.5,0],"onClickFn":"editGoToPage(14,240,220,'ePerDet','cPerDet')"},
            {"sprite": "btn_sp_1","x": 540,"y": 112,"loop": false,"timing": 0,"delay": 2, "toTopObj":1, "anchor":[0,0], "onClickFn":"audioReplay()"}
        ],

           "text_animations": [
           {"text": [{"content":"Pre Issuance Verification Call "}], "sx": -1000,"sy": 50,"x": 190 ,"y": 50,"size": 28,"weight":"bold","color":"#003869","tween_type": "Elastic.easeOut","timing": 200,"delay":0.2, "anchor":[0,0] },
            {"text": [
                    {"content":"పప్రతిపాదన సంఖ్య: "},
                    {"content":"$var.window.p_PROPOSAL_NUMBER"}
                ], "sx": -1000,"sy": 120,"x": 40 ,"y": 150,"size": 22,"weight":"bold","color":"#235291","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },
             {"text": [
                    {"content":"కేసులో మీరు మళ్ళీ వినాలనుకుంటే, ఇక్కడ క్లిక్ చేయండి ---->"}
                              ], "sx": -1000,"sy": 120,"x": 238 ,"y": 120,"size": 12,"weight":"bold","color":"#235291","tween_type": "Elastic.easeOut","timing": 200,"delay":2, "anchor":[0,0],"wordWrap": true, "wordWrapWidth":520,"align":"right" },
            {"text": [{"content":"అనారోగ్య వివరాలు"}], "sx": -1000,"sy": 120,"x": 300 ,"y": 181,"size": 26,"weight":"bold","color":"#FFFFFF","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0.5,0] },

            {"text": [{"content":"అనారోగ్యం పేరు"}], "sx": -1000,"sy": 120,"x": 54 ,"y": 392,"size": 17,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },
            {"text": [{"content":":"}], "sx": -1000,"sy": 120,"x": 296 ,"y": 392,"size": 17,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },

            {"text": [{"content":"నిర్ధారణ కాలం"}], "sx": -1000,"sy": 120,"x": 54 ,"y": 462,"size": 17,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },
            {"text": [{"content":":"}], "sx": -1000,"sy": 120,"x": 296 ,"y": 462,"size": 17,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },


            {"text": [{"content":"వినియోగం షధ వినియోగ వివరాలు"}], "sx": -1000,"sy": 120,"x": 54 ,"y": 532,"size": 17,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },
            {"text": [{"content":":"}], "sx": -1000,"sy": 120,"x": 296 ,"y": 532,"size": 17,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },


            {"text": [
                    {"content":'దయచేసి "పసుపు" అని గుర్తు పెట్టబడిన పెట్టెల్లో అవసరమైన దిద్దుబాట్లు చేయండి, ఆపై మరింత కొనసాగడానికి "సేవ్ అండ్ ప్రొసీడ్" పై క్లిక్ చేయండి.'}
                     ], "sx": -1000,"sy": 120,"x": 300 ,"y": 650,"size": 18,"weight":"bold","color":"#235291","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0.5,0],"wordWrap":true,"wordWrapWidth":500,"align":"center" },
            {"text": [{"content":'సేవ్ & కొనసాగండి'}], "sx": -1000,"sy": 120,"x": 300 ,"y": 764,"size": 19,"weight":"bold","color":"#FFFFFF","tween_type": "Elastic.easeOut","timing": 200,"delay":4.5, "anchor":[0.5,0] },
        ],
        "input_animations": [
            {"text": [{"content":""}], "placeHolder":" ", "key":"in_illness", "sx": -1000,"sy": 120,"x": 310 ,"y": 392,"size": 17,"weight":"bold","width":255,"backgroundColor":"#e9e579","fill":"#2961ab","tween_type": "Elastic.easeOut","timing": 200,"delay":1, "anchor":[0,0] },
            {"text": [{"content":""}], "placeHolder":" ", "key":"in_period_dia", "sx": -1000,"sy": 120,"x": 310 ,"y": 462,"size": 17,"weight":"bold","width":255,"backgroundColor":"#e9e579","fill":"#2961ab","tween_type": "Elastic.easeOut","timing": 200,"delay":1, "anchor":[0,0] },
            {"text": [{"content":""}], "placeHolder":" ", "key":"in_medicine", "sx": -1000,"sy": 120,"x": 310 ,"y": 532,"size": 17,"weight":"bold","width":255,"backgroundColor":"#e9e579","fill":"#2961ab","tween_type": "Elastic.easeOut","timing": 200,"delay":1, "anchor":[0,0] },

        ],
        "sound_list": [
            {
              "sound": ["illness_details_audio"]
            }
        ],
        "functions": [

            {"fn": "SetBGTile('bg_02')", "delay": 0 }

        ],
        "name": "Illness Details - Capture",
        "timing": -1,
        "index":12

    },

//money back....

 {
        "sprite_animations": [
            {"sprite": "bar_01","x": 0,"y": 0,"loop": false,"timing": 0,"delay": 0,"toTopObj":1, "anchor":[0,0]},
            {"sprite": "logo_01","x": 10,"y": 33,"loop": false,"timing": 0,"delay": 0.1,"toTopObj":2, "anchor":[0,0]},
            {"sprite": "bar_03","x": 300,"y": 180,"loop": false,"timing": 0,"delay": 0, "toTopObj":1, "anchor":[0.5,0]},
            {"sprite": "box_02","x": 300,"y": 230,"loop": false,"timing": 0,"delay": 0, "toTopObj":1, "anchor":[0.5,0]},
            {"sprite": "box_02","x": 300,"y": 450,"loop": false,"timing": 0,"delay": 0, "toTopObj":1, "anchor":[0.5,0]},
            {"sprite": "maturity_icon","x": 300,"y": 280,"loop": false,"timing": 0,"delay": 0.5, "toTopObj":1, "anchor":[0.5,0]},
            {"sprite": "dea_icon","x": 300,"y": 500,"loop": false,"timing": 0,"delay": 0.5, "toTopObj":1, "anchor":[0.5,0]},

             {"sprite": "btn_02","x": 300,"y": 738,"loop": false,"timing": 0,"delay":1.5 , "toTopObj":1, "anchor":[0.5,0],"onClickFn":"goToPage(11)"},
            {"sprite": "btn_sp_1","x": 540,"y": 112,"loop": false,"timing": 0,"delay": 2, "toTopObj":1, "anchor":[0,0], "onClickFn":"audioReplay()"}
        ],

         "text_animations": [
        {"text": [{"content":"Pre Issuance Verification Call "}], "sx": -1000,"sy": 50,"x": 190 ,"y": 50,"size": 28,"weight":"bold","color":"#003869","tween_type": "Elastic.easeOut","timing": 200,"delay":0.2, "anchor":[0,0] },

        {"text": [
                    {"content":"పప్రతిపాదన సంఖ్య: "},
                    {"content":"$var.window.p_PROPOSAL_NUMBER"}
        ], "sx": -1000,"sy": 120,"x": 40 ,"y": 150,"size": 22,"weight":"bold","color":"#235291","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },

        {"text": [
                    {"content":"కేసులో మీరు మళ్ళీ వినాలనుకుంటే, ఇక్కడ క్లిక్ చేయండి ---->"}
         ], "sx": -1000,"sy": 120,"x": 238 ,"y": 120,"size": 12,"weight":"bold","color":"#235291","tween_type": "Elastic.easeOut","timing": 200,"delay":2, "anchor":[0,0],"wordWrap": true, "wordWrapWidth":520,"align":"right" },

        {"text": [{"content":"ఉత్పత్తి ప్రయోజనాలు"}], "sx": -1000,"sy": 120,"x": 300 ,"y": 181,"size": 26,"weight":"bold","color":"#FFFFFF","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0.5,0] },

        {"text": [{"content":"మెచ్యూరిటీ బెనిఫిట్"}], "sx": -1000,"sy": 120,"x": 300 ,"y": 240,"size": 18,"weight":"bold","color":"","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0.5,0] },

        {"text": [{"content":"ఒకవేళ మీ పాలసీ మెచ్యూరిటి అయితే, ప్రాథమిక బీమా సొమ్ము + సింపుల్ రివర్షనరీ బోనస్ పొందుతారు + టెర్మినల్ బోనస్ (ఏదైనా ఉంటే) చెల్లించబడుతుంది."}], "sx": -1000,"sy": 120,"x": 60 ,"y": 342,"size": 16,"weight":"bold","color":"#003869","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5,"wordWrap": true, "wordWrapWidth":500,"align":"center", "anchor":[0,0] },


        {"text": [{"content":"డెత్ బెనిఫిట్"}], "sx": -1000,"sy": 120,"x": 300 ,"y": 460,"size": 18,"weight":"bold","color":"","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0.5,0] },

        {"text": [{"content":"ఒకవేళ లైఫ్ అష్యూర్డ్ మరణిస్తే, నామినీ బీమా సొమ్ము లేదా వార్షిక ప్రీమియానికి 10 రెట్లు లేదా మరణించిన తేదీ నాటికి ప్రాప్తించిన బోనస్ + రైడర్ బీమా సొమ్ము, ఎంచుకుంటే, వీటిల్లో ఏది అధికమైతే అది చెల్లించబడుతుంది."}], "sx": -1000,"sy": 120,"x": 60 ,"y": 567,"size": 16,"weight":"bold","color":"#003869","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5,"wordWrap": true, "wordWrapWidth":500,"align":"center", "anchor":[0,0] },


        {"text": [{"content":"ఈ విధానం యొక్క అన్ని లక్షణాలను మీరు పూర్తిగా అర్థం చేసుకున్నారని ఆశిస్తున్నాము."}], "sx": -1000,"sy": 120,"x": 300 ,"y": 695,"size": 17,"weight":"bold","color":"#235291","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0.5,0],"wordWrap":true,"wordWrapWidth":510,"lineSpacing":-5 },

        {"text": [{"content":"కొనసాగండి"}], "sx": -1000,"sy": 750,"x": 300 ,"y": 755,"size": 22,"weight":"bold","color":"#FFFFFF","tween_type": "Elastic.easeOut","timing": 200,"delay":1, "anchor":[0.5,0] }



        ],
        "sound_list": [
            {
                    "sound": ["some_of","jeevan_maturity_benefit","jeevan_death_benefit","hope"]

            }
        ],
        "functions": [
            {"fn": "SetBGTile('bg_02')", "delay": 0 }

        ],
        "name": "Benefits Details - Show",
        "timing": -1,
        "index":13
    },


    {
           "sprite_animations": [
           {"sprite": "bar_01","x": 0,"y": 0,"loop": false,"timing": 0,"delay": 0,"toTopObj":1, "anchor":[0,0]},
            {"sprite": "logo_01","x": 10,"y": 33,"loop": false,"timing": 0,"delay": 0.1,"toTopObj":2, "anchor":[0,0]},
            {"sprite": "bar_03","x": 300,"y": 180,"loop": false,"timing": 0,"delay": 0, "toTopObj":1, "anchor":[0.5,0]},
            {"sprite": "box_072","x": 300,"y": 220,"loop": false,"timing": 0,"delay": 0, "toTopObj":1, "anchor":[0.5,0]},
            {"sprite": "btn_03_3","x": 300,"y": 742,"loop": false,"timing": 0,"delay": 0, "toTopObj":1, "anchor":[0.5,0],"onClickFn":"onVideoRecord('btn_03_3',7,13,220,715,15)"},
            {"sprite": "btn_sp_1","x": 540,"y": 112,"loop": false,"timing": 0,"delay": 2, "toTopObj":1, "anchor":[0,0], "onClickFn":"audioreplay2()"}
        ],
          "text_animations": [
            {"text": [{"content":"Pre Issuance Verification Call "}], "sx": -1000,"sy": 50,"x": 190 ,"y": 50,"size": 28,"weight":"bold","color":"#003869","tween_type": "Elastic.easeOut","timing": 200,"delay":0.2, "anchor":[0,0] },
            {"text": [
                    {"content":"పప్రతిపాదన సంఖ్య: "},
                    {"content":"$var.window.p_PROPOSAL_NUMBER"}
                ], "sx": -1000,"sy": 120,"x": 40 ,"y": 150,"size": 22,"weight":"bold","color":"#235291","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },
            {"text": [
                    {"content":"కేసులో మీరు మళ్ళీ వినాలనుకుంటే, ఇక్కడ క్లిక్ చేయండి ---->"}
                              ], "sx": -1000,"sy": 120,"x": 238 ,"y": 120,"size": 12,"weight":"bold","color":"#235291","tween_type": "Elastic.easeOut","timing": 200,"delay":2, "anchor":[0,0],"wordWrap": true, "wordWrapWidth":520,"align":"right" },
            {"text": [{"content":"వీడియో కన్సెంట్"}], "sx": -1000,"sy": 120,"x": 300 ,"y": 181,"size": 26,"weight":"bold","color":"#FFFFFF","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0.5,0] },

             {"text": [

            // {"content":'Please ensure that you are able to view yourself within the square box clearly till the screen displays as "Face Detected" and then click on "Record" to proceed further.'}
             {"content":"నేను "},
            {"content":"$var.window.p_CUSTOMER_NAME"},
            {"content":" , అందువల్ల నా ప్రతిపాదన యొక్క పై వివరాలను ధృవీకరిస్తాను మరియు భీమా పాలసీ మరింత ముందుకు సాగడానికి నా సమ్మతిని ఇస్తాను. దరఖాస్తు ఫారం మరియు బెనిఫిట్ ఇలస్ట్రేషన్ నేను వ్యక్తిగతంగా సంతకం చేశానని కూడా ధృవీకరించాను."}

             ], "sx": -1000,"sy": 205,"x": 45 ,"y": 221,"size": 17,"weight":"normal","color":"#545252","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5,"wordWrap": true, "wordWrapWidth":530,"align":"center", "anchor":[0,0] },

             {"text": [{"content":'రికార్డెడ్'}], "sx": -1000,"sy": 120,"x": 300 ,"y": 757,"size": 20,"weight":"bold","color":"#FFFFFF","tween_type": "Elastic.easeOut","timing": 200,"delay":0, "anchor":[0.5,0] },

        ],
        "functions": [
              {"fn": "SetBGTile('bg_02')", "delay": 0 },
              {"fn": "cameraAccessError()", "delay" : 0.5 },
              {"fn": "initWebCamPos(300,350,500,350)", "delay" : 1  },
              {"fn": "faceDetectStart(60,360)", "delay" : 2  },



            ],
         "sound_list": [
            {
                 "sound": ["video_audio"]
            }
        ],
        "name": "Video consent - Confirm",
        "timing": -1,
        "index": 14

    },

     {
           "sprite_animations": [
           {"sprite": "bar_01","x": 0,"y": 0,"loop": false,"timing": 0,"delay": 0,"toTopObj":1, "anchor":[0,0]},
            {"sprite": "logo_01","x": 10,"y": 33,"loop": false,"timing": 0,"delay": 0.1,"toTopObj":2, "anchor":[0,0]},
            {"sprite": "bar_03","x": 300,"y": 180,"loop": false,"timing": 0,"delay": 0, "toTopObj":1, "anchor":[0.5,0]},
            {"sprite": "box_10","x": 300,"y": 223,"loop": false,"timing": 0,"delay": 0, "toTopObj":1, "anchor":[0.5,0]},
            {"sprite":"hand","x":300,"y":220,"loop":false,"timing":0,"delay":29,"toTopObj":1, "anchor":[0.5,0]},

            {"sprite":"icn_10","x":300,"y":273,"loop":false,"timing":0,"delay":0,"toTopObj":1, "anchor":[0.5,0],"disappear":28},
           {"sprite": "phno","x":120,"y": 400,"loop":false,"timing":1000,"delay": 31,"toTopObj":1,"anchor":[0.5,0]},
            {"sprite": "Email","x":120,"y": 506,"loop":false,"timing":1000,"delay": 44,"toTopObj":1,"anchor":[0.5,0]},
           {"sprite": "web_icon","x":120,"y": 580,"loop":false,"timing":1000,"delay": 50,"toTopObj":1,"anchor":[0.5,0]},

            {"sprite": "btn_sp_1","x": 540,"y": 112,"loop": false,"timing": 0,"delay": 2, "toTopObj":1, "anchor":[0,0], "onClickFn":"audioReplay()"}
        ],
          "text_animations": [
           {"text": [{"content":"Pre Issuance Verification Call "}], "sx": -1000,"sy": 50,"x": 190 ,"y": 50,"size": 28,"weight":"bold","color":"#003869","tween_type": "Elastic.easeOut","timing": 200,"delay":0.2, "anchor":[0,0] },
           {"text": [
                    {"content":"పప్రతిపాదన సంఖ్య: "},
                    {"content":"$var.window.p_PROPOSAL_NUMBER"}
                ], "sx": -1000,"sy": 120,"x": 40 ,"y": 150,"size": 22,"weight":"bold","color":"#235291","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },
            {"text": [
                    {"content":"కేసులో మీరు మళ్ళీ వినాలనుకుంటే, ఇక్కడ క్లిక్ చేయండి ---->"}
                              ], "sx": -1000,"sy": 120,"x": 238 ,"y": 120,"size": 12,"weight":"bold","color":"#235291","tween_type": "Elastic.easeOut","timing": 200,"delay":2, "anchor":[0,0],"wordWrap": true, "wordWrapWidth":520,"align":"right" },
            {"text": [{"content":"ధన్యవాదాలు"}], "sx": -1000,"sy": 120,"x": 300 ,"y": 181,"size": 26,"weight":"bold","color":"#FFFFFF","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0.5,0] },

            {"text": [
            {"content":"ప్రియమైన "},
            {"content":"$var.window.p_CUSTOMER_NAME"},
            {"content":",  మీ చెల్లింపు పద్ధతిగా ఇసిఎస్/డైరెక్ట్ డెబిట్ని ఎంచుకున్నందుకు కూడా మీకు మా ధన్యవాదాలు. జంజాటం లేని విధానంలో మీరు చెల్లింపులు చేసేందుకు ఇది మీకు సహాయపడుతుంది. మీరు చేయవలసిందల్లా గడువు తేదీన మీరు మీ అకౌంట్లో తగినంత డబ్బు ఉంచడమే."}
            ], "sx": -1000,"sy": 120,"x": 50 ,"y": 435,"size": 20,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0],"wordWrap": true, "wordWrapWidth":520,"align":"center","disappear":28 },

             {"text": [{"content":" మా అండర్రైటింగ్ టీమ్ మీ దరఖాస్తును పరిశీలిస్తోంది మరియు త్వరలోనే వాళ్ళు నిర్ణయం తెలియజేస్తారు. ఒకవేళ, మీ నుంచి ఏదైనా కావలసి వస్తే, ఇండియాఫస్ట్ త్వరలోనే మిమ్మల్ని సంప్రదిస్తుంది."}], "sx": -1000,"sy": 120,"x": 50 ,"y": 600,"size": 20,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0],"wordWrap": true, "wordWrapWidth":520,"align":"center","disappear":28},

           {"text": [{"content":"మరిన్ని ప్రశ్నల విషయంలో, దయచేసి మమ్మల్ని సంప్రదించడానికి సంకోచించకండి"}], "sx": -1000,"sy": 120,"x": 60 ,"y": 320,"size": 20,"weight":"normal","color":"#003869","tween_type": "Elastic.easeOut","timing": 200,"delay":29, "anchor":[0,0],"wordWrap": true, "wordWrapWidth":520,"align":"center" },


           {"text": [{"content":"1800-209-8700"}], "sx": -1000,"sy": 120,"x": 177 ,"y": 420,"size": 22,"weight":"normal","color":"#0098DE","tween_type": "Elastic.easeOut","timing": 200,"delay":32, "anchor":[0,0],"wordWrap": true, "wordWrapWidth":520,"align":"center" },

          {"text": [{"content":"[ఉదయం 9 - సాయంత్రం 7 (సోమ - శని)]"}], "sx": -1000,"sy": 120,"x": 177 ,"y": 450,"size": 22,"weight":"normal","color":"#0098DE","tween_type": "Elastic.easeOut","timing": 200,"delay":38, "anchor":[0,0],"wordWrap": true, "wordWrapWidth":520,"align":"center" },


          {"text": [{"content":"customer.first@indiafirstlife.com"}], "sx": -1000,"sy": 120,"x": 177 ,"y": 520,"size": 22,"weight":"normal","color":"#0098DE","tween_type": "Elastic.easeOut","timing": 200,"delay":45, "anchor":[0,0],"wordWrap": true, "wordWrapWidth":520,"align":"center" },

          {"text": [{"content":"www.indiafirstlife.com"}], "sx": -1000,"sy": 120,"x": 177 ,"y": 600,"size": 22,"weight":"normal","color":"#0098DE","tween_type": "Elastic.easeOut","timing": 200,"delay":51, "anchor":[0,0],"wordWrap": true, "wordWrapWidth":520,"align":"center" },


           {"text": [{"content":"!!! ఆహ్లాదకరమైన రోజు !!!"}], "sx": -1000,"sy": 120,"x": 160 ,"y": 700,"size": 30,"weight":"normal","color":"#003869","tween_type": "Elastic.easeOut","timing": 200,"delay":67, "anchor":[0,0],"wordWrap": true, "wordWrapWidth":520,"align":"center" },

        ],
        "functions": [
            {"fn": "SetBGTile('bg_02')", "delay": 0 }
        ],
         "sound_list": [
            {
                "sound": ["tq_audio_1","tq_audio_2","tq_audio_3"]
              
            }
        ],
        "name": "Thankyou - Confirm",
        "timing": -1,
        "index": 15
    },

   {
          "sprite_animations": [
             {"sprite": "bar_01","x": 0,"y": 0,"loop": false,"timing": 0,"delay": 0,"toTopObj":1, "anchor":[0,0]},
            {"sprite": "logo_01","x": 10,"y": 33,"loop": false,"timing": 0,"delay": 0.1,"toTopObj":2, "anchor":[0,0]},
            {"sprite": "bar_03","x": 300,"y": 180,"loop": false,"timing": 0,"delay": 0, "toTopObj":1, "anchor":[0.5,0]},
            {"sprite": "box_22","x": 300,"y": 223,"loop": false,"timing": 0,"delay": 0, "toTopObj":1, "anchor":[0.5,0]},
           {"sprite": "equity_icon","x": 300,"y": 240,"loop": false,"timing": 0,"delay": 0.5, "toTopObj":1, "anchor":[0.5,0]},


            {"sprite": "btn_02","x": 300,"y": 704,"loop": false,"timing": 0,"delay": 2, "toTopObj":1, "anchor":[0.5,0],"onClickFn":"goToPage(13)"},

           // {"sprite": "btn_03_3","x": 450,"y": 674,"loop": false,"timing": 0,"delay": 2, "toTopObj":1, "anchor":[0.5,0],"onClickFn":"goToPageBack(8)"},

            {"sprite": "btn_sp_1","x": 540,"y": 112,"loop": false,"timing": 0,"delay": 2, "toTopObj":1, "anchor":[0,0], "onClickFn":"audioReplay()"}
        ],
          "text_animations": [
           {"text": [{"content":"Pre Issuance Verification Call "}], "sx": -1000,"sy": 50,"x": 190 ,"y": 50,"size": 28,"weight":"bold","color":"#003869","tween_type": "Elastic.easeOut","timing": 200,"delay":0.2, "anchor":[0,0] },
            {"text": [
                    {"content":"పప్రతిపాదన సంఖ్య: "},
                    {"content":"$var.window.p_PROPOSAL_NUMBER"}
                ], "sx": -1000,"sy": 120,"x": 40 ,"y": 150,"size": 22,"weight":"bold","color":"#235291","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },
             {"text": [
                    {"content":"కేసులో మీరు మళ్ళీ వినాలనుకుంటే, ఇక్కడ క్లిక్ చేయండి ---->"}
                              ], "sx": -1000,"sy": 120,"x": 238 ,"y": 120,"size": 12,"weight":"bold","color":"#235291","tween_type": "Elastic.easeOut","timing": 200,"delay":2, "anchor":[0,0],"wordWrap": true, "wordWrapWidth":520,"align":"right" },
            {"text": [{"content":"ఈక్విటీ"}], "sx": -1000,"sy": 120,"x": 300 ,"y": 181,"size": 26,"weight":"bold","color":"#FFFFFF","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0.5,0] },

            {"text": [{"content":"ఈక్విటీ నష్టభయం ఎక్కువగా ఉండే ఆప్షన్. ఇన్వెస్టర్గా, మీరు దీర్ఘ కాలం పెట్టుబడిపెడితే, ఈక్విటీ పెట్టుబడులు మీకు ఉత్తమంగా పనిచేస్తాయి."}], "sx": -1000,"sy": 120,"x": 50 ,"y": 350,"size": 19.5,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0],"wordWrap":true,"wordWrapWidth":510,"align":"center"},//,"lineSpacing": },

            {"text": [{"content":"మీ పాలసీలో ఆటో ట్రిగ్గర్ ఆధారిత విశిష్టత లేదు. ఒకవేళ ఈక్విటీ 1 ఫండ్లో, రాబడి 10% లేదా ఎక్కువ ఉంటే, వృద్ధికి సమానమైన మొత్తం డెట్ 1 ఫండ్కి బదిలీ చేయబడుతుంది."}], "sx": -1000,"sy": 120,"x": 50 ,"y": 450,"size": 19.5,"weight":"normal","color":"#3b3a3a ","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0],"wordWrap":true,"wordWrapWidth":510,"align":"center"},//"lineSpacing":},

            {"text": [{"content":"ఉదాహరణకు, పాలసీని తీసుకున్న సమయంలో ఫండ్ విలువ రూ. 100/- ఉంటే మరియు పెరుగుదల రూ. 10/- (అంటే ఫండ్ విలువలో 110%), పెరిగిన రూ. 10/- సొమ్ము డెట్ 1 ఫండ్కి బదిలీ చేయబడుతుంది."}], "sx": -1000,"sy": 120,"x": 50 ,"y": 557,"size": 19.5,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0],"wordWrap":true,"wordWrapWidth":510,"align":"center"},//,"lineSpacing":},


           // {"text": [{"content":"We believe that all the policy features have been explained to you correctly. The amount paid by you is only towards the premium of the Policy and you have not been promised with any kind of bonus, loan, Mobile tower installation or refund against any other policy. Please do not believe in any such false promises and highlight any such concern to us immediately. So can we confirm that no such promise has been made to you."}], "sx": -1000,"sy": 120,"x": 50 ,"y": 360,"size": 22,"weight":"bold","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0],"wordWrap": true, "wordWrapWidth":520,"align":"center" },

            {"text": [{"content":'కొనసాగండి'}], "sx": -1000,"sy": 120,"x": 300 ,"y": 722,"size": 24,"weight":"bold","color":"#FFFFFF","tween_type": "Elastic.easeOut","timing": 200,"delay":2, "anchor":[0.5,0] },
           // {"text": [{"content":'NO'}], "sx": -1000,"sy": 120,"x": 450 ,"y": 688,"size": 26,"weight":"bold","color":"#FFFFFF","tween_type": "Elastic.easeOut","timing": 200,"delay":2, "anchor":[0.5,0] },
        ],
        "functions": [

            {"fn": "SetBGTile('bg_02')", "delay": 0 }

        ],
         "sound_list": [
            {
                 "sound": ["equity_audio_1","equity_audio_2","equity_audio_3"]
            }
        ],
         "name": "equity - Confirm",
        "timing": -1,
        "index": 16
      },

        {
           "sprite_animations": [
                  {"sprite": "bar_01","x": 0,"y": 0,"loop": false,"timing": 0,"delay": 0,"toTopObj":1, "anchor":[0,0]},
                  {"sprite": "logo_01","x": 10,"y": 33,"loop": false,"timing": 0,"delay": 0.1,"toTopObj":2, "anchor":[0,0]},
                  {"sprite": "bar_03","x": 300,"y": 180,"loop": false,"timing": 0,"delay": 0, "toTopObj":1, "anchor":[0.5,0]},
                  {"sprite": "box_18","x": 300,"y": 223,"loop": false,"timing": 0,"delay": 0, "toTopObj":1, "anchor":[0.5,0]},
               //   {"sprite": "icn_02","x": 33,"y": 343,"loop": false,"timing": 0,"delay": 0, "toTopObj":1, "anchor":[0,0]},

            {"sprite": "btn_03_2","x": 150,"y": 740,"loop": false,"timing": 0,"delay": 4, "toTopObj":1, "anchor":[0.5,0],"onClickFn":"goToScreen(7,'cPerDet',1),yesno_update('Agree','Personal Details - Show')"},
            {"sprite": "btn_03_3","x": 450,"y": 740,"loop": false,"timing": 0,"delay": 4, "toTopObj":1, "anchor":[0.5,0],"onClickFn":"goToPage(5),yesno_update('Disagree','Personal Details - Show'),specLoad('tel')"},
                  {"sprite": "btn_sp_1","x": 540,"y": 112,"loop": false,"timing": 0,"delay": 2, "toTopObj":1, "anchor":[0,0], "onClickFn":"audioReplay()"}
              ],
              "text_animations": [
                 {"text": [{"content":"Pre Issuance Verification Call "}], "sx": -1000,"sy": 50,"x": 190 ,"y": 50,"size": 28,"weight":"bold","color":"#003869","tween_type": "Elastic.easeOut","timing": 200,"delay":0.2, "anchor":[0,0] },
                  {"text": [
                          {"content":"పప్రతిపాదన సంఖ్య: "},
                          {"content":"$var.window.p_PROPOSAL_NUMBER"}
                      ], "sx": -1000,"sy": 120,"x": 40 ,"y": 150,"size": 22,"weight":"bold","color":"#235291","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },
                   {"text": [
                          {"content":"కేసులో మీరు మళ్ళీ వినాలనుకుంటే, ఇక్కడ క్లిక్ చేయండి ---->"}
                                    ], "sx": -1000,"sy": 120,"x": 238 ,"y": 120,"size": 12,"weight":"bold","color":"#235291","tween_type": "Elastic.easeOut","timing": 200,"delay":2, "anchor":[0,0],"wordWrap": true, "wordWrapWidth":520,"align":"right" },
                  {"text": [{"content":"వ్యక్తిగత & సంప్రదింపు వివరాలు"}], "sx": -1000,"sy": 120,"x": 300 ,"y": 181,"size": 26,"weight":"bold","color":"#FFFFFF","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0.5,0] },
                  {"text": [{"content":"మా రికార్డ్స్ ప్రకారం మీ వ్యక్తిగత వివరాలు "}], "sx": -1000,"sy": 120,"x": 80 ,"y": 235,"size": 18,"weight":"bold","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },

                  {"text": [{"content":"పేరు"}], "sx": -1000,"sy": 120,"x": 80 ,"y": 270,"size": 18,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },
                  {"text": [{"content":":"}], "sx": -1000,"sy": 120,"x": 310 ,"y": 270,"size": 18,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },
                  {"text": [{"content":"$var.window.up_CUSTOMER_NAME"}], "sx": -1000,"sy": 120,"x": 325 ,"y": 270,"size": 18,"weight":"bold","color":"#0098DE","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },


                  {"text": [{"content":"పుట్టిన తేది"}], "sx": -1000,"sy": 120,"x": 80 ,"y": 310,"size": 18,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },
                  {"text": [{"content":":"}], "sx": -1000,"sy": 120,"x": 310 ,"y": 310,"size": 18,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },
                  {"text": [{"content":"$var.window.up_DOB_PH"}], "sx": -1000,"sy": 120,"x": 325 ,"y": 310,"size": 18,"weight":"bold","color":"#0098DE","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },


                  {"text": [{"content":"జెండర్"}], "sx": -1000,"sy": 120,"x": 80 ,"y": 350,"size": 18,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },
                  {"text": [{"content":":"}], "sx": -1000,"sy": 120,"x": 310 ,"y": 350,"size": 18,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },
                  {"text": [{"content":"$var.window.up_GENDER"}], "sx": -1000,"sy": 120,"x": 325 ,"y": 350,"size": 18,"weight":"bold","color":"#0098DE","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },


                  {"text": [{"content":"ఇమెయిల్ ఐడి"}], "sx": -1000,"sy": 120,"x": 80 ,"y":390,"size": 18,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },
                  {"text": [{"content":":"}], "sx": -1000,"sy": 120,"x": 310 ,"y": 390,"size": 18,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },
                  {"text": [{"content":"$var.window.up_EMAIL"}], "sx": -1000,"sy": 120,"x": 325 ,"y": 390,"size": 18,"weight":"bold","color":"#0098DE","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },


                  {"text": [{"content":"మొబైల్ నం."}], "sx": -1000,"sy": 120,"x": 80 ,"y": 430,"size": 18,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },
                  {"text": [{"content":":"}], "sx": -1000,"sy": 120,"x": 305 ,"y": 430,"size": 18,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },
                  {"text": [{"content":"$var.window.up_MOBILE_NUMBER"}], "sx": -1000,"sy": 120,"x": 320 ,"y": 430,"size": 18,"weight":"bold","color":"#0098DE","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },

                  // {"text": [{"content":"ఐడి కార్డ్ ప్రూఫ్ రకం"}], "sx": -1000,"sy": 120,"x": 80 ,"y": 470,"size": 18,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },
                  // {"text": [{"content":":"}], "sx": -1000,"sy": 120,"x": 310 ,"y": 470,"size": 18,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },
                  // {"text": [{"content":"$var.window.up_IDPROF"}], "sx": -1000,"sy": 120,"x": 325 ,"y": 470,"size": 18,"weight":"bold","color":"#0098DE","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },

                  {"text": [{"content":"నామినీ పేరు & సంబంధం"}], "sx": -1000,"sy": 120,"x": 80 ,"y": 470,"size": 18,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },
                  {"text": [{"content":":"}], "sx": -1000,"sy": 120,"x": 310 ,"y": 470,"size": 18,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },
                  {"text": [{"content":"$var.window.up_NOMINEE_NAME"}], "sx": -1000,"sy": 120,"x": 325 ,"y": 470,"size": 18,"weight":"bold","color":"#0098DE","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },

                  {"text": [{"content":"వార్షిక ఆదాయం"}], "sx": -1000,"sy": 120,"x": 80 ,"y": 510,"size": 18,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },
                  {"text": [{"content":":"}], "sx": -1000,"sy": 120,"x": 310 ,"y": 510,"size": 18,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },
                  {"text": [{"content":"$var.window.up_ANNUALINCOME"}], "sx": -1000,"sy": 120,"x": 325 ,"y": 510,"size": 18,"weight":"bold","color":"#0098DE","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },


                  {"text": [{"content":"చిరునామా"}], "sx": -1000,"sy": 120,"x": 80 ,"y": 550,"size": 18,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },
                  {"text": [{"content":":"}], "sx": -1000,"sy": 120,"x": 310 ,"y": 550,"size": 18,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },
                  {"text": [
                             {"content":"$var.window.up_address"}
                            
                  ], "sx": -1000,"sy": 120,"x": 325 ,"y": 550,"size": 18,"weight":"bold","color":"#0098DE","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0],"wordWrap":true,"wordWrapWidth":250,"align":"left","lineSpacing":-6},


            {"text": [{"content":'తెరపై ప్రదర్శించబడే మొత్తం సమాచారం సరైనది అయితే, దయచేసి "అంగీకరిస్తున్నాను" పై క్లిక్ చేయండి, లేకపోతే "అంగీకరించలేదు" క్లిక్ చేయండి'}], "sx": -1000,"sy": 120,"x": 300 ,"y": 670,"size": 17,"weight":"bold","color":"#235291","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0.5,0],"wordWrap":true,"wordWrapWidth":500 },
   
            {"text": [{"content":'అంగీకరిస్తున్నారు'}], "sx": -1000,"sy": 120,"x": 150 ,"y": 754,"size": 19,"weight":"bold","color":"#FFFFFF","tween_type": "Elastic.easeOut","timing": 200,"delay":4, "anchor":[0.5,0] },
            {"text": [{"content":'విభేదిస్తున్నారు'}], "sx": -1000,"sy": 120,"x": 450 ,"y": 754,"size": 19,"weight":"bold","color":"#FFFFFF","tween_type": "Elastic.easeOut","timing": 200,"delay":4, "anchor":[0.5,0] },
              ],
              "sound_list": [
                  {
                       "sound": ["personal_details_audio"]
                  }
              ],
              "functions": [
                  {"fn": "SetBGTile('bg_02')", "delay": 0 },
                   {"fn": "cameraAccessError()", "delay" : 0.5 },
                   {"fn": "captureImage()", "delay": 1.5 }
              ],
              "name": "Personal Details - updated",
              "timing": -1,
              "index": 17
          },
{
        "sprite_animations": [
            {"sprite": "bar_01","x": 0,"y": 0,"loop": false,"timing": 0,"delay": 0,"toTopObj":1, "anchor":[0,0]},
            {"sprite": "logo_01","x": 20,"y": 35,"loop": false,"timing": 0,"delay": 0.1,"toTopObj":2, "anchor":[0,0]},
            {"sprite": "no-pictures","x": 300,"y": 200,"loop": false,"timing": 0,"delay": 0,"toTopObj":1, "anchor":[0.5,0],"scale":0.5},
               //   {"sprite": "btn_sp_1","x": 540,"y": 112,"loop": false,"timing": 0,"delay": 2, "toTopObj":1, "anchor":[0,0], "onClickFn":"audioReplay()"}
              ],
        "text_animations": [
            {"text": [{"content":"Pre Issuance Verification Call"}], "sx": -1000,"sy": 50,"x": 190 ,"y": 50,"size": 28,"weight":"bold","color":"#003869","tween_type": "Elastic.easeOut","timing": 200,"delay":0.2, "anchor":[0,0] },
          
             // {"text": [
             //        {"content":"కేసులో మీరు మళ్ళీ వినాలనుకుంటే, ఇక్కడ క్లిక్ చేయండి ---->"}
             //                  ], "sx": -1000,"sy": 120,"x": 238 ,"y": 120,"size": 12,"weight":"bold","color":"#235291","tween_type": "Elastic.easeOut","timing": 200,"delay":2, "anchor":[0,0],"wordWrap": true, "wordWrapWidth":520,"align":"right" },

            {"text": [
                    {"content":"కెమెరా యాక్సెస్ చేయబడదు. దయచేసి కెమెరాను ప్రారంభించి, మళ్లీ ప్రయత్నించండి!"}
                ], "sx": -1000,"sy": 340,"x": 300 ,"y": 340,"size": 22,"weight":"bold","color":"#575755","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0.5,0], "wordWrap": true, "wordWrapWidth":500,"align":"center" }
        ],
        "functions": [
            {"fn": "SetBGTile('bg_02')", "delay": 0},
            {"fn": "reloadPage()", "delay" : 25 }
        ],
        "name": "Camera Error Page",
        "timing": -1,
        "index": 18
    },

/*
     {
     "sprite_animations": [
            {"sprite": "bar_01","x": 0,"y": 0,"loop": false,"timing": 0,"delay": 0,"toTopObj":1, "anchor":[0,0]},
            {"sprite": "logo_01","x": 10,"y": 33,"loop": false,"timing": 0,"delay": 0.1,"toTopObj":2, "anchor":[0,0]},
            {"sprite": "bar_03","x": 300,"y": 180,"loop": false,"timing": 0,"delay": 0, "toTopObj":1, "anchor":[0.5,0]},
            {"sprite": "box_06","x": 300,"y": 223,"loop": false,"timing": 0,"delay": 0, "toTopObj":1, "anchor":[0.5,0]},
            {"sprite": "icn_02","x": 35,"y": 343,"loop": false,"timing": 0,"delay": 0, "toTopObj":1, "anchor":[0,0]},

            {"sprite": "btn_03_2","x": 150,"y": 740,"loop": false,"timing": 0,"delay": 4, "toTopObj":1, "anchor":[0.5,0],"onClickFn":"goToScreen(7,'cPerDet',1)"},
            {"sprite": "btn_03_3","x": 450,"y": 740,"loop": false,"timing": 0,"delay": 4, "toTopObj":1, "anchor":[0.5,0],"onClickFn":"goToPage(5)"},
            {"sprite": "btn_sp_1","x": 540,"y": 112,"loop": false,"timing": 0,"delay": 2, "toTopObj":1, "anchor":[0,0], "onClickFn":"audioReplay()"}
        ],
        "text_animations": [
           {"text": [{"content":"Pre Issuance Verification Call "}], "sx": -1000,"sy": 50,"x": 190 ,"y": 50,"size": 28,"weight":"bold","color":"#003869","tween_type": "Elastic.easeOut","timing": 200,"delay":0.2, "anchor":[0,0] },
            {"text": [
                    {"content":"పప్రతిపాదన సంఖ్య: "},
                    {"content":"$var.window.p_PROPOSAL_NUMBER"}
                ], "sx": -1000,"sy": 120,"x": 40 ,"y": 150,"size": 22,"weight":"bold","color":"#235291","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },
             {"text": [
                    {"content":"కేసులో మీరు మళ్ళీ వినాలనుకుంటే, ఇక్కడ క్లిక్ చేయండి ---->"}
                              ], "sx": -1000,"sy": 120,"x": 285 ,"y": 120,"size": 12,"weight":"bold","color":"#235291","tween_type": "Elastic.easeOut","timing": 200,"delay":2, "anchor":[0,0],"wordWrap": true, "wordWrapWidth":520,"align":"right" },
            {"text": [{"content":"వ్యక్తిగత & సంప్రదింపు వివరాలు"}], "sx": -1000,"sy": 120,"x": 300 ,"y": 186,"size": 26,"weight":"bold","color":"#FFFFFF","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0.5,0] },
            {"text": [{"content":"మా రికార్డ్స్ ప్రకారం మీ వ్యక్తిగత వివరాలు "}], "sx": -1000,"sy": 120,"x": 100 ,"y": 240,"size": 18,"weight":"bold","color":"#a3a5a6","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },

            {"text": [{"content":"Name"}], "sx": -1000,"sy": 120,"x": 104 ,"y": 272,"size": 14,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },
            {"text": [{"content":":"}], "sx": -1000,"sy": 120,"x": 296 ,"y": 272,"size": 14,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },
            {"text": [{"content":"$var.window.p_CUSTOMER_NAME"}], "sx": -1000,"sy": 120,"x": 310 ,"y": 272,"size": 14,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },


            {"text": [{"content":"Date of Birth"}], "sx": -1000,"sy": 120,"x": 104 ,"y": 308,"size": 14,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },
            {"text": [{"content":":"}], "sx": -1000,"sy": 120,"x": 296 ,"y": 308,"size": 14,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },
            {"text": [{"content":"$var.window.p_DOB_PH"}], "sx": -1000,"sy": 120,"x": 310 ,"y": 308,"size": 14,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },


            {"text": [{"content":"Gender"}], "sx": -1000,"sy": 120,"x": 104 ,"y": 344,"size": 14,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },
            {"text": [{"content":":"}], "sx": -1000,"sy": 120,"x": 296 ,"y": 344,"size": 14,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },
            {"text": [{"content":"$var.window.p_MA_GENDER"}], "sx": -1000,"sy": 120,"x": 310 ,"y": 344,"size": 14,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },


            {"text": [{"content":"Email Id"}], "sx": -1000,"sy": 120,"x": 104 ,"y": 380,"size": 14,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },
            {"text": [{"content":":"}], "sx": -1000,"sy": 120,"x": 296 ,"y": 380,"size": 14,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },
            {"text": [{"content":"$var.window.p_EMAIL"}], "sx": -1000,"sy": 120,"x": 310 ,"y": 380,"size": 14,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },


            {"text": [{"content":"Mobile No."}], "sx": -1000,"sy": 120,"x": 104 ,"y": 416,"size": 14,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },
            {"text": [{"content":":"}], "sx": -1000,"sy": 120,"x": 296 ,"y": 416,"size": 14,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },
            {"text": [{"content":"$var.window.p_MOBILE_NUMBER"}], "sx": -1000,"sy": 120,"x": 310 ,"y": 416,"size": 14,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },

            {"text": [{"content":"Type of ID card Proof"}], "sx": -1000,"sy": 120,"x": 104 ,"y": 452,"size": 14,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },
            {"text": [{"content":":"}], "sx": -1000,"sy": 120,"x": 296 ,"y": 452,"size": 14,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },
            {"text": [{"content":"$var.window.p_IDPROF"}], "sx": -1000,"sy": 120,"x": 310 ,"y": 452,"size": 14,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },

            {"text": [{"content":"Nominee name & relationship"}], "sx": -1000,"sy": 120,"x": 104 ,"y": 488,"size": 14,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },
            {"text": [{"content":":"}], "sx": -1000,"sy": 120,"x": 296 ,"y": 488,"size": 14,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },
            {"text": [{"content":"$var.window.p_NOMINEE_NAME"}], "sx": -1000,"sy": 120,"x": 310 ,"y": 488,"size": 14,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },

            {"text": [{"content":"Annual income"}], "sx": -1000,"sy": 120,"x": 104 ,"y": 524,"size": 14,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },
            {"text": [{"content":":"}], "sx": -1000,"sy": 120,"x": 296 ,"y": 524,"size": 14,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },
            {"text": [{"content":"$var.window.p_ANNUALINCOME"}], "sx": -1000,"sy": 120,"x": 310 ,"y": 524,"size": 14,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },


            {"text": [{"content":"Address"}], "sx": -1000,"sy": 120,"x": 104 ,"y": 560,"size": 14,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },
            {"text": [{"content":":"}], "sx": -1000,"sy": 120,"x": 296 ,"y": 560,"size": 14,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },
            {"text": [
                       {"content":"$var.window.p_MAILINGADDRESS1"},
                        {"content":", "},
                       {"content": "$var.window.p_MAILINGADDRESS2"},
                        {"content":", "},
                       {"content": "$var.window.p_MAILINGCITY"}
            ], "sx": -1000,"sy": 120,"x": 310 ,"y": 560,"size": 14,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },


            {"text": [{"content":"If all information displayed on the screen is correct,"}], "sx": -1000,"sy": 120,"x": 300 ,"y": 676,"size": 18,"weight":"bold","color":"#4c7dbf","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0.5,0] },
            {"text": [{"content":'Please click on "Agree", else click "Disagree"'}], "sx": -1000,"sy": 120,"x": 300 ,"y": 698,"size": 18,"weight":"bold","color":"#4c7dbf","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0.5,0] },

            {"text": [{"content":'Agree'}], "sx": -1000,"sy": 120,"x": 150 ,"y": 754,"size": 24,"weight":"bold","color":"#FFFFFF","tween_type": "Elastic.easeOut","timing": 200,"delay":4, "anchor":[0.5,0] },
            {"text": [{"content":'Disagree'}], "sx": -1000,"sy": 120,"x": 450 ,"y": 754,"size": 24,"weight":"bold","color":"#FFFFFF","tween_type": "Elastic.easeOut","timing": 200,"delay":4, "anchor":[0.5,0] },
        ],
        "sound_list": [
            {
                "sound": ["au_4_1"]
            }
        ],
        "functions": [
            {"fn": "SetBGTile('bg_02')", "delay": 0 },
            // {"fn": "cameraAccessError()", "delay" : 0.5 },
            // {"fn": "captureImage()", "delay": 1.5 }
        ],
        "name": "Personal Details",
        "timing": -1,
        "index": 4
    },


{
        "sprite_animations": [
            {"sprite": "bar_01","x": 0,"y": 0,"loop": false,"timing": 0,"delay": 0,"toTopObj":1, "anchor":[0,0]},
            {"sprite": "logo_01","x": 10,"y": 33,"loop": false,"timing": 0,"delay": 0.1,"toTopObj":2, "anchor":[0,0]},
            {"sprite": "bar_03","x": 300,"y": 180,"loop": false,"timing": 0,"delay": 0, "toTopObj":1, "anchor":[0.5,0]},
            {"sprite": "box_06","x": 300,"y": 223,"loop": false,"timing": 0,"delay": 0, "toTopObj":1, "anchor":[0.5,0]},
            {"sprite": "icn_02","x": 35,"y": 343,"loop": false,"timing": 0,"delay": 0, "toTopObj":1, "anchor":[0,0]},
            {"sprite": "btn_03_3","x": 300,"y": 748,"loop": false,"timing": 0,"delay": 4.5, "toTopObj":1, "anchor":[0.5,0],"onClickFn":"editGoToPage(8,240,197,'ePerDet','cPerDet')"},
            {"sprite": "btn_sp_1","x": 540,"y": 112,"loop": false,"timing": 0,"delay": 2, "toTopObj":1, "anchor":[0,0], "onClickFn":"audioReplay()"}
        ],

           "text_animations": [
           {"text": [{"content":"Pre Issuance Verification Call "}], "sx": -1000,"sy": 50,"x": 190 ,"y": 50,"size": 28,"weight":"bold","color":"#003869","tween_type": "Elastic.easeOut","timing": 200,"delay":0.2, "anchor":[0,0] },
            {"text": [
                    {"content":"పప్రతిపాదన సంఖ్య: "},
                    {"content":"$var.window.p_PROPOSAL_NUMBER"}
                ], "sx": -1000,"sy": 120,"x": 40 ,"y": 150,"size": 22,"weight":"bold","color":"#235291","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },
             {"text": [
                    {"content":"కేసులో మీరు మళ్ళీ వినాలనుకుంటే, ఇక్కడ క్లిక్ చేయండి ---->"}
                              ], "sx": -1000,"sy": 120,"x": 285 ,"y": 120,"size": 12,"weight":"bold","color":"#235291","tween_type": "Elastic.easeOut","timing": 200,"delay":2, "anchor":[0,0],"wordWrap": true, "wordWrapWidth":520,"align":"right" },

            {"text": [{"content":"వ్యక్తిగత & సంప్రదింపు వివరాలు"}], "sx": -1000,"sy": 120,"x": 300 ,"y": 186,"size": 26,"weight":"bold","color":"#FFFFFF","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0.5,0] },

            {"text": [{"content":"మా రికార్డ్స్ ప్రకారం మీ వ్యక్తిగత వివరాలు "}], "sx": -1000,"sy": 120,"x": 100 ,"y": 240,"size": 18,"weight":"bold","color":"#a3a5a6","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },

            {"text": [{"content":"Name"}], "sx": -1000,"sy": 120,"x": 104 ,"y": 272,"size": 14,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },
            {"text": [{"content":":"}], "sx": -1000,"sy": 120,"x": 296 ,"y": 272,"size": 14,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },

            {"text": [{"content":"Date of Birth"}], "sx": -1000,"sy": 120,"x": 104 ,"y": 308,"size": 14,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },
            {"text": [{"content":":"}], "sx": -1000,"sy": 120,"x": 296 ,"y": 308,"size": 14,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },

            {"text": [{"content":"Gender"}], "sx": -1000,"sy": 120,"x": 104 ,"y": 344,"size": 14,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },
            {"text": [{"content":":"}], "sx": -1000,"sy": 120,"x": 296 ,"y": 344,"size": 14,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },

            {"text": [{"content":"Email Id"}], "sx": -1000,"sy": 120,"x": 104 ,"y": 380,"size": 14,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },
            {"text": [{"content":":"}], "sx": -1000,"sy": 120,"x": 296 ,"y": 380,"size": 14,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },

            {"text": [{"content":"Mobile No."}], "sx": -1000,"sy": 120,"x": 104 ,"y": 416,"size": 14,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },
            {"text": [{"content":":"}], "sx": -1000,"sy": 120,"x": 296 ,"y": 416,"size": 14,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },

            {"text": [{"content":"Type of ID card Proof"}], "sx": -1000,"sy": 120,"x": 104 ,"y": 452,"size": 14,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },
            {"text": [{"content":":"}], "sx": -1000,"sy": 120,"x": 296 ,"y": 452,"size": 14,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },

            {"text": [{"content":"Nominee name & relationship"}], "sx": -1000,"sy": 120,"x": 104 ,"y": 488,"size": 14,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },
            {"text": [{"content":":"}], "sx": -1000,"sy": 120,"x": 296 ,"y": 488,"size": 14,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },

            {"text": [{"content":"Annual income"}], "sx": -1000,"sy": 120,"x": 104 ,"y": 524,"size": 14,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },
            {"text": [{"content":":"}], "sx": -1000,"sy": 120,"x": 296 ,"y": 524,"size": 14,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },


            {"text": [{"content":"Address"}], "sx": -1000,"sy": 120,"x": 104 ,"y": 560,"size": 14,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },
            {"text": [{"content":":"}], "sx": -1000,"sy": 120,"x": 296 ,"y": 560,"size": 14,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },


            {"text": [{"content":"Please do the necessary corrections in boxes marked 'yellow',\n"},
                    {"content":"and then click on 'Save and Proceed' to proceed further."}
                     ], "sx": -1000,"sy": 120,"x": 300 ,"y": 690,"size": 18,"weight":"bold","color":"#4c7dbf","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0.5,0] },
            {"text": [{"content":'Save & Proceed'}], "sx": -1000,"sy": 120,"x": 300 ,"y": 764,"size": 18,"weight":"bold","color":"#FFFFFF","tween_type": "Elastic.easeOut","timing": 200,"delay":4.5, "anchor":[0.5,0] },
        ],
        "input_animations": [
            {"text": [{"content":"$var.window.p_CUSTOMER_NAME"}], "placeHolder":" ", "key":"in_name", "sx": -1000,"sy": 120,"x": 310 ,"y": 272,"size": 14,"weight":"bold","width":255,"backgroundColor":"#e9e579","fill":"#2961ab","tween_type": "Elastic.easeOut","timing": 200,"delay":1, "anchor":[0,0] },
            {"text": [{"content":"$var.window.p_DOB_PH"}], "placeHolder":" ", "key":"in_dob", "sx": -1000,"sy": 120,"x": 310 ,"y": 308,"size": 14,"weight":"bold","width":255,"backgroundColor":"#e9e579","fill":"#2961ab","tween_type": "Elastic.easeOut","timing": 200,"delay":1, "anchor":[0,0] },
            {"text": [{"content":"$var.window.p_MA_GENDER"}], "placeHolder":" ", "key":"in_gender", "sx": -1000,"sy": 120,"x": 310 ,"y": 344,"size": 14,"weight":"bold","width":255,"backgroundColor":"#e9e579","fill":"#2961ab","tween_type": "Elastic.easeOut","timing": 200,"delay":1, "anchor":[0,0] },
            {"text": [{"content":"$var.window.p_EMAIL"}], "placeHolder":" ", "key":"in_occupation", "sx": -1000,"sy": 120,"x": 310 ,"y": 380,"size": 14,"weight":"bold","width":255,"backgroundColor":"#e9e579","fill":"#2961ab","tween_type": "Elastic.easeOut","timing": 200,"delay":1, "anchor":[0,0] },
            {"text": [{"content":"$var.window.p_MOBILE_NUMBER"}], "placeHolder":" ", "key":"in_nominee_name","sx": -1000,"sy": 120,"x": 310 ,"y": 416,"size": 14,"weight":"bold","width":255,"backgroundColor":"#e9e579","fill":"#2961ab","tween_type": "Elastic.easeOut","timing": 200,"delay":1, "anchor":[0,0] },
            {"text": [{"content":"$var.window.p_IDPROF"}], "placeHolder":" ", "key":"in_nominee_relation", "sx": -1000,"sy": 120,"x": 310 ,"y": 452,"size": 14,"weight":"bold","width":255,"backgroundColor":"#e9e579","fill":"#2961ab","tween_type": "Elastic.easeOut","timing": 200,"delay":1, "anchor":[0,0] },
            {"text": [{"content":"$var.window.p_NOMINEE_NAME"}], "placeHolder":" ", "key":"in_email", "sx": -1000,"sy": 120,"x": 310 ,"y": 488,"size": 14,"weight":"bold","width":255,"backgroundColor":"#e9e579","fill":"#2961ab","tween_type": "Elastic.easeOut","timing": 200,"delay":1, "anchor":[0,0] },
            {"text": [{"content":"$var.window.p_ANNUALINCOME"}], "placeHolder":" ", "key":"in_mobile_no", "sx": -1000,"sy": 120,"x": 310 ,"y": 524,"size": 14,"weight":"bold","width":255,"backgroundColor":"#e9e579","fill":"#2961ab","tween_type": "Elastic.easeOut","timing": 200,"delay":1, "anchor":[0,0] },
            {"text": [
                       {"content":"$var.window.p_MAILINGADDRESS1"},
                       {"content":", "},
                       {"content": "$var.window.p_MAILINGADDRESS2"},
                       {"content":", "},
                       {"content": "$var.window.p_MAILINGCITY"}
             ], "placeHolder":" ", "key":"in_mobile_no", "sx": -1000,"sy": 120,"x": 310 ,"y": 560,"size": 14,"weight":"bold","width":255,"backgroundColor":"#e9e579","fill":"#2961ab","tween_type": "Elastic.easeOut","timing": 200,"delay":1, "anchor":[0,0] }

        ],
        "sound_list": [
            {
                "sound": ["ab_6"]
            }
        ],
        "functions": [

            {"fn": "SetBGTile('bg_02')", "delay": 0 }

        ],
        "name": "Personal Details - Disagree",
        "timing": -1,
        "index": 6
    },




 {
       "sprite_animations": [
            {"sprite": "bar_01","x": 0,"y": 0,"loop": false,"timing": 0,"delay": 0,"toTopObj":1, "anchor":[0,0]},
            {"sprite": "logo_01","x": 10,"y": 33,"loop": false,"timing": 0,"delay": 0.1,"toTopObj":2, "anchor":[0,0]},
            {"sprite": "bar_03","x": 300,"y": 180,"loop": false,"timing": 0,"delay": 0, "toTopObj":1, "anchor":[0.5,0]},
            {"sprite": "box_18","x": 300,"y": 223,"loop": false,"timing": 0,"delay": 0, "toTopObj":1, "anchor":[0.5,0]},
//{"sprite": "icn_02","x": 35,"y": 343,"loop": false,"timing": 0,"delay": 0, "toTopObj":1, "anchor":[0,0]},
            {"sprite": "btn_03_2","x": 150,"y": 740,"loop": false,"timing": 0,"delay": 4, "toTopObj":1, "anchor":[0.5,0],"onClickFn":"goToScreen(11,'cPerDet',1)"},
            {"sprite": "btn_03_3","x": 450,"y": 740,"loop": false,"timing": 0,"delay": 4, "toTopObj":1, "anchor":[0.5,0],"onClickFn":"goToPage(9)"},
            {"sprite": "btn_sp_1","x": 540,"y": 112,"loop": false,"timing": 0,"delay": 2, "toTopObj":1, "anchor":[0,0], "onClickFn":"audioReplay()"}
        ],
        "text_animations": [
           {"text": [{"content":"Pre Issuance Verification Call "}], "sx": -1000,"sy": 50,"x": 190 ,"y": 50,"size": 28,"weight":"bold","color":"#003869","tween_type": "Elastic.easeOut","timing": 200,"delay":0.2, "anchor":[0,0] },
            {"text": [
                    {"content":"పప్రతిపాదన సంఖ్య: "},
                    {"content":"$var.window.p_PROPOSAL_NUMBER"}
                ], "sx": -1000,"sy": 120,"x": 40 ,"y": 150,"size": 22,"weight":"bold","color":"#235291","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },
             {"text": [
                    {"content":"కేసులో మీరు మళ్ళీ వినాలనుకుంటే, ఇక్కడ క్లిక్ చేయండి ---->"}
                              ], "sx": -1000,"sy": 120,"x": 285 ,"y": 120,"size": 12,"weight":"bold","color":"#235291","tween_type": "Elastic.easeOut","timing": 200,"delay":2, "anchor":[0,0],"wordWrap": true, "wordWrapWidth":520,"align":"right" },
            {"text": [{"content":"PLAN DETAILS"}], "sx": -1000,"sy": 120,"x": 300 ,"y": 186,"size": 26,"weight":"bold","color":"#FFFFFF","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0.5,0] },
            {"text": [{"content":"Your plan details as per  our records are "}], "sx": -1000,"sy": 120,"x": 100 ,"y": 240,"size": 18,"weight":"bold","color":"#3b3a3a ","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },

            {"text": [{"content":"Plan Name"}], "sx": -1000,"sy": 120,"x": 104 ,"y": 290,"size": 16,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },
            {"text": [{"content":":"}], "sx": -1000,"sy": 120,"x": 296 ,"y": 290,"size": 16,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },
            {"text": [{"content":"$var.window.p_CUSTOMER_NAME"}], "sx": -1000,"sy": 120,"x": 310 ,"y": 290,"size": 16,"weight":"bold","color":"#0098DE","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },


            {"text": [{"content":"Premium Amount"}], "sx": -1000,"sy": 120,"x": 104 ,"y": 340,"size": 16,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },
            {"text": [{"content":":"}], "sx": -1000,"sy": 120,"x": 296 ,"y": 340,"size": 16,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },
            {"text": [{"content":"$var.window.p_CUSTOMER_NAME"}], "sx": -1000,"sy": 120,"x": 310 ,"y": 340,"size": 16,"weight":"bold","color":"#0098DE","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },


            {"text": [{"content":"Payment Frequency"}], "sx": -1000,"sy": 120,"x": 104 ,"y":390,"size": 16,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },
            {"text": [{"content":":"}], "sx": -1000,"sy": 120,"x": 296 ,"y": 390,"size": 16,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },
            {"text": [{"content":"$var.window.p_CUSTOMER_NAME"}], "sx": -1000,"sy": 120,"x": 310 ,"y": 390,"size": 16,"weight":"bold","color":"#0098DE","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },


            {"text": [{"content":"Premium Paying Term"}], "sx": -1000,"sy": 120,"x": 104 ,"y": 440,"size": 16,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },
            {"text": [{"content":":"}], "sx": -1000,"sy": 120,"x": 296 ,"y": 440,"size": 16,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },
            {"text": [{"content":"$var.window.p_DOB_PH"}], "sx": -1000,"sy": 120,"x": 310 ,"y": 440,"size": 16,"weight":"bold","color":"#0098DE","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },


            {"text": [{"content":"Policy Term"}], "sx": -1000,"sy": 120,"x": 104 ,"y": 490,"size": 16,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },
            {"text": [{"content":":"}], "sx": -1000,"sy": 120,"x": 296 ,"y": 490,"size": 16,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },
            {"text": [{"content":"$var.window.p_MA_GENDER"}], "sx": -1000,"sy": 120,"x": 310 ,"y": 490,"size": 16,"weight":"bold","color":"#0098DE","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },

            {"text": [{"content":"Sum Assured"}], "sx": -1000,"sy": 120,"x": 104 ,"y": 540,"size": 16,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },
            {"text": [{"content":":"}], "sx": -1000,"sy": 120,"x": 296 ,"y": 540,"size": 16,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },
            {"text": [{"content":"$var.window.p_PROPOSER_OCCUPATION"}], "sx": -1000,"sy": 120,"x": 310 ,"y": 540,"size": 16,"weight":"bold","color":"#0098DE","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },

//             {"text": [{"content":"Equity "}], "sx": -1000,"sy": 120,"x": 104 ,"y": 488,"size": 14,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },
//             {"text": [{"content":":"}], "sx": -1000,"sy": 120,"x": 296 ,"y": 488,"size": 14,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },
//             {"text": [{"content":"$var.window.p_NOMINEE_NAME"}], "sx": -1000,"sy": 120,"x": 310 ,"y": 488,"size": 14,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },
// //

            {"text": [{"content":"If all information displayed on the screen is correct,"}], "sx": -1000,"sy": 120,"x": 300 ,"y": 676,"size": 18,"weight":"bold","color":"#4c7dbf","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0.5,0] },
            {"text": [{"content":'Please click on "Agree", else click "Disagree"'}], "sx": -1000,"sy": 120,"x": 300 ,"y": 698,"size": 18,"weight":"bold","color":"#4c7dbf","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0.5,0] },

            {"text": [{"content":'Agree'}], "sx": -1000,"sy": 120,"x": 150 ,"y": 754,"size": 24,"weight":"bold","color":"#FFFFFF","tween_type": "Elastic.easeOut","timing": 200,"delay":4, "anchor":[0.5,0] },
            {"text": [{"content":'Disagree'}], "sx": -1000,"sy": 120,"x": 450 ,"y": 754,"size": 24,"weight":"bold","color":"#FFFFFF","tween_type": "Elastic.easeOut","timing": 200,"delay":4, "anchor":[0.5,0] },
        ],
        "sound_list": [
            {
                "sound": ["au_4_1"]
            }
        ],
        "functions": [

            {"fn": "SetBGTile('bg_02')", "delay": 0 },

        ],
        "name": "Plan Details",
        "timing": -1,
        "index": 16
    },

  {
  "sprite_animations": [
             {"sprite": "bar_01","x": 0,"y": 0,"loop": false,"timing": 0,"delay": 0,"toTopObj":1, "anchor":[0,0]},
            {"sprite": "logo_01","x": 10,"y": 33,"loop": false,"timing": 0,"delay": 0.1,"toTopObj":2, "anchor":[0,0]},
            {"sprite": "bar_03","x": 300,"y": 180,"loop": false,"timing": 0,"delay": 0, "toTopObj":1, "anchor":[0.5,0]},
            {"sprite": "box_18","x": 300,"y": 223,"loop": false,"timing": 0,"delay": 0, "toTopObj":1, "anchor":[0.5,0]},
            {"sprite": "btn_03_3","x": 300,"y": 748,"loop": false,"timing": 0,"delay": 4.5, "toTopObj":1, "anchor":[0.5,0],"onClickFn":"editGoToPage(11,240,660,'ePerDet','cPerDet')"},
            {"sprite": "btn_sp_1","x": 540,"y": 112,"loop": false,"timing": 0,"delay": 2, "toTopObj":1, "anchor":[0,0], "onClickFn":"audioReplay()"}
        ],
        "text_animations": [
           {"text": [{"content":"Pre Issuance Verification Call "}], "sx": -1000,"sy": 50,"x": 190 ,"y": 50,"size": 28,"weight":"bold","color":"#003869","tween_type": "Elastic.easeOut","timing": 200,"delay":0.2, "anchor":[0,0] },
            {"text": [
                    {"content":"పప్రతిపాదన సంఖ్య: "},
                    {"content":"$var.window.p_PROPOSAL_NUMBER"}
                ], "sx": -1000,"sy": 120,"x": 40 ,"y": 150,"size": 22,"weight":"bold","color":"#235291","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },
             {"text": [
                    {"content":"కేసులో మీరు మళ్ళీ వినాలనుకుంటే, ఇక్కడ క్లిక్ చేయండి ---->"}
                              ], "sx": -1000,"sy": 120,"x": 285 ,"y": 120,"size": 12,"weight":"bold","color":"#235291","tween_type": "Elastic.easeOut","timing": 200,"delay":2, "anchor":[0,0],"wordWrap": true, "wordWrapWidth":520,"align":"right" },
            {"text": [{"content":"PLAN DETAILS"}], "sx": -1000,"sy": 120,"x": 300 ,"y": 186,"size": 26,"weight":"bold","color":"#FFFFFF","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0.5,0] },
          //  {"text": [{"content":"మా రికార్డ్స్ ప్రకారం మీ వ్యక్తిగత వివరాలు "}], "sx": -1000,"sy": 120,"x": 100 ,"y": 240,"size": 18,"weight":"bold","color":"#a3a5a6","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },

            {"text": [{"content":"Plan Name"}], "sx": -1000,"sy": 120,"x": 104 ,"y": 280,"size": 16,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },
            {"text": [{"content":":"}], "sx": -1000,"sy": 120,"x": 296 ,"y": 280,"size": 16,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },
           {"text": [{"content":"$var.window.p_CUSTOMER_NAME"}], "sx": -1000,"sy": 120,"x": 310 ,"y": 280,"size": 16,"weight":"bold","color":"#0098DE","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },


            {"text": [{"content":"Premium Amount"}], "sx": -1000,"sy": 120,"x": 104 ,"y": 330,"size": 16,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },
            {"text": [{"content":":"}], "sx": -1000,"sy": 120,"x": 296 ,"y": 330,"size": 16,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },
            {"text": [{"content":"$var.window.p_CUSTOMER_NAME"}], "sx": -1000,"sy": 120,"x": 310 ,"y": 330,"size": 16,"weight":"bold","color":"#0098DE","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },


            {"text": [{"content":"Payment Frequency"}], "sx": -1000,"sy": 120,"x": 104 ,"y": 380,"size": 16,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },
            {"text": [{"content":":"}], "sx": -1000,"sy": 120,"x": 296 ,"y": 380,"size": 16,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },
            {"text": [{"content":"$var.window.p_CUSTOMER_NAME"}], "sx": -1000,"sy": 120,"x": 310 ,"y": 380,"size": 16,"weight":"bold","color":"#0098DE","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },


            {"text": [{"content":"Premium Paying Term"}], "sx": -1000,"sy": 120,"x": 104 ,"y": 430,"size": 16,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },
            {"text": [{"content":":"}], "sx": -1000,"sy": 120,"x": 296 ,"y": 430,"size": 16,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },
            {"text": [{"content":"$var.window.p_DOB_PH"}], "sx": -1000,"sy": 120,"x": 310 ,"y": 430,"size": 16,"weight":"bold","color":"#0098DE","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },


            {"text": [{"content":"Policy Term"}], "sx": -1000,"sy": 120,"x": 104 ,"y": 480,"size": 16,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },
            {"text": [{"content":":"}], "sx": -1000,"sy": 120,"x": 296 ,"y": 480,"size": 16,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },
            {"text": [{"content":"$var.window.p_MA_GENDER"}], "sx": -1000,"sy": 120,"x": 310 ,"y": 480,"size": 16,"weight":"bold","color":"#0098DE","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },

            {"text": [{"content":"Sum Assured"}], "sx": -1000,"sy": 120,"x": 104 ,"y": 530,"size": 16,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },
            {"text": [{"content":":"}], "sx": -1000,"sy": 120,"x": 296 ,"y": 530,"size": 16,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },
            {"text": [{"content":"$var.window.p_PROPOSER_OCCUPATION"}], "sx": -1000,"sy": 120,"x": 310 ,"y": 530,"size": 16,"weight":"bold","color":"#0098DE","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },

         //   {"text": [{"content":"Equity "}], "sx": -1000,"sy": 120,"x": 104 ,"y": 488,"size": 16,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },
//       {"text": [{"content":":"}], "sx": -1000,"sy": 120,"x": 296 ,"y": 488,"size": 16,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },
           {"text": [
                    {"content":"Enter your disagreement on the box provided above and click 'Save and Proceed'"}
                     ], "sx": -1000,"sy": 120,"x": 300 ,"y": 700,"size": 16,"weight":"bold","color":"#4c7dbf","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0.5,0] },
            {"text": [{"content":'Save & Proceed'}], "sx": -1000,"sy": 120,"x": 300 ,"y": 764,"size": 18,"weight":"bold","color":"#FFFFFF","tween_type": "Elastic.easeOut","timing": 200,"delay":4.5, "anchor":[0.5,0] },
        ],
         "input_animations": [
          {"text": [{"content":""}], "key":"in_disagreement", "sx": -1000,"sy": 120,"x": 20 ,"y": 670,"size": 20,"weight":"bold","width":558,"tween_type": "Elastic.easeOut","timing": 200,"delay":1,"backgroundColor":"#e9e9e9","placeHolder":"Enter your disagreement ...", "anchor":[0,0] }
        ],
        "sound_list": [
            {
                "sound": ["au_4_1"]
            }
        ],
        "functions": [

            {"fn": "SetBGTile('bg_02')", "delay": 0 },

        ],
        "name": "Plan Details",
        "timing": -1,
        "index": 17
    },




    {

          "sprite_animations": [
             {"sprite": "bar_01","x": 0,"y": 0,"loop": false,"timing": 0,"delay": 0,"toTopObj":1, "anchor":[0,0]},
            {"sprite": "logo_01","x": 10,"y": 33,"loop": false,"timing": 0,"delay": 0.1,"toTopObj":2, "anchor":[0,0]},
            {"sprite": "bar_03","x": 300,"y": 180,"loop": false,"timing": 0,"delay": 0, "toTopObj":1, "anchor":[0.5,0]},
            {"sprite": "box_18","x": 300,"y": 223,"loop": false,"timing": 0,"delay": 0, "toTopObj":1, "anchor":[0.5,0]},
           {"sprite": "loan_icon","x": 300,"y": 240,"loop": false,"timing": 0,"delay": 0.5, "toTopObj":1, "anchor":[0.5,0]},


            {"sprite": "btn_03_2","x": 150,"y": 674,"loop": false,"timing": 0,"delay": 2, "toTopObj":1, "anchor":[0.5,0],"onClickFn":"goToPage(8)"},

            {"sprite": "btn_03_3","x": 450,"y": 674,"loop": false,"timing": 0,"delay": 2, "toTopObj":1, "anchor":[0.5,0],"onClickFn":"goToPageBack(8)"},

            {"sprite": "btn_sp_1","x": 540,"y": 112,"loop": false,"timing": 0,"delay": 2, "toTopObj":1, "anchor":[0,0], "onClickFn":"audioReplay()"}
        ],
          "text_animations": [
           {"text": [{"content":"Pre Issuance Verification Call "}], "sx": -1000,"sy": 50,"x": 190 ,"y": 50,"size": 28,"weight":"bold","color":"#003869","tween_type": "Elastic.easeOut","timing": 200,"delay":0.2, "anchor":[0,0] },
            {"text": [
                    {"content":"పప్రతిపాదన సంఖ్య: "},
                    {"content":"$var.window.p_PROPOSAL_NUMBER"}
                ], "sx": -1000,"sy": 120,"x": 40 ,"y": 150,"size": 22,"weight":"bold","color":"#235291","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },
             {"text": [
                    {"content":"కేసులో మీరు మళ్ళీ వినాలనుకుంటే, ఇక్కడ క్లిక్ చేయండి ---->"}
                              ], "sx": -1000,"sy": 120,"x": 285 ,"y": 120,"size": 12,"weight":"bold","color":"#235291","tween_type": "Elastic.easeOut","timing": 200,"delay":2, "anchor":[0,0],"wordWrap": true, "wordWrapWidth":520,"align":"right" },
            {"text": [{"content":"LOAN QUESTIONS"}], "sx": -1000,"sy": 120,"x": 300 ,"y": 184,"size": 26,"weight":"bold","color":"#FFFFFF","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0.5,0] },

            {"text": [{"content":"We believe that all the policy features have been explained to you correctly. The amount paid by you is only towards the premium of the Policy and you have not been promised with any kind of bonus, loan, Mobile tower installation or refund against any other policy. Please do not believe in any such false promises and highlight any such concern to us immediately. So can we confirm that no such promise has been made to you."}], "sx": -1000,"sy": 120,"x": 50 ,"y": 360,"size": 22,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0],"wordWrap": true, "wordWrapWidth":520,"align":"center" },

            {"text": [{"content":'Yes'}], "sx": -1000,"sy": 120,"x": 150 ,"y": 688,"size": 26,"weight":"bold","color":"#FFFFFF","tween_type": "Elastic.easeOut","timing": 200,"delay":2, "anchor":[0.5,0] },
            {"text": [{"content":'No'}], "sx": -1000,"sy": 120,"x": 450 ,"y": 688,"size": 26,"weight":"bold","color":"#FFFFFF","tween_type": "Elastic.easeOut","timing": 200,"delay":2, "anchor":[0.5,0] },
        ],
        "functions": [

            {"fn": "SetBGTile('bg_02')", "delay": 0 }

        ],
         "sound_list": [
            {
                "sound": ["loan_question_audio"]
            }
        ],
        "name": "Loan Details - Confirm",
        "timing": -1,
        "index": 7
      },










{

"sprite_animations": [
           {"sprite": "bar_01","x": 0,"y": 0,"loop": false,"timing": 0,"delay": 0,"toTopObj":1, "anchor":[0,0]},
            {"sprite": "logo_01","x": 10,"y": 33,"loop": false,"timing": 0,"delay": 0.1,"toTopObj":2, "anchor":[0,0]},
            {"sprite": "bar_03","x": 300,"y": 180,"loop": false,"timing": 0,"delay": 0, "toTopObj":1, "anchor":[0.5,0]},
            {"sprite": "box_06","x": 300,"y": 223,"loop": false,"timing": 0,"delay": 0, "toTopObj":1, "anchor":[0.5,0]},
            {"sprite": "btn_03_3","x": 300,"y": 748,"loop": false,"timing": 0,"delay": 4.5, "toTopObj":1, "anchor":[0.5,0],"onClickFn":"editGoToPage(13,240,197,'ePerDet','cPerDet')"},
            {"sprite": "btn_sp_1","x": 540,"y": 112,"loop": false,"timing": 0,"delay": 2, "toTopObj":1, "anchor":[0,0], "onClickFn":"audioReplay()"}
        ],

           "text_animations": [
           {"text": [{"content":"Pre Issuance Verification Call "}], "sx": -1000,"sy": 50,"x": 190 ,"y": 50,"size": 28,"weight":"bold","color":"#003869","tween_type": "Elastic.easeOut","timing": 200,"delay":0.2, "anchor":[0,0] },
            {"text": [
                    {"content":"పప్రతిపాదన సంఖ్య: "},
                    {"content":"$var.window.p_PROPOSAL_NUMBER"}
                ], "sx": -1000,"sy": 120,"x": 40 ,"y": 150,"size": 22,"weight":"bold","color":"#235291","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },
             {"text": [
                    {"content":"కేసులో మీరు మళ్ళీ వినాలనుకుంటే, ఇక్కడ క్లిక్ చేయండి ---->"}
                              ], "sx": -1000,"sy": 120,"x": 285 ,"y": 120,"size": 12,"weight":"bold","color":"#235291","tween_type": "Elastic.easeOut","timing": 200,"delay":2, "anchor":[0,0],"wordWrap": true, "wordWrapWidth":520,"align":"right" },
            {"text": [{"content":"ILLNESS DETAILS"}], "sx": -1000,"sy": 120,"x": 300 ,"y": 186,"size": 26,"weight":"bold","color":"#FFFFFF","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0.5,0] },

            {"text": [{"content":"Name of the illness"}], "sx": -1000,"sy": 120,"x": 74 ,"y": 292,"size": 18,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },
            {"text": [{"content":":"}], "sx": -1000,"sy": 120,"x": 296 ,"y": 292,"size": 18,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },

            {"text": [{"content":"Period of diagnose"}], "sx": -1000,"sy": 120,"x": 74 ,"y": 362,"size": 18,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },
            {"text": [{"content":":"}], "sx": -1000,"sy": 120,"x": 296 ,"y": 362,"size": 18,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },


            {"text": [{"content":"Medicine consumption details"}], "sx": -1000,"sy": 120,"x": 74 ,"y": 432,"size": 18,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },
            {"text": [{"content":":"}], "sx": -1000,"sy": 120,"x": 296 ,"y": 432,"size": 18,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },


            {"text": [
                    {"content":"Capture the name of the illness, period of diagnose and medicine consumption details and click 'Save & Proceed'."}
                     ], "sx": -1000,"sy": 120,"x": 300 ,"y": 660,"size": 18,"weight":"bold","color":"#235291","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0.5,0],"wordWrap":true,"wordWrapWidth":450,"align":"left" },
            {"text": [{"content":'Save & Proceed'}], "sx": -1000,"sy": 120,"x": 300 ,"y": 764,"size": 18,"weight":"bold","color":"#FFFFFF","tween_type": "Elastic.easeOut","timing": 200,"delay":4.5, "anchor":[0.5,0] },
        ],
        "input_animations": [
            {"text": [{"content":" "}], "placeHolder":" ", "key":"in_name", "sx": -1000,"sy": 120,"x": 310 ,"y": 292,"size": 18,"weight":"bold","width":255,"backgroundColor":"#e9e579","fill":"#2961ab","tween_type": "Elastic.easeOut","timing": 200,"delay":1, "anchor":[0,0] },
            {"text": [{"content":" "}], "placeHolder":" ", "key":"in_dob", "sx": -1000,"sy": 120,"x": 310 ,"y": 362,"size": 18,"weight":"bold","width":255,"backgroundColor":"#e9e579","fill":"#2961ab","tween_type": "Elastic.easeOut","timing": 200,"delay":1, "anchor":[0,0] },
            {"text": [{"content":" "}], "placeHolder":" ", "key":"in_gender", "sx": -1000,"sy": 120,"x": 310 ,"y": 432,"size": 18,"weight":"bold","width":255,"backgroundColor":"#e9e579","fill":"#2961ab","tween_type": "Elastic.easeOut","timing": 200,"delay":1, "anchor":[0,0] },

        ],
        "sound_list": [
            {
                "sound": ["ab_6"]
            }
        ],
        "functions": [

            {"fn": "SetBGTile('bg_02')", "delay": 0 }

        ],
        "name": "Illness Details - Disagree",
        "timing": -1,
        "index":18

    },

        {
          "sprite_animations": [
             {"sprite": "bar_01","x": 0,"y": 0,"loop": false,"timing": 0,"delay": 0,"toTopObj":1, "anchor":[0,0]},
            {"sprite": "logo_01","x": 10,"y": 33,"loop": false,"timing": 0,"delay": 0.1,"toTopObj":2, "anchor":[0,0]},
            {"sprite": "bar_03","x": 300,"y": 180,"loop": false,"timing": 0,"delay": 0, "toTopObj":1, "anchor":[0.5,0]},
            {"sprite": "box_20","x": 300,"y": 223,"loop": false,"timing": 0,"delay": 0, "toTopObj":1, "anchor":[0.5,0]},

            {"sprite": "btn_03_2","x": 150,"y": 544,"loop": false,"timing": 0,"delay": 2, "toTopObj":1, "anchor":[0.5,0],"onClickFn":"goToPage(8)"},

            {"sprite": "btn_03_3","x": 450,"y": 544,"loop": false,"timing": 0,"delay": 2, "toTopObj":1, "anchor":[0.5,0],"onClickFn":"goToPageBack(8)"},

            {"sprite": "btn_sp_1","x": 540,"y": 112,"loop": false,"timing": 0,"delay": 2, "toTopObj":1, "anchor":[0,0], "onClickFn":"audioReplay()"}
        ],
          "text_animations": [
           {"text": [{"content":"Pre Issuance Verification Call "}], "sx": -1000,"sy": 50,"x": 190 ,"y": 50,"size": 28,"weight":"bold","color":"#003869","tween_type": "Elastic.easeOut","timing": 200,"delay":0.2, "anchor":[0,0] },
            {"text": [
                    {"content":"పప్రతిపాదన సంఖ్య: "},
                    {"content":"$var.window.p_PROPOSAL_NUMBER"}
                ], "sx": -1000,"sy": 120,"x": 40 ,"y": 150,"size": 22,"weight":"bold","color":"#235291","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },
             {"text": [
                    {"content":"కేసులో మీరు మళ్ళీ వినాలనుకుంటే, ఇక్కడ క్లిక్ చేయండి ---->"}
                              ], "sx": -1000,"sy": 120,"x": 285 ,"y": 120,"size": 12,"weight":"bold","color":"#235291","tween_type": "Elastic.easeOut","timing": 200,"delay":2, "anchor":[0,0],"wordWrap": true, "wordWrapWidth":520,"align":"right" },
            {"text": [{"content":"LOAN QUESTIONS"}], "sx": -1000,"sy": 120,"x": 300 ,"y": 186,"size": 26,"weight":"bold","color":"#FFFFFF","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0.5,0] },

            {"text": [{"content":"We believe that all the policy features have been explained to you correctly. The amount paid by you is only towards the premium of the Policy and you have not been promised with any kind of bonus, loan, Mobile tower installation or refund against any other policy. Please do not believe in any such false promises and highlight any such concern to us immediately. So can we confirm that no such promise has been made to you."}], "sx": -1000,"sy": 120,"x": 50 ,"y": 241,"size": 22,"weight":"bold","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0],"wordWrap": true, "wordWrapWidth":520,"align":"center" },

            {"text": [{"content":'YES'}], "sx": -1000,"sy": 120,"x": 150 ,"y": 558,"size": 26,"weight":"bold","color":"#FFFFFF","tween_type": "Elastic.easeOut","timing": 200,"delay":2, "anchor":[0.5,0] },
            {"text": [{"content":'NO'}], "sx": -1000,"sy": 120,"x": 450 ,"y": 558,"size": 26,"weight":"bold","color":"#FFFFFF","tween_type": "Elastic.easeOut","timing": 200,"delay":2, "anchor":[0.5,0] },
        ],
        "functions": [

            {"fn": "SetBGTile('bg_02')", "delay": 0 }

        ],
         "sound_list": [
            {
                "sound": ["ab_5"]
            }
        ],
        "name": "Loan Details - Confirm",
        "timing": -1,
        "index": 19
      },



  {
         "sprite_animations": [
            {"sprite": "bar_01","x": 0,"y": 0,"loop": false,"timing": 0,"delay": 0,"toTopObj":1, "anchor":[0,0]},
            {"sprite": "logo_01","x": 10,"y": 33,"loop": false,"timing": 0,"delay": 0.1,"toTopObj":2, "anchor":[0,0]},
            {"sprite": "bar_03","x": 300,"y": 180,"loop": false,"timing": 0,"delay": 0, "toTopObj":1, "anchor":[0.5,0]},
            {"sprite": "box_16","x": 300,"y": 223,"loop": false,"timing": 0,"delay": 0, "toTopObj":1, "anchor":[0.5,0]},
             {"sprite": "medicon","x": 300,"y": 250,"loop": false,"timing": 0,"delay": 0, "toTopObj":1, "anchor":[0.5,0]},

            {"sprite": "btn_03_2","x": 150,"y": 504,"loop": false,"timing": 0,"delay": 2, "toTopObj":1, "anchor":[0.5,0],"onClickFn":"goToPage(13)"},

            {"sprite": "btn_03_3","x": 450,"y": 504,"loop": false,"timing": 0,"delay": 2, "toTopObj":1, "anchor":[0.5,0],"onClickFn":"goToPageBack(12)"},

            {"sprite": "btn_sp_1","x": 540,"y": 112,"loop": false,"timing": 0,"delay": 2, "toTopObj":1, "anchor":[0,0], "onClickFn":"audioReplay()"}

        ],
          "text_animations": [
           {"text": [{"content":"Pre Issuance Verification Call "}], "sx": -1000,"sy": 50,"x": 190 ,"y": 50,"size": 28,"weight":"bold","color":"#003869","tween_type": "Elastic.easeOut","timing": 200,"delay":0.2, "anchor":[0,0] },
            {"text": [
                    {"content":"పప్రతిపాదన సంఖ్య: "},
                    {"content":"$var.window.p_PROPOSAL_NUMBER"}
                ], "sx": -1000,"sy": 120,"x": 40 ,"y": 150,"size": 22,"weight":"bold","color":"#235291","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },
             {"text": [
                    {"content":"కేసులో మీరు మళ్ళీ వినాలనుకుంటే, ఇక్కడ క్లిక్ చేయండి ---->"}
                              ], "sx": -1000,"sy": 120,"x": 285 ,"y": 120,"size": 12,"weight":"bold","color":"#235291","tween_type": "Elastic.easeOut","timing": 200,"delay":2, "anchor":[0,0],"wordWrap": true, "wordWrapWidth":520,"align":"right" },
            {"text": [{"content":"MEDICAL QUESTIONS"}], "sx": -1000,"sy": 120,"x": 300 ,"y": 186,"size": 26,"weight":"bold","color":"#FFFFFF","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0.5,0] },

            {"text": [{"content":"There were a set of medical questions in the application form, which you have answered as “No” which means that you are not under any medication and you are in good health."}], "sx": -1000,"sy": 120,"x": 50 ,"y": 340,"size": 22,"weight":"bold","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0],"wordWrap": true, "wordWrapWidth":520,"align":"center" },

            {"text": [{"content":'YES'}], "sx": -1000,"sy": 120,"x": 150 ,"y": 518,"size": 26,"weight":"bold","color":"#FFFFFF","tween_type": "Elastic.easeOut","timing": 200,"delay":2, "anchor":[0.5,0] },
            {"text": [{"content":'NO'}], "sx": -1000,"sy": 120,"x": 450 ,"y": 518,"size": 26,"weight":"bold","color":"#FFFFFF","tween_type": "Elastic.easeOut","timing": 200,"delay":2, "anchor":[0.5,0] },
        ],
        "functions": [

            {"fn": "SetBGTile('bg_02')", "delay": 0 }

        ],
         "sound_list": [
            {
                "sound": ["ab_5"]
            }
        ],
        "name": "Medical Questions - Confirm",
        "timing": -1,
        "index": 20
    },

 {

          "sprite_animations": [
           {"sprite": "bar_01","x": 0,"y": 0,"loop": false,"timing": 0,"delay": 0,"toTopObj":1, "anchor":[0,0]},
            {"sprite": "logo_01","x": 10,"y": 33,"loop": false,"timing": 0,"delay": 0.1,"toTopObj":2, "anchor":[0,0]},
            {"sprite": "bar_03","x": 300,"y": 180,"loop": false,"timing": 0,"delay": 0, "toTopObj":1, "anchor":[0.5,0]},
            {"sprite": "box_06","x": 300,"y": 223,"loop": false,"timing": 0,"delay": 0, "toTopObj":1, "anchor":[0.5,0]},
            {"sprite": "illness_icon","x": 300,"y": 240,"loop": false,"timing": 0,"delay": 0, "toTopObj":1, "anchor":[0.5,0]},
            {"sprite": "btn_03_3","x": 300,"y": 748,"loop": false,"timing": 0,"delay": 4.5, "toTopObj":1, "anchor":[0.5,0],"onClickFn":"editGoToPage(13,240,197,'ePerDet','cPerDet')"},
            {"sprite": "btn_sp_1","x": 540,"y": 112,"loop": false,"timing": 0,"delay": 2, "toTopObj":1, "anchor":[0,0], "onClickFn":"audioReplay()"}
        ],

           "text_animations": [
           {"text": [{"content":"Pre Issuance Verification Call "}], "sx": -1000,"sy": 50,"x": 190 ,"y": 50,"size": 28,"weight":"bold","color":"#003869","tween_type": "Elastic.easeOut","timing": 200,"delay":0.2, "anchor":[0,0] },
            {"text": [
                    {"content":"పప్రతిపాదన సంఖ్య: "},
                    {"content":"$var.window.p_PROPOSAL_NUMBER"}
                ], "sx": -1000,"sy": 120,"x": 40 ,"y": 150,"size": 22,"weight":"bold","color":"#235291","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },
             {"text": [
                    {"content":"కేసులో మీరు మళ్ళీ వినాలనుకుంటే, ఇక్కడ క్లిక్ చేయండి ---->"}
                              ], "sx": -1000,"sy": 120,"x": 285 ,"y": 120,"size": 12,"weight":"bold","color":"#235291","tween_type": "Elastic.easeOut","timing": 200,"delay":2, "anchor":[0,0],"wordWrap": true, "wordWrapWidth":520,"align":"right" },
            {"text": [{"content":"ILLNESS DETAILS"}], "sx": -1000,"sy": 120,"x": 300 ,"y": 186,"size": 26,"weight":"bold","color":"#FFFFFF","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0.5,0] },

            {"text": [{"content":"Name of the illness"}], "sx": -1000,"sy": 120,"x": 74 ,"y": 362,"size": 18,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },
            {"text": [{"content":":"}], "sx": -1000,"sy": 120,"x": 296 ,"y": 362,"size": 18,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },

            {"text": [{"content":"Period of diagnose"}], "sx": -1000,"sy": 120,"x": 74 ,"y": 432,"size": 18,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },
            {"text": [{"content":":"}], "sx": -1000,"sy": 120,"x": 296 ,"y": 432,"size": 18,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },


            {"text": [{"content":"Medicine consumption details"}], "sx": -1000,"sy": 120,"x": 74 ,"y": 502,"size": 18,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },
            {"text": [{"content":":"}], "sx": -1000,"sy": 120,"x": 296 ,"y": 502,"size": 18,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },


            {"text": [
                    {"content":"Capture the name of the illness, period of diagnose and medicine consumption details and click 'Save & Proceed'."}
                     ], "sx": -1000,"sy": 120,"x": 300 ,"y": 660,"size": 18,"weight":"bold","color":"#235291","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0.5,0],"wordWrap":true,"wordWrapWidth":450,"align":"left" },
            {"text": [{"content":'Save & Proceed'}], "sx": -1000,"sy": 120,"x": 300 ,"y": 764,"size": 18,"weight":"bold","color":"#FFFFFF","tween_type": "Elastic.easeOut","timing": 200,"delay":4.5, "anchor":[0.5,0] },
        ],
        "input_animations": [
            {"text": [{"content":" "}], "placeHolder":" ", "key":"in_name", "sx": -1000,"sy": 120,"x": 310 ,"y": 362,"size": 18,"weight":"bold","width":255,"backgroundColor":"#e9e579","fill":"#2961ab","tween_type": "Elastic.easeOut","timing": 200,"delay":1, "anchor":[0,0] },
            {"text": [{"content":" "}], "placeHolder":" ", "key":"in_dob", "sx": -1000,"sy": 120,"x": 310 ,"y": 432,"size": 18,"weight":"bold","width":255,"backgroundColor":"#e9e579","fill":"#2961ab","tween_type": "Elastic.easeOut","timing": 200,"delay":1, "anchor":[0,0] },
            {"text": [{"content":" "}], "placeHolder":" ", "key":"in_gender", "sx": -1000,"sy": 120,"x": 310 ,"y": 502,"size": 18,"weight":"bold","width":255,"backgroundColor":"#e9e579","fill":"#2961ab","tween_type": "Elastic.easeOut","timing": 200,"delay":1, "anchor":[0,0] },

        ],
        "sound_list": [
            {
                "sound": ["ab_6"]
            }
        ],
        "functions": [

            {"fn": "SetBGTile('bg_02')", "delay": 0 }

        ],
        "name": "Illness Details - Disagree",
        "timing": -1,
        "index":21

    },


//maha jeevan

 {
        "sprite_animations": [
            {"sprite": "bar_01","x": 0,"y": 0,"loop": false,"timing": 0,"delay": 0,"toTopObj":1, "anchor":[0,0]},
            {"sprite": "logo_01","x": 10,"y": 33,"loop": false,"timing": 0,"delay": 0.1,"toTopObj":2, "anchor":[0,0]},
            {"sprite": "bar_03","x": 300,"y": 180,"loop": false,"timing": 0,"delay": 0, "toTopObj":1, "anchor":[0.5,0]},
            {"sprite": "box_02","x": 300,"y": 230,"loop": false,"timing": 0,"delay": 0, "toTopObj":1, "anchor":[0.5,0]},
            {"sprite": "box_02","x": 300,"y": 450,"loop": false,"timing": 0,"delay": 0, "toTopObj":1, "anchor":[0.5,0]},
            {"sprite": "maturity_icon","x": 300,"y": 280,"loop": false,"timing": 0,"delay": 0, "toTopObj":1, "anchor":[0.5,0]},
            {"sprite": "dea_icon","x": 300,"y": 500,"loop": false,"timing": 0,"delay": 0, "toTopObj":1, "anchor":[0.5,0]},

             {"sprite": "btn_03_3","x": 300,"y": 748,"loop": false,"timing": 0,"delay": 4.5, "toTopObj":1, "anchor":[0.5,0],"onClickFn":"goToPage(11)"},
            {"sprite": "btn_sp_1","x": 540,"y": 112,"loop": false,"timing": 0,"delay": 2, "toTopObj":1, "anchor":[0,0], "onClickFn":"audioReplay()"}
        ],

           "text_animations": [
           {"text": [{"content":"Pre Issuance Verification Call "}], "sx": -1000,"sy": 50,"x": 190 ,"y": 50,"size": 28,"weight":"bold","color":"#003869","tween_type": "Elastic.easeOut","timing": 200,"delay":0.2, "anchor":[0,0] },
            {"text": [
                    {"content":"పప్రతిపాదన సంఖ్య: "},
                    {"content":"$var.window.p_PROPOSAL_NUMBER"}
                ], "sx": -1000,"sy": 120,"x": 40 ,"y": 150,"size": 22,"weight":"bold","color":"#235291","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },
             {"text": [
                    {"content":"కేసులో మీరు మళ్ళీ వినాలనుకుంటే, ఇక్కడ క్లిక్ చేయండి ---->"}
                              ], "sx": -1000,"sy": 120,"x": 285 ,"y": 120,"size": 12,"weight":"bold","color":"#235291","tween_type": "Elastic.easeOut","timing": 200,"delay":2, "anchor":[0,0],"wordWrap": true, "wordWrapWidth":520,"align":"right" },
            {"text": [{"content":"PRODUCT BENEFITS"}], "sx": -1000,"sy": 120,"x": 300 ,"y": 186,"size": 26,"weight":"bold","color":"#FFFFFF","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0.5,0] },

              {"text": [{"content":"Maturity Benefit"}], "sx": -1000,"sy": 120,"x": 300 ,"y": 240,"size": 18,"weight":"bold","color":"","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0.5,0] },
              {"text": [{"content":"In case of maturity of your policy the basic sum assured + the simple reversionary bonus + terminal Bonus (If any ) will be paid."}], "sx": -1000,"sy": 120,"x": 100 ,"y": 340,"size": 18,"weight":"normal","color":"#003869","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5,"wordWrap": true, "wordWrapWidth":420,"align":"center", "anchor":[0,0] },

              {"text": [{"content":"Death Benefit"}], "sx": -1000,"sy": 120,"x": 300 ,"y": 460,"size": 18,"weight":"bold","color":"","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0.5,0] },

              {"text": [{"content":"In case of death of the life assured nominee shall receive the higher of sum assured or 10 times of annual premium + accrued bonus till death + term rider sum assured, if opted."}], "sx": -1000,"sy": 120,"x": 100 ,"y": 560,"size": 18,"weight":"normal","color":"#003869","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5,"wordWrap": true, "wordWrapWidth":420,"align":"center", "anchor":[0,0] },


              // {"text": [{"content":"Surrender"}], "sx": -1000,"sy": 120,"x": 300 ,"y": 558,"size": 16,"weight":"bold","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0.5,0] },
              // {"text": [
              //         {"content":""}
              //     ], "sx": -1000,"sy": 120,"x": 170 ,"y": 570,"size": 16,"lineSpacing":-5,"weight":"normal","color":"#235291","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5,"wordWrap": true, "wordWrapWidth":374,"align":"left", "anchor":[0,0] },

            {"text": [{"content":"Hope you have fully understood all the features of this policy."}], "sx": -1000,"sy": 120,"x": 300 ,"y": 710,"size": 18,"weight":"normal","color":"#235291","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0.5,0] },

           {"text": [{"content":"Proceed"}], "sx": -1000,"sy": 750,"x": 300 ,"y": 760,"size": 22,"weight":"normal","color":"#FFFFFF","tween_type": "Elastic.easeOut","timing": 200,"delay":3, "anchor":[0.5,0] }



        ],
        "sound_list": [
            {
                "sound": ["ab_6"]
            }
        ],
        "functions": [
            {"fn": "SetBGTile('bg_02')", "delay": 0 }

        ],
        "name": "Illness Details - Disagree",
        "timing": -1,
        "index":23
    },

//little champ

{
        "sprite_animations": [
            {"sprite": "bar_01","x": 0,"y": 0,"loop": false,"timing": 0,"delay": 0,"toTopObj":1, "anchor":[0,0]},
            {"sprite": "logo_01","x": 10,"y": 33,"loop": false,"timing": 0,"delay": 0.1,"toTopObj":2, "anchor":[0,0]},
            {"sprite": "bar_03","x": 300,"y": 180,"loop": false,"timing": 0,"delay": 0, "toTopObj":1, "anchor":[0.5,0]},
            {"sprite": "box_02","x": 300,"y": 230,"loop": false,"timing": 0,"delay": 0, "toTopObj":1, "anchor":[0.5,0]},
            {"sprite": "box_16","x": 300,"y": 450,"loop": false,"timing": 0,"delay": 0, "toTopObj":1, "anchor":[0.5,0]},
            {"sprite": "maturity_icon","x": 300,"y": 280,"loop": false,"timing": 0,"delay": 0, "toTopObj":1, "anchor":[0.5,0]},
            {"sprite": "dea_icon","x": 300,"y": 500,"loop": false,"timing": 0,"delay": 0, "toTopObj":1, "anchor":[0.5,0]},

             {"sprite": "btn_03_3","x": 300,"y": 748,"loop": false,"timing": 0,"delay": 4.5, "toTopObj":1, "anchor":[0.5,0],"onClickFn":"goToPage(11)"},
            {"sprite": "btn_sp_1","x": 540,"y": 112,"loop": false,"timing": 0,"delay": 2, "toTopObj":1, "anchor":[0,0], "onClickFn":"audioReplay()"}
        ],

           "text_animations": [
           {"text": [{"content":"Pre Issuance Verification Call "}], "sx": -1000,"sy": 50,"x": 190 ,"y": 50,"size": 28,"weight":"bold","color":"#003869","tween_type": "Elastic.easeOut","timing": 200,"delay":0.2, "anchor":[0,0] },
            {"text": [
                    {"content":"పప్రతిపాదన సంఖ్య: "},
                    {"content":"$var.window.p_PROPOSAL_NUMBER"}
                ], "sx": -1000,"sy": 120,"x": 40 ,"y": 150,"size": 22,"weight":"bold","color":"#235291","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },
             {"text": [
                    {"content":"కేసులో మీరు మళ్ళీ వినాలనుకుంటే, ఇక్కడ క్లిక్ చేయండి ---->"}
                              ], "sx": -1000,"sy": 120,"x": 285 ,"y": 120,"size": 12,"weight":"bold","color":"#235291","tween_type": "Elastic.easeOut","timing": 200,"delay":2, "anchor":[0,0],"wordWrap": true, "wordWrapWidth":520,"align":"right" },
            {"text": [{"content":"PRODUCT BENEFITS"}], "sx": -1000,"sy": 120,"x": 300 ,"y": 186,"size": 26,"weight":"bold","color":"#FFFFFF","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0.5,0] },

              {"text": [{"content":"Maturity Benefit"}], "sx": -1000,"sy": 120,"x": 300 ,"y": 240,"size": 18,"weight":"bold","color":"","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0.5,0] },

              {"text": [{"content":"You will receive guaranteed payout As per option chosen by you 101% to 125% Of your Sum Assured additionally you will get Simple Reversionary Bonus till date of maturity + may get terminal Bonus."}], "sx": -1000,"sy": 120,"x": 100 ,"y": 340,"size": 18,"weight":"bold","color":"#003869","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5,"wordWrap": true, "wordWrapWidth":420,"align":"center", "anchor":[0,0] },

              {"text": [{"content":"Death Benefit"}], "sx": -1000,"sy": 120,"x": 300 ,"y": 460,"size": 18,"weight":"bold","color":"","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0.5,0] },

              {"text": [{"content":"In case of death of the life assured nominee shall receive the higher of sum assured or 10 times Of Annual premium or 105% of total premium paid till death + All guaranteed payout & maturity benefit are paid as scheduled + Policy continue to accrue Bonuses."}], "sx": -1000,"sy": 120,"x": 100 ,"y": 560,"size": 18,"weight":"bold","color":"#003869","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5,"wordWrap": true, "wordWrapWidth":420,"align":"center", "anchor":[0,0] },

          //    {"text": [{"content":"Hence we would strongly recommend you to continue to pay the premium for the entire Paying term of <> years."}], "sx": -1000,"sy": 120,"x": 100 ,"y": 560,"size": 18,"weight":"normal","color":"#003869","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5,"wordWrap": true, "wordWrapWidth":420,"align":"center", "anchor":[0,0] },

              // {"text": [{"content":"Surrender"}], "sx": -1000,"sy": 120,"x": 300 ,"y": 558,"size": 16,"weight":"bold","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0.5,0] },
              // {"text": [
              //         {"content":""}
              //     ], "sx": -1000,"sy": 120,"x": 170 ,"y": 570,"size": 16,"lineSpacing":-5,"weight":"normal","color":"#235291","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5,"wordWrap": true, "wordWrapWidth":374,"align":"left", "anchor":[0,0] },

            {"text": [{"content":"Hope you have fully understood all the features of this policy."}], "sx": -1000,"sy": 120,"x": 300 ,"y": 710,"size": 18,"weight":"bold","color":"#235291","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0.5,0] },

           {"text": [{"content":"Proceed"}], "sx": -1000,"sy": 750,"x": 300 ,"y": 760,"size": 22,"weight":"normal","color":"#FFFFFF","tween_type": "Elastic.easeOut","timing": 200,"delay":3, "anchor":[0.5,0] }



        ],
        "sound_list": [
            {
                "sound": ["ab_6"]
            }
        ],
        "functions": [
            {"fn": "SetBGTile('bg_02')", "delay": 0 }

        ],
        "name": "Illness Details - Disagree",
        "timing": -1,
        "index":24
    },

// call back

     {
        "sprite_animations": [
            {"sprite": "bar_01","x": 0,"y": 0,"loop": false,"timing": 0,"delay": 0,"toTopObj":1, "anchor":[0,0]},
            {"sprite": "logo_01","x": 10,"y": 33,"loop": false,"timing": 0,"delay": 0.1,"toTopObj":2, "anchor":[0,0]},
            {"sprite": "bar_03","x": 300,"y": 180,"loop": false,"timing": 0,"delay": 0, "toTopObj":1, "anchor":[0.5,0]},
            {"sprite": "box_07","x": 300,"y": 223,"loop": false,"timing": 0,"delay": 0, "toTopObj":1, "anchor":[0.5,0]},
            {"sprite": "box_07","x": 300,"y": 386,"loop": false,"timing": 0,"delay": 0, "toTopObj":1, "anchor":[0.5,0]},
            {"sprite": "box_07","x": 300,"y": 550,"loop": false,"timing": 0,"delay": 0, "toTopObj":1, "anchor":[0.5,0]},

            {"sprite": "btn_03_3","x": 300,"y": 748,"loop": false,"timing": 0,"delay": 4.5, "toTopObj":1, "anchor":[0.5,0],"onClickFn":"goToPage(11)"},
            {"sprite": "btn_sp_1","x": 540,"y": 112,"loop": false,"timing": 0,"delay": 2, "toTopObj":1, "anchor":[0,0], "onClickFn":"audioReplay()"}
        ],

           "text_animations": [
           {"text": [{"content":"Pre Issuance Verification Call "}], "sx": -1000,"sy": 50,"x": 190 ,"y": 50,"size": 28,"weight":"bold","color":"#003869","tween_type": "Elastic.easeOut","timing": 200,"delay":0.2, "anchor":[0,0] },
            {"text": [
                    {"content":"పప్రతిపాదన సంఖ్య: "},
                    {"content":"$var.window.p_PROPOSAL_NUMBER"}
                ], "sx": -1000,"sy": 120,"x": 40 ,"y": 150,"size": 22,"weight":"bold","color":"#235291","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },
             {"text": [
                    {"content":"కేసులో మీరు మళ్ళీ వినాలనుకుంటే, ఇక్కడ క్లిక్ చేయండి ---->"}
                              ], "sx": -1000,"sy": 120,"x": 285 ,"y": 120,"size": 12,"weight":"bold","color":"#235291","tween_type": "Elastic.easeOut","timing": 200,"delay":2, "anchor":[0,0],"wordWrap": true, "wordWrapWidth":520,"align":"right" },
            {"text": [{"content":"PRODUCT BENEFITS"}], "sx": -1000,"sy": 120,"x": 300 ,"y": 186,"size": 26,"weight":"bold","color":"#FFFFFF","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0.5,0] },

              {"text": [{"content":"Survival Benefit"}], "sx": -1000,"sy": 120,"x": 300 ,"y": 230,"size": 16,"weight":"bold","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0.5,0] },
              {"text": [{"content":""}], "sx": -1000,"sy": 120,"x": 170 ,"y": 241,"size": 16,"weight":"normal","color":"#235291","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5,"wordWrap": true, "wordWrapWidth":374,"align":"left", "anchor":[0,0],"lineSpacing":-5 },

              {"text": [{"content":"Maturity Benefit"}], "sx": -1000,"sy": 120,"x": 300 ,"y": 400,"size": 16,"weight":"bold","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0.5,0] },
              {"text": [
                      {"content":""}
                  ], "sx": -1000,"sy": 120,"x": 170 ,"y": 240,"size": 16,"lineSpacing":-5,"weight":"normal","color":"#235291","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5,"wordWrap": true, "wordWrapWidth":374,"align":"left", "anchor":[0,0] },
               {"text": [
                       {"content":""}
                  ], "sx": -1000,"sy": 120,"x": 170 ,"y": 245,"size": 16,"lineSpacing":-5,"weight":"normal","color":"#235291","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5,"wordWrap": true, "wordWrapWidth":374,"align":"left", "anchor":[0,0] },


              {"text": [{"content":"Surrender"}], "sx": -1000,"sy": 120,"x": 300 ,"y": 558,"size": 16,"weight":"bold","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0.5,0] },
              {"text": [
                      {"content":""}
                  ], "sx": -1000,"sy": 120,"x": 170 ,"y": 570,"size": 16,"lineSpacing":-5,"weight":"normal","color":"#235291","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5,"wordWrap": true, "wordWrapWidth":374,"align":"left", "anchor":[0,0] },

            {"text": [{"content":"Hope you have fully understood all the features of this policy."}], "sx": -1000,"sy": 120,"x": 300 ,"y": 720,"size": 18,"weight":"bold","color":"#235291","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0.5,0] },

           {"text": [{"content":"Proceed"}], "sx": -1000,"sy": 750,"x": 300 ,"y": 760,"size": 22,"weight":"normal","color":"#FFFFFF","tween_type": "Elastic.easeOut","timing": 200,"delay":3, "anchor":[0.5,0] }



        ],
        "sound_list": [
            {
                "sound": ["benefits_audio"]
            }
        ],
        "functions": [
            {"fn": "SetBGTile('bg_02')", "delay": 0 }

        ],
        "name": "Illness Details - Disagree",
        "timing": -1,
        "index":13
    },




*/

 // {
 //       "sprite_animations": [
 //            {"sprite": "bar_01","x": 0,"y": 0,"loop": false,"timing": 0,"delay": 0,"toTopObj":1, "anchor":[0,0]},
 //            {"sprite": "logo_01","x": 10,"y": 33,"loop": false,"timing": 0,"delay": 0.1,"toTopObj":2, "anchor":[0,0]},
 //            {"sprite": "bar_03","x": 300,"y": 180,"loop": false,"timing": 0,"delay": 0, "toTopObj":1, "anchor":[0.5,0]},
 //            {"sprite": "box_22","x": 300,"y": 223,"loop": false,"timing": 0,"delay": 0, "toTopObj":1, "anchor":[0.5,0]},
 //             {"sprite": "box_21","x": 300,"y": 518,"loop": false,"timing": 0,"delay": 0, "toTopObj":1, "anchor":[0.5,0]},
 //             {"sprite": "btn_equ","x": 300,"y": 483,"loop": false,"timing": 0,"delay": 0, "toTopObj":1, "anchor":[0.5,0]},
 //            {"sprite": "btn_03_2","x": 150,"y": 740,"loop": false,"timing": 0,"delay": 4, "toTopObj":1, "anchor":[0.5,0],"onClickFn":"goToScreen(16,'cPerDet',1)"},
 //            {"sprite": "btn_03_3","x": 450,"y": 740,"loop": false,"timing": 0,"delay": 4, "toTopObj":1, "anchor":[0.5,0],"onClickFn":"goToPage(9)"},
 //            {"sprite": "btn_sp_1","x": 540,"y": 112,"loop": false,"timing": 0,"delay": 2, "toTopObj":1, "anchor":[0,0], "onClickFn":"audioReplay()"}
 //        ],
 //        "text_animations": [

 //           {"text": [{"content":"Pre Issuance Verification Call "}], "sx": -1000,"sy": 50,"x": 190 ,"y": 50,"size": 28,"weight":"bold","color":"#003869","tween_type": "Elastic.easeOut","timing": 200,"delay":0.2, "anchor":[0,0] },
 //            {"text": [
 //                    {"content":"పప్రతిపాదన సంఖ్య: "},
 //                    {"content":"$var.window.p_PROPOSAL_NUMBER"}
 //                ], "sx": -1000,"sy": 120,"x": 40 ,"y": 150,"size": 22,"weight":"bold","color":"#235291","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },
 //             {"text": [
 //                    {"content":"కేసులో మీరు మళ్ళీ వినాలనుకుంటే, ఇక్కడ క్లిక్ చేయండి ---->"}
 //                              ], "sx": -1000,"sy": 120,"x": 285 ,"y": 120,"size": 12,"weight":"bold","color":"#235291","tween_type": "Elastic.easeOut","timing": 200,"delay":2, "anchor":[0,0],"wordWrap": true, "wordWrapWidth":520,"align":"right" },
 //            {"text": [{"content":"PLAN DETAILS"}], "sx": -1000,"sy": 120,"x": 300 ,"y": 184,"size": 26,"weight":"bold","color":"#FFFFFF","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0.5,0] },
 //            {"text": [{"content":"Your plan details as per  our records are "}], "sx": -1000,"sy": 120,"x": 100 ,"y": 235,"size": 18,"weight":"bold","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },

 //            {"text": [{"content":"Plan Name"}], "sx": -1000,"sy": 120,"x": 104 ,"y": 272,"size": 16,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },
 //            {"text": [{"content":":"}], "sx": -1000,"sy": 120,"x": 296 ,"y": 272,"size": 16,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },
 //            {"text": [{"content":"$var.window.p_planname"}], "sx": -1000,"sy": 120,"x": 310 ,"y": 272,"size": 16,"weight":"bold","color":"#0098DE","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },


 //            {"text": [{"content":"Premium Amount"}], "sx": -1000,"sy": 120,"x": 104 ,"y": 308,"size": 16,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },
 //            {"text": [{"content":":"}], "sx": -1000,"sy": 120,"x": 296 ,"y": 308,"size": 16,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },
 //            {"text": [{"content":"$var.window.p_premiumamount"}], "sx": -1000,"sy": 120,"x": 310 ,"y": 308,"size": 16,"weight":"bold","color":"#0098DE","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },


 //            {"text": [{"content":"Payment Frequency"}], "sx": -1000,"sy": 120,"x": 104 ,"y": 344,"size": 16,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },
 //            {"text": [{"content":":"}], "sx": -1000,"sy": 120,"x": 296 ,"y": 344,"size": 16,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },
 //            {"text": [{"content":"$var.window.p_paymentFre"}], "sx": -1000,"sy": 120,"x": 310 ,"y": 344,"size": 16,"weight":"bold","color":"#0098DE","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },


 //            {"text": [{"content":"Premium Paying Term"}], "sx": -1000,"sy": 120,"x": 104 ,"y": 380,"size": 16,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },
 //            {"text": [{"content":":"}], "sx": -1000,"sy": 120,"x": 296 ,"y": 380,"size": 16,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },
 //            {"text": [{"content":"$var.window.p_payingTerm"}], "sx": -1000,"sy": 120,"x": 310 ,"y": 380,"size": 16,"weight":"bold","color":"#0098DE","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },


 //            {"text": [{"content":"Policy Term"}], "sx": -1000,"sy": 120,"x": 104 ,"y": 416,"size": 16,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },
 //            {"text": [{"content":":"}], "sx": -1000,"sy": 120,"x": 296 ,"y": 416,"size": 16,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },
 //            {"text": [{"content":"$var.window.p_policyTerm"}], "sx": -1000,"sy": 120,"x": 310 ,"y": 416,"size": 16,"weight":"bold","color":"#0098DE","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },

 //            {"text": [{"content":"Sum Assured"}], "sx": -1000,"sy": 120,"x": 104 ,"y": 452,"size": 16,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },
 //            {"text": [{"content":":"}], "sx": -1000,"sy": 120,"x": 296 ,"y": 452,"size": 16,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },
 //            {"text": [{"content":"$var.window.p_Assuredsum"}], "sx": -1000,"sy": 120,"x": 310 ,"y": 452,"size": 16,"weight":"bold","color":"#0098DE","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },

 //            {"text": [{"content":"Equity "}], "sx": -1000,"sy": 120,"x": 275 ,"y": 490,"size": 18,"weight":"bold","color":"#003869","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },
 //          //  {"text": [{"content":":"}], "sx": -1000,"sy": 120,"x": 296 ,"y": 488,"size": 16,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },
 //          //  {"text": [{"content":"$var.window.p_NOMINEE_NAME"}], "sx": -1000,"sy": 120,"x": 310 ,"y": 488,"size": 16,"weight":"bold","color":"#0098DE","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },

 //            {"text": [{"content":"Equity is a high risk investment option. As an investor, if you have a long term horizon, equity investments will work the best for you."}], "sx": -1000,"sy": 120,"x": 50 ,"y": 528,"size": 14,"weight":"bold","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0],"wordWrap":true,"wordWrapWidth":510,"align":"left","lineSpacing":-5 },

 //            {"text": [{"content":"There is an Auto Trigger Based Feature in your policy. In case the return is 10% or higher in Equity I fund, the amount equal to the appreciation will be shifted to the Debt I fund."}], "sx": -1000,"sy": 120,"x": 50 ,"y": 575,"size": 14,"weight":"bold","color":"#3b3a3a ","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0],"wordWrap":true,"wordWrapWidth":510,"align":"left","lineSpacing":-5},

 //            {"text": [{"content":"For e.g If the fund value at the time of taking the policy was Rs. 100/- & it increases by Rs. 10/- (i.e. 110% of Fund Value) then the increased amount of Rs. 10 /- will be shifted to Debt I fund."}], "sx": -1000,"sy": 120,"x": 50 ,"y": 635,"size": 14,"weight":"bold","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0],"wordWrap":true,"wordWrapWidth":510,"align":"left","lineSpacing":-5},


 //            {"text": [{"content":"If all information displayed on the screen is correct,"}], "sx": -1000,"sy": 120,"x": 300 ,"y": 696,"size": 18,"weight":"bold","color":"#235291","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0.5,0] },
 //            {"text": [{"content":'Please click on "Agree", else click "Disagree"'}], "sx": -1000,"sy": 120,"x": 300 ,"y": 718,"size": 18,"weight":"bold","color":"#235291","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0.5,0] },

 //            {"text": [{"content":'Agree'}], "sx": -1000,"sy": 120,"x": 150 ,"y": 754,"size": 24,"weight":"bold","color":"#FFFFFF","tween_type": "Elastic.easeOut","timing": 200,"delay":4, "anchor":[0.5,0] },
 //            {"text": [{"content":'Disagree'}], "sx": -1000,"sy": 120,"x": 450 ,"y": 754,"size": 24,"weight":"bold","color":"#FFFFFF","tween_type": "Elastic.easeOut","timing": 200,"delay":4, "anchor":[0.5,0] },
 //        ],
 //        "sound_list": [
 //            {
 //                "sound": ["plan_details_audio"]
 //            }
 //        ],
 //        "functions": [

 //            {"fn": "SetBGTile('bg_02')", "delay": 0 },

 //        ],
 //        "name": "Plan Details - Show",
 //        "timing": -1,
 //        "index": 8
 //    },




 // {
 //          "sprite_animations": [
 //             {"sprite": "bar_01","x": 0,"y": 0,"loop": false,"timing": 0,"delay": 0,"toTopObj":1, "anchor":[0,0]},
 //            {"sprite": "logo_01","x": 10,"y": 33,"loop": false,"timing": 0,"delay": 0.1,"toTopObj":2, "anchor":[0,0]},
 //            {"sprite": "bar_03","x": 300,"y": 180,"loop": false,"timing": 0,"delay": 0, "toTopObj":1, "anchor":[0.5,0]},
 //            {"sprite": "box_18","x": 300,"y": 223,"loop": false,"timing": 0,"delay": 0, "toTopObj":1, "anchor":[0.5,0]},
 //           {"sprite": "loan_icon","x": 300,"y": 240,"loop": false,"timing": 0,"delay": 0.5, "toTopObj":1, "anchor":[0.5,0]},
 //
 //
 //            {"sprite": "btn_03_2","x": 150,"y": 674,"loop": false,"timing": 0,"delay": 2, "toTopObj":1, "anchor":[0.5,0],"onClickFn":"goToPage(8)"},
 //
 //            {"sprite": "btn_03_3","x": 450,"y": 674,"loop": false,"timing": 0,"delay": 2, "toTopObj":1, "anchor":[0.5,0],"onClickFn":"goToPageBack(8)"},
 //
 //            {"sprite": "btn_sp_1","x": 540,"y": 112,"loop": false,"timing": 0,"delay": 2, "toTopObj":1, "anchor":[0,0], "onClickFn":"audioReplay()"}
 //        ],
 //          "text_animations": [
 //           {"text": [{"content":"Pre Issuance Verification Call "}], "sx": -1000,"sy": 50,"x": 190 ,"y": 50,"size": 28,"weight":"bold","color":"#003869","tween_type": "Elastic.easeOut","timing": 200,"delay":0.2, "anchor":[0,0] },
 //            {"text": [
 //                    {"content":"పప్రతిపాదన సంఖ్య: "},
 //                    {"content":"$var.window.p_PROPOSAL_NUMBER"}
 //                ], "sx": -1000,"sy": 120,"x": 40 ,"y": 150,"size": 22,"weight":"bold","color":"#235291","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },
 //             {"text": [
 //                    {"content":"కేసులో మీరు మళ్ళీ వినాలనుకుంటే, ఇక్కడ క్లిక్ చేయండి ---->"}
 //                              ], "sx": -1000,"sy": 120,"x": 285 ,"y": 120,"size": 12,"weight":"bold","color":"#235291","tween_type": "Elastic.easeOut","timing": 200,"delay":2, "anchor":[0,0],"wordWrap": true, "wordWrapWidth":520,"align":"right" },
 //            {"text": [{"content":"LOAN QUESTIONS"}], "sx": -1000,"sy": 120,"x": 300 ,"y": 184,"size": 26,"weight":"bold","color":"#FFFFFF","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0.5,0] },
 //
 //            {"text": [{"content":"We believe that all the policy features have been explained to you correctly. The amount paid by you is only towards the premium of the Policy and you have not been promised with any kind of bonus, loan, Mobile tower installation or refund against any other policy. Please do not believe in any such false promises and highlight any such concern to us immediately. So can we confirm that no such promise has been made to you."}], "sx": -1000,"sy": 120,"x": 50 ,"y": 360,"size": 20,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0],"wordWrap": true, "wordWrapWidth":520,"align":"center" },
 //
 //            {"text": [{"content":"Please note that IndiaFirst may not consider any such complaint in the matter on a later date."}], "sx": -1000,"sy": 120,"x": 50 ,"y": 570,"size": 20,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0],"wordWrap": true, "wordWrapWidth":520,"align":"center" },
 //
 //
 //            {"text": [{"content":'YES'}], "sx": -1000,"sy": 120,"x": 150 ,"y": 688,"size": 26,"weight":"bold","color":"#FFFFFF","tween_type": "Elastic.easeOut","timing": 200,"delay":2, "anchor":[0.5,0] },
 //            {"text": [{"content":'NO'}], "sx": -1000,"sy": 120,"x": 450 ,"y": 688,"size": 26,"weight":"bold","color":"#FFFFFF","tween_type": "Elastic.easeOut","timing": 200,"delay":2, "anchor":[0.5,0] },
 //        ],
 //        "functions": [
 //
 //            {"fn": "SetBGTile('bg_02')", "delay": 0 }
 //
 //        ],
 //         "sound_list": [
 //            {
 //                "sound": ["loan_question_audio"]
 //            }
 //        ],
 //        "name": "Loan Details - Confirm",
 //        "timing": -1,
 //        "index": 17
 //      },
 //
 //
 // // {
 // //          "sprite_animations": [
 // //             {"sprite": "bar_01","x": 0,"y": 0,"loop": false,"timing": 0,"delay": 0,"toTopObj":1, "anchor":[0,0]},
 // //            {"sprite": "logo_01","x": 10,"y": 33,"loop": false,"timing": 0,"delay": 0.1,"toTopObj":2, "anchor":[0,0]},
 // //            {"sprite": "bar_03","x": 300,"y": 180,"loop": false,"timing": 0,"delay": 0, "toTopObj":1, "anchor":[0.5,0]},
 // //            {"sprite": "box_18","x": 300,"y": 223,"loop": false,"timing": 0,"delay": 0, "toTopObj":1, "anchor":[0.5,0]},
 // //           {"sprite": "equity_icon","x": 300,"y": 240,"loop": false,"timing": 0,"delay": 0.5, "toTopObj":1, "anchor":[0.5,0]},
 //
 //
 // //            {"sprite": "btn_03_2","x": 300,"y": 674,"loop": false,"timing": 0,"delay": 2, "toTopObj":1, "anchor":[0.5,0],"onClickFn":"goToPage(8)"},
 //
 // //           // {"sprite": "btn_03_3","x": 450,"y": 674,"loop": false,"timing": 0,"delay": 2, "toTopObj":1, "anchor":[0.5,0],"onClickFn":"goToPageBack(8)"},
 //
 // //            {"sprite": "btn_sp_1","x": 540,"y": 112,"loop": false,"timing": 0,"delay": 2, "toTopObj":1, "anchor":[0,0], "onClickFn":"audioReplay()"}
 // //        ],
 // //          "text_animations": [
 // //           {"text": [{"content":"Pre Issuance Verification Call "}], "sx": -1000,"sy": 50,"x": 190 ,"y": 50,"size": 28,"weight":"bold","color":"#003869","tween_type": "Elastic.easeOut","timing": 200,"delay":0.2, "anchor":[0,0] },
 // //            {"text": [
 // //                    {"content":"పప్రతిపాదన సంఖ్య: "},
 // //                    {"content":"$var.window.p_PROPOSAL_NUMBER"}
 // //                ], "sx": -1000,"sy": 120,"x": 40 ,"y": 150,"size": 22,"weight":"bold","color":"#235291","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },
 // //             {"text": [
 // //                    {"content":"కేసులో మీరు మళ్ళీ వినాలనుకుంటే, ఇక్కడ క్లిక్ చేయండి ---->"}
 // //                              ], "sx": -1000,"sy": 120,"x": 285 ,"y": 120,"size": 12,"weight":"bold","color":"#235291","tween_type": "Elastic.easeOut","timing": 200,"delay":2, "anchor":[0,0],"wordWrap": true, "wordWrapWidth":520,"align":"right" },
 // //            {"text": [{"content":"EQUITY"}], "sx": -1000,"sy": 120,"x": 300 ,"y": 184,"size": 26,"weight":"bold","color":"#FFFFFF","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0.5,0] },
 //
 // //            {"text": [{"content":"Equity is a high risk investment option. As an investor, if you have a long term horizon, equity investments will work the best for you."}], "sx": -1000,"sy": 120,"x": 50 ,"y": 340,"size": 18,"weight":"bold","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0],"wordWrap":true,"wordWrapWidth":510,"align":"left","lineSpacing":-5 },
 //
 // //            {"text": [{"content":"There is an Auto Trigger Based Feature in your policy. In case the return is 10% or higher in Equity I fund, the amount equal to the appreciation will be shifted to the Debt I fund."}], "sx": -1000,"sy": 120,"x": 50 ,"y": 420,"size": 18,"weight":"bold","color":"#3b3a3a ","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0],"wordWrap":true,"wordWrapWidth":510,"align":"left","lineSpacing":-5},
 //
 // //            {"text": [{"content":"For e.g If the fund value at the time of taking the policy was Rs. 100/- & it increases by Rs. 10/- (i.e. 110% of Fund Value) then the increased amount of Rs. 10 /- will be shifted to Debt I fund."}], "sx": -1000,"sy": 120,"x": 50 ,"y": 520,"size": 18,"weight":"bold","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0],"wordWrap":true,"wordWrapWidth":510,"align":"left","lineSpacing":-5},
 //
 //
 // //           // {"text": [{"content":"We believe that all the policy features have been explained to you correctly. The amount paid by you is only towards the premium of the Policy and you have not been promised with any kind of bonus, loan, Mobile tower installation or refund against any other policy. Please do not believe in any such false promises and highlight any such concern to us immediately. So can we confirm that no such promise has been made to you."}], "sx": -1000,"sy": 120,"x": 50 ,"y": 360,"size": 22,"weight":"bold","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0],"wordWrap": true, "wordWrapWidth":520,"align":"center" },
 //
 // //            {"text": [{"content":'Proceed'}], "sx": -1000,"sy": 120,"x": 300 ,"y": 688,"size": 26,"weight":"bold","color":"#FFFFFF","tween_type": "Elastic.easeOut","timing": 200,"delay":2, "anchor":[0.5,0] },
 // //           // {"text": [{"content":'NO'}], "sx": -1000,"sy": 120,"x": 450 ,"y": 688,"size": 26,"weight":"bold","color":"#FFFFFF","tween_type": "Elastic.easeOut","timing": 200,"delay":2, "anchor":[0.5,0] },
 // //        ],
 // //        "functions": [
 //
 // //            {"fn": "SetBGTile('bg_02')", "delay": 0 }
 //
 // //        ],
 // //         "sound_list": [
 // //            {
 // //                "sound": ["loan_question_audio"]
 // //            }
 // //        ],
 // //        "name": "equity - Confirm",
 // //        "timing": -1,
 // //        "index": 18
 // //      },
 //
 // //       {
 // //         "sprite_animations": [
 // //            {"sprite": "bar_01","x": 0,"y": 0,"loop": false,"timing": 0,"delay": 0,"toTopObj":1, "anchor":[0,0]},
 // //            {"sprite": "logo_01","x": 10,"y": 33,"loop": false,"timing": 0,"delay": 0.1,"toTopObj":2, "anchor":[0,0]},
 // //            {"sprite": "bar_03","x": 300,"y": 180,"loop": false,"timing": 0,"delay": 0, "toTopObj":1, "anchor":[0.5,0]},
 // //            {"sprite": "box_20","x": 300,"y": 223,"loop": false,"timing": 0,"delay": 0, "toTopObj":1, "anchor":[0.5,0]},
 // //             {"sprite": "medicon","x": 300,"y": 250,"loop": false,"timing": 0,"delay": 0.5, "toTopObj":1, "anchor":[0.5,0]},
 //
 // //            {"sprite": "btn_03_2","x": 150,"y": 604,"loop": false,"timing": 0,"delay": 2, "toTopObj":1, "anchor":[0.5,0],"onClickFn":"goToPage(14)"},
 //
 // //            {"sprite": "btn_03_3","x": 450,"y": 604,"loop": false,"timing": 0,"delay": 2, "toTopObj":1, "anchor":[0.5,0],"onClickFn":"goToPageBack(12)"},
 //
 // //            {"sprite": "btn_sp_1","x": 540,"y": 112,"loop": false,"timing": 0,"delay": 2, "toTopObj":1, "anchor":[0,0], "onClickFn":"audioReplay()"}
 //
 // //        ],
 // //          "text_animations": [
 // //           {"text": [{"content":"Pre Issuance Verification Call "}], "sx": -1000,"sy": 50,"x": 190 ,"y": 50,"size": 28,"weight":"bold","color":"#003869","tween_type": "Elastic.easeOut","timing": 200,"delay":0.2, "anchor":[0,0] },
 // //            {"text": [
 // //                    {"content":"పప్రతిపాదన సంఖ్య: "},
 // //                    {"content":"$var.window.p_PROPOSAL_NUMBER"}
 // //                ], "sx": -1000,"sy": 120,"x": 40 ,"y": 150,"size": 22,"weight":"bold","color":"#235291","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },
 // //             {"text": [
 // //                    {"content":"కేసులో మీరు మళ్ళీ వినాలనుకుంటే, ఇక్కడ క్లిక్ చేయండి ---->"}
 // //                              ], "sx": -1000,"sy": 120,"x": 285 ,"y": 120,"size": 12,"weight":"bold","color":"#235291","tween_type": "Elastic.easeOut","timing": 200,"delay":2, "anchor":[0,0],"wordWrap": true, "wordWrapWidth":520,"align":"right" },
 // //            {"text": [{"content":"MEDICAL QUESTIONS"}], "sx": -1000,"sy": 120,"x": 300 ,"y": 184,"size": 26,"weight":"bold","color":"#FFFFFF","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0.5,0] },
 //
 // //            {"text": [{"content":"There were a set of medical questions in the application form, which you have answered as “No” which means that you are not under any medication and you are in good health."}], "sx": -1000,"sy": 120,"x": 50 ,"y": 390,"size": 22,"weight":"bold","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0],"wordWrap": true, "wordWrapWidth":520,"align":"center" },
 //
 // //            {"text": [{"content":'Agree'}], "sx": -1000,"sy": 120,"x": 150 ,"y": 618,"size": 26,"weight":"bold","color":"#FFFFFF","tween_type": "Elastic.easeOut","timing": 200,"delay":2, "anchor":[0.5,0] },
 // //            {"text": [{"content":'Disagree'}], "sx": -1000,"sy": 120,"x": 450 ,"y": 618,"size": 26,"weight":"bold","color":"#FFFFFF","tween_type": "Elastic.easeOut","timing": 200,"delay":2, "anchor":[0.5,0] },
 // //        ],
 // //        "functions": [
 //
 // //            {"fn": "SetBGTile('bg_02')", "delay": 0 }
 //
 // //        ],
 // //         "sound_list": [
 // //            {
 // //                "sound": ["medical_audio"]
 // //            }
 // //        ],
 // //        "name": "Medical Questions - Confirm",
 // //        "timing": -1,
 // //        "index": 11
 // //    },
 //
 //
 //
    // {
    //        "sprite_animations": [
    //        {"sprite": "bar_01","x": 0,"y": 0,"loop": false,"timing": 0,"delay": 0,"toTopObj":1, "anchor":[0,0]},
    //         {"sprite": "logo_01","x": 10,"y": 33,"loop": false,"timing": 0,"delay": 0.1,"toTopObj":2, "anchor":[0,0]},
    //         {"sprite": "bar_03","x": 300,"y": 180,"loop": false,"timing": 0,"delay": 0, "toTopObj":1, "anchor":[0.5,0]},
    //         {"sprite": "box_10","x": 300,"y": 223,"loop": false,"timing": 0,"delay": 0, "toTopObj":1, "anchor":[0.5,0]},
    //         {"sprite":"icn_10","x":300,"y":223,"loop":false,"timing":0,"delay":0,"toTopObj":1, "anchor":[0.5,0]},

    //         {"sprite": "btn_sp_1","x": 540,"y": 112,"loop": false,"timing": 0,"delay": 2, "toTopObj":1, "anchor":[0,0], "onClickFn":"audioReplay()"}
    //     ],
    //       "text_animations": [
    //        {"text": [{"content":"Pre Issuance Verification Call "}], "sx": -1000,"sy": 50,"x": 190 ,"y": 50,"size": 28,"weight":"bold","color":"#003869","tween_type": "Elastic.easeOut","timing": 200,"delay":0.2, "anchor":[0,0] },
    //        {"text": [
    //                 {"content":"పప్రతిపాదన సంఖ్య: "},
    //                 {"content":"$var.window.p_PROPOSAL_NUMBER"}
    //             ], "sx": -1000,"sy": 120,"x": 40 ,"y": 150,"size": 22,"weight":"bold","color":"#235291","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },
    //         {"text": [
    //                 {"content":"కేసులో మీరు మళ్ళీ వినాలనుకుంటే, ఇక్కడ క్లిక్ చేయండి ---->"}
    //                           ], "sx": -1000,"sy": 120,"x": 285 ,"y": 120,"size": 12,"weight":"bold","color":"#235291","tween_type": "Elastic.easeOut","timing": 200,"delay":2, "anchor":[0,0],"wordWrap": true, "wordWrapWidth":520,"align":"right" },
    //         {"text": [{"content":"THANK YOU"}], "sx": -1000,"sy": 120,"x": 300 ,"y": 184,"size": 26,"weight":"bold","color":"#FFFFFF","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0.5,0] },

    //         {"text": [
    //         {"content":"Dear "},
    //         {"content":"$var.window.p_CUSTOMER_NAME"},
    //         {"content":", we would also like to thank you for choosing ECS/Direct debit as your mode of payment. This shall help you making your payments in a hassle free manner. Just ensure to keep sufficient funds in your account on the due date."}
    //         ], "sx": -1000,"sy": 120,"x": 50 ,"y": 351,"size": 18,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0],"wordWrap": true, "wordWrapWidth":520,"align":"left" },

    //          {"text": [{"content":"Our underwriting team is going through the application and shall provide their decision soon. In case, there is any requirement from your side, IndiaFirst shall soon get in touch with you."}], "sx": -1000,"sy": 120,"x": 50 ,"y": 480,"size": 18,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0],"wordWrap": true, "wordWrapWidth":520,"align":"left" },

    //          {"text": [{"content":"In case of further queries, please feel free to contact us on our toll free number 1800-209-8700 from 9AM to 7PM [Mon- Sat] or email us at customer.first@indiafirstlife.com. You can also visit our website www.indiafirstlife.com. Thank you so much for being patient during the call sir, we highly appreciate your business with IndiaFirst and look forward serving you in future. Have a pleasant day."}], "sx": -1000,"sy": 120,"x": 50 ,"y": 590,"size": 18,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0],"wordWrap": true, "wordWrapWidth":520,"align":"left" },


    //     ],
    //     "functions": [
    //         {"fn": "SetBGTile('bg_02')", "delay": 0 }
    //        // {"fn": "cameraAccessError()", "delay" : 0.5 },
    //     ],
    //      "sound_list": [
    //         {
    //             "sound": ["tq_audio"]
    //         }
    //     ],
    //     "name": "Thankyou - Confirm",
    //     "timing": -1,
    //     "index": 15
    // },




];





for(var i=0; i<screens_eng.length; i++ )
{
    if(i==5)
    {
        if(window.pa_PREMIUM_POLICY_TYPE!==false)
        {
            if(window.pa_PREMIUM_POLICY_TYPE==='regular')
            {
                console.log("Enter : regular");
                screens_eng[i]['sound_list'] = [{"sound": ["au_5_1", "$var.window.pa_product", "au_5_2", "$var.window.pa_category", "au_5_3", "$var.window.pa_PREMIUM_POLICY_TYPE", "au_5_4", "$var.currency_window.p_PREMIUM_AMOUNT", "$var.window.pa_FREQUENCY", "au_5_5", "$var.number_window.pa_PAYMENT_TERM", "years", "au_5_6", "$var.number_window.pa_BENEFIT_TERM", "au_5_7", "$var.currency_window.p_SUM_ASSURED", "au_5_8"]}];
            }
            else if(window.pa_PREMIUM_POLICY_TYPE==='single')
            {
                console.log("Enter : single");
                screens_eng[i]['sound_list'] = [{"sound": ["au_5_1", "$var.window.pa_product", "au_5_2", "$var.window.pa_category", "au_5_3", "$var.window.pa_PREMIUM_POLICY_TYPE", "au_5_4_1", "$var.currency_window.p_PREMIUM_AMOUNT", "au_5_6", "$var.number_window.pa_BENEFIT_TERM", "au_5_7", "$var.currency_window.p_SUM_ASSURED", "au_5_8"]}];
            }
        }
    }
    window.stage.screens.push(screens_eng[i]);
}
