var webcam, bmd, spriteCam, video, type; // webcam related variables

function webcam_preload()
{
    game.load.script('webcam', './assets/js/Webcam.js');
}

function webCamCreate()
{
	bmd = game.make.bitmapData(400, 300);
	webcam = game.plugins.add(Phaser.Plugin.Webcam);
}

function initWebCam()
{
    spriteCam = bmd.addToWorld();
    spriteCam.x = 400;
    spriteCam.y = 220;
    spriteCam.anchor.setTo(0.5,0.5);
    //spriteCam.scale.x *= -1;

	webcam.start(600, 360, bmd.context);
	obj_list.push(spriteCam);
}

function initWebCamPos(x,y,width,height)
{
    bmd = game.make.bitmapData(width, height);
    spriteCam = bmd.addToWorld();
    spriteCam.x = x;
    spriteCam.y = y;
    spriteCam.anchor.setTo(0.5,0);
    //spriteCam.scale.x *= -1;

    webcam.start(width, height, bmd.context);

    console.log("spriteCam : ",spriteCam);
    console.log("webcam : ",webcam);
    obj_list.push(spriteCam);
}

function initCamOnly()
{
    console.log("initCamOnly fn : start");
    var imgCanvasReg = document.createElement("canvas");
    var img_context = imgCanvasReg.getContext('2d');
    webcam.start(600, 360, img_context);
}


function stopCam() {
		webcam.stop();
}

function  pauseCam(flag)
{

}

function getImgDataURL()
{
	return webcam.imgCapture();
}

function camAccessStatus()
{
    var camStatus = webcam.camStatus();
    if(camStatus)
    {
        console.log("Camera is accessible");
        return true;
    }
    else
    {
        console.log("Camera is not accessible");
        return false;
    }
}

function videoRecordStart(){
    webcam.videoRecord();
}

function videoRecordStop(){
    webcam.videoRecordStop();
    var vBlob = webcam.videoRecordGetBlob();
    return (vBlob)? vBlob : null;
}

function videoRecordAutoStop(nxt_scrn,vLoadScrn)
{
    var time_milisec = 1000*20;
    var d = new Date().getTime();
    var video_file_name = window.p_name+"_"+window.product_slug+"_"+d;
    video_file_name = GetFriendlyName(video_file_name);

    webcam.videoRecordAutoStop(time_milisec,video_file_name,nxt_scrn,vLoadScrn);
}