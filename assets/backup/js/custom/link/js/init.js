/*
Init JS
Initialize the Product Forms.
 */

$(document).ready(function(){

    $('#sbi_dob').attr('max', moment().format('YYYY-MM-DD'));

    $('#sbi_dob').on('change', function(){
        var sbi_dob_val = this.value;
        var years = moment().diff(sbi_dob_val, 'years',false);
        $('#sbi_age').val(years);
    });

    $("#sbi_link_add_form").LoadingOverlay("show", {
        image       : "",
        text        : "Fetching product details..."
    });

    $('#sbi_product_id').val($('#sbi_product').find(':selected').attr('data-lid'));

    var jq_plan_data = $.post("./assets/js/custom/link/data/"+$('#sbi_product_id').val()+".html", {"plan_key":"em@il@2018"}, function(data) { }, 'html');
    jq_plan_data.done(function(data){
        if('sbip_smart_women_advantage'===$('#sbi_product_id').val())
        {
            $("#sbi_gender").val("Female");
            $('#sbi_gender').attr("style", "pointer-events: none; touch-action: none;");
        }
        else
        {
            $('#sbi_gender').attr("style", "pointer-events: auto; touch-action: auto;");
        }
        $('#plan_form_data').html(data);
        $("#sbi_link_add_form").LoadingOverlay("hide");
    });

    $('#sbi_product').change(function(){
        $("#sbi_link_add_form").LoadingOverlay("show", {
            image       : "",
            text        : "Fetching product details..."
        });
        $('#sbi_product_id').val($(this).children(':selected').attr('data-lid'));
        var jq_plan_dynamic_data = $.post("assets/js/custom/link/data/"+$('#sbi_product_id').val()+".html", {"plan_key":"em@il@2018"}, function(data) { }, 'html');
        jq_plan_dynamic_data.done(function(data){
            if('sbip_smart_women_advantage'===$('#sbi_product_id').val())
            {
                $("#sbi_gender").val("Female");
                $('#sbi_gender').attr("style", "pointer-events: none; touch-action: none; background: #eee;");
            }
            else
            {
                $('#sbi_gender').attr("style", "pointer-events: auto; touch-action: auto; background: #ffffff;");
            }
            $('#plan_form_data').html(data);
            $("#sbi_link_add_form").LoadingOverlay("hide");
        });
    });

    $("#sbi_link_add_form").submit(function(e){
        e.preventDefault();

        if('sbip_smart_wealth_builder'===$('#sbi_product_id').val())
        {
            var total_investment = 0;
            total_investment = parseInt($('#sbi_perInvEquityFund').val()) +  parseInt($('#sbi_perInvEquityOptimiserFund').val()) + parseInt($('#sbi_perInvgrowthFund').val()) + parseInt($('#sbi_perInvBalancedFund').val()) + parseInt($('#sbi_perInvBondFund').val()) + parseInt($('#sbi_perInvMoneyMarketFund').val()) + parseInt($('#sbi_perInvTop300Fund').val());

            if(total_investment!=100)
            {
                var html='<a href="javascript:void(0)" class="close" data-dismiss="alert" aria-label="close">&times;</a>';
                html+="Please enter a proper fund value summation should be 100%";
                $('#alert-msg-div').addClass('alert alert-danger');
                $('#alert-msg-div').html(html);
                $('html, body').animate({
                    scrollTop: $('#logo_header').offset().top
                }, 1000);
                $("#alert-msg-div").fadeIn("slow").delay(20000).fadeOut("slow");
                return;
            }
        }


        $("#sbi_link_add_form").LoadingOverlay("show", {
            image       : "",
            text        : "Generating ADC link..."
        });

        $('#alert-msg-div').html('');
        $('#alert-msg-div').removeClass();
        $("#kfdLink").attr("href",'');
        $("#kfdLink").html('');


        // Use Ajax to submit form data
        var formData = new FormData(this);

        var jq_generate_data = $.ajax({
            type:'POST',
            dataType:'JSON',
            url: $(this).attr('action'),
            data:formData,
            cache:false,
            contentType: false,
            processData: false});

        jq_generate_data.done(function(data){
            $("#sbi_link_add_form").LoadingOverlay("hide");
            if(data.status)
            {
                var html='<a href="javascript:void(0)" class="close" data-dismiss="alert" aria-label="close">&times;</a>';
                html+=data.msg;
                $('#alert-msg-div').removeClass().addClass('alert alert-success');
                $('#alert-msg-div').html(html);
                $("#kfdLink").attr("href", data.short_url);
                $("#kfdLink").html(data.short_url);
                $("#alert-msg-div").fadeIn("slow").delay(15000).fadeOut("slow",function(){
                    window.location.reload(true);
                });
            }
            else if((data===404) || (data==='404'))
            {
                var html='<a href="javascript:void(0)" class="close" data-dismiss="alert" aria-label="close">&times;</a>';
                html+='Unable to process. Please try again!';
                $('#alert-msg-div').removeClass().addClass('alert alert-danger');
                $('#alert-msg-div').html(html);
                $('html, body').animate({
                    scrollTop: $('#logo_header').offset().top
                }, 1000);
                $('#alert-msg-div').fadeIn("slow").delay(5000).fadeOut("slow");
            }
            else
            {
                var html='<a href="javascript:void(0)" class="close" data-dismiss="alert" aria-label="close">&times;</a>';
                html+=data.msg;
                $('#alert-msg-div').removeClass().addClass('alert alert-danger');
                $('#alert-msg-div').html(html);
                $('html, body').animate({
                    scrollTop: $('#logo_header').offset().top
                }, 1000);
                $("#alert-msg-div").fadeIn("slow").delay(5000).fadeOut("slow");
            }
        });

        jq_generate_data.fail(function() {
            $("#sbi_link_add_form").LoadingOverlay("hide");
            var html='<a href="javascript:void(0)" class="close" data-dismiss="alert" aria-label="close">&times;</a>';
            html+="Unable to process, Please try later!";
            $('#alert-msg-div').removeClass().addClass('alert alert-danger');
            $('#alert-msg-div').html(html);
            $('html, body').animate({
                scrollTop: $('#logo_header').offset().top
            }, 1000);
            $("#alert-msg-div").fadeIn("slow").delay(5000).fadeOut("slow");
        })

    });

});

/*
sbip_smart_women_advantage
 */
$(document).on('change', '#sbi_sumAssured', function () {
    let sbi_sumAssured_val = this.value;
    $('#sbi_critIllSumAssured').val(sbi_sumAssured_val);
    let sbi_sumAssured_val_20 = 0;
    if(sbi_sumAssured_val>0)
    {
        sbi_sumAssured_val_20 = (sbi_sumAssured_val*20)/100;
    }
    $('#sbi_apcNcaSumAssured').val(sbi_sumAssured_val_20);
});