/*
 Custom Assets
 */

// Define Paths
var PRODUCT_PATH = './assets/product_assets/'+window.flow_slug+'/';
var PRODUCT_COMMON_IMG_PATH = PRODUCT_PATH+'images/';
var PRODUCT_LANG_IMG_PATH = PRODUCT_COMMON_IMG_PATH+'tel/';
var PRODUCT_SCENE_AUDIO_PATH = PRODUCT_PATH+'audio/tel/scenes/';
var PRODUCT_COMMON_AUDIO_PATH = PRODUCT_PATH+'audio/tel/common/';

var COMMON_IMG_PATH = './assets/images/common/product/';
var COMMON_LANG_IMG_PATH = COMMON_IMG_PATH+'tel/';

var COMMON_JS_PATH = './assets/js/common/';
var CUSTOM_IMG_PATH = './assets/images/custom/';
var CUSTOM_IMG_LANG_PATH = './assets/images/custom/tel/';
var CUSTOM_IMG_PRODUCT_PATH = './assets/images/custom/product/';

var COMMON_AUDIO_PATH = './assets/audio/common/';
var COMMON_AUDIO_LANG_PATH = COMMON_AUDIO_PATH+'tel/';

var TYPE_SCENE_AUDIO_PATH = './assets/audio/product/type/mconnect/tel/scenes/';
var TYPE_COMMON_AUDIO_PATH = './assets/audio/product/type/mconnect/tel/common/';

function langAssets()
{
    // SpriteSheet


    // Images


    // Audio

    // Common


    // Screens



    // Scripts


    // Load Start
    //game.load.start();
}


function langAssetsRest()
{
    console.log("langAssetsRest : Init");

    // SpriteSheet
    game.load.spritesheet('iu_1_1', COMMON_LANG_IMG_PATH+'image_upload_01.png', 300, 225);


    // Images
    game.load.image('bar_03', COMMON_LANG_IMG_PATH+'bar_03.png');
    game.load.image('box_02', COMMON_LANG_IMG_PATH+'box_02.png');
    game.load.image('icn_02', COMMON_LANG_IMG_PATH+'icn_02.png');
    game.load.image('box_03', COMMON_LANG_IMG_PATH+'box_03.png');
    game.load.image('icn_03', COMMON_LANG_IMG_PATH+'icn_03.png');
    game.load.image('box_04', COMMON_LANG_IMG_PATH+'box_04.png');
    game.load.image('icn_04', COMMON_LANG_IMG_PATH+'icn_04.png');
    game.load.image('btn_03_1', COMMON_LANG_IMG_PATH+'btn_03.png');
    game.load.image('btn_03_2', COMMON_LANG_IMG_PATH+'btn_03.png');
    game.load.image('btn_03_3', COMMON_LANG_IMG_PATH+'btn_03.png');

    game.load.image('box_05', COMMON_LANG_IMG_PATH+'box_05.png');
    game.load.image('bar_04', COMMON_LANG_IMG_PATH+'bar_04.png');

    game.load.image('box_06', COMMON_LANG_IMG_PATH+'box_06.png');

    game.load.image('icn_05', COMMON_LANG_IMG_PATH+'icn_05.png');

    game.load.image('box_071_1', COMMON_LANG_IMG_PATH+'box_071.png');
    game.load.image('box_07_1', COMMON_LANG_IMG_PATH+'box_07.png');
    game.load.image('box_07_2', COMMON_LANG_IMG_PATH+'box_07.png');
    game.load.image('box_07_3', COMMON_LANG_IMG_PATH+'box_07.png');
    game.load.image('icn_06', COMMON_LANG_IMG_PATH+'icn_06.png');
    game.load.image('icn_07', COMMON_LANG_IMG_PATH+'icn_07.png');
    game.load.image('icn_08', COMMON_LANG_IMG_PATH+'icn_08.png');

    game.load.image('box_08', COMMON_LANG_IMG_PATH+'box_08.png');
    game.load.image('icn_09_1', COMMON_LANG_IMG_PATH+'icn_09.png');
    game.load.image('icn_09_2', COMMON_LANG_IMG_PATH+'icn_09.png');
    game.load.image('icn_09_3', COMMON_LANG_IMG_PATH+'icn_09.png');
    game.load.image('icn_09_4', COMMON_LANG_IMG_PATH+'icn_09.png');
    game.load.image('icn_09_5', COMMON_LANG_IMG_PATH+'icn_09.png');
    game.load.image('icn_09_6', COMMON_LANG_IMG_PATH+'icn_09.png');

    game.load.image('bar_05', COMMON_LANG_IMG_PATH+'bar_05.png');
    game.load.image('bar_06', COMMON_LANG_IMG_PATH+'bar_06.png');
    game.load.image('bar_07', COMMON_LANG_IMG_PATH+'bar_07.png');

    game.load.image('box_09', COMMON_LANG_IMG_PATH+'box_09.png');

    game.load.image('box_10', COMMON_LANG_IMG_PATH+'box_10.png');
    game.load.image('btn_04_1', COMMON_LANG_IMG_PATH+'btn_03.png');
    game.load.image('btn_04_2', COMMON_LANG_IMG_PATH+'btn_03.png');

    game.load.image('btn_07_1', COMMON_LANG_IMG_PATH+'btn_03.png');

    game.load.image('bar_08', COMMON_LANG_IMG_PATH+'bar_08.png');
    game.load.image('btn_eng_01', PRODUCT_LANG_IMG_PATH+'btn_eng_01.png');

    game.load.image('icn_10', COMMON_LANG_IMG_PATH+'icn_10.png');

    game.load.image('box_11', COMMON_LANG_IMG_PATH+'box_11.png');
    game.load.image('btn_09_1', COMMON_LANG_IMG_PATH+'btn_03.png');
    game.load.image('btn_09_2', COMMON_LANG_IMG_PATH+'btn_03.png');

    game.load.image('box_12', COMMON_LANG_IMG_PATH+'box_12.png');
    game.load.image('box_13', COMMON_LANG_IMG_PATH+'box_13.png');
    game.load.image('box_14', COMMON_LANG_IMG_PATH+'box_14.png');
    game.load.image('box_22', COMMON_LANG_IMG_PATH+'box_22.png');
    game.load.image('btn_equ', COMMON_LANG_IMG_PATH+'btn_equ.png');
    game.load.image('box_23', COMMON_LANG_IMG_PATH+'box_23.png');
        game.load.image('box_073', COMMON_LANG_IMG_PATH+'box_073.png');
          game.load.image('no-pictures', COMMON_LANG_IMG_PATH+'no-pictures.png');



     //sachin add
    game.load.image('box_16', COMMON_LANG_IMG_PATH+'box_16.png');
    game.load.image('box_18', COMMON_LANG_IMG_PATH+'box_18.png');
    game.load.image('box_07', COMMON_LANG_IMG_PATH+'box_07.png');
    game.load.image('box_20', COMMON_LANG_IMG_PATH+'box_20.png');
    game.load.image('box_21', COMMON_LANG_IMG_PATH+'box_21.png');
    game.load.image('medicon', COMMON_LANG_IMG_PATH+'medicon.png');
    game.load.image('illness_icon', COMMON_LANG_IMG_PATH+'illness_icon.png');
    game.load.image('loan_icon', COMMON_LANG_IMG_PATH+'loan_icon.png');
    game.load.image('dea_icon', COMMON_LANG_IMG_PATH+'dea_icon.png');
    game.load.image('surr_icon', COMMON_LANG_IMG_PATH+'surr_icon.png');
    game.load.image('maturity_icon', COMMON_LANG_IMG_PATH+'maturity_icon.png');
    game.load.image('equity_icon', COMMON_LANG_IMG_PATH+'equity_icon.png');
    game.load.image('Email', COMMON_LANG_IMG_PATH+'Email.png');
    game.load.image('phno', COMMON_LANG_IMG_PATH+'phno.png');
    game.load.image('web_icon', COMMON_LANG_IMG_PATH+'web_icon.png');
    game.load.image('box_072', COMMON_LANG_IMG_PATH+'box_072.png');
    game.load.image('box_071', COMMON_LANG_IMG_PATH+'box_071.png');
    game.load.image('box_071', COMMON_LANG_IMG_PATH+'box_071.png');
    game.load.image('hand', COMMON_LANG_IMG_PATH+'hand.png');




    game.load.image('icn_11', COMMON_LANG_IMG_PATH+'icn_11.png');

	game.load.image('disagree', COMMON_LANG_IMG_PATH+'frown.png');
	game.load.image('agree', COMMON_LANG_IMG_PATH+'smile.png');

	game.load.image('heart', COMMON_LANG_IMG_PATH+'heart.png');
	game.load.image('brain', COMMON_LANG_IMG_PATH+'brain.png');
	game.load.image('cancer', COMMON_LANG_IMG_PATH+'cancer.png');
	game.load.image('diabetes', COMMON_LANG_IMG_PATH+'diabetes.png');
	game.load.image('ent', COMMON_LANG_IMG_PATH+'ent.png');
	game.load.image('respiratory', COMMON_LANG_IMG_PATH+'respiratory.png');
	game.load.image('digestion', COMMON_LANG_IMG_PATH+'digestion.png');

	game.load.spritesheet('welcome_anim', COMMON_LANG_IMG_PATH+'welcome_anim.png',195,210);
	game.load.spritesheet('thumbsdown', COMMON_LANG_IMG_PATH+'thumbsdown.png',169,178);

    // Audio

    // Scenes


	// game.load.audio('ab_3', TYPE_SCENE_AUDIO_PATH+'ab_3.mp3');

    //sachin added

    game.load.audio('welcome1_audio', TYPE_SCENE_AUDIO_PATH+'welcome1_audio.mp3');
    game.load.audio('welcome2_audio', TYPE_SCENE_AUDIO_PATH+'welcome2_audio.mp3');
    game.load.audio('video_consent_audio', TYPE_SCENE_AUDIO_PATH+'video_consent_audio.mp3');
    game.load.audio('tq_audio', TYPE_SCENE_AUDIO_PATH+'tq_audio.mp3');
    game.load.audio('start_audio', TYPE_SCENE_AUDIO_PATH+'start_audio.mp3');
    game.load.audio('pre_lang_audio', TYPE_SCENE_AUDIO_PATH+'pre_lang_audio.mp3');
    game.load.audio('plan_disagree_audio', TYPE_SCENE_AUDIO_PATH+'plan_disagree_audio.mp3');
    game.load.audio('plan_details_audio', TYPE_SCENE_AUDIO_PATH+'plan_details_audio.mp3');
    game.load.audio('personal_disagree_audio', TYPE_SCENE_AUDIO_PATH+'personal_disagree_audio.mp3');
    game.load.audio('personal_details_audio', TYPE_SCENE_AUDIO_PATH+'personal_details_audio.mp3');
    game.load.audio('medical_audio', TYPE_SCENE_AUDIO_PATH+'medical_audio.mp3');
    game.load.audio('loan_question_audio', TYPE_SCENE_AUDIO_PATH+'loan_question_audio.mp3');
    game.load.audio('illness_details_audio', TYPE_SCENE_AUDIO_PATH+'illness_details_audio.mp3');
    game.load.audio('benefits_audio', TYPE_SCENE_AUDIO_PATH+'benefits_audio.mp3');
    game.load.audio('video_audio', TYPE_SCENE_AUDIO_PATH+'video_audio.mp3');
    game.load.audio('video_audio2', TYPE_SCENE_AUDIO_PATH+'video_audio2.mp3');
    game.load.audio('plan_yesno_audio', TYPE_SCENE_AUDIO_PATH+'plan_yesno_audio.mp3');
    game.load.audio('personal_yesno_audio', TYPE_SCENE_AUDIO_PATH+'personal_yesno_audio.mp3');
    game.load.audio('equity_audio', TYPE_SCENE_AUDIO_PATH+'equity_audio.mp3');
    game.load.audio('death_benefit', TYPE_SCENE_AUDIO_PATH+'death_benefit.mp3');
    game.load.audio('maturity_benefit', TYPE_SCENE_AUDIO_PATH+'maturity_benefit.mp3');
    game.load.audio('startplan', TYPE_SCENE_AUDIO_PATH+'startplan.mp3');
    game.load.audio('newbenefit', TYPE_SCENE_AUDIO_PATH+'newbenefit.mp3');
            game.load.audio('plantype', TYPE_SCENE_AUDIO_PATH+'plantype.mp3');





    game.load.audio('plan_name', TYPE_SCENE_AUDIO_PATH+'plan_name.mp3');
    game.load.audio('premium_amount', TYPE_SCENE_AUDIO_PATH+'premium_amount.mp3');
    game.load.audio('payment_frequency', TYPE_SCENE_AUDIO_PATH+'payment_frequency.mp3');
    game.load.audio('paying_term', TYPE_SCENE_AUDIO_PATH+'paying_term.mp3');
    game.load.audio('policy_term', TYPE_SCENE_AUDIO_PATH+'policy_term.mp3');
    game.load.audio('sum_assured', TYPE_SCENE_AUDIO_PATH+'sum_assured.mp3');





    // Common

    // Product Dependent
    game.load.audio('Money_plan', TYPE_COMMON_AUDIO_PATH+'Money_plan.mp3');
    game.load.audio('little_champ_plan', TYPE_COMMON_AUDIO_PATH+'little_champ_plan.mp3');
    game.load.audio('maha_jeevan_plan', TYPE_COMMON_AUDIO_PATH+'maha_jeevan_plan.mp3');
    game.load.audio('cash_back_plan', TYPE_COMMON_AUDIO_PATH+'cash_back_plan.mp3');

    game.load.audio('tradition', TYPE_COMMON_AUDIO_PATH+'tradition.mp3');
    game.load.audio('ulip', TYPE_COMMON_AUDIO_PATH+'ulip.mp3');
    game.load.audio('term', TYPE_COMMON_AUDIO_PATH+'term.mp3');
    game.load.audio('pension', TYPE_COMMON_AUDIO_PATH+'pension.mp3');
    game.load.audio('limited', TYPE_COMMON_AUDIO_PATH+'limited.mp3');

    game.load.audio('single', TYPE_COMMON_AUDIO_PATH+'single.mp3');
    game.load.audio('regular', TYPE_COMMON_AUDIO_PATH+'regular.mp3');

    game.load.audio('annuelle', TYPE_COMMON_AUDIO_PATH+'annuelle.mp3');
    game.load.audio('years', TYPE_COMMON_AUDIO_PATH+'years.mp3');
    game.load.audio('yearly', TYPE_COMMON_AUDIO_PATH+'yearly.mp3');
    game.load.audio('halfyearly', TYPE_COMMON_AUDIO_PATH+'halfyearly.mp3');
    game.load.audio('quarterly', TYPE_COMMON_AUDIO_PATH+'quarterly.mp3');
    game.load.audio('monthly', TYPE_COMMON_AUDIO_PATH+'monthly.mp3');
    game.load.audio('weekly', TYPE_COMMON_AUDIO_PATH+'weekly.mp3');


    // Number
    game.load.audio('and', [COMMON_AUDIO_LANG_PATH+'number/and.mp3']);
    game.load.audio('zero', [COMMON_AUDIO_LANG_PATH+'number/zero.mp3']);
    game.load.audio('one', [COMMON_AUDIO_LANG_PATH+'number/one.mp3']);
    game.load.audio('two', [COMMON_AUDIO_LANG_PATH+'number/two.mp3']);
    game.load.audio('three', [COMMON_AUDIO_LANG_PATH+'number/three.mp3']);
    game.load.audio('four', [COMMON_AUDIO_LANG_PATH+'number/four.mp3']);
    game.load.audio('five', [COMMON_AUDIO_LANG_PATH+'number/five.mp3']);
    game.load.audio('six', [COMMON_AUDIO_LANG_PATH+'number/six.mp3']);
    game.load.audio('seven', [COMMON_AUDIO_LANG_PATH+'number/seven.mp3']);
    game.load.audio('eight', [COMMON_AUDIO_LANG_PATH+'number/eight.mp3']);
    game.load.audio('nine', [COMMON_AUDIO_LANG_PATH+'number/nine.mp3']);
    game.load.audio('ten', [COMMON_AUDIO_LANG_PATH+'number/ten.mp3']);
    game.load.audio('eleven', [COMMON_AUDIO_LANG_PATH+'number/eleven.mp3']);
    game.load.audio('twelve', [COMMON_AUDIO_LANG_PATH+'number/twelve.mp3']);
    game.load.audio('thirteen', [COMMON_AUDIO_LANG_PATH+'number/thirteen.mp3']);
    game.load.audio('fourteen', [COMMON_AUDIO_LANG_PATH+'number/fourteen.mp3']);
    game.load.audio('fifteen', [COMMON_AUDIO_LANG_PATH+'number/fifteen.mp3']);
    game.load.audio('sixteen', [COMMON_AUDIO_LANG_PATH+'number/sixteen.mp3']);
    game.load.audio('seventeen', [COMMON_AUDIO_LANG_PATH+'number/seventeen.mp3']);
    game.load.audio('eighteen', [COMMON_AUDIO_LANG_PATH+'number/eighteen.mp3']);
    game.load.audio('nineteen', [COMMON_AUDIO_LANG_PATH+'number/nineteen.mp3']);
    game.load.audio('twenty', [COMMON_AUDIO_LANG_PATH+'number/twenty.mp3']);
    game.load.audio('thirty', [COMMON_AUDIO_LANG_PATH+'number/thirty.mp3']);
    game.load.audio('forty', [COMMON_AUDIO_LANG_PATH+'number/forty.mp3']);
    game.load.audio('fifty', [COMMON_AUDIO_LANG_PATH+'number/fifty.mp3']);
    game.load.audio('sixty', [COMMON_AUDIO_LANG_PATH+'number/sixty.mp3']);
    game.load.audio('seventy', [COMMON_AUDIO_LANG_PATH+'number/seventy.mp3']);
    game.load.audio('eighty', [COMMON_AUDIO_LANG_PATH+'number/eighty.mp3']);
    game.load.audio('ninety', [COMMON_AUDIO_LANG_PATH+'number/ninety.mp3']);
    game.load.audio('hundred', [COMMON_AUDIO_LANG_PATH+'number/hundred.mp3']);
    game.load.audio('thousand', [COMMON_AUDIO_LANG_PATH+'number/thousand.mp3']);
    game.load.audio('lakh', [COMMON_AUDIO_LANG_PATH+'number/lakh.mp3']);
    game.load.audio('million', [COMMON_AUDIO_LANG_PATH+'number/million.mp3']);
    game.load.audio('crore', [COMMON_AUDIO_LANG_PATH+'number/crore.mp3']);
    game.load.audio('billion', [COMMON_AUDIO_LANG_PATH+'number/billion.mp3']);
    game.load.audio('trillion', [COMMON_AUDIO_LANG_PATH+'number/trillion.mp3']);
    game.load.audio('quadrillion', [COMMON_AUDIO_LANG_PATH+'number/quadrillion.mp3']);
    game.load.audio('quintillion', [COMMON_AUDIO_LANG_PATH+'number/quintillion.mp3']);
    game.load.audio('sextillion', [COMMON_AUDIO_LANG_PATH+'number/sextillion.mp3']);
    game.load.audio('septillion', [COMMON_AUDIO_LANG_PATH+'number/septillion.mp3']);
    game.load.audio('octillion', [COMMON_AUDIO_LANG_PATH+'number/octillion.mp3']);
    game.load.audio('nonillion', [COMMON_AUDIO_LANG_PATH+'number/nonillion.mp3']);

    // Currency
    game.load.audio('rupee', [COMMON_AUDIO_LANG_PATH+'currency/rupee.mp3']);
    game.load.audio('rupees', [COMMON_AUDIO_LANG_PATH+'currency/rupees.mp3']);

    game.load.audio('paisa', [COMMON_AUDIO_LANG_PATH+'currency/paisa.mp3']);
    game.load.audio('dollar', [COMMON_AUDIO_LANG_PATH+'currency/dollar.mp3']);
    game.load.audio('cent', [COMMON_AUDIO_LANG_PATH+'currency/cent.mp3']);
    game.load.audio('pound', [COMMON_AUDIO_LANG_PATH+'currency/pound.mp3']);
    game.load.audio('pence', [COMMON_AUDIO_LANG_PATH+'currency/pence.mp3']);
    game.load.audio('yuan', [COMMON_AUDIO_LANG_PATH+'currency/yuan.mp3']);
    game.load.audio('jiao', [COMMON_AUDIO_LANG_PATH+'currency/jiao.mp3']);
    game.load.audio('euro', [COMMON_AUDIO_LANG_PATH+'currency/euro.mp3']);

    // Alphabets
    game.load.audio('a', [COMMON_AUDIO_LANG_PATH+'alphabet/a.mp3']);
    game.load.audio('b', [COMMON_AUDIO_LANG_PATH+'alphabet/b.mp3']);
    game.load.audio('c', [COMMON_AUDIO_LANG_PATH+'alphabet/c.mp3']);
    game.load.audio('d', [COMMON_AUDIO_LANG_PATH+'alphabet/d.mp3']);
    game.load.audio('e', [COMMON_AUDIO_LANG_PATH+'alphabet/e.mp3']);
    game.load.audio('f', [COMMON_AUDIO_LANG_PATH+'alphabet/f.mp3']);
    game.load.audio('g', [COMMON_AUDIO_LANG_PATH+'alphabet/g.mp3']);
    game.load.audio('h', [COMMON_AUDIO_LANG_PATH+'alphabet/h.mp3']);
    game.load.audio('i', [COMMON_AUDIO_LANG_PATH+'alphabet/i.mp3']);
    game.load.audio('j', [COMMON_AUDIO_LANG_PATH+'alphabet/j.mp3']);
    game.load.audio('k', [COMMON_AUDIO_LANG_PATH+'alphabet/k.mp3']);
    game.load.audio('l', [COMMON_AUDIO_LANG_PATH+'alphabet/l.mp3']);
    game.load.audio('m', [COMMON_AUDIO_LANG_PATH+'alphabet/m.mp3']);
    game.load.audio('n', [COMMON_AUDIO_LANG_PATH+'alphabet/n.mp3']);
    game.load.audio('o', [COMMON_AUDIO_LANG_PATH+'alphabet/o.mp3']);
    game.load.audio('p', [COMMON_AUDIO_LANG_PATH+'alphabet/p.mp3']);
    game.load.audio('q', [COMMON_AUDIO_LANG_PATH+'alphabet/q.mp3']);
    game.load.audio('r', [COMMON_AUDIO_LANG_PATH+'alphabet/r.mp3']);
    game.load.audio('s', [COMMON_AUDIO_LANG_PATH+'alphabet/s.mp3']);
    game.load.audio('t', [COMMON_AUDIO_LANG_PATH+'alphabet/t.mp3']);
    game.load.audio('u', [COMMON_AUDIO_LANG_PATH+'alphabet/u.mp3']);
    game.load.audio('v', [COMMON_AUDIO_LANG_PATH+'alphabet/v.mp3']);
    game.load.audio('w', [COMMON_AUDIO_LANG_PATH+'alphabet/w.mp3']);
    game.load.audio('x', [COMMON_AUDIO_LANG_PATH+'alphabet/x.mp3']);
    game.load.audio('y', [COMMON_AUDIO_LANG_PATH+'alphabet/y.mp3']);
    game.load.audio('z', [COMMON_AUDIO_LANG_PATH+'alphabet/z.mp3']);
    game.load.audio('dot', [COMMON_AUDIO_LANG_PATH+'alphabet/dot.mp3']);
    game.load.audio('comma', [COMMON_AUDIO_LANG_PATH+'alphabet/comma.mp3']);

    // Months
    game.load.audio('january', [COMMON_AUDIO_LANG_PATH+'date/january.mp3']);
    game.load.audio('february', [COMMON_AUDIO_LANG_PATH+'date/february.mp3']);
    game.load.audio('march', [COMMON_AUDIO_LANG_PATH+'date/march.mp3']);
    game.load.audio('april', [COMMON_AUDIO_LANG_PATH+'date/april.mp3']);
    game.load.audio('may', [COMMON_AUDIO_LANG_PATH+'date/may.mp3']);
    game.load.audio('june', [COMMON_AUDIO_LANG_PATH+'date/june.mp3']);
    game.load.audio('july', [COMMON_AUDIO_LANG_PATH+'date/july.mp3']);
    game.load.audio('august', [COMMON_AUDIO_LANG_PATH+'date/august.mp3']);
    game.load.audio('september', [COMMON_AUDIO_LANG_PATH+'date/september.mp3']);
    game.load.audio('october', [COMMON_AUDIO_LANG_PATH+'date/october.mp3']);
    game.load.audio('november', [COMMON_AUDIO_LANG_PATH+'date/november.mp3']);
    game.load.audio('december', [COMMON_AUDIO_LANG_PATH+'date/december.mp3']);

    game.load.start();

    console.log("langAssetsRest : Started");
}
