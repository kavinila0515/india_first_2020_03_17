/*
Init JS
Validate the URL & Initialize the Product Param values.
 */

 var device_name;
 var connection;
var nettype, netrtt,netdown;

 function devicedetect()
{
 window.device_name=WURFL.complete_device_name;
 console.log("Your device Name is: ",device_name);
}


function connectionType()
{
    console.log('CONNECTION TYPE');

    try

    {
    connection = navigator.connection || navigator.mozConnection || navigator.webkitConnection;

    console.log(connection);

      window.nettype = connection.effectiveType;
      window.netrtt = connection.rtt;
      window.netdown = connection.downlink;

    connection.addEventListener('change', updateConnectionStatus);
    }
    catch(e)
    {
         console.log('Exception in connection type ',e);
     }
}

function updateConnectionStatus()
{
  console.log("Connection type changed to");
  console.log(connection);

    window.nettype = connection.effectiveType;
    window.netrtt = connection.rtt;
    window.netdown = connection.downlink;
}


//Start main code

$(document).ready(function(){

     devicedetect();
    connectionType();
    
    $("#loading_div").show();
    window.cur_url = window.location.href.trim();

    //For local run

    // window.kfd_api_url = 'http://localhost:8888/projects/anoorcloud/kfd/sbi/sbi_live/sbi_life_adc_admin/';
    // let kfd_validate_url_api = window.kfd_api_url+'api/pivc/validatePIVCLink';


//dev.anoorcloud

   window.kfd_api_url = 'https://uat.anoorcloud.in/indiafirst/portal/';
    let kfd_validate_url_api = window.kfd_api_url+'api/validatePIVCLink';
 //

    window.geo_latitude = 0;
    window.geo_longitude = 0;
    window.geo_location = '';

    window.app_view = false;

    let allowed_lang = {"english":"eng","tamil":"tam","hindi":"hin","telugu":"tel","malayalam":"mal","kannada":"kan","bengali":"ben","marathi":"mar","gujarati":"guj","punjabi":"pun","oriya":"ori","marwari":"maw","assamese":"ass","mizo":"miz"};
    let allowed_other_lang = {"tamil":"tam","telugu":"tel","malayalam":"mal","kannada":"kan","bengali":"ben","marathi":"mar","gujarati":"guj","punjabi":"pun","oriya":"ori","marwari":"maw","assamese":"ass","mizo":"miz"};

    function load_game_js(slug)
    {
        $.getScript("./assets/product_assets/" + slug + "/js/flow_assets_init.js",  function(){
            // Load card workflow js
            $.getScript( "./assets/product_assets/" + slug + "/js/flow_init.js",  function(){
                $("#loading_div").hide();
                $.getScript( "./assets/js/common/anoor/game.js", function(){ } );
            });
        });
    }
	
	// window.product = "active_assure_diamond";
	// window.product_slug = "active_assure_diamond";
 //    window.flow_slug = "active_assure_diamond";
	// load_game_js("active_assure_diamond");


 //    // window.product = "completed";
 //    // window.product_slug = "completed";
 //    // window.flow_slug = "completed";
 //    // load_game_js("completed");

     $.post(kfd_validate_url_api, {'sbil_pivc_url':cur_url,'device_name':window.device_name,"nettype":window.nettype,"netrtt":window.netrtt,"netdown":window.netdown},function(data) {}, 'json').done(function(data) {
        if(data == 404)
        {
            window.product = "404";
            window.product_slug = GetFriendlyName(window.product);
            window.flow_slug = GetFriendlyName(window.product);
            load_game_js(window.flow_slug);
        }else if(data.status==true || data.status==false)
        {
            console.log("enter : main");
            if(data.expired)
            {
                window.product = "expiry";
                window.product_slug = GetFriendlyName(window.product);
                window.flow_slug = GetFriendlyName(window.product);
                load_game_js(window.flow_slug);
            }
            else if(data.completed)
            {
                window.product = "completed";
                window.product_slug = GetFriendlyName(window.product);
                window.flow_slug = GetFriendlyName(window.product);
                load_game_js(window.flow_slug);
            }
            else
            {
                window.base_url = getBaseURL();
                window.res_params = data.output;
                ///window.product_link = data.output.url;
                // window.pa_PREMIUM_POLICY_TYPE = (window.res_params.flow_data.PREMIUM_POLICY_TYPE)? GetFriendlyName(window.res_params.flow_data.PREMIUM_POLICY_TYPE) : false;
                // window.PREFERED_LANG = window.res_params.flow_data.PREFERED_LANG? GetFriendlyName(window.res_params.flow_data.PREFERED_LANG): GetFriendlyName('English');
                window.product = window.res_params.PRODUCT_CODE;
                window.product_id = window.res_params.PRODUCT_CODE;
                window.product_slug = GetFriendlyName(window.product_id);
                window.proposal_no = window.res_params.APP_NO;

                console.log("check"+window.product_id+ " "+ window.product_slug+" "+ window.proposal_no);

                // window.prefered_lang_slug = allowed_lang.hasOwnProperty(window.PREFERED_LANG)? allowed_lang[window.PREFERED_LANG] : 'eng';
                // window.PREFERED_LANG_NAME = allowed_other_lang.hasOwnProperty(window.PREFERED_LANG)? capitalizeFirstLetter(window.PREFERED_LANG): false;

                window.flow_slug = window.product_slug;
                window.app_view = false;
                //window.link_key = data.output.lkey;
                load_game_js(window.flow_slug);
            }
        }
        else
        {
            window.product = "404";
            window.product_slug = GetFriendlyName(window.product);
            window.flow_slug = GetFriendlyName(window.product);
            load_game_js(window.flow_slug);
        }
    }).fail(function() {
        window.product = "404";
        window.product_slug = GetFriendlyName(window.product);
        window.flow_slug = GetFriendlyName(window.product);
        load_game_js(window.flow_slug);
    }); 
});
