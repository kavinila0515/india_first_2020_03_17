/*
    Utility Functions
 */

    function getParameterByName(name, url)
    {
        if (!url) {
            url = window.location.href;
        }
        name = name.replace(/[\[\]]/g, "\\$&");
        var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
            results = regex.exec(url);
        if (!results) return null;
        if (!results[2]) return '';
        return decodeURIComponent(results[2].replace(/\+/g, " "));
    }

    function getBaseURL() {
        var url = window.location.origin + window.location.pathname;
        var url_comp = url.split('index.html');
        return url_comp[0];
    }

    function GetFriendlyName(pname)
    {
        var op = pname.replace(/ +/g, '_').toLowerCase();
        return op.replace(/[^\w\s]/gi, '_').toLowerCase();
    }

    function capitalizeFirstLetter(string)
    {
        return string.charAt(0).toUpperCase() + string.slice(1);
    }

    function findObjListKey(obj,keyName)
    {
        for(var i = 0; i < obj.length; i++)
        {
            if(obj[i].key == keyName)
            {
                return i;
            }
        }
    }