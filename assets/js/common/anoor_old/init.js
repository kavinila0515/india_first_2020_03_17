/*
Init JS
Validate the URL & Initialize the Product Param values.
 */

$(document).ready(function(){
    $("#loading_div").show();
    window.cur_url = window.location.href.trim();
    window.kfd_api_url = 'http://localhost:8888/projects/anoorcloud/kfd/sbi/sbi_live/sbi_life_adc_admin/';
    let kfd_validate_url_api = window.kfd_api_url+'api/pivc/validatePIVCLink';

    window.geo_latitude = 0;
    window.geo_longitude = 0;
    window.geo_location = '';

    window.app_view = false;

    let allowed_lang = {"english":"eng","tamil":"tam","hindi":"hin","telugu":"tel","malayalam":"mal","kannada":"kan","bengali":"ben","marathi":"mar","gujarati":"guj","punjabi":"pun","oriya":"ori","marwari":"maw","assamese":"ass","mizo":"miz"};
    let allowed_other_lang = {"tamil":"tam","telugu":"tel","malayalam":"mal","kannada":"kan","bengali":"ben","marathi":"mar","gujarati":"guj","punjabi":"pun","oriya":"ori","marwari":"maw","assamese":"ass","mizo":"miz"};

    function load_game_js(slug)
    {
        $.getScript("./assets/product_assets/" + slug + "/js/flow_assets_init.js",  function(){
            // Load card workflow js
            $.getScript( "./assets/product_assets/" + slug + "/js/flow_init.js",  function(){
                $("#loading_div").hide();
                $.getScript( "./assets/js/common/anoor/game.js", function(){ } );
            });
        });
    }
	
	window.product = "active_assure_diamond";
	window.product_slug = "active_assure_diamond";
    window.flow_slug = "active_assure_diamond";
	load_game_js("active_assure_diamond");

    /* $.post(kfd_validate_url_api, {'sbil_pivc_url':cur_url},function(data) {}, 'json').done(function(data) {
        if(data == 404)
        {
            window.product = "404";
            window.product_slug = GetFriendlyName(window.product);
            window.flow_slug = GetFriendlyName(window.product);
            load_game_js(window.flow_slug);
        }else if(data.status)
        {
            console.log("enter : main");
            if(data.expired)
            {
                window.product = "expiry";
                window.product_slug = GetFriendlyName(window.product);
                window.flow_slug = GetFriendlyName(window.product);
                load_game_js(window.flow_slug);
            }
            else if(data.completed)
            {
                window.product = "completed";
                window.product_slug = GetFriendlyName(window.product);
                window.flow_slug = GetFriendlyName(window.product);
                load_game_js(window.flow_slug);
            }
            else
            {
                window.base_url = getBaseURL();
                window.res_params = data.output.param;
                window.product_link = data.output.url;
                window.pa_PREMIUM_POLICY_TYPE = (window.res_params.flow_data.PREMIUM_POLICY_TYPE)? GetFriendlyName(window.res_params.flow_data.PREMIUM_POLICY_TYPE) : false;
                window.PREFERED_LANG = window.res_params.flow_data.PREFERED_LANG? GetFriendlyName(window.res_params.flow_data.PREFERED_LANG): GetFriendlyName('English');
                window.product = data.output.param.flow_data.PRODUCT;
                window.product_id = window.res_params.flow_data.PRODUCT;
                window.product_slug = GetFriendlyName(window.product_id);
                window.proposal_no = window.res_params.flow_data.PROPOSAL_NUMBER;

                window.prefered_lang_slug = allowed_lang.hasOwnProperty(window.PREFERED_LANG)? allowed_lang[window.PREFERED_LANG] : 'eng';
                window.PREFERED_LANG_NAME = allowed_other_lang.hasOwnProperty(window.PREFERED_LANG)? capitalizeFirstLetter(window.PREFERED_LANG): false;

                window.flow_slug = data.output.param.flow_key;
                window.app_view = data.output.app_view;
                window.link_key = data.output.lkey;
                load_game_js(window.flow_slug);
            }
        }
        else
        {
            window.product = "404";
            window.product_slug = GetFriendlyName(window.product);
            window.flow_slug = GetFriendlyName(window.product);
            load_game_js(window.flow_slug);
        }
    }).fail(function() {
        window.product = "404";
        window.product_slug = GetFriendlyName(window.product);
        window.flow_slug = GetFriendlyName(window.product);
        load_game_js(window.flow_slug);
    }); */
});
