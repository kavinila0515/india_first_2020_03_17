/*
 Main KFD Controller
 */
// Environment
var PROD_ENV = true;
var LOADER_SCREEN = true;

// Product Types
let mConnectProducts = ["indiafirst_cash_back_plan","indiafirst_little_champ_plan","indiafirst_maha_jeevan_plan","indiafirst_money_balance_plan"];
//let mConnectProducts = ["sbilm_smart_bachat","sbilm_smart_elite","sbilm_smart_money_back_gold","sbilm_smart_privilege","sbilm_smart_swadhan_plus","sbilm_smart_wealth_builder","sbilm_saral_insure_wealth_plus","sbilm_saral_maha_anand","sbilm_smart_insure_wealth_plus","sbilm_smart_power_insurance","sbilm_smart_wealth_assure","sbilm_retire_smart","sbilm_saral_pension","sbilm_shudh_nivesh","sbilm_smart_income_protect","sbilm_smart_money_planner","sbilm_smart_women_advantage","sbilm_smart_scholar","sbilm_smart_champ_insurance","sbilm_saral_shield","sbilm_smart_shield","sbilm_flexi_smart_plus"];


let smartAdvisorProducts = ["sbilsa_smart_bachat","sbilsa_smart_elite","sbilsa_smart_money_back_gold","sbilsa_smart_privilege","sbilsa_smart_swadhan_plus","sbilsa_smart_wealth_builder","sbilsa_saral_insure_wealth_plus","sbilsa_saral_maha_anand","sbilsa_smart_insure_wealth_plus","sbilsa_smart_power_insurance","sbilsa_smart_wealth_assure","sbilsa_retire_smart","sbilsa_saral_pension","sbilsa_shudh_nivesh","sbilsa_smart_income_protect","sbilsa_smart_money_planner","sbilsa_smart_women_advantage","sbilsa_smart_scholar","sbilsa_smart_champ_insurance","sbilsa_saral_shield","sbilsa_smart_shield","sbilsa_flexi_smart_plus"];
let planTypeArr = ["single","regular"];
let screenBackBtn = false;

// Phaser Initialize
var XRes = 600, YRes = 800;
var game_canvas_id = "sbilife-canvas";
var StartX  = -1000, count = 0,SfxIndex = 0,offset = 0, sfx_offset = 0,cur_scr = 0,currentTween,currentSound,currentTimer,loaderText;
var game = new Phaser.Game(XRes, YRes, Phaser.CANVAS, 'sbilife-phaser', { preload: preLoad, create: create, render: render, update:update});

var obj_list = new Array();
var obj_but_list = new Array();
var obj_text_list = new Array();
var obj_input_list = new Array();
let text_group,input_group,cust_group,top_group1,top_group2,top_group3,top_group4,top_group5;
var screen_list = [];
var cur_sfx_list  = new Array();
var events_list = [];
var lang = 'eng';
var sysLang = "eng";
var sysFlow = "normal";
var START_SCREEN = 0;
var load_scrn = START_SCREEN;
var translitLangArr = {"eng":"english","tam":"tamil","hin":"hindi","tel":"telugu","mal":"malayalam","kan":"kannada","ben":"bengali","mar":"marathi","guj":"gujarati","pun":"punjabi","ori":"oriya","maw":"marwari","ass":"assamese","miz":"mizo"};
var fontFamilyLangArr = {"eng":"Whitney Book","tam":"Tamil","hin":"Hind","tel":"Telugu","mal":"malayalam","kan":"kannada","ben":"bengali","mar":"marathi","guj":"gujarati","pun":"punjabi","ori":"oriya","maw":"marwari","ass":"assamese","miz":"Calibri"};
var numberSystemLangArr = {"eng":"common","tam":"common","hin":"Hind","tel":"common","mal":"Hind","kan":"common","ben":"common","mar":"common","guj":"common","pun":"common","ori":"common","maw":"common","ass":"common","miz":"common"};
var choosenLangArr = {"eng":"English","tam":"Tamil","hin":"Hindi","tel":"Telugu","mal":"Malayalam","kan":"Kannada","ben":"bengali","mar":"marathi","guj":"gujarati","pun":"punjabi","oriya":"ori","marwari":"maw","assamese":"ass","mizo":"miz"};

// Default for camera
var webcamtext;
var camera_record_status = false;

// Default for SMS OTP
var smsOTPText;
var smsOTP_btn_status = false;
var smsOTPCur = 'M@yjo$';
var smsOTPOk = false;
var smsOTPValidTxt;
var smsOTPValid_btn_status = false;

// Default for rest load
var restLoadText;
var restLoadStatus = false;
var restLoadString;

// Default Face Detect
var intervalFaceDetectCam;
var faceDetectStatus = false;
var faceDetectText;
var faceDStr='', faceDNStr='';
var camera_btn_status = false;

// Input Null Check
var inputNullStr='';
var inputNullTxt;
var edit_btn_status = false;

// Default Light Sensor
var lightcamtext;


// Personal Details - Check Box
var check_status = {'name':true,'email_id':true,'address':true,'dob':true,'phone_no':true};

// Repeatedly Captured Photo Image
var cap_photo_img_append = false;
var cap_consent_img_append = false;
var cap_captured_img_append = false;
var cap_screen_img_append = false;

// Screen Name
let cur_screen_name;

// AutoLoad Screen
let auto_load_enable = true;
let auto_load_scrn_no = 3;

// Camera Error
let cameraErrorPageStatus = true;
let cameraErrorPageNo = 5;
let videoLoadPageNo = 20;

// Photo Load
let imgLoadPageNo = 0;
let imgLoadEnable = false;
let imgRequest = 0;
let intervalImgRequest;
let intervalImgCount=0;

// Disagreement Variables
let disagreement_status = false;
let thankDisPageNo = 0;
let thankNorPageNo = 0;


// Default Font Values
var dbg_color = '#ffffff';
var df_color = '#000000';
var df_size = 27;
var df_weight = 'normal';
var df_align = 'center';
var df_family = 'Whitney Book';
var df_boundsAlignH='left';
var df_wordWrap = false;
var df_wordWrapWidth = 400;


// Default Input Field Values
var di_size = 14;
var di_fill = '#3b3a3a';
var di_weight = 'normal';
var di_width = 150;
var di_height = 14;
var di_padding=0;
var di_placeHolder = 'Enter value ...';
var di_backgroundColor = '#ffffff';
var di_placeHolderColor = '#3b3a3a';
var di_cursorColor = '#3b3a3a';


    // window.up_CUSTOMER_NAME='';
    // window.up_DOB_PH='';
    // window.up_GENDER='';
    // window.up_EMAIL='';
    // window.up_MOBILE_NUMBER=''
    // window.up_IDPROF='';
    // window.up_NOMINEE_NAME='';
    // window.up_ANNUALINCOME='';
    // window.up_address='';     

   //  window.audio='yes';

// Initially Pre-load All Assets
function loadAssets()
{
    (typeof commonAssets === 'function')? commonAssets() : '';
    (typeof customAssets === 'function')? customAssets() : '';
}


//sachin
function sach(data)
{
   if(data=='block')
   {
    document.getElementById('img1').style.display='block';
    console.log("textarea");
   }
   else
   {
    document.getElementById('img1').style.display='none';
   }

}



/*
    Loader Module
 */

function loaderScreenInit()
{
    game.stage.backgroundColor = dbg_color;
    loaderText = game.add.text(XRes/2, YRes/2, '', { fill: df_color });
    loaderText.anchor.set(0.5);
    loadAssets();
    this.game.scale.pageAlignHorizontally = true;
    this.game.scale.pageAlignVertically = true;
    this.game.scale.refresh();
    game.load.onLoadStart.add(loadStart, this);
    game.load.onFileComplete.add(fileComplete, this);
    game.load.onLoadComplete.add(loadComplete, this);
    game.load.start();
}

/*
Load Rest Assets
 */

function loadRestAssetStart()
{
    restLoadStatus = true;
    restLoadString = "Loading assets...";
    restLoadString = transliterateText(restLoadString,translitLangArr[sysLang]);

    restLoadText = game.add.text(XRes/2, YRes/2, restLoadString, {
        font: fontFamilyLangArr[sysLang],
        fontSize: "28px",
        fontWeight: "bold",
        fill: "#000000",
        align: "center"
    });

    restLoadText.anchor.setTo(0.5, 0.5);
    restLoadText.alpha = 0;
    game.add.tween(restLoadText).to( { alpha: 1 }, 500, Phaser.Easing.Linear.None, true, 0, 250, true);

    obj_list.push(restLoadText);
    obj_text_list.push(restLoadText);

}

function loadRestAssetProgress(lprec)
{
    if(lprec === undefined) { lprec =0; }
    var restLoadProgressString = restLoadString + " "+lprec+" % ";
    restLoadText.setText(restLoadProgressString);
}

function loadRestAssetComplete()
{
    restLoadStatus = false;
    restLoadText.setText("");

    if(auto_load_enable)
    {
        goToPage(auto_load_scrn_no);
    }
}

function resetValues()
{
    cap_photo_img_append = false;
    cap_screen_img_append = false;
    cap_consent_img_append = false;
    smsOTPCur = 'M@yjo$';
    smsOTPOk = false;
    cap_captured_img_append = false;
    disagreement_status = false;
    imgLoadEnable = false;
    imgRequest = 0;
    intervalImgCount=0;
}


function loadStart()
{
    $('#'+game_canvas_id).css('pointer-events', 'none');
    loaderText.text="Loading...";

    if(cur_scr==2) { loadRestAssetStart(); }
}

function fileComplete(progress, cacheKey, success, totalLoaded, totalFiles)
{
    loaderText.text = "Loading... "+progress + "%";
    if(cur_scr==2) { loadRestAssetProgress(progress); }
}

function loadComplete()
{
    $('#'+game_canvas_id).css('pointer-events', 'auto');
    loaderText.visible = false;

    if(cur_scr==0) { resetValues(); }
    if(cur_scr==2) { loadRestAssetComplete(); }

    if((load_scrn==0) || (load_scrn==2) && ((cur_scr==0) || (cur_scr==1))) { goScreen(load_scrn); }
}

function preLoad()
{
    // Groups
    text_group = game.add.group();
    input_group = game.add.group();
    top_group1 = game.add.group();
    top_group2 = game.add.group();
    top_group3 = game.add.group();
    top_group4 = game.add.group();
    top_group5 = game.add.group();
    game.world.bringToTop(text_group);

    (LOADER_SCREEN)? loaderScreenInit() : loadAssets();
  // game.load.image('btn_sp_3', 'assets/images/common/btn_sp_3.png');
    //sachin
}

function create()
{
    var image = game.add.image(100, 100, 'btn_sp_3');
    game.canvas.id = game_canvas_id;
    game.stage.backgroundColor = dbg_color;
    if((window.flow_slug=="sbilsa_smart_bachat") || (window.flow_slug=="sbilsa_smart_elite") || (window.flow_slug=="sbilsa_smart_money_back_gold") || (window.flow_slug=="sbilsa_smart_privilege") || (window.flow_slug=="sbilsa_smart_swadhan_plus") || (window.flow_slug=="sbilsa_smart_wealth_builder"))
    {
        game.stage.disableVisibilityChange = true;
    }
    else
    {
        game.stage.disableVisibilityChange = false;
    }
    game.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
    //game.scale.scaleMode = Phaser.ScaleManager.EXACT_FIT;

    //game.scale.fullScreenScaleMode = Phaser.ScaleManager.EXACT_FIT;
    game.add.tileSprite(0, 0, XRes, YRes, 'bg_01');
    game.onPause.add(onGamePause, this);
    game.onResume.add(onGameResume, this);
    game.stage.smoothed = false;
    if((window.flow_slug=="sbilsa_smart_bachat") || (window.flow_slug=="sbilsa_smart_elite") || (window.flow_slug=="sbilsa_smart_money_back_gold") || (window.flow_slug=="sbilsa_smart_privilege") || (window.flow_slug=="sbilsa_smart_swadhan_plus") || (window.flow_slug=="sbilsa_smart_wealth_builder"))
    {
        game.stage.disableVisibilityChange = true;
    }
    else
    {
        game.stage.disableVisibilityChange = false;
    }
    this.game.scale.pageAlignHorizontally = true;
    this.game.scale.pageAlignVertically = true;
    game.input.touch.preventDefault = false;
    this.game.scale.refresh();

    // Full Screen - OnTap
    // (!SHOW_FULL_SCREEN_BUTTON)? game.input.onTap.add(goFull, this): '';

    // Start screen
    (!LOADER_SCREEN)? goScreen(START_SCREEN): '';

//    if(index==14)
//    {
// //      this.image.add('btn_sp_3
//    }


}


function update()
{
    game.world.bringToTop(top_group1);
    game.world.bringToTop(top_group2);
    game.world.bringToTop(top_group3);
    game.world.bringToTop(top_group4);
    game.world.bringToTop(top_group5);
    game.world.bringToTop(input_group);
    game.world.bringToTop(text_group);
}


function onGamePause()
{
    if(currentTween != null)currentTween.pause();
    if(currentSound != null)currentSound.pause();
    if(currentTimer != null)currentTimer.pause();
}

function onGameResume()
{
    if(currentTween != null)currentTween.resume();
    if(currentSound != null)currentSound.resume();
    if(currentTimer != null)currentTimer.resume();
}

// Disable canvas inputs
function canvasInputDisable()
{
    console.log("Fn : canvasInputDisable ");
    $('#'+game_canvas_id).css('pointer-events', 'none');
}

// Enable canvas inputs
function canvasInputEnable()
{
    console.log("Fn : canvasInputEnable ");
    $('#'+game_canvas_id).css('pointer-events', 'auto');
}

// Text styling
function SetupText(obj,txt)
{
    var font_family = (obj.fontFamily)? obj.fontFamily : fontFamilyLangArr[sysLang];
    var font_size = obj.size || df_size;
    var align = obj.align || df_align;
    var weight = obj.weight || df_weight;
    var color = obj.color || df_color;
    var boundsAlign=obj.boundsAlignH || df_boundsAlignH;
    var wordWrap=obj.wordWrap || df_wordWrap;
    var wordWrapWidth=obj.wordWrapWidth || df_wordWrapWidth;

    var fontParams = {
        font: font_family,
        fontSize: font_size,
        fill: color,
        align : align,
        boundsAlignH: boundsAlign,
        boundsAlignV: "top",
        fontWeight: weight,
        wordWrap: wordWrap,
        wordWrapWidth: wordWrapWidth
    };

    var font_obj  = this.game.add.text(obj.sx, obj.sy, txt,fontParams);

    if(obj.lineSpacing)
    {
        font_obj.lineSpacing = obj.lineSpacing;
    }
    if(obj.padding)
    {
        font_obj.padding.set(obj.padding[0],obj.padding[1]);
    }
    // font_obj.setTextBounds(0, 0, XRes,YRes);
    font_obj.setTextBounds(0,0,game.width, game.height);
    font_obj.anchor.setTo(0.5, 0.5);
    font_obj.inputEnabled = false;

    text_group.add(font_obj);

    return font_obj;
}

// Input Field styling
function SetupInputField(obj,txt)
{
    console.log("Text ",txt);
    var font_family = (obj.fontFamily)? obj.fontFamily : fontFamilyLangArr[sysLang];
    var font_size = obj.size || di_size;
    var font = font_size+"px "+font_family;
    var fill = obj.fill || di_fill;
    var fontWeight = obj.fontWeight || di_weight;
    var width = obj.width || di_width;
    var height = obj.height || di_height;
    var padding = obj.padding || di_padding;
    var placeHolder = obj.placeHolder || di_placeHolder;
    var backgroundColor = obj.backgroundColor || di_backgroundColor;
    var placeHolderColor = obj.placeHolderColor || di_placeHolderColor;
    var cursorColor = obj.cursorColor || di_cursorColor;
    var wordWrap=obj.wordWrap || df_wordWrap;
    var wordWrapWidth=obj.wordWrapWidth || df_wordWrapWidth;
    var align = obj.align || df_align;
   

    console.log("backgroundColor : ",backgroundColor);

    var inputParams = {
        font: font,
        fill: fill,
        fontWeight: fontWeight,
        width: width,
        height: height,
        padding: padding,
        placeHolder: placeHolder,
        backgroundColor : backgroundColor,
        placeHolderColor : placeHolderColor,
        cursorColor: cursorColor,
        borderWidth:0,
        borderColor:backgroundColor,
        wordWrap:wordWrap,
        wordWrapWidth:wordWrapWidth,
        align:align


    };

    // console.log("inputParams  : ",inputParams);

    var input_obj  = this.game.add.inputField(obj.sx, obj.sy, inputParams);

    
    if(txt) { input_obj.setText(txt); } else { input_obj.setText(''); }
    if(obj.key) { input_obj.key = obj.key; }
    input_obj.anchor.setTo(0.5, 0.5);

    input_group.add(input_obj);
    console.log("Input  : ",input_obj);

    return input_obj;
}


function LoadAnimation(obj, params)
{
    var anim_name = "default";
    var sprite_name = params.sprite;
    var x = params.x;
    var y = params.y;
    if (!obj)
    {
        obj = game.add.sprite( 0, 0, sprite_name);
        obj.anchor.set(0.5);
        var anim = obj.animations.add(anim_name);
    }
    obj.x = x;
    obj.y = y;
    obj.inputEnabled = true;

    if(params.toTopObj) {
        groupToTop(obj,params.toTopObj);
    }

    return obj;
}


function AddEvent(ev)
{
    events_list[events_list.length] = ev;
}

function PlayVideo(anim)
{
    AddEvent(game.time.events.add(Phaser.Timer.SECOND * anim.delay, function(){
        if(video == null) video = game.add.video(anim.video);
        else video.changeSource(anim.video);
        video.play(false);
        if( videoWorld == null)
            videoWorld = video.addToWorld(anim.x,anim.y,0.5,0.5,1,1); // (x,y,anchorx,anchory,scalex,scaley)
        video.onComplete.add(function(obj){eval(anim.fn);});
    }, this));
}


function ShowButton(anim)
{
    AddEvent(game.time.events.add(Phaser.Timer.SECOND * anim.delay, function(){
        var button = game.add.button(anim.x, anim.y, anim.sprite, function(){eval(anim.onClickFn)});
        if(anim.toTopObj) {
            groupToTop(button,anim.toTopObj);
        }
        if(anim.anchor != null) {
            button.anchor.setTo(anim.anchor[0], anim.anchor[1]);
        }
        obj_list.push(button);
        obj_but_list.push(button);
    }, this));
}

function PlaySpriteAnim(anim)
{
    AddEvent(game.time.events.add(Phaser.Timer.SECOND * anim.delay, function(){

        var temp = LoadAnimation(temp, anim);//.x, anim.y, anim.sprite);

        temp.id = anim.id;
        temp.fn_type = anim.fn_type;
        temp.fn_param = anim.fn_param;
        if(anim.fn_type)
            temp.input.useHandCursor = true;
        temp.play('default', anim.timing, anim.loop);
        if(anim.scale)
            temp.scale.set(anim.scale);

        if(anim.anchor != null) {
            temp.anchor.setTo(anim.anchor[0], anim.anchor[1]);
        }
        else {
            temp.anchor.set(0.5);
        }

        var params = anim.params;
        if(params)
        {
            var type = anim.anim_type;
            var x1 = anim.x, y1 = anim.y, x2 = params[0], y2 = params[1];

            // Move Type
            temp.anchor.setTo(0.5, 0.5);
            if (anim.anim_type == "move")
            {
                game.add.tween(temp).to( { x: x2, y : y2 }, Phaser.Timer.SECOND * anim.timing, anim.tween_type, true, Phaser.Timer.SECOND * anim.delay);
                if(anim.disappear)
                {
                    AddEvent(game.time.events.add(Phaser.Timer.SECOND * anim.disappear, function(){
                        game.add.tween(temp.scale).to( { x: 0, y : 0 }, 200, anim.tween_type, true, Phaser.Timer.SECOND * anim.disappear);
                    }, this));
                }
            }
            else if (anim.anim_type == "ms0xy") // Scale & Move to a specific position & scale value from 0 - x.
            {
                var scale_params = anim.scale_params;
                var scale_x = scale_params[0]? scale_params[0]:1;
                var scale_y = scale_params[1]? scale_params[1]:1;

                temp.scale.set(0);
                game.add.tween(temp.scale).to( {x:scale_x,y:scale_y}, Phaser.Timer.SECOND * anim.timing, anim.tween_type, true, Phaser.Timer.SECOND * anim.delay);

                // Move event
                AddEvent(game.time.events.add(Phaser.Timer.SECOND, function(){

                    game.add.tween(temp).to( { x: x2, y : y2 }, Phaser.Timer.SECOND * anim.timing, anim.tween_type, true, Phaser.Timer.SECOND * anim.delay);

                }, this));

            }
            else if (anim.anim_type == "scale")
            {
                temp.scale.setTo(0,0);
                game.add.tween(temp.scale).to( { x: x2, y : y2 }, Phaser.Timer.SECOND * anim.timing, anim.tween_type, true, Phaser.Timer.SECOND * anim.delay);
                if(anim.disappear)
                {
                    AddEvent(game.time.events.add(Phaser.Timer.SECOND * anim.disappear, function(){
                        game.add.tween(temp.scale).to( { x: 0, y : 0 }, Phaser.Timer.SECOND * anim.timing, anim.tween_type, true);
                    }, this));
                }
            }
            else if (anim.anim_type == "alpha")
            {
                temp.alpha = x2;
                game.add.tween(temp).to( { alpha: y2 }, Phaser.Timer.SECOND * anim.timing,anim.tween_type, true);

            }
            else if (anim.anim_type == "spin")
            {
                var tween = game.add.tween(temp).to( { angle: x2 }, Phaser.Timer.SECOND * anim.timing,anim.tween_type, true);
                if(anim.loop)
                {
                    tween.loop(true);
                    tween.yoyo(true, Phaser.Timer.SECOND * y2);
                }
            }
        }
        else if(anim.disappear)
            game.add.tween(temp.scale).to( { x: 0, y : 0 }, 200, anim.tween_type, true, Phaser.Timer.SECOND * anim.disappear);

        if(anim.onClickFn)
        {
            temp.inputEnabled = true;
            temp.input.useHandCursor = true;
            temp.events.onInputDown.add(function () { eval(anim.onClickFn) }, this);
        }

        obj_list.push(temp);
        schedule_tween(temp);

    }, this));
}

function schedule_tween(obj)
{
    if(!window.stage.screens[cur_scr].tweens) return;
    for(var i = 0; i < window.stage.screens[cur_scr].tweens.length; i++)
    {
        var tween = window.stage.screens[cur_scr].tweens[i];
        if(tween.obj.startsWith(obj.key))
        {
            game.add.tween(obj).to( {x: tween.x, y: tween.y }, tween.timing, tween.tween_type, true, Phaser.Timer.SECOND * tween.delay);
            break;
        }
    }
}

function PlayTextAnim(anim)
{
    var text_toDisplay = "";
    for(var i = 0; i < anim.text.length; i++ )
    {
        if(anim.text[i].content.startsWith("$var."))
        {
            var str1 = anim.text[i].content.slice(5);

            if(str1.startsWith("number_"))
            {
                var orgStr = eval(str1.slice(7));
                if(anim.text[i].enableNumericType)
                {
                    orgStr = formatNumber(orgStr);
                }

                if(anim.text[i].defaultDecimalPart)
                {
                    orgStr = validateDecimalPart(orgStr);
                }

                var str_in_words = numberInWords(orgStr,numberSystemLangArr[sysLang]);
                sfx_offset += str_in_words.length * 0.5;

                text_toDisplay += orgStr;
            }
            else if(str1.startsWith("alphanumeric_"))
            {
                var orgStr = eval(str1.slice(13));
                var str_in_words = strInLetter(orgStr);

                sfx_offset += str_in_words.length * 0.5;
                text_toDisplay += orgStr;
            }
            else
            {
                var orgStr = eval(str1);
                if(anim.text[i].transliterate)
                {
                    if (['hindi', 'tamil', 'telugu'].indexOf(anim.text[i].transliterate) >= 0)
                    {
                        var text_arr = orgStr.split(" ");
                        var transliterate = anim.text[i].transliterate;

                        if(text_arr.length>0)
                        {
                            var transliterate_result = "";
                            for(var j = 0; j < text_arr.length; j++ )
                            {
                                if(isNaN(text_arr[j]))
                                {
                                    transliterate_result += transliterateText(text_arr[j],transliterate);
                                }
                                else
                                {
                                    transliterate_result += text_arr[j];
                                }

                                transliterate_result += " ";
                            }
                            text_toDisplay += transliterate_result;
                        }
                    }
                }
                else
                {
                    text_toDisplay += orgStr;
                }
            }
        }
        else
        {
            var orgStr = anim.text[i].content;
            if(anim.text[i].transliterate)
            {
                if (['hindi', 'tamil', 'telugu'].indexOf(anim.text[i].transliterate) >= 0)
                {
                    var text_arr = orgStr.split(" ");
                    var transliterate = anim.text[i].transliterate;

                    if(text_arr.length>0)
                    {
                        var transliterate_result = "";
                        for(var j = 0; j < text_arr.length; j++ )
                        {
                            if(isNaN(text_arr[j]))
                            {
                                transliterate_result += transliterateText(text_arr[j],transliterate);
                            }
                            else
                            {
                                transliterate_result += text_arr[j];
                            }

                            transliterate_result += " ";
                        }
                        text_toDisplay += transliterate_result;
                    }
                }
            }
            else
            {
                text_toDisplay += orgStr;
            }
        }
    }

    var tween_type = anim.tween_type.toLowerCase();
    var txt = SetupText(anim,text_toDisplay);
    txt.id = anim.id;

    if(anim.anchor != null)
    {
        if(typeof anim.anchor[1] !== undefined)
        {
            txt.anchor.setTo(anim.anchor[0], anim.anchor[1]);
        }
        else
        {
            txt.anchor.setTo(anim.anchor[0], 0);
        }

    }

    if(tween_type.startsWith("typewrite"))
    {
        txt.text = "";
        AddEvent(game.time.events.add(Phaser.Timer.SECOND * anim.delay, function(){
            txt = typeWriter(txt, text_toDisplay, anim.timing);
            obj_list.push(txt);
            obj_text_list.push(txt);
            }, this));
    }
    else
    {
        AddEvent(game.time.events.add(Phaser.Timer.SECOND * anim.delay, function(){
            // if(anim.anchor != null){ txt.anchor.setTo(anim.anchor[0], 0)}
            game.add.tween(txt).to({x:anim.x, y: anim.y}, anim.timing, anim.tween_type, true);
            txt.fn_type = anim.fn_type;
            txt.fn_param = anim.fn_param;
            if(anim.onClickFn)
            {
                txt.inputEnabled = true;
                txt.input.useHandCursor = true;
                txt.events.onInputDown.add(function(){eval(anim.onClickFn)}, this);
            }
            if(anim.fn_type)
            {
                txt.inputEnabled = true;
                txt.input.useHandCursor = true;
            }

            if(anim.disappear)
                game.add.tween(txt.scale).to( { x: 0, y : 0 }, 0.1, anim.tween_type, true, Phaser.Timer.SECOND * anim.disappear);

            obj_list.push(txt);
            obj_text_list.push(txt);
        }, this));
    }

    return txt;
}

// Text typewriting animation
function typeWriter(obj, txt, interval, n,  cb)
{
    n = n || 0;
    cb = cb || null;
    if(n == 0) obj.text = "";
    if(n<txt.length)
    {
        obj.text += txt.charAt(n);
        n++;
        setTimeout(function(){typeWriter(obj, txt, interval, n, cb);game.world.bringToTop(text_group);},interval);
    }
    else
    if (cb != null) cb.call(this);
    return obj;
}

function PlayInputAnim(anim)
{
    var text_toDisplay = "";
    for(var i = 0; i < anim.text.length; i++ )
    {
        if(anim.text[i].content.startsWith("$var."))
        {
            var str1 = anim.text[i].content.slice(5);

            if(str1.startsWith("number_"))
            {
                var orgStr = eval(str1.slice(7));
                if(anim.text[i].enableNumericType)
                {
                    orgStr = formatNumber(orgStr);
                }

                if(anim.text[i].defaultDecimalPart)
                {
                    orgStr = validateDecimalPart(orgStr);
                }

                var str_in_words = numberInWords(orgStr,numberSystemLangArr[sysLang]);
                sfx_offset += str_in_words.length * 0.5;

                text_toDisplay += orgStr;
            }
            else if(str1.startsWith("alphanumeric_"))
            {
                var orgStr = eval(str1.slice(13));
                var str_in_words = strInLetter(orgStr);

                sfx_offset += str_in_words.length * 0.5;
                text_toDisplay += orgStr;
            }
            else
            {
                var orgStr = eval(str1);
                if(anim.text[i].transliterate)
                {
                    if (['hindi', 'tamil', 'telugu'].indexOf(anim.text[i].transliterate) >= 0)
                    {
                        var text_arr = orgStr.split(" ");
                        var transliterate = anim.text[i].transliterate;

                        if(text_arr.length>0)
                        {
                            var transliterate_result = "";
                            for(var j = 0; j < text_arr.length; j++ )
                            {
                                if(isNaN(text_arr[j]))
                                {
                                    transliterate_result += transliterateText(text_arr[j],transliterate);
                                }
                                else
                                {
                                    transliterate_result += text_arr[j];
                                }

                                transliterate_result += " ";
                            }
                            text_toDisplay += transliterate_result;
                        }
                    }
                }
                else
                {
                    text_toDisplay += orgStr;
                }
            }
        }
        else
        {
            var orgStr = anim.text[i].content;
            if(anim.text[i].transliterate)
            {
                if (['hindi', 'tamil', 'telugu'].indexOf(anim.text[i].transliterate) >= 0)
                {
                    var text_arr = orgStr.split(" ");
                    var transliterate = anim.text[i].transliterate;

                    if(text_arr.length>0)
                    {
                        var transliterate_result = "";
                        for(var j = 0; j < text_arr.length; j++ )
                        {
                            if(isNaN(text_arr[j]))
                            {
                                transliterate_result += transliterateText(text_arr[j],transliterate);
                            }
                            else
                            {
                                transliterate_result += text_arr[j];
                            }

                            transliterate_result += " ";
                        }
                        text_toDisplay += transliterate_result;
                    }
                }
            }
            else
            {
                text_toDisplay += orgStr;
            }
        }
    }

    var tween_type = anim.tween_type.toLowerCase();
    var inputField = SetupInputField(anim,text_toDisplay);
    inputField.id = anim.id;

    if(anim.anchor != null)
    {
        if(typeof anim.anchor[1] !== undefined)
        {
            inputField.anchor.setTo(anim.anchor[0], anim.anchor[1]);
        }
        else
        {
            inputField.anchor.setTo(anim.anchor[0], 0);
        }

    }

    AddEvent(game.time.events.add(Phaser.Timer.SECOND * anim.delay, function(){
        // if(anim.anchor != null){ txt.anchor.setTo(anim.anchor[0], 0)}
        game.add.tween(inputField).to({x:anim.x, y: anim.y}, anim.timing, anim.tween_type, true);
        inputField.fn_type = anim.fn_type;
        inputField.fn_param = anim.fn_param;

        if(anim.fn_type)
        {
            inputField.inputEnabled = true;
            inputField.input.useHandCursor = true;
        }

        if(anim.disappear)
            game.add.tween(inputField.scale).to( { x: 0, y : 0 }, 0.1, anim.tween_type, true, Phaser.Timer.SECOND * anim.disappear);

        obj_list.push(inputField);
        obj_input_list.push(inputField);
    }, this));

    return inputField;
}


function OnStopCB(v)
{
    return function() { if (cur_sfx_list[v + 1]) {  currentSound = cur_sfx_list[v + 1].play(); } };

}

function onSoundStop()
{
    console.log("Fn : onSoundStop");
   // canvasInputEnable();
}

function onSoundPlay()
{
    console.log("Fn : onSoundPlay");
   // canvasInputDisable();
}


function PlaySound(anim)
{
    cur_sfx_list.length = 0;
    for(var i = 0; i < anim.sound.length; i++)
    {
        if(anim.sound[i].startsWith("$var."))
        {
            var str1 = anim.sound[i].slice(5);

            if(str1.startsWith("number_"))
            {
                var str2 = str1.slice(7);
                var res = numberInWords(eval(str2),numberSystemLangArr[sysLang]);
                for(var j = 0; j < res.length; j++)
                {
                    (res[j])? cur_sfx_list.push(game.add.audio(res[j])):'';
                }
            }else if(str1.startsWith("currency_"))
            {
                var str2 = str1.slice(9);
                var res = currencyInWords(eval(str2),numberSystemLangArr[sysLang]);
                for(var j = 0; j < res.length; j++)
                {
                    (res[j])? cur_sfx_list.push(game.add.audio(res[j])):'';
                }
            }else if(str1.startsWith("alphanumeric_"))
            {
                console.log("Str Sound : alphanumeric_");
                var str2 = str1.slice(13);
                var strValue = (str2.startsWith("window."))? eval(str2) : str2;
                var res = strInLetter(strValue);
                for(var j = 0; j < res.length; j++)
                {
                    (res[j])? cur_sfx_list.push(game.add.audio(res[j])):'';
                }
            }else if(str1.startsWith("dateStr_"))
            {
                var str2 = str1.slice(8);
                var res = strInDate(eval(str2));
                for(var j = 0; j < res.length; j++)
                {
                    (res[j])? cur_sfx_list.push(game.add.audio(res[j])):'';
                }
            }
            else if(str1.startsWith("dateMonthStr_"))
            {
                var str2 = str1.slice(13);
                var res = strInDateMonth(eval(str2));
                for(var j = 0; j < res.length; j++)
                {
                    (res[j])? cur_sfx_list.push(game.add.audio(res[j])):'';
                }
            }
            else
            {
                var audioName = eval(str1);
                cur_sfx_list.push(game.add.audio(audioName));
            }
        }
        else
        {
            cur_sfx_list.push(game.add.audio(anim.sound[i]));
        }
    }

    console.log("cur_sfx_list.length ",cur_sfx_list.length);
    if(cur_sfx_list.length == 0) return;
    var v = 0;

    for(var i = 0; i <= cur_sfx_list.length - 1; i++)
    {
        console.log("cur_sfx_list for : ",i);
        v = i;

        if(cur_scr>0)
        {
            cur_sfx_list[i].onPlay.add(onSoundPlay);
            cur_sfx_list[i].onStop.add(onSoundStop);
        }
        cur_sfx_list[i].onStop.add(OnStopCB(i));
    }
    if (cur_sfx_list.length > 0){
        currentSound = cur_sfx_list[0].play();
    }
}


// If need to play a specific screen directly
function actionOnClick(scr_no)
{
    CleanUp();
    StartPage(scr_no);
    ShowScreen();
}

function actionOnClickDelay(scr_no,delay)
{
    canvasInputDisable();
    if(cur_scr===0) {
        cur_sfx_list[0].play();
    }
    setTimeout(function(){ canvasInputEnable(); goToPage(scr_no); }, 1000*delay);
   // canvasInputEnable();
}

function actionOnClickDelay2(delay)
{
    canvasInputDisable();
    if(cur_scr===0) {
        cur_sfx_list[0].play();
    }
    setTimeout(function(){ canvasInputEnable();}, 1000*delay);
}


function goToPage(scr_no)
{
    CleanUp();
    StartPage(scr_no);
    ShowScreen();
}

function goToPage55(scr_no,delay)
{
  console.log("gotooooooooodddddddddddddddddddd",delay);
  setTimeout(function(){ goToPage(scr_no); }, 1000*delay);
    // CleanUp();
    // StartPage(scr_no);
    // ShowScreen();
   
}

function goToPageBack(scr_no)
{
    CleanUp();
    StartPage(scr_no);
    ShowScreen();
    screenBackBtn = true;
}

var illness,medicine,period;
function editGoToPage(scr_no,xIN,yIN,eKey='')
{
   
 console.log("cur_scr", cur_scr);
if(scr_no==14 && cur_scr==12)
{
illness=obj_input_list[0].value;
period=obj_input_list[1].value;
medicine=obj_input_list[2].value;


if(illness !='' && medicine !='' && period !='')
{
illness_update();
console.log("illness medicine period",illness ,medicine, period);
}
}

if(cur_scr==10)
{
disagree_state=obj_input_list[0].value;
console.log("disagree_state",disagree_state);
yesno_update(disagree_state,'Plan Details - edit');
}
    if(edit_btn_status) {
        return;
    }
      
    edit_btn_status = true;
    let obj_list_count = 0;
    let input_null_val = false;
    let edit_obj_list = {};
 
    if(obj_input_list.length > 0)
    {
        let obj_input_list_len = obj_input_list.length;
        //console.log("obj_input_list[0]['in_add1']"+obj_input_list[7].value);
        if(obj_input_list.length>5)
        {
	        if(obj_input_list[7].value=='' && obj_input_list[8].value=='' && obj_input_list[9].value=='' && obj_input_list[10].value=='')
	        {
	            input_null_val = true;
	        }
	    }
        obj_input_list.forEach(
            function(item, index){
                console.log("Index : ",index," == Key == ",item['key'], " == Value == ",item['value']);
                edit_obj_list[item['key']]=item['value'];
                let keyarr = new Array(7,8,9,10);
                if(keyarr.includes(index)==false)
                {
                if(item['value']=='' || item['value']== null) { input_null_val = true; console.log("Null inputfield Key Name",item['key']); }
                }

                obj_list_count++;

             // var address = obj_input_list['in_add1']+obj_input_list['in_add2']+obj_input_list['in_add3']+obj_input_list['in_add4'];
             
              console.log('chk sathiz:');
                
                if(obj_list_count == obj_input_list_len )
                {
                    if(input_null_val)
                    {
                        inputNullStr = 'Provide input values';
                        inputNullStr = transliterateText(inputNullStr,translitLangArr[sysLang]);

                        inputNullTxt = game.add.text(xIN, yIN, inputNullStr, {
                            font: fontFamilyLangArr[sysLang],
                            fontSize: "14px",
                            fontWeight: "bold",
                            fill: "#ff0000",
                            align: "center"
                        });

                        text_group.add(inputNullTxt);

                        inputNullTxt.anchor.setTo(0, 0);

                        AddEvent(game.time.events.add(0.1, function(){
                            var inputTxtTween = game.add.tween(inputNullTxt).to({alpha: 0}, 5000, Phaser.Easing.Linear.None, true);
                            inputTxtTween.onComplete.add(function(){
                                console.log("inputTxtTween : onComplete");
                                edit_btn_status = false;
                            },this);
                        }, this));

                        obj_list.push(inputNullTxt);
                        obj_text_list.push(inputNullTxt);

                    }
                    else
                    {
                        if(eKey!=''){ updateEditLinkResponse(eKey,cur_screen_name,edit_obj_list); }
                        edit_btn_status = false;
                        goToPage(scr_no);
                     showvalue();

                      

                    }
                }
            }
        );
    }
}

function input_animations(scr_no,xIN,yIN,eKey='',cKey='')
{
    if(edit_btn_status) {
        return;
    }
    edit_btn_status = true;
    let obj_list_count = 0;
    let input_null_val = false;
    let edit_obj_list = {};
    if(obj_input_list.length > 0)
    {
        let obj_input_list_len = obj_input_list.length;

        console.log("please check: "+ obj_input_list);
        obj_input_list.forEach(
            function(item, index){
                console.log("Index : ",index," == Key == ",item['key'], " == Value == ",item['value']);
                edit_obj_list[item['key']]=item['value'];
                if(item['value']=='') { input_null_val = true; }
                obj_list_count++;
                if(obj_list_count == obj_input_list_len)
                {
                    if(input_null_val)
                    {
                        inputNullStr = 'Provide input values';
                        inputNullStr = transliterateText(inputNullStr,translitLangArr[sysLang]);

                        inputNullTxt = game.add.text(xIN, yIN, inputNullStr, {
                            font: fontFamilyLangArr[sysLang],
                            fontSize: "14px",
                            fontWeight: "bold",
                            fill: "#ff0000",
                            align: "center"
                        });

                        text_group.add(inputNullTxt);

                        inputNullTxt.anchor.setTo(0, 0);

                        AddEvent(game.time.events.add(0.1, function(){
                            var inputTxtTween = game.add.tween(inputNullTxt).to({alpha: 0}, 5000, Phaser.Easing.Linear.None, true);
                            inputTxtTween.onComplete.add(function(){
                                console.log("inputTxtTween : onComplete");
                                edit_btn_status = false;
                            },this);
                        }, this));

                        obj_list.push(inputNullTxt);
                        obj_text_list.push(inputNullTxt);

                    }
                    else
                    {
                        if(eKey!=''){ updateEditLinkResponse(eKey,cur_screen_name,edit_obj_list); }
                        if(cKey!=''){ updateLinkResponse(cKey,cur_screen_name,0); }
                        edit_btn_status = false;
                       goToPage(scr_no);

                    }
                }
            }
        );
    }







}

function ShowScreen()
{
    sfx_offset = 0;
    game.sound.stopAll();
    if(window.stage.screens.count <= 0)
        return;
    if(window.stage.screens[cur_scr].condition != null)
    {
        var cond = eval(window.stage.screens[cur_scr].condition) ;
        if(eval(window.stage.screens[cur_scr].condition) == false)
        {
            TransitScreen();
            return;
        }
    }

    // Videos
    if (window.stage.screens[cur_scr].video != null)
        for(var i = 0; i < window.stage.screens[cur_scr].video.length; i++)
            PlayVideo(window.stage.screens[cur_scr].video[i]);

    // Buttons
    if(window.stage.screens[cur_scr].buttons != null)
        for(var i = 0; i < window.stage.screens[cur_scr].buttons.length; i++)
            ShowButton(window.stage.screens[cur_scr].buttons[i]);

    // Sprite Animations
    if (window.stage.screens[cur_scr].sprite_animations != null)
        for(var i = 0; i < window.stage.screens[cur_scr].sprite_animations.length; i++)
            PlaySpriteAnim(window.stage.screens[cur_scr].sprite_animations[i]);

    // Text Animations
    if (window.stage.screens[cur_scr].text_animations  != null)
        for(var i = 0; i < window.stage.screens[cur_scr].text_animations.length; i++)
            PlayTextAnim(window.stage.screens[cur_scr].text_animations[i]);

    // Input Field Animations
    if (window.stage.screens[cur_scr].input_animations  != null)
        for(var i = 0; i < window.stage.screens[cur_scr].input_animations.length; i++)
            PlayInputAnim(window.stage.screens[cur_scr].input_animations[i]);

    // Sound
    if (window.stage.screens[cur_scr].sound_list != null)
        for(var i = 0; i < window.stage.screens[cur_scr].sound_list.length; i++)
            PlaySound(window.stage.screens[cur_scr].sound_list[i]);

    // Functions
    if(window.stage.screens[cur_scr].functions  != null)
        for(var i = 0; i < window.stage.screens[cur_scr].functions.length; i++)
            call_fn(window.stage.screens[cur_scr].functions[i]);

    // Screen Names
    if(window.stage.screens[cur_scr].name  != null)
        cur_screen_name = window.stage.screens[cur_scr].name;

    if (window.stage.screens[cur_scr].timing >= 0)
        game.time.events.add(Phaser.Timer.SECOND * (window.stage.screens[cur_scr].timing + sfx_offset), TransitScreen, this);
}

function CleanUp()
{
    for(var i = 0; i < obj_but_list.length; i++)
    {
        obj_but_list[i].x = -game.width * 2;
        obj_but_list[i]= null;
    }
    for(var i = 0; i < obj_text_list.length; i++)
    {
        obj_text_list[i].x = -game.width * 2;
        obj_text_list[i]= null;
    }
    for(var i = 0; i < obj_input_list.length; i++)
    {
        obj_input_list[i].x = -game.width * 2;
        obj_input_list[i]= null;
    }
    for(var i = 0; i < obj_list.length; i++)
    {
        obj_list[i].x = -game.width * 2;
        obj_list[i] = null;
    }
    for(var i = 0; i < events_list.length; i++)
    {
        game.time.events.remove(events_list[i]);
    }

    obj_but_list = obj_but_list.filter(function (el) {
        return el != null;
    });

    obj_text_list = obj_text_list.filter(function (el) {
        return el != null;
    });

    obj_input_list = obj_input_list.filter(function (el) {
        return el != null;
    });

    obj_list = obj_list.filter(function (el) {
        return el != null;
    });

    if(currentSound!=null)
        currentSound.pause();
    cur_sfx_list.length = 0;     //audio not suffel


    screenBackBtn = false;
}

function TransitScreen()
{
    CleanUp();
    cur_scr = ++cur_scr % window.stage.screens.length;
    ShowScreen();
}

function ReloadScreen()
{
    CleanUp();
    ShowScreen();
}

function prevScreen()
{
    CleanUp();
    cur_scr = cur_scr - 1;
    if (cur_scr  < 0)
    {
        cur_scr = 0;
        return;
    }
    ShowScreen();
}

function goScreen(scr_no)
{
    CleanUp();

    if(scr_no>0)
    {
        if((scr_no+1)<=window.stage.screens.length)
        {
            cur_scr = scr_no;
            ShowScreen();
        }
        else
        {
            cur_scr = 0;
            ShowScreen();
        }
    }
    else
    {
        cur_scr = 0;
        ShowScreen();
    }
}

function nextScreen()
{
    CleanUp();
    cur_scr = ++cur_scr % window.stage.screens.length;
    ShowScreen();
}



/*
    Show Full Screen
 */

function goFull()
{
    if(game.scale.isFullScreen)
    {
        game.scale.stopFullScreen();
    }
    else
    {
        game.scale.startFullScreen(false);
    }
}


// Custom group to Top
function groupToTop(obj,i)
{
    if(i==1) { top_group1.add(obj); game.world.bringToTop(top_group1);}
    else if(i==2) { top_group2.add(obj); game.world.bringToTop(top_group2);}
    else if(i==3) { top_group3.add(obj); game.world.bringToTop(top_group3);}
    else if(i==4) { top_group4.add(obj); game.world.bringToTop(top_group4);}
    else if(i==5) { top_group5.add(obj); game.world.bringToTop(top_group5);}
}

function buttonVisibility(status)
{
    if(status == true)
    {
        for(var i = 0; i < obj_but_list.length; i++)
        {
            obj_but_list[i].alpha = 1;
        }
    }
    else if(status == false)
    {
        for(var i = 0; i < obj_but_list.length; i++)
        {
            obj_but_list[i].alpha = 0;
        }
    }

}

function textVisibility(status)
{
    if(status == true)
    {
        for(var i = 0; i < obj_text_list.length; i++)
        {
            obj_text_list[i].alpha = 1;
        }
    }
    else if(status == false)
    {
        for(var i = 0; i < obj_text_list.length; i++)
        {
            obj_text_list[i].alpha = 0;
        }
    }

}

function inputFieldVisibility(status)
{
    if(status == true)
    {
        for(var i = 0; i < obj_input_list.length; i++)
        {
            obj_input_list[i].alpha = 1;
        }
    }
    else if(status == false)
    {
        for(var i = 0; i < obj_input_list.length; i++)
        {
            obj_input_list[i].alpha = 0;
        }
    }

}


function transliterateText(str,transLang)
{
    var transLangStatus = true;
    var transStrResult = '';
    if(transLang=='hindi')
    {
        pramukhIME.addKeyboard(PramukhIndic, 'hindi');
    }
    else if(transLang=='tamil')
    {
        pramukhIME.addKeyboard(PramukhIndic, 'tamil');
    }
    else if(transLang=='telugu')
    {
        pramukhIME.addKeyboard(PramukhIndic, 'telugu');
    }
    else if(transLang=='malayalam')
    {
        pramukhIME.addKeyboard(PramukhIndic, 'malayalam');
    }
    else if(transLang=='kannada')
    {
        pramukhIME.addKeyboard(PramukhIndic, 'kannada');
    }
    else if(transLang=='bengali')
    {
        pramukhIME.addKeyboard(PramukhIndic, 'bengali');
    }
    else if(transLang=='marathi')
    {
        pramukhIME.addKeyboard(PramukhIndic, 'marathi');
    }
    else if(transLang=='gujarati')
    {
        pramukhIME.addKeyboard(PramukhIndic, 'gujarati');
    }
    else if(transLang=='punjabi')
    {
        pramukhIME.addKeyboard(PramukhIndic, 'punjabi');
    }
    else
    {
        transLangStatus = false;
    }


    if(transLangStatus)
    {
        str = (str)? str.toLowerCase() : '';
        transStrResult = pramukhIME.convert(str);
    }
    else
    {
        transStrResult = str;
    }

    console.log("transStrResult - fn -: ",transStrResult," == ",transLang);

    return transStrResult;
}

function init()
{
    setProductParams();
}
function call_fn(func){ AddEvent(game.time.events.add(Phaser.Timer.SECOND * func.delay, function(){eval(func.fn)}, this));}
function SetBGColor(color){ game.stage.backgroundColor = color;}
function SetBGTile(bg_sprite){ game.add.tileSprite(0, 0, XRes, YRes, bg_sprite); }
function StartPage(num) { cur_scr = num;}
String.prototype.startsWith = function (str){ return this.indexOf(str) == 0;};
function find(str, sub){ if(str.indexOf(sub) == -1) { return false; } else { return true;} }

/*
 Debug Modules
 */

function render()
{
    if(!PROD_ENV)
    {
        // Input debug info
        game.debug.inputInfo(32, 32);
        //game.debug.spriteInputInfo(sprite, 32, 130);
        game.debug.pointer( game.input.activePointer );
    }
}

function addPlugins()
{
    console.log("Add Plugin Function");
    this.game.add.plugin(PhaserInput.Plugin);
}

/*
Custom Functions
 */

function download_local_file(file)
{
    var furl = "assets/files/download/"+file;
    var ext = file.substr(file.lastIndexOf('.') + 1);
    if((ext!==undefined) && (ext!==''))
    {
        ext = ext.toLowerCase();
        var allowed_ext = {"pdf":"application/pdf","doc":"application/msword","docx":"application/vnd.openxmlformats-officedocument.wordprocessingml.document"};

        if(allowed_ext[ext]!==undefined)
        {
            var x=new XMLHttpRequest();
            x.open("GET", furl, true);
            x.responseType = 'blob';
            x.onload=function(e){download(x.response, file, allowed_ext[ext] ); };
            x.send();
        }
        else
        {
            return false;
        }

    }
    else
    {
        return false;
    }
}

function download_pdf_url(furl)
{
    if(furl) { furl = (furl.startsWith("window."))? eval(furl) : furl; }

    if(furl)
    {
        var file_name = window.product_slug+".pdf";
        var x=new XMLHttpRequest();
        x.open("GET", furl, true);
        x.responseType = 'blob';
        x.onload=function(e){download(x.response, file_name, "application/pdf" ); };
        x.send();
    }
}

function open_url(url_link)
{
    if((url_link!==undefined) && (url_link!==''))
    {
        window.open(url_link,'_blank');
    }
    else
    {
        return false;
    }
}


/*
    Lang Related functions
 */
function specLoad(sysLang)
{
    var lang_flow_fpath = './assets/product_assets/'+window.flow_slug+'/js/flow_'+sysLang+'_normal.js';
        $.getScript(lang_flow_fpath,function(){
            console.log('check sathiz');
        });
}

function loadLangFlow(setLang,setFlow,setScreen)
{
    sysLang = (setLang)? setLang : sysLang;
    sysFlow = (setFlow)? setFlow : sysFlow;
    load_scrn = (setScreen)? setScreen : 2;
    var lang_assets_fpath = './assets/product_assets/'+window.flow_slug+'/js/flow_assets_'+sysLang+'_'+sysFlow+'.js';
    var lang_flow_fpath = './assets/product_assets/'+window.flow_slug+'/js/flow_'+sysLang+'_'+sysFlow+'.js';

    var camera_utility_fpath = './assets/js/common/camera/camera_utility.js';
    var recordRTC_fpath = './assets/js/common/camera/RecordRTC.js';
    var adapter_fpath = './assets/js/common/camera/adapter-latest.js';

    var facedetection_fpath = './assets/js/common/camera/face/jquery.facedetection.min.js';

    var inputPlugin_fpath = './assets/js/common/input/phaser-input.js';

    $.getScript(camera_utility_fpath,function(){
       console.log("Camera utility file is loaded");
        $.getScript(adapter_fpath,function(){
            console.log("RecordRTC file is loaded");
            $.getScript(recordRTC_fpath,function(){
                console.log("adapter file is loaded");

                $.getScript(facedetection_fpath,function(){
                    console.log("Face detection path = "+facedetection_fpath)
                    console.log("Face detection file is loaded");
                });
                $.getScript(inputPlugin_fpath,function(){
                    console.log("Input plugin path = "+inputPlugin_fpath);
                    console.log("Input Plugin file is loaded");
                    addPlugins();
                });
            });
        });
    });

    $.getScript(lang_assets_fpath, function()
    {
        $.getScript( lang_flow_fpath,  function(){
            $.when(langAssets()).then(function(){
                game.load.start();
            });
        });
    });

}
function DateCon(dt)
{
    var date=dt.split('-');
    return date[2]+'-'+date[1]+'-'+date[0];
}
/*
Product Related Custom functions
 */
function setProductParams()
{
   
  //alert(window.product_slug);
 if( (mConnectProducts.includes(window.flow_slug)))
    {
        cameraErrorPageNo = 18;
        var fl_data=window.res_params;
        window.p_product = window.product;
        window.p_plan = window.product;
        window.p_plan_slug = window.product_slug;
        window.p_flow_slug = window.product_slug;
        window.p_name = fl_data.CLIENT_NAME;
        window.p_CUSTOMER_NAME = fl_data.CLIENT_NAME;
        window.p_PROPOSAL_NUMBER = fl_data.APP_NO;
        window.p_DOB_PH = DateCon(fl_data.DOB);
        window.p_GENDER = fl_data.GENDER;
        window.p_EMAIL = fl_data.CLIENT_EMAIL_ID;
        window.p_MOBILE_NUMBER = fl_data.MOBILE_NO;
        window.p_IDPROF='Aadhar Card';
        window.p_ANNUALINCOME=fl_data.ANNUAL_INCOME;
        window.p_address = fl_data.ADDRESS;
       window.p_NOMINEE_NAME = fl_data.NOMINEE_NAME;

        window.up_CUSTOMER_NAME='';
        window.up_DOB_PH='';
        window.up_GENDER='';
        window.up_EMAIL='';
        window.up_MOBILE_NUMBER=''
        window.up_IDPROF='';
        window.up_NOMINEE_NAME='';
        window.up_ANNUALINCOME='';
        window.up_address=''; 

        window.add1='';
        window.add2='';
        window.add3='';
        window.add4='';
          
    
        window.p_planname=fl_data.PRODUCT_CODE;
        window.p_paymentFre=fl_data.MODE_OF_PREMIUM;
        window.p_payingTerm=fl_data.PREM_TERM;
        window.p_policyTerm=fl_data.POLICY_TERM;
        window.p_Assuredsum=fl_data.SUM_ASSURE;
        window.p_premiumamount=fl_data.PREMIUM;

        window.audio="yes"; 
        window.currency ='rupee';
         window.pa_paymentFre="Monthly";

        var address_full = [];
        (window.p_MAILINGADDRESS3)? address_full.push(" "+window.p_MAILINGADDRESS3):'';
        (window.p_MAILINGCITY && window.p_MAILINGPINCODE)? address_full.push(" "+window.p_MAILINGCITY+" - "+window.p_MAILINGPINCODE):'';
        (window.p_MAILINGSTATE)? address_full.push(" "+window.p_MAILINGSTATE):'';
        window.p_MAILINGADDRESSR = address_full.join();
    }
 
}

function getGeoLocation()
{
    console.log("Location Finished : fn ");
    if(navigator.geolocation)
    {
        console.log("Location Finished : geolocation ");
        navigator.geolocation.getCurrentPosition(function(pos){
            console.log("Location Finished : geolocation - pos");
            window.geo_latitude = pos.coords.latitude;
            window.geo_longitude = pos.coords.longitude;
        });
    }
}

function getGeoLocationText()
{
    console.log("Fn : getGeoLocationText ");

    let kfd_get_geoloc_url = window.kfd_api_url+'api/pivc/getGeoLocationAddress';

    let get_geoloc_params = {
        "sbil_key":(window.link_key)? window.link_key : '',
        "sbil_geo_lat":window.geo_latitude,
        "sbil_geo_long":window.geo_longitude
    };

    let jq_get_geoloc_res = $.post(kfd_get_geoloc_url, get_geoloc_params, function(data) { }, 'json');

    jq_get_geoloc_res.done(function(data){
        console.log(JSON.stringify(data));
        if(data.status)
        {
            console.log("getGeoLocationText Response Data : ",data.msg);
            window.geo_location = data.output.address;
        }
    });

}

function productInit()
{
    getGeoLocation();
}

function pd_checkbox_show()
{
    console.log("Fn : pd_checkbox_show");
    for(var sKey in check_status)
    {
        check_status[sKey] = true;
    }

    var chb_btn1_count = 0;
    var chb_btn1 = game.add.button(770, 90, 'agree_btn_01', function(){
        chb_btn1_count++;
        if(chb_btn1_count%2){
            console.log("Check - Yes");
            check_status.name = true;
            chb_btn1.setFrames(0,0,0,0);
        }
        else
        {
            console.log("Check - No");
            check_status.name = false;
            chb_btn1.setFrames(1,1,1,1);
        }
    }, 0, 0, 0, 0);
    chb_btn1.anchor.setTo(0.5, 0.5);
    obj_list.push(chb_btn1);

    var chb_btn2_count = 0;
    var chb_btn2 = game.add.button(770, 135, 'agree_btn_01', function(){
        chb_btn2_count++;
        if(chb_btn2_count%2){
            check_status.email_id = true;
            chb_btn2.setFrames(0,0,0,0);
        }
        else
        {
            check_status.email_id = false;
            chb_btn2.setFrames(1,1,1,1);
        }
    }, 0, 0, 0, 0);
    chb_btn2.anchor.setTo(0.5, 0.5);
    obj_list.push(chb_btn2);

    var chb_btn3_count = 0;
    var chb_btn3 = game.add.button(770, 180, 'agree_btn_01', function(){
        chb_btn3_count++;
        if(chb_btn3_count%2){
            check_status.address = true;
            chb_btn3.setFrames(0,0,0,0);
        }
        else
        {
            check_status.address = false;
            chb_btn3.setFrames(1,1,1,1);
        }
    }, 0, 0, 0, 0);
    chb_btn3.anchor.setTo(0.5, 0.5);
    obj_list.push(chb_btn3);

    var chb_btn4_count = 0;
    var chb_btn4 = game.add.button(770, 225, 'agree_btn_01', function(){
        chb_btn4_count++;
        if(chb_btn4_count%2){
            check_status.dob = true;
            chb_btn4.setFrames(0,0,0,0);
        }
        else
        {
            check_status.dob = false;
            chb_btn4.setFrames(1,1,1,1);
        }
    }, 0, 0, 0, 0);
    chb_btn4.anchor.setTo(0.5, 0.5);
    obj_list.push(chb_btn4);

    var chb_btn5_count = 0;
    var chb_btn5 = game.add.button(770, 270, 'agree_btn_01', function(){
        chb_btn5_count++;
        if(chb_btn5_count%2){
            check_status.phone_no = true;
            chb_btn5.setFrames(0,0,0,0);
        }
        else
        {
            check_status.phone_no = false;
            chb_btn5.setFrames(1,1,1,1);
        }
    }, 0, 0, 0, 0);
    chb_btn5.anchor.setTo(0.5, 0.5);
    obj_list.push(chb_btn5);

}

function pd_submit(go_scrn,fgo_scrn)
{
    console.log("Fn : pd_submit");
    console.log("Check Status : ",check_status);

    captureScreen();
    setTimeout(function(){ pd_submit_fns(go_scrn,fgo_scrn); }, 1000);
}

function pd_submit_fns(go_scrn,fgo_scrn)
{
    console.log("Fn : pd_submit_fns");
    console.log("Check Status : ",check_status);

    var pd_status = true;

    for(var sKey in check_status)
    {
        if(!check_status[sKey])
        {
            pd_status = false;
        }
    }

    post_pd_response();

    if(pd_status){
        goToPage(go_scrn)
    }
    else
    {
        goToPage(fgo_scrn)
    }
}

function post_pd_response()
{
    console.log("Fn : post_pd_response - start");

    var check_status_str = JSON.stringify(check_status);
    var kfd_post_pd_res_url = window.kfd_api_url+'api/data/post_pd_response';

    var post_pd_res_params = {
        "sbi_kfd_link":(window.product_link)? window.product_link : '',
        "sbi_pd_res_data":(check_status_str)? check_status_str : ''
    };

    var jq_post_pd_res = $.post(kfd_post_pd_res_url, post_pd_res_params, function(data) { }, 'json');

    jq_post_pd_res.done(function(data){
        console.log(JSON.stringify(data));
        if(data.status)
        {
            console.log("Post PD Response Data : ",data.msg);
        }
    });

}

/*
Photo Capture Functions
 */
function onPhotoAgree(nxt_scrn)
{
    console.log("Photo Agree");
    if(!faceDetectStatus) {
        console.log("Face detect status is false");
        return;
    }

    buttonVisibility(false);
    textVisibility(false);
    takePhoto(nxt_scrn);

}
function onPhotoCancel(nxt_scrn)
{
    console.log("Photo Cancel");
    faceDetectDisable();
    goToPage(nxt_scrn);
}

function downloadImageJPG(furl,fname)
{
    var x=new XMLHttpRequest();
    x.open("GET", furl, true);
    x.responseType = 'blob';
    x.onload=function(e){download(x.response,fname, "image/jpeg" ); };
    x.send();
}

function takePhoto(scrn_no)
{
    console.log("Take Photo");
    imgLoaderEnable();
    game.time.events.add(Phaser.Timer.SECOND * 0.3, function()  {

        let dataURL = getImgDataURL();

        let image_save_script_url = window.kfd_api_url+'api/data/add_consent_image';
        let image_save_script_params = {
            "sbi_kfd_img":dataURL,
            "sbi_kfd_link":(window.product_link)? window.product_link : '',
            "sbi_media_append":Boolean(cap_photo_img_append),
            "sbi_kfd_lat":window.geo_latitude,
            "sbi_kfd_long":window.geo_longitude,
            "sbi_kfd_loc":window.geo_location,
            "sbi_kfd_scrn":cur_screen_name,
            "sbi_kfd_lang":(choosenLangArr[sysLang])?choosenLangArr[sysLang]:''
        };

        let jq_image_save_data = $.post(image_save_script_url, image_save_script_params, function(data) { }, 'json');

        jq_image_save_data.done(function(data){
            console.log(JSON.stringify(data));
            if(data.status)
            {
                console.log("Image Save Data : ",data.msg);
                imgLoaderDisable();
            }
        });
        //stopCam(); // FPHOTO
        faceDetectDisable();
        goToPage(scrn_no);
    }, this);
}

function captureImage()
{
    if(screenBackBtn)
    {
        return;
    }
    console.log("Fn : captureImage : start");
    imgLoaderEnable();
    game.time.events.add(Phaser.Timer.SECOND * 0.3, function()  {
        let dataURL = getImgDataURL();

        let image_save_script_url ='https://uat.anoorcloud.in/indiafirst/portal/api/addImage';
        let image_save_script_params = {
            "reg_img":dataURL,
            "app_no":window.p_PROPOSAL_NUMBER,
            "lat":window.geo_latitude,
            "long":window.geo_longitude,
            "curscrn":cur_screen_name,
        };
    
        let jq_image_save_data = $.post(image_save_script_url, image_save_script_params, function(data) { }, 'json');

        cap_captured_img_append = true;
        jq_image_save_data.done(function(data){
            console.log(JSON.stringify(data));
            if(data.status)
            {
                console.log("Capture Image Save Data : ",data.msg);
                imgLoaderDisable();
            }
        });
    }, this);
}

function getScreenImgDataURL()
{
    var phaserCanvas = document.getElementById(game_canvas_id);
    return (phaserCanvas.toDataURL('image/jpeg',1.0))? phaserCanvas.toDataURL('image/jpeg',1.0) : null;
}

function captureScreen()
{
    console.log("Fn : captureScreen : start");
    imgLoaderEnable();
    game.time.events.add(Phaser.Timer.SECOND * 0.3, function()  {

        let dataURL = getScreenImgDataURL();

        let image_save_script_url = window.kfd_api_url+'api/data/add_screen_image';
        let image_save_script_params = {
            "sbi_kfd_img":dataURL,
            "sbi_kfd_link":(window.product_link)? window.product_link : '',
            "sbi_media_append":Boolean(cap_photo_img_append),
            "sbi_kfd_lat":window.geo_latitude,
            "sbi_kfd_long":window.geo_longitude,
            "sbi_kfd_loc":window.geo_location,
            "sbi_kfd_scrn":cur_screen_name,
            "sbi_kfd_lang":(choosenLangArr[sysLang])?choosenLangArr[sysLang]:''
        };

        let jq_image_save_data = $.post(image_save_script_url, image_save_script_params, function(data) { }, 'json');

        jq_image_save_data.done(function(data){
            console.log(JSON.stringify(data));
            if(data.status)
            {
                console.log("Capture Screen Save Data : ",data.msg);
                imgLoaderDisable();
            }
        });
    }, this);
}

/*
Video Record Functions
 */
function onVideoRecord(skip_btn,nxt_scrn,hide_txt,xVRtxt=220,yVRtxt=700,vLoadScrn=videoLoadPageNo){

    // canvasInputDisable();
    console.log("Video Record : onVideoRecord");
    if(!faceDetectStatus) {
        console.log("Face detect status is false");
        return;
    }

    if(camera_record_status) {
        return;
    }
    camera_record_status = true;


     window.audio="no";
     console.log("audio pass:"+window.audio)
    faceDetectDisable();
  

    for(var i=0; i<obj_but_list.length; i++)
    {
        var data = obj_but_list[i];
        for(var key in data) {
            if(data.hasOwnProperty(key)) {
                var value = data[key];
                if(key=='key' && value==skip_btn)
                {
                    data.visible = false;
                }
            }
        }
    }

    // Spcl code
    /* if(hide_txt)
    {
        console.log("but_test ",obj_text_list[hide_txt]);
        obj_text_list[hide_txt].setText("");
    } */

    var rTime = 1;
    var record_text = "Recording.... ";
    var sec_text = "seconds";
    //record_text = transliterateText(record_text,translitLangArr[sysLang]);
   // sec_text = transliterateText(sec_text,translitLangArr[sysLang]);


    webcamtext = game.add.text(xVRtxt, yVRtxt, record_text, {
        font: fontFamilyLangArr[sysLang],
        fontSize: "18px",
        fontWeight: "bold",
        fill: "#ff0000",
        align: "center"
    });

    text_group.add(webcamtext);

    webcamtext.anchor.setTo(0, 0);
    webcamtext.alpha = 0;
    game.add.tween(webcamtext).to( { alpha: 1 }, 500, Phaser.Easing.Linear.None, true, 0, 250, true);

    var timesRun = 0;
    var intervalVideoCam = setInterval(function(){
        timesRun += 1;
        if(timesRun === 16){
            clearInterval(intervalVideoCam);
            webcamtext.setText("");

           // goToPage(15);
              console.log("thank_page go");  //My changes in code

        }
             
        webcamtext.setText(record_text+" "+rTime+" "+sec_text);
      
        rTime += 1;
    }, 1000);

 
    obj_list.push(webcamtext);
    obj_text_list.push(webcamtext);

    videoRecordAutoStop(nxt_scrn,vLoadScrn);

    console.log("videoRecordAutoStop : called");

 

}

function onVideoRecordStop(nxt_scrn){
    camera_record_status = false;
    if(typeof webcamtext !== 'undefined'){
        webcamtext.setText("");

    }
   goToPage(nxt_scrn);
   console.log("thank_page go");
}

function downloadVideoWebm(furl,fname)
{
    console.log("downloadVideoWebm : In ");
    var x=new XMLHttpRequest();
    x.open("GET", furl, true);
    x.responseType = 'blob';
    x.onload=function(e){download(x.response,fname, "video/webm" ); };
    x.send();
}

function saveVideoRecord(videoBlod,nxt_scrn)
{
  
    //goToPage(nxt_scrn);

     let video_save_script_url = 'https://uat.anoorcloud.in/indiafirst/portal/api/addVideo';

    let videoBlodContent = videoBlod;
   // let sbil_key = (window.link_key)? window.link_key : '';

    let videoFormData = new FormData();
    videoFormData.append('app_no', window.p_PROPOSAL_NUMBER);
    videoFormData.append('link_video', videoBlodContent);

    $.ajax({
        type: 'POST',
        dataType:'JSON',
        url: video_save_script_url,
        data: videoFormData,
        processData: false,
        contentType: false
    }).done(function(videoResponse) {
        console.log(JSON.stringify(videoResponse));
        console.log("Next screen : ",nxt_scrn);
        if(videoResponse.Status)
        {
            console.log("Video Saved Message : ",videoResponse.msg);
            complete_update(videoResponse.Status);

        }


      //  goToPage(nxt_scrn);
    }); 

}

/*
Face Detection
 */
function faceDetectEnable() {
    console.log("Face Detect Fn");
    "use strict";
    var phaserElem = $('#'+game_canvas_id);
    var top_offset = (phaserElem.offset().top)? phaserElem.offset().top : 0;
    var left_offset = (phaserElem.offset().left)? phaserElem.offset().left : 0;

    $('#'+game_canvas_id).faceDetection({
        complete: function (faces) {
            if(faces.length===0)
            {
                faceDetectStatus = false;
                console.log("No Face detected");

                faceDetectText.setText(faceDNStr);
                faceDetectText.addColor('#FF0000',0);

            }
            else
            {
                faceDetectStatus = true;
                faceDetectText.setText(faceDStr);
                faceDetectText.addColor('#008000',0);
            }
        },
        error:function (code, message) {
            faceDetectStatus = false;
            console.log('Face Detect Error: ' + message);
        }
    });

}

function faceDetectStart(xFD=40,yFD=360)
{
    faceDStr = 'Face Detected';
    //faceDStr = transliterateText(faceDStr,translitLangArr[sysLang]);

    faceDNStr = 'No Face Detected';
    //faceDNStr = transliterateText(faceDNStr,translitLangArr[sysLang]);

    faceDetectText = game.add.text(xFD, yFD, faceDNStr, {
        font: fontFamilyLangArr[sysLang],
        fontSize: "14px",
        fontWeight: "bold",
        fill: "#ff0000",
        align: "center"
    });

    text_group.add(faceDetectText);

    faceDetectText.anchor.setTo(0, 0);

    obj_list.push(faceDetectText);
    obj_text_list.push(faceDetectText);

    intervalFaceDetectCam = setInterval(function(){
        faceDetectEnable();
    }, 100);
}

function faceDetectDisable()
{
    clearInterval(intervalFaceDetectCam);
    faceDetectText.setText('');
    faceDetectStatus = false;
}

// Custom Text Type
function typeCustomText(txt_anim_str)
{
    let anim = JSON.parse(txt_anim_str);
    PlayTextAnim(anim);
}

// Update Complete Status
function setCompleteStatus()
{
    console.log("Fn : setCompleteStatus ");

    let kfd_setCStatus_url = window.kfd_api_url+'api/pivc/updateCompleteStatus';

    let get_setCStatus_params = {
        "sbil_key":(window.link_key)? window.link_key : '',
        "sbil_cstatus":true
    };

    let jq_get_setCStatus_res = $.post(kfd_setCStatus_url, get_setCStatus_params, function(data) { }, 'json');

    jq_get_setCStatus_res.done(function(data){
        console.log(JSON.stringify(data));
        if(data.status)
        {
            console.log("setCompleteStatus Response Data : ",data.msg);
        }
    });

}

// Reload Page
function reloadPage()
{
    window.location.reload(true);
}

// Camera Error original
function cameraAccessError1()
{
    
    let cameraStatus = camAccessStatus();

    if(!cameraStatus)
    {
        if(cameraErrorPageStatus)
        {
            setTimeout(function () {
                CleanUp();
                StartPage(cameraErrorPageNo);
              //  StartPage(18);
                ShowScreen();
            }, 1500);

        }

    }
}


function cameraAccessError()
{
   

  let cameraStatus=webcam.camStatus();

    if(isEdge || game.device.webcam)
{
     console.log("sssssssssssssss",cameraStatus);
}
    if(!cameraStatus)
    {
        if(cameraErrorPageStatus)
        {
            setTimeout(function () {
                CleanUp();
                StartPage(cameraErrorPageNo);
              //  StartPage(18);
                ShowScreen();
            }, 1500);

        }
        
    }
  
}



// Photo Capture with Facial Detection
function goToPagePCFD(scrn_no,cKey='',cAStatus='')
{
    console.log("Fn : Photo Capture with Facial Detection");
    console.log("Fn : goToPagePCFD");
    if(!faceDetectStatus) {
        console.log("Face detect status is false");
        return;
    }

    if(camera_btn_status) {
        return;
    }
    camera_btn_status = true;

    faceDetectDisable();
    imgLoaderEnable();

    camera_btn_status = false;
        goToPage(scrn_no);

   /*  game.time.events.add(Phaser.Timer.SECOND * 0.1, function()   {

        let dataURL = getImgDataURL();

        let image_save_script_url = window.kfd_api_url+'api/data/addConsentImage';
        let image_save_script_params = {
            "sbil_consent_img":dataURL,
            "sbil_key":(window.link_key)? window.link_key : '',
            "sbil_media_append":Boolean(cap_consent_img_append),
            "sbil_lat":window.geo_latitude,
            "sbil_long":window.geo_longitude,
            "sbil_loc":window.geo_location,
            "sbil_scrn":cur_screen_name,
            "sbil_lang":(choosenLangArr[sysLang])?choosenLangArr[sysLang]:''
        };

        let jq_image_save_data = $.post(image_save_script_url, image_save_script_params, function(data) { }, 'json');
        cap_consent_img_append = true;
        if(cKey!=''){ updateLinkResponse(cKey,cur_screen_name,cAStatus); }

        jq_image_save_data.done(function(data){
            console.log(JSON.stringify(data));
            if(data.status)
            {
                console.log("cap_consent_img_append : ",cap_consent_img_append);
                console.log("Image Save Data : ",data.msg);
                imgLoaderDisable();
            }
        }); */
        //stopCam(); //FPHOTO


   // }, this);
}

// Photo Capture without Facial Detection
function goToPagePC(scrn_no,cKey='',cAStatus='')
{
    console.log("Fn : Photo Capture without Facial Detection");
    console.log("Fn : goToPagePC");

    if(camera_btn_status) {
        return;
    }
    camera_btn_status = true;

    imgLoaderEnable();
    camera_btn_status = false;
        goToPage(scrn_no);
   /*  game.time.events.add(Phaser.Timer.SECOND * 0.1, function()   {

        let dataURL = getImgDataURL();

        let image_save_script_url = window.kfd_api_url+'api/data/addConsentImage';
        let image_save_script_params = {
            "sbil_consent_img":dataURL,
            "sbil_key":(window.link_key)? window.link_key : '',
            "sbil_media_append":Boolean(cap_consent_img_append),
            "sbil_lat":window.geo_latitude,
            "sbil_long":window.geo_longitude,
            "sbil_loc":window.geo_location,
            "sbil_scrn":cur_screen_name,
            "sbil_lang":(choosenLangArr[sysLang])?choosenLangArr[sysLang]:''
        };

        let jq_image_save_data = $.post(image_save_script_url, image_save_script_params, function(data) { }, 'json');
        cap_consent_img_append = true;
        if(cKey!=''){ updateLinkResponse(cKey,cur_screen_name,cAStatus); }

        jq_image_save_data.done(function(data){
            console.log(JSON.stringify(data));
            if(data.status)
            {
                console.log("cap_consent_img_append : ",cap_consent_img_append);
                console.log("Image Save Data : ",data.msg);
                imgLoaderDisable();
            }
        });
        //stopCam(); //FPHOTO
        camera_btn_status = false;
        goToPage(scrn_no);
    }, this); */
}

// Sent OTP
function sendOTPSMS($ph_no)
{
    console.log("Fn : sendOTPSMS ");

    let kfd_sendOTPSms_url = window.kfd_api_url+'api/pivc/sendOTPSms';

    let get_sendOTPSms_params = {
        "sbil_key":(window.link_key)? window.link_key : '',
        "sbil_mobile":($ph_no)? $ph_no : ''
    };

    let jq_get_sendOTPSms_res = $.post(kfd_sendOTPSms_url, get_sendOTPSms_params, function(data) { }, 'json');

    jq_get_sendOTPSms_res.done(function(data){
        console.log(JSON.stringify(data));
        if(data.status)
        {
            console.log("sendOTPSMS Response Data : ",data.msg);
            smsOTPCur = data.output.otp;
            console.log("sendOTPSMS Response - CUrrent OTP  : ",smsOTPCur);
        }
    });

}

function sendOTPBtn(xTxt,yTxt,$ph_no)
{
    console.log("Send OTP function");

    if(smsOTP_btn_status) {
        return;
    }
    smsOTP_btn_status = true;

    console.log("Mobile No : ",$ph_no);

    var SMSTxt = "OTP SMS send to "+$ph_no+" .";
    SMSTxt = transliterateText(SMSTxt,translitLangArr[sysLang]);


    smsOTPText = game.add.text(xTxt, yTxt, SMSTxt, {
        font: fontFamilyLangArr[sysLang],
        fontSize: "18px",
        fontWeight: "normal",
        fill: "#008000",
        align: "center"
    });

    text_group.add(smsOTPText);

    smsOTPText.anchor.setTo(0.5, 0);

    sendOTPSMS($ph_no);

    AddEvent(game.time.events.add(0.1, function(){
        var smsTxtTween = game.add.tween(smsOTPText).to({alpha: 0}, 15000, Phaser.Easing.Linear.None, true);
        smsTxtTween.onComplete.add(function(){
            console.log("smsTxtTween : onComplete");
            smsOTP_btn_status = false;
        },this);
    }, this));

    obj_list.push(smsOTPText);
    obj_text_list.push(smsOTPText);
}

function goOTPBtn(xTxt,yTxt,otp_key,nxt_srn,cKey='',cAStatus='')
{
    console.log("Fn : goOTPBtn");

    if(smsOTPValid_btn_status) {
        return;
    }
    smsOTPValid_btn_status = true;

    console.log("OTP Key : ",otp_key);

    var otpKeyIndex = obj_input_list.findIndex(function(obj_input_list) {
        return obj_input_list['key'] == otp_key;
    });

    console.log("otpKeyIndex : ",otpKeyIndex);
    console.log("otpKeyValue : ",obj_input_list[otpKeyIndex]['value']);

    var smsOTPValidResTxt = '';

    smsOTPValidTxt = game.add.text(xTxt, yTxt, smsOTPValidResTxt, {
        font: fontFamilyLangArr[sysLang],
        fontSize: "18px",
        fontWeight: "normal",
        fill: "#FF0000",
        align: "center"
    });

    text_group.add(smsOTPValidTxt);

    smsOTPValidTxt.anchor.setTo(0.5, 0);

    if(obj_input_list[otpKeyIndex]['value'] != '')
    {
        console.log("otpKeyValue had value");
        if(obj_input_list[otpKeyIndex]['value'] == smsOTPCur)
        {
            console.log("otpKeyValue is valid");
            smsOTPValidResTxt = "OTP validating ...";
            smsOTPValidResTxt = transliterateText(smsOTPValidResTxt,translitLangArr[sysLang]);

            smsOTPValidTxt.setText(smsOTPValidResTxt);

            smsOTPOk = true;

            obj_list.push(smsOTPValidTxt);
            obj_text_list.push(smsOTPValidTxt);

            smsOTPValidTxt.setText('');
            smsOTPValid_btn_status = false;
            smsOTPCur = 'M@yjo$';

            if(cKey!=''){ updateLinkResponse(cKey,cur_screen_name,cAStatus); }

            goToPage(nxt_srn);

        }
        else
        {
            console.log("otpKeyValue Is Invalid");
            smsOTPValidResTxt = "Please enter valid OTP"
        }
    }
    else
    {
        console.log("otpKeyValue had no value");
        smsOTPValidResTxt = "Please enter OTP"
    }

    smsOTPValidResTxt = transliterateText(smsOTPValidResTxt,translitLangArr[sysLang]);
    smsOTPValidTxt.setText(smsOTPValidResTxt);

    AddEvent(game.time.events.add(0.1, function(){
        var smsOTPValidTxtTween = game.add.tween(smsOTPValidTxt).to({alpha: 0}, 5000, Phaser.Easing.Linear.None, true);
        smsOTPValidTxtTween.onComplete.add(function(){
            console.log("smsOTPValidTxtTween : onComplete");
            smsOTPValid_btn_status = false;
        },this);
    }, this));

    obj_list.push(smsOTPValidTxt);
    obj_text_list.push(smsOTPValidTxt);

}

// Update Response
function updateLinkResponse(cKey,cPage,cAStatus)
{
    /* console.log("Fn : updateLinkResponse ");

    let kfd_updateLRes_url = window.kfd_api_url+'api/pivc/updateLinkResponse';

    (cAStatus)? '' : (disagreement_status=true);

    let get_updateLRes_params = {
        "sbil_key":(window.link_key)? window.link_key : '',
        "sbil_ckey":(cKey)? cKey : '',
        "sbil_cpage":(cPage)? cPage : '',
        "sbil_castatus":(cAStatus)? true : false
    };

    let jq_get_updateLRes_res = $.post(kfd_updateLRes_url, get_updateLRes_params, function(data) { }, 'json');

    jq_get_updateLRes_res.done(function(data){
        console.log(JSON.stringify(data));
        if(data.status)
        {
            console.log("updateLinkResponse Response Data : ",data.msg);
        }
    }); */

}
 var newadd;
// GoToPage with Response
function goToPageWResponse(nxt_scrn,cKey='',cAStatus='')
{
    updateLinkResponse(cKey,cur_screen_name,cAStatus);
    goToPage(nxt_scrn);
}

// Update Edit Input Response
function updateEditLinkResponse(eKey,ePage,eArr)
{
    console.log("Fn : updateLinkResponse ");
    disagreement_status=true;

    let kfd_updateELRes_url = 'https://uat.anoorcloud.in/indiafirst/portal/api/UpdateDetails';
    let eArrJson = JSON.stringify(eArr);



//$.getScript("./assets/product_assets/active_assure_diamond/js/flow_eng_normal.js");

   window.up_CUSTOMER_NAME=eArr.in_name
   customer=window.up_CUSTOMER_NAME;
            window.up_DOB_PH=eArr.in_dob;
            window.up_GENDER=eArr.in_gender;
            window.up_EMAIL=eArr.in_email;
            window.up_MOBILE_NUMBER=eArr.in_mob;
            window.up_IDPROF=eArr.in_id;
            window.up_NOMINEE_NAME=eArr.in_nom;
            window.up_ANNUALINCOME=eArr.in_annul;
            //  window.up_address=eArr.in_add;

               // window.up_address=eArr.in_add1+eArr.in_add2+eArr.in_add3+eArr.in_add4;

               // window.add1=eArr.in_add1;
               //  window.add2=eArr.in_add2;
               //   window.add3=eArr.in_add3+eArr.in_add4;

             //  if(eArr.in_add2=='' && eArr.in_add3=='' && eArr.in_add4=='')
             // {
             //    eArr.in_add2=" ";
             //    eArr.in_add3=" ";
             //    eArr.in_add4=" ";
             // }
             window.up_address=eArr.in_add1+","+eArr.in_add2+","+eArr.in_add3+","+eArr.in_add4;

            
          
    // window.up_CUSTOMER_NAME=eArr.in_name;
    // window.up_DOB_PH=eArr.in_dob;
    // window.up_MA_GENDER==eArr.in_gender;
    // window.up_EMAIL=eArr.in_email;
    // window.up_MOBILE_NUMBER=eArr.in_mob;
    // window.up_IDPROF=eArr.in_id;
    // window.up_NOMINEE_NAME=eArr.in_nom;
    // window.up_ANNUALINCOME=eArr.in_annul;
    // window.up_address=eArr.in_add;

  //  alert(window.up_CUSTOMER_NAME);

//  $.getScript("./assets/product_assets/active_assure_diamond/js/flow_eng_normal.js");
    let get_updateELRes_params = {
        "app_no":window.p_PROPOSAL_NUMBER,
        "edata":(eArrJson)? eArrJson : ''
    };

    let jq_get_updateELRes_res = $.post(kfd_updateELRes_url, get_updateELRes_params, function(data) { }, 'json');

    jq_get_updateELRes_res.done(function(data){
        console.log(JSON.stringify(data));
        if(data.status)
        {
            console.log("updateEditLinkResponse Response Data : ",data.msg);
        }
    });
}

// goToScreen - Status
function goToScreen(scrn_no,cKey='',cAStatus='')
{
    console.log("Fn : goToScreen");
    if(cKey!=''){ updateLinkResponse(cKey,cur_screen_name,cAStatus); }
    goToPage(scrn_no);
}

// Photo Capture with Facial Detection & Edit
function goToPagePCFDEdit(scrn_no,xPE,yPE,peKey='')
{
    console.log("Fn : Photo Capture with Facial Detection & Edit");
    console.log("Fn : goToPagePCFDEdit");
    if(!faceDetectStatus) {
        console.log("Face detect status is false");
        return;
    }

    if(camera_btn_status) {
        return;
    }
    camera_btn_status = true;


    let obj_list_count = 0;
    let input_null_val = false;
    let edit_obj_list = {};
    if(obj_input_list.length > 0)
    {
        let obj_input_list_len = obj_input_list.length;
        obj_input_list.forEach(
            function(item, index){
                console.log("Index : ",index," == Key == ",item['key'], " == Value == ",item['value']);
                edit_obj_list[item['key']]=item['value'];
                if(item['value']=='') { input_null_val = true; }
                obj_list_count++;
                if(obj_list_count == obj_input_list_len)
                {
                    if(input_null_val)
                    {
                        inputNullStr = 'Provide input values';
                        inputNullStr = transliterateText(inputNullStr,translitLangArr[sysLang]);

                        inputNullTxt = game.add.text(xPE, yPE, inputNullStr, {
                            font: fontFamilyLangArr[sysLang],
                            fontSize: "14px",
                            fontWeight: "bold",
                            fill: "#ff0000",
                            align: "center"
                        });

                        text_group.add(inputNullTxt);

                        inputNullTxt.anchor.setTo(0, 0);

                        AddEvent(game.time.events.add(0.1, function(){
                            var inputTxtTween = game.add.tween(inputNullTxt).to({alpha: 0}, 5000, Phaser.Easing.Linear.None, true);
                            inputTxtTween.onComplete.add(function(){
                                console.log("inputTxtTween : onComplete");
                                camera_btn_status = false;
                            },this);
                        }, this));

                        obj_list.push(inputNullTxt);
                        obj_text_list.push(inputNullTxt);

                    }
                    else
                    {
                        faceDetectDisable();
                        imgLoaderEnable();

                        game.time.events.add(Phaser.Timer.SECOND * 0.1, function()  {
                            let dataURL = getImgDataURL();

                            let image_save_script_url = window.kfd_api_url+'api/data/addConsentImage';
                            let image_save_script_params = {
                                "sbil_consent_img":dataURL,
                                "sbil_key":(window.link_key)? window.link_key : '',
                                "sbil_media_append":Boolean(cap_consent_img_append),
                                "sbil_lat":window.geo_latitude,
                                "sbil_long":window.geo_longitude,
                                "sbil_loc":window.geo_location,
                                "sbil_scrn":cur_screen_name,
                                "sbil_lang":(choosenLangArr[sysLang])?choosenLangArr[sysLang]:''
                            };

                            let jq_image_save_data = $.post(image_save_script_url, image_save_script_params, function(data) { }, 'json');
                            cap_consent_img_append = true;

                            if(peKey!=''){ updateEditLinkResponse(peKey,cur_screen_name,edit_obj_list); }

                            jq_image_save_data.done(function(data){
                                console.log(JSON.stringify(data));
                                if(data.status)
                                {
                                    console.log("cap_consent_img_append : ",cap_consent_img_append);
                                    console.log("Image Save Data : ",data.msg);
                                    imgLoaderDisable();
                                }
                            });
                            camera_btn_status = false;
                            goToPage(scrn_no);
                        },this);
                    }
                }
            }
        );
    }
}

// Photo Capture & Edit
function goToPagePCEdit(scrn_no,xPE,yPE,peKey='')
{
    console.log("Fn : Photo Capture & Edit");
    console.log("Fn : goToPagePCEdit");

    if(camera_btn_status) {
        return;
    }
    camera_btn_status = true;


    let obj_list_count = 0;
    let input_null_val = false;
    let edit_obj_list = {};
    if(obj_input_list.length > 0)
    {
        let obj_input_list_len = obj_input_list.length;
        obj_input_list.forEach(
            function(item, index){
                console.log("Index : ",index," == Key == ",item['key'], " == Value == ",item['value']);
                edit_obj_list[item['key']]=item['value'];
                if(item['value']=='') { input_null_val = true; }
                obj_list_count++;
                if(obj_list_count == obj_input_list_len)
                {
                    if(input_null_val)
                    {
                        inputNullStr = 'Provide input values';
                        inputNullStr = transliterateText(inputNullStr,translitLangArr[sysLang]);

                        inputNullTxt = game.add.text(xPE, yPE, inputNullStr, {
                            font: fontFamilyLangArr[sysLang],
                            fontSize: "14px",
                            fontWeight: "bold",
                            fill: "#ff0000",
                            align: "center"
                        });

                        text_group.add(inputNullTxt);

                        inputNullTxt.anchor.setTo(0, 0);

                        AddEvent(game.time.events.add(0.1, function(){
                            var inputTxtTween = game.add.tween(inputNullTxt).to({alpha: 0}, 5000, Phaser.Easing.Linear.None, true);
                            inputTxtTween.onComplete.add(function(){
                                console.log("inputTxtTween : onComplete");
                                camera_btn_status = false;
                            },this);
                        }, this));

                        obj_list.push(inputNullTxt);
                        obj_text_list.push(inputNullTxt);

                    }
                    else
                    {
                        imgLoaderEnable();
                        game.time.events.add(Phaser.Timer.SECOND * 0.1, function()  {
                            let dataURL = getImgDataURL();

                            let image_save_script_url = window.kfd_api_url+'api/data/addConsentImage';
                            let image_save_script_params = {
                                "sbil_consent_img":dataURL,
                                "sbil_key":(window.link_key)? window.link_key : '',
                                "sbil_media_append":Boolean(cap_consent_img_append),
                                "sbil_lat":window.geo_latitude,
                                "sbil_long":window.geo_longitude,
                                "sbil_loc":window.geo_location,
                                "sbil_scrn":cur_screen_name,
                                "sbil_lang":(choosenLangArr[sysLang])?choosenLangArr[sysLang]:''
                            };

                            let jq_image_save_data = $.post(image_save_script_url, image_save_script_params, function(data) { }, 'json');
                            cap_consent_img_append = true;

                            if(peKey!=''){ updateEditLinkResponse(peKey,cur_screen_name,edit_obj_list); }

                            jq_image_save_data.done(function(data){
                                console.log(JSON.stringify(data));
                                if(data.status)
                                {
                                    console.log("cap_consent_img_append : ",cap_consent_img_append);
                                    console.log("Image Save Data : ",data.msg);
                                    imgLoaderDisable();
                                }
                            });
                            camera_btn_status = false;
                            goToPage(scrn_no);
                        },this);
                    }
                }
            }
        );
    }
}

function imgLoaderEnable()
{
    console.log("fn : imgLoaderEnable");
    imgLoadEnable = true;
    imgRequest++;
}

function imgLoaderDisable()
{
    console.log("fn : imgLoaderDisable");
    imgLoadEnable = false;
    imgRequest--;
}

function checkImgRequest(fSrn)
{
    console.log('imgRequest : ',imgRequest);
    intervalImgCount++;
    if(imgRequest===0)
    {
        console.log('fSrn : ',fSrn);
        clearInterval(intervalImgRequest);
        console.log('fSrn2 : ',fSrn);
        goToPage(fSrn)
    }
    else if(intervalImgCount===1)
    {
        console.log('imgLoadPageNo : ',imgLoadPageNo);
        goToPage(imgLoadPageNo);
    }

}

// Get ThankYou Page
function goToThankYouPage(dSrn,nSrn)
{
    console.log("Fn : goToThankYouPage ");
    console.log("disagreement_status : ",disagreement_status);
    let goPage = '';
    if(disagreement_status) { goPage = dSrn; } else { goPage = nSrn; }

    intervalImgRequest = setInterval(checkImgRequest,1000,goPage);
}

// Update disagreement Status
function setDisAgreeStatus()
{
    console.log("Fn : setDisagreementStatus ");

    let kfd_setDStatus_url = window.kfd_api_url+'api/pivc/updateDisAgreeStatus';

    let get_setDStatus_params = {
        "sbil_key":(window.link_key)? window.link_key : '',
        "sbil_dstatus":false
    };

    let jq_get_setDStatus_res = $.post(kfd_setDStatus_url, get_setDStatus_params, function(data) { }, 'json');

    jq_get_setDStatus_res.done(function(data){
        console.log(JSON.stringify(data));
        if(data.status)
        {
            console.log("setDisagreementStatus Response Data : ",data.msg);
        }
    });

}

function click(x, y)
{
    var ev = new MouseEvent('click', {
        'view': window,
        'bubbles': true,
        'cancelable': true,
        'screenX': x,
        'screenY': y
    });

    var el = document.elementFromPoint(x, y);

    el.dispatchEvent(ev);
}

function soundInit()
{
    console.log("fn : soundInit");
}

function audioReplay()
{
    console.log('test : '+webcamtext);
    console.log("fn : audioReplay");
    if (cur_sfx_list.length > 0){
        currentSound = cur_sfx_list[0].play();
    }
}


function first_img()
{
  var s= game.add.sprite(72,71,'anoor');
}


function audioreplay2()
{
   
   
   if(window.audio=='yes')
   {
       audioReplay();
      canvasInputDisable();
   
   }
   else
   {
     console.log("sachin audio stop");
     // canvasInputEnable();

   }
 
   
}

function complete_update(data)
{
     let image_save_script_url = 'https://uat.anoorcloud.in/indiafirst/portal/api/CompleteStatus';
        let image_save_script_params = {
            "complete":data,  
            "app_no":window.p_PROPOSAL_NUMBER,
           
        };

        let jq_image_save_data = $.post(image_save_script_url, image_save_script_params, function(data) { }, 'json');

}


function yesno_update(data,scrn)
{
     let image_save_script_url = 'https://uat.anoorcloud.in/indiafirst/portal/api/ResponseUpdate';
        let image_save_script_params = {
            "curscrn": scrn, 
            "status":data, 
            "app_no":window.p_PROPOSAL_NUMBER,
           
        };

        let jq_image_save_data = $.post(image_save_script_url, image_save_script_params, function(data) { }, 'json');

}


function illness_update()
{
        let image_save_script_url = 'https://uat.anoorcloud.in/indiafirst/portal/api/IllnessUpdate';
        let image_save_script_params = {
            // "curscrn": scrn, 
            // "status":data, 
            "illness":illness,
            "period":period,
             "medicine":medicine,
            "app_no":window.p_PROPOSAL_NUMBER,
           
        };

        let jq_image_save_data = $.post(image_save_script_url, image_save_script_params, function(data) { }, 'json');

}

function showvalue()
{
    console.log("window.p_CUSTOMER_NAME:"+window.p_CUSTOMER_NAME);
    console.log("window.up_CUSTOMER_NAME:"+window.up_CUSTOMER_NAME);
     console.log(" window.p_GENDER=:"+ window.p_GENDER);
    console.log("window.up_GENDER:"+window.up_GENDER);
    console.log("customer name", customer);
  //   $.getScript( "./assets/js/common/anoor/init.js", function(){ } );

}


function sdd()
{
    setTimeout(function(){ canvasInputEnable() }, 13000*delay);

     //setTimeout(function(){ canvasInputEnable(); goToPage(scr_no); }, 1000*delay);

}

function filter_array(test_array) {
    var index = -1,
        arr_length = test_array ? test_array.length : 0,
        resIndex = -1,
        result = [];

    while (++index < arr_length) {
        var value = test_array[index];

        if (value) {
            result[++resIndex] = value;
        }
    }

    return result;
}


function address_mode(v)
 {
   var i;
   
    var str=v;
    window.res = str.split(",");

  
        for(i=0;i<window.res.length;i++)
        {
        if(window.res[i]==" " || window.res[i]==null)
        {
        // window.res[i]=window.res[i+1];
        window.res[i]= " ";
        console.log(window.res);

        }

        }

        window.add1=window.res;

       console.log("window.add1"+window.add1);

 

}


function check_add(v)
{
   
  var j;

      for(j=0;j<=8;j++)
        {
        if(window.add1[j]==" " || window.add1[j]==null || window.add1[j]=='undefined')
        {

           window.add1[j]=" ";
        // console.log("window.add1[j]"+window.add1);
       

        }

        }

}





// Orientation Exp

let orientation = screen.msOrientation || (screen.orientation || screen.mozOrientation || {});
console.log("orientation : ",orientation);
orientation.lock('portrait').catch(function(error) {
    console.log("Orientation Error : ", error);
});

