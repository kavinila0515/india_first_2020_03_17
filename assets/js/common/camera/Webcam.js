/**
* Provides access to the Webcam (if available)
*/
Phaser.Plugin.Webcam = function (game, parent) {

	Phaser.Plugin.call(this, game, parent);

	if (!game.device.getUserMedia)
	{
		return false;
	}

	navigator.getUserMedia = navigator.getUserMedia || navigator.webkitGetUserMedia ||  navigator.mediaDevices.getUserMedia || navigator.msGetUserMedia || navigator.mozGetUserMedia;

	this.context = null;
	this.stream = null;
    this.cameraAccess = true;

	this.video = document.createElement('video');
    this.video.setAttribute("id", "vid1");
    this.video.playsInline = true;
    this.video.autoplay = true;
    this.video.volume = 0.0000001;
    this.recorder = null;

};

Phaser.Plugin.Webcam.prototype = Object.create(Phaser.Plugin.prototype);
Phaser.Plugin.Webcam.prototype.constructor = Phaser.Plugin.Webcam;

Phaser.Plugin.Webcam.prototype.start = function (width, height, context) {

console.log('Webcam start', width, height);

	this.context = context;
    this.video.height = height;
    this.video.width = width;

	if (!this.stream)
	{
	    if(window.app_view)
        {
            navigator.getUserMedia( { video: { mandatory: { minWidth: width, minHeight: height } } }, this.connectCallback.bind(this), this.errorCallback.bind(this));
        }
        else
        {
            navigator.getUserMedia( { video: { mandatory: { minWidth: width, minHeight: height } }, audio:true }, this.connectCallback.bind(this), this.errorCallback.bind(this));
        }
	}

};

Phaser.Plugin.Webcam.prototype.stop = function () {

	if (this.stream)
	{
        this.stream.getTracks().forEach(function (track) { track.stop(); });
		this.stream = null;
		console.log("Camera is stopped!");
	}
};

Phaser.Plugin.Webcam.prototype.connectCallback = function (stream) {

	this.stream = stream;
    this.cameraAccess = true;

    this.video.srcObject = this.stream;

};

Phaser.Plugin.Webcam.prototype.errorCallback = function (e) {

	console.log('Video Stream rejected', e);
    this.cameraAccess = false;

};

Phaser.Plugin.Webcam.prototype.grab = function (context, x, y) {

	if (this.stream)
	{
		context.drawImage(this.video, x, y);
	}

};

Phaser.Plugin.Webcam.prototype.update = function () {

	if (this.stream)
	{
		this.context.drawImage(this.video, 0, 0);
	}

};

/**
* @name Phaser.Plugin.Webcam#active
* @property {boolean} active - Is this Webcam plugin capturing a video stream or not?
* @readonly
*/
Object.defineProperty(Phaser.Plugin.Webcam.prototype, "active", {

    get: function() {
        return (this.stream);
    }

});

/*
	Custom Codes
 */
Phaser.Plugin.Webcam.prototype.imgCapture = function () {

    console.log('Image Capture');
    var imgCanvas = document.createElement("canvas");
    imgCanvas.width = this.video.videoWidth;
    imgCanvas.height = this.video.videoHeight;
    imgCanvas.getContext('2d')
		.drawImage(this.video, 0, 0, imgCanvas.width, imgCanvas.height);

    return (imgCanvas.toDataURL('image/jpeg',0.80))? imgCanvas.toDataURL('image/jpeg',0.80) : null;
};

Phaser.Plugin.Webcam.prototype.camStatus = function () {

    console.log('Camera Status');

    return this.cameraAccess;
};

/*
Record Functions
 */

Phaser.Plugin.Webcam.prototype.videoRecord = function () {

    this.video.srcObject = this.stream;
    this.video.muted = "muted";
    this.video.play();

    this.recorder = RecordRTC(this.stream, {
        type: 'video'
    });

    this.recorder.startRecording();
    this.recorder.camera = this.stream;
};

Phaser.Plugin.Webcam.prototype.videoRecordStop = function () {
    this.recorder.stopRecording(this.videoRecordStopCallback);
};

Phaser.Plugin.Webcam.prototype.videoRecordStopCallback= function() {
    this.recorder.camera.stop();
};

Phaser.Plugin.Webcam.prototype.videoRecordGetBlob= function() {
    return this.recorder.getBlob();
};

Phaser.Plugin.Webcam.prototype.videoRecordAutoStop = function (video_timer,video_file_name,nxt_scrn,vLoadScrn) {

    this.video.srcObject = this.stream;
    this.video.muted = "muted";
    this.video.play();

    this.recorder = RecordRTC(this.stream, {
        type: 'video'
    });
    video_timer=16000; //newly added
    this.recorder.setRecordingDuration(video_timer).onRecordingStopped(function() {
        var blob = this.getBlob();
        //download(blob, video_file_name, "video/webm");
        saveVideoRecord(blob,nxt_scrn);
        // this.camera.stop();
        onVideoRecordStop(vLoadScrn);
    });

    this.recorder.camera = this.stream;
    this.recorder.startRecording();

};

