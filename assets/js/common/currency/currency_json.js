/*
	Currency Type : JSON String
	Define the currency details
 */

var currencyType = {
		"rupee": {"main": "rupee","sub": "paisa","symbol":"₹","numericType":"indian"},
		"dollar": {"main": "dollar","sub": "cent","symbol":"$","numericType":"foreign"},
		"pound": {"main": "pound","sub": "pence","symbol":"£","numericType":"foreign"},
		"yuan": {"main": "yuan","sub": "jiao","symbol":"元","numericType":"foreign"},
		"euro": {"main": "euro","sub": "cent","symbol":"€","numericType":"foreign"}
	};