/*
 Common Assets
 */

// Define Paths
var IMG_COM_PATH = 'assets/images/common/';
var AUDIO_COM_PATH = 'assets/audio/en/common/';
var AUDIO_CUST_PATH = 'assets/audio/custom/';
var AUDIO_CUST_NEED_PATH = AUDIO_CUST_PATH+'needed/';

function commonAssets()
{
	// Image

	// Common
	game.load.image('home_btn_01', IMG_COM_PATH+'home_btn_01.png');
	game.load.image('back_btn_01', IMG_COM_PATH+'back_btn_01.png');
	game.load.image('gofull_btn_01', IMG_COM_PATH+'gofull_btn_01.png');
	game.load.image('nofull_btn_01', IMG_COM_PATH+'nofull_btn_01.png');

	// Start Screen
    game.load.image('bar_01', IMG_COM_PATH+'bar_01.png');
    game.load.image('logo_01', IMG_COM_PATH+'logo_01.png');
    game.load.image('start_btn_r01', IMG_COM_PATH+'start_btn_r01.png');
    game.load.image('start_btn_r02', IMG_COM_PATH+'start_btn_r02.png');
    game.load.image('start_btn_r03', IMG_COM_PATH+'start_btn_r03.png');
    game.load.image('start_btn', IMG_COM_PATH+'start_btn.png');
    game.load.image('mark_01', IMG_COM_PATH+'mark_01.png');
    game.load.image('icn_01', IMG_COM_PATH+'icn_01.png');
    game.load.image('box_01', IMG_COM_PATH+'box_01.png');
    game.load.image('bar_02', IMG_COM_PATH+'bar_02.png');
    game.load.image('btn_01', IMG_COM_PATH+'btn_01.png');
    game.load.image('btn_02', IMG_COM_PATH+'btn_02.png');
    game.load.image('btn_sp_1', IMG_COM_PATH+'btn_sp_1.png');

    game.load.image('btn_01_1', IMG_COM_PATH+'btn_01.png');
    game.load.image('btn_01_2', IMG_COM_PATH+'btn_01.png');
    game.load.image('btn_01_3', IMG_COM_PATH+'btn_01.png');
  //   game.load.image('anoor', IMG_COM_PATH+'anoor.png');

	// SpriteSheet
	game.load.spritesheet('bg_01', IMG_COM_PATH+'bg_01.png', 600, 800);
    game.load.spritesheet('bg_02', IMG_COM_PATH+'bg_02.png', 600, 800);
    game.load.spritesheet('bg_03', IMG_COM_PATH+'bg_03.png', 600, 800);
	game.load.spritesheet('hand', IMG_COM_PATH+'hand.png', 200, 200);

	// Audio
	// Common
    game.load.audio('null', AUDIO_CUST_NEED_PATH+'null.mp3');


	// Number


	// Currency


    // Alphabets


    // Months


}