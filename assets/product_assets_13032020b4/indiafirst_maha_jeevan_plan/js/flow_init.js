/*
 Animation Flow
 PageByPage
 */

window.stage = {
  "screens":[]
};


window.common_screens = [
    {
        "buttons" : [
            {"sprite": "start_btn","x": 300,"y": 472,"delay": 1,"loop":true,"toTopObj":1, "onClickFn": "actionOnClick(1)","anchor":[0.5,0]}
        ],
        "sprite_animations": [
            {"sprite": "start_btn_r01","x": 300,"y": 540,"loop": true,"timing": 5,"delay": 1,"anim_type":"spin", "params" :[180,0],"anchor":[0.5,0]},
            {"sprite": "start_btn_r02","x": 300,"y": 540,"loop": true,"timing": 6,"delay": 1,"anim_type":"spin", "params" :[-270,0],"anchor":[0.5,0]},
            {"sprite": "start_btn_r03","x": 300,"y": 540,"loop": true,"timing": 7,"delay": 1,"anim_type":"spin", "params" :[360,0],"anchor":[0.5,0]},
            {"sprite": "mark_01","x": 400,"y": 380,"loop": false,"timing": 0,"delay": 1, "anchor":[0,0]},
            {"sprite": "bar_01","x": 0,"y": 0,"loop": false,"timing": 0,"delay": 0,"toTopObj":1, "anchor":[0,0]},
            {"sprite": "logo_01","x": 10,"y": 33,"loop": false,"timing": 0,"delay": 0.1,"toTopObj":2, "anchor":[0,0]},
            {"sprite": "btn_sp_1","x": 540,"y": 112,"loop": false,"timing": 0,"delay": 2, "toTopObj":1, "anchor":[0,0], "onClickFn":"audioReplay()"}

        ],
        "text_animations": [

         {"text": [{"content":"In Case if you wish to hear again, Click here ---->"}

                ], "sx": -1000,"sy": 120,"x": 290 ,"y": 120,"size": 12,"weight":"bold","color":"#003869","tween_type": "Elastic.easeOut","timing": 200,"delay":2, "anchor":[0,0],"wordWrap": true, "wordWrapWidth":520,"align":"right"},

            // {"text": [{"content":"Pre Issuance Verification Call"}], "sx": -1000,"sy": 50,"x": 190 ,"y": 50,"size": 28,"weight":"bold","color":"#003869","tween_type": "Elastic.easeOut","timing": 200,"delay":0.2, "anchor":[0,0] },
            // {"text": [{"content":"Welcome"}], "sx": -1000,"sy": 186,"x": 300 ,"y": 186,"size": 25,"weight":"bold","color":"#1A487C","tween_type": "Elastic.easeOut","timing": 200,"delay":1, "anchor":[0.5,0] },
            // {"text": [{"content":"To Start IndiaFirst"}], "sx": -1000,"sy": 266,"x": 300 ,"y": 266,"size": 25,"weight":"bold","color":"#1A487C","tween_type": "Elastic.easeOut","timing": 200,"delay":2, "anchor":[0.5,0] },
            // {"text": [{"content":"Pre Issuance Verification Call"}], "sx": -1000,"sy": 312,"x": 300 ,"y": 312,"size": 25,"weight":"bold","color":"#1A487C","tween_type": "Elastic.easeOut","timing": 200,"delay":3, "anchor":[0.5,0] },
            // {"text": [{"content":"click here"}], "sx": -1000,"sy": 354,"x": 300 ,"y": 354,"size": 25,"weight":"bold","color":"#1A487C","tween_type": "Elastic.easeOut","timing": 200,"delay":4, "anchor":[0.5,0] }
            {"text": [{"content":"Pre Issuance Verification Call"}], "sx": -1000,"sy": 50,"x": 190 ,"y": 50,"size": 28,"weight":"bold","color":"#003869","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },
            {"text": [{"content":"Welcome"}], "sx": -300,"sy": 186,"x": 300 ,"y": 186,"size": 25,"weight":"bold","color":"#1A487C","tween_type": "Elastic.easeIn","timing": 20,"delay":0.5, "anchor":[0.5,0] },
            {"text": [{"content":"To Start IndiaFirst"}], "sx": -300,"sy": 266,"x": 300 ,"y": 266,"size": 25,"weight":"bold","color":"#1A487C","tween_type": "Elastic.easeOut","timing": 20,"delay":0.5, "anchor":[0.5,0] },
             {"text": [{"content":"Pre Issuance Verification Call"}], "sx": -300,"sy": 312,"x": 300 ,"y": 312,"size": 25,"weight":"bold","color":"#1A487C","tween_type": "Elastic.easeOut","timing": 20,"delay":0.5, "anchor":[0.5,0] },
            {"text": [{"content":"click here"}], "sx": -300,"sy": 354,"x": 300 ,"y": 354,"size": 25,"weight":"bold","color":"#1A487C","tween_type": "Elastic.easeOut","timing": 20,"delay":0.5, "anchor":[0.5,0] }

],
        "sound_list": [
            {
                "sound": ["start_audio"]
            }
        ],
        "functions": [
            {"fn": "init()", "delay": 0},
            {"fn": "SetBGTile('bg_01')", "delay": 0},
            {"fn": "productInit()", "delay": 0}
        ],
        "name": "Start",
        "timing": -1,
        "index": 0
    },
    {
        "sprite_animations": [
            {"sprite": "bar_01","x": 0,"y": 0,"loop": false,"timing": 0,"delay": 0,"toTopObj":1, "anchor":[0,0]},
            {"sprite": "logo_01","x": 10,"y": 33,"loop": false,"timing": 0,"delay": 0.1,"toTopObj":2, "anchor":[0,0]},
            {"sprite": "icn_01","x": 300,"y": 300,"loop": false,"timing": 0,"delay": 0, "toTopObj":2, "anchor":[0.5,0]},
            {"sprite": "bar_02","x": 300,"y": 244,"loop": false,"timing": 0,"delay": 0, "toTopObj":1, "anchor":[0.5,0]},
            {"sprite": "box_01","x": 300,"y": 250,"loop": false,"timing": 0,"delay": 0, "toTopObj":1, "anchor":[0.5,0]},
            {"sprite": "btn_01_1","x": 200,"y": 490,"loop": false,"timing": 0,"delay": 0.5, "toTopObj":1, "anchor":[0.5,0],"onClickFn":"loadLangFlow(\"eng\",\"normal\",2)"},
            {"sprite": "btn_01_1","x": 400,"y": 490,"loop": false,"timing": 0,"delay": 0.5, "toTopObj":1, "anchor":[0.5,0], "onClickFn":"loadLangFlow(\"hin\",\"normal\",2)"},
            {"sprite": "btn_01_1","x": 200,"y": 550,"loop": false,"timing": 0,"delay": 0.5, "toTopObj":1, "anchor":[0.5,0], "onClickFn":"loadLangFlow(\"tel\",\"normal\",2)"},
            {"sprite": "btn_01_1","x": 400,"y": 550,"loop": false,"timing": 0,"delay": 0.5, "toTopObj":1, "anchor":[0.5,0], "onClickFn":"loadLangFlow(\"guj\",\"normal\",2)"},
           
            //{"sprite": "btn_01_3","x": 300,"y": 550,"loop": false,"timing": 0,"delay": 0.5, "toTopObj":1, "anchor":[0.5,0],"onClickFn":"loadLangFlow("+window.prefered_lang_slug+",\"normal\",2)"},
            {"sprite": "btn_sp_1","x": 540,"y": 112,"loop": false,"timing": 0,"delay": 2, "toTopObj":1, "anchor":[0,0], "onClickFn":"audioReplay()"}
        ],
        "text_animations": [
            {"text": [{"content":"Pre Issuance Verification Call"}], "sx": -1000,"sy": 50,"x": 190 ,"y": 50,"size": 28,"weight":"bold","color":"#003869","tween_type": "Elastic.easeOut","timing": 200,"delay":0.2, "anchor":[0,0] },
            {"text": [{"content":"Please choose your preferred language"}], "sx": -1000,"sy": 120,"x": 300 ,"y": 400,"size": 24,"weight":"bold","color":"#545252","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0.5,0] },
            {"text": [{"content":"English"}], "sx": -1000,"sy": 120,"x": 200 ,"y": 512,"size": 24,"weight":"normal","color":"#FFFFFF","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0.5,0] },
             {"text": [{"content":"हिंदी"}], "sx": -1000,"sy": 120,"x": 400 ,"y": 512,"size": 24,"weight":"normal","color":"#FFFFFF","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0.5,0] },
             {"text": [{"content":"తెలుగు"}], "sx": -1000,"sy": 120,"x": 200 ,"y": 567,"size": 24,"weight":"normal","color":"#FFFFFF","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0.5,0] },
            {"text": [{"content":"ગુજરાતી"}], "sx": -1000,"sy": 120,"x": 400 ,"y": 567,"size": 24,"weight":"normal","color":"#FFFFFF","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0.5,0] },
          // //
          //  {"text": [{"content":"$var.window.PREFERED_LANG_NAME"}], "sx": -1000,"sy": 120,"x": 300 ,"y": 572,"size": 24,"weight":"normal","color":"#FFFFFF","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0.5,0] },
            {"text": [
                    {"content":"In Case if you wish to hear again, Click here ---->"}
                   // {"content":"Click here ---->"}
                ], "sx": -1000,"sy": 120,"x": 290 ,"y": 120,"size": 12,"weight":"bold","color":"#235291","tween_type": "Elastic.easeOut","timing": 200,"delay":2, "anchor":[0,0],"wordWrap": true, "wordWrapWidth":520,"align":"right" }
        ],
        "sound_list": [
            {
                "sound": ["pre_lang_audio"]
            }
        ],
        "functions": [
            {"fn": "SetBGTile('bg_02')", "delay": 0 }
        ],
        "name": "Language Selection",
        "timing": -1,
        "index": 1
    }
];

//  if(window.PREFERED_LANG_NAME!==false)
// {
//     window.common_screens[1]['sprite_animations'].push({"sprite": "btn_01_3","x": 300,"y": 550,"loop": false,"timing": 0,"delay": 0.5, "toTopObj":1, "anchor":[0.5,0],"onClickFn":"loadLangFlow('"+window.prefered_lang_slug+"',\"normal\",2)"});
//     window.common_screens[1]['text_animations'].push({"text": [{"content":"$var.window.PREFERED_LANG_NAME"}], "sx": -1000,"sy": 120,"x": 300 ,"y": 572,"size": 24,"weight":"normal","color":"#FFFFFF","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0.5,0] });
// } 

for(var i=0; i<window.common_screens.length; i++ )
{
    window.stage.screens.push(window.common_screens[i]);
}
