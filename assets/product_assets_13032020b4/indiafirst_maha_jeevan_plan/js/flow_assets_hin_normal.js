/*
 Custom Assets
 */

// Define Paths
var PRODUCT_PATH = './assets/product_assets/'+window.flow_slug+'/';
var PRODUCT_COMMON_IMG_PATH = PRODUCT_PATH+'images/';
var PRODUCT_LANG_IMG_PATH = PRODUCT_COMMON_IMG_PATH+'hin/';
var PRODUCT_SCENE_AUDIO_PATH = PRODUCT_PATH+'audio/hin/scenes/';
var PRODUCT_COMMON_AUDIO_PATH = PRODUCT_PATH+'audio/hin/common/';

var COMMON_IMG_PATH = './assets/images/common/product/';
var COMMON_LANG_IMG_PATH = COMMON_IMG_PATH+'hin/';

var COMMON_JS_PATH = './assets/js/common/';
var CUSTOM_IMG_PATH = './assets/images/custom/';
var CUSTOM_IMG_LANG_PATH = './assets/images/custom/hin/';
var CUSTOM_IMG_PRODUCT_PATH = './assets/images/custom/product/';

var COMMON_AUDIO_PATH = './assets/audio/common/';
var COMMON_AUDIO_LANG_PATH = COMMON_AUDIO_PATH+'hin/';

var TYPE_SCENE_AUDIO_PATH = './assets/audio/product/type/mconnect/hin/scenes/';
var TYPE_COMMON_AUDIO_PATH = './assets/audio/product/type/mconnect/hin/common/';

function langAssets()
{
    // SpriteSheet


    // Images


    // Audio

    // Common


    // Screens



    // Scripts


    // Load Start
    //game.load.start();
}


function langAssetsRest()
{
    console.log("langAssetsRest : Init");

    // SpriteSheet
    game.load.spritesheet('iu_1_1', COMMON_LANG_IMG_PATH+'image_upload_01.png', 300, 225);


    // Images
    game.load.image('bar_03', COMMON_LANG_IMG_PATH+'bar_03.png');
    game.load.image('box_02', COMMON_LANG_IMG_PATH+'box_02.png');
    game.load.image('icn_02', COMMON_LANG_IMG_PATH+'icn_02.png');
    game.load.image('box_03', COMMON_LANG_IMG_PATH+'box_03.png');
    game.load.image('icn_03', COMMON_LANG_IMG_PATH+'icn_03.png');
    game.load.image('box_04', COMMON_LANG_IMG_PATH+'box_04.png');
    game.load.image('icn_04', COMMON_LANG_IMG_PATH+'icn_04.png');
    game.load.image('btn_03_1', COMMON_LANG_IMG_PATH+'btn_03.png');
    game.load.image('btn_03_2', COMMON_LANG_IMG_PATH+'btn_03.png');
    game.load.image('btn_03_3', COMMON_LANG_IMG_PATH+'btn_03.png');

    game.load.image('box_05', COMMON_LANG_IMG_PATH+'box_05.png');
    game.load.image('bar_04', COMMON_LANG_IMG_PATH+'bar_04.png');

    game.load.image('box_06', COMMON_LANG_IMG_PATH+'box_06.png');

    game.load.image('icn_05', COMMON_LANG_IMG_PATH+'icn_05.png');

    game.load.image('box_071_1', COMMON_LANG_IMG_PATH+'box_071.png');
    game.load.image('box_07_1', COMMON_LANG_IMG_PATH+'box_07.png');
    game.load.image('box_07_2', COMMON_LANG_IMG_PATH+'box_07.png');
    game.load.image('box_07_3', COMMON_LANG_IMG_PATH+'box_07.png');
    game.load.image('icn_06', COMMON_LANG_IMG_PATH+'icn_06.png');
    game.load.image('icn_07', COMMON_LANG_IMG_PATH+'icn_07.png');
    game.load.image('icn_08', COMMON_LANG_IMG_PATH+'icn_08.png');

    game.load.image('box_08', COMMON_LANG_IMG_PATH+'box_08.png');
    game.load.image('icn_09_1', COMMON_LANG_IMG_PATH+'icn_09.png');
    game.load.image('icn_09_2', COMMON_LANG_IMG_PATH+'icn_09.png');
    game.load.image('icn_09_3', COMMON_LANG_IMG_PATH+'icn_09.png');
    game.load.image('icn_09_4', COMMON_LANG_IMG_PATH+'icn_09.png');
    game.load.image('icn_09_5', COMMON_LANG_IMG_PATH+'icn_09.png');
    game.load.image('icn_09_6', COMMON_LANG_IMG_PATH+'icn_09.png');

    game.load.image('bar_05', COMMON_LANG_IMG_PATH+'bar_05.png');
    game.load.image('bar_06', COMMON_LANG_IMG_PATH+'bar_06.png');
    game.load.image('bar_07', COMMON_LANG_IMG_PATH+'bar_07.png');

    game.load.image('box_09', COMMON_LANG_IMG_PATH+'box_09.png');

    game.load.image('box_10', COMMON_LANG_IMG_PATH+'box_10.png');
    game.load.image('btn_04_1', COMMON_LANG_IMG_PATH+'btn_03.png');
    game.load.image('btn_04_2', COMMON_LANG_IMG_PATH+'btn_03.png');

    game.load.image('btn_07_1', COMMON_LANG_IMG_PATH+'btn_03.png');

    game.load.image('bar_08', COMMON_LANG_IMG_PATH+'bar_08.png');
    game.load.image('btn_eng_01', PRODUCT_LANG_IMG_PATH+'btn_eng_01.png');

    game.load.image('icn_10', COMMON_LANG_IMG_PATH+'icn_10.png');

    game.load.image('box_11', COMMON_LANG_IMG_PATH+'box_11.png');
    game.load.image('btn_09_1', COMMON_LANG_IMG_PATH+'btn_03.png');
    game.load.image('btn_09_2', COMMON_LANG_IMG_PATH+'btn_03.png');

    game.load.image('box_12', COMMON_LANG_IMG_PATH+'box_12.png');
    game.load.image('box_13', COMMON_LANG_IMG_PATH+'box_13.png');
    game.load.image('box_14', COMMON_LANG_IMG_PATH+'box_14.png');
    game.load.image('box_22', COMMON_LANG_IMG_PATH+'box_22.png');
    game.load.image('btn_equ', COMMON_LANG_IMG_PATH+'btn_equ.png');
    game.load.image('box_23', COMMON_LANG_IMG_PATH+'box_23.png');
        game.load.image('box_073', COMMON_LANG_IMG_PATH+'box_073.png');
          game.load.image('no-pictures', COMMON_LANG_IMG_PATH+'no-pictures.png');


     //sachin add
    game.load.image('box_16', COMMON_LANG_IMG_PATH+'box_16.png');
    game.load.image('box_18', COMMON_LANG_IMG_PATH+'box_18.png');
    game.load.image('box_07', COMMON_LANG_IMG_PATH+'box_07.png');
    game.load.image('box_20', COMMON_LANG_IMG_PATH+'box_20.png');
    game.load.image('box_21', COMMON_LANG_IMG_PATH+'box_21.png');
    game.load.image('medicon', COMMON_LANG_IMG_PATH+'medicon.png');
    game.load.image('illness_icon', COMMON_LANG_IMG_PATH+'illness_icon.png');
    game.load.image('loan_icon', COMMON_LANG_IMG_PATH+'loan_icon.png');
    game.load.image('dea_icon', COMMON_LANG_IMG_PATH+'dea_icon.png');
    game.load.image('surr_icon', COMMON_LANG_IMG_PATH+'surr_icon.png');
    game.load.image('maturity_icon', COMMON_LANG_IMG_PATH+'maturity_icon.png');
    game.load.image('equity_icon', COMMON_LANG_IMG_PATH+'equity_icon.png');
    game.load.image('Email', COMMON_LANG_IMG_PATH+'Email.png');
    game.load.image('phno', COMMON_LANG_IMG_PATH+'phno.png');
    game.load.image('web_icon', COMMON_LANG_IMG_PATH+'web_icon.png');
    game.load.image('box_072', COMMON_LANG_IMG_PATH+'box_072.png');
    game.load.image('box_071', COMMON_LANG_IMG_PATH+'box_071.png');
    game.load.image('box_071', COMMON_LANG_IMG_PATH+'box_071.png');
    game.load.image('hand', COMMON_LANG_IMG_PATH+'hand.png');
      game.load.image('survial_icon', COMMON_LANG_IMG_PATH+'survial_icon.png');




    game.load.image('icn_11', COMMON_LANG_IMG_PATH+'icn_11.png');

    game.load.image('disagree', COMMON_LANG_IMG_PATH+'frown.png');
    game.load.image('agree', COMMON_LANG_IMG_PATH+'smile.png');

    game.load.image('heart', COMMON_LANG_IMG_PATH+'heart.png');
    game.load.image('brain', COMMON_LANG_IMG_PATH+'brain.png');
    game.load.image('cancer', COMMON_LANG_IMG_PATH+'cancer.png');
    game.load.image('diabetes', COMMON_LANG_IMG_PATH+'diabetes.png');
    game.load.image('ent', COMMON_LANG_IMG_PATH+'ent.png');
    game.load.image('respiratory', COMMON_LANG_IMG_PATH+'respiratory.png');
    game.load.image('digestion', COMMON_LANG_IMG_PATH+'digestion.png');

    game.load.spritesheet('welcome_anim', COMMON_LANG_IMG_PATH+'welcome_anim.png',195,210);
    game.load.spritesheet('thumbsdown', COMMON_LANG_IMG_PATH+'thumbsdown.png',169,178);

  
  // Scenes audio

   //welcome screen
    game.load.audio('welcome1_audio', TYPE_SCENE_AUDIO_PATH+'hin_welcome_1.mp3');
    game.load.audio('welcome2_audio', TYPE_SCENE_AUDIO_PATH+'hin_welcome_2.mp3');

    //personal screen
    game.load.audio('personal_details_audio', TYPE_SCENE_AUDIO_PATH+'hin_personal_agree.mp3');
   game.load.audio('personal_disagree_audio', TYPE_SCENE_AUDIO_PATH+'hin_personal_disagree.mp3');
     game.load.audio('personal_yesno_audio', TYPE_SCENE_AUDIO_PATH+'hin_disagree.mp3');


    //plan screen
    
    game.load.audio('startplan', TYPE_SCENE_AUDIO_PATH+'hin_your_plan.mp3');
   game.load.audio('plantype', TYPE_SCENE_AUDIO_PATH+'hin_plan_type.mp3');
    game.load.audio('plan_name', TYPE_SCENE_AUDIO_PATH+'hin_plan_name.mp3');
    game.load.audio('premium_amount', TYPE_SCENE_AUDIO_PATH+'hin_premium_amount.mp3');
    game.load.audio('payment_frequency', TYPE_SCENE_AUDIO_PATH+'hin_payment_frequency.mp3');
    game.load.audio('paying_term', TYPE_SCENE_AUDIO_PATH+'hin_premium_paying_term.mp3');
    game.load.audio('policy_term', TYPE_SCENE_AUDIO_PATH+'hin_policy_term.mp3');
    game.load.audio('sum_assured', TYPE_SCENE_AUDIO_PATH+'hin_sum_assured.mp3');
    game.load.audio('plan_disagree_audio', TYPE_SCENE_AUDIO_PATH+'hin_plan_disagree.mp3');
     game.load.audio('non_linked', TYPE_SCENE_AUDIO_PATH+'hin_non_linked.mp3');

        game.load.audio('plan_yesno_audio', TYPE_SCENE_AUDIO_PATH+'hin_disagree.mp3');
      


   
    //video consent screen
     game.load.audio('video_audio', TYPE_SCENE_AUDIO_PATH+'hin_video_consent.mp3');
   
    //tq screen     
    game.load.audio('tq_audio_1', TYPE_SCENE_AUDIO_PATH+'hin_thank_1.mp3');
    game.load.audio('tq_audio_2', TYPE_SCENE_AUDIO_PATH+'hin_thank_2.mp3');
    game.load.audio('tq_audio_3', TYPE_SCENE_AUDIO_PATH+'hin_thank_3.mp3');

    //medical question screen
    game.load.audio('medical_audio', TYPE_SCENE_AUDIO_PATH+'hin_medical_details.mp3');
 
    //equity screen
    game.load.audio('equity_audio_1', TYPE_SCENE_AUDIO_PATH+'hin_equity_1.mp3');
    game.load.audio('equity_audio_2', TYPE_SCENE_AUDIO_PATH+'hin_equity_2.mp3');
    game.load.audio('equity_audio_3', TYPE_SCENE_AUDIO_PATH+'hin_equity_3.mp3');

    //illness screen
    game.load.audio('illness_details_audio', TYPE_SCENE_AUDIO_PATH+'hin_illness_details.mp3');

    //Benefits audio for common
   
    game.load.audio('some_of', TYPE_SCENE_AUDIO_PATH+'hin_pb_some.mp3');
    game.load.audio('hope', TYPE_SCENE_AUDIO_PATH+'hin_pb_hope.mp3');

//cash back
    game.load.audio('cash_death_benefit', TYPE_SCENE_AUDIO_PATH+'hin_cash_death_benefit.mp3');
    game.load.audio('cash_maturity_benefit', TYPE_SCENE_AUDIO_PATH+'hin_cash_maturity_benefit.mp3');
    game.load.audio('cash_survial_benefit', TYPE_SCENE_AUDIO_PATH+'hin_cash_survial_benefit.mp3');

 //maha_jeevan
    game.load.audio('jeevan_death_benefit', TYPE_SCENE_AUDIO_PATH+'hin_maha_death_benefit.mp3');
    game.load.audio('jeevan_maturity_benefit', TYPE_SCENE_AUDIO_PATH+'hin_maha_maturity_benefit.mp3');
   
 //money balance   
    game.load.audio('money_death_benefit', TYPE_SCENE_AUDIO_PATH+'hin_money_death_benefit.mp3');
    game.load.audio('money_maturity_benefit', TYPE_SCENE_AUDIO_PATH+'hin_money_maturity_benefit.mp3');

  //little champ  

    game.load.audio('champ_death_benefit', TYPE_SCENE_AUDIO_PATH+'hin_little_death_benefit.mp3');
    game.load.audio('champ_maturity_benefit', TYPE_SCENE_AUDIO_PATH+'hin_little_maturity_benefit.mp3');
 
   //loan details
   
    game.load.audio('loan_question_audio_1', TYPE_SCENE_AUDIO_PATH+'hin_loan_details_1.mp3');
    game.load.audio('loan_question_audio_2', TYPE_SCENE_AUDIO_PATH+'hin_loan_details_2.mp3');

    //plan name

    game.load.audio('Money_plan', TYPE_COMMON_AUDIO_PATH+'hin_money_balance_plan.mp3');
    game.load.audio('little_champ_plan', TYPE_COMMON_AUDIO_PATH+'hin_little_champ_plan.mp3');
    game.load.audio('maha_jeevan_plan', TYPE_COMMON_AUDIO_PATH+'hin_maha_jeevan_plan.mp3');
    game.load.audio('cash_back_plan', TYPE_COMMON_AUDIO_PATH+'hin_cash_back_plan.mp3');





/*
    game.load.audio('video_consent_audio', TYPE_SCENE_AUDIO_PATH+'video_consent_audio.mp3');
    game.load.audio('tq_audio', TYPE_SCENE_AUDIO_PATH+'tq_audio.mp3');
    game.load.audio('start_audio', TYPE_SCENE_AUDIO_PATH+'start_audio.mp3');
    game.load.audio('pre_lang_audio', TYPE_SCENE_AUDIO_PATH+'pre_lang_audio.mp3');
    game.load.audio('plan_disagree_audio', TYPE_SCENE_AUDIO_PATH+'plan_disagree_audio.mp3');
    game.load.audio('plan_details_audio', TYPE_SCENE_AUDIO_PATH+'plan_details_audio.mp3');
    game.load.audio('personal_disagree_audio', TYPE_SCENE_AUDIO_PATH+'personal_disagree_audio.mp3');
    game.load.audio('personal_details_audio', TYPE_SCENE_AUDIO_PATH+'personal_details_audio.mp3');
    game.load.audio('medical_audio', TYPE_SCENE_AUDIO_PATH+'medical_audio.mp3');
    game.load.audio('loan_question_audio', TYPE_SCENE_AUDIO_PATH+'loan_question_audio.mp3');
    game.load.audio('illness_details_audio', TYPE_SCENE_AUDIO_PATH+'illness_details_audio.mp3');
    game.load.audio('benefits_audio', TYPE_SCENE_AUDIO_PATH+'benefits_audio.mp3');
    game.load.audio('video_audio', TYPE_SCENE_AUDIO_PATH+'video_audio.mp3');
    game.load.audio('video_audio2', TYPE_SCENE_AUDIO_PATH+'video_audio2.mp3');
    game.load.audio('plan_yesno_audio', TYPE_SCENE_AUDIO_PATH+'plan_yesno_audio.mp3');
    game.load.audio('personal_yesno_audio', TYPE_SCENE_AUDIO_PATH+'personal_yesno_audio.mp3');
    game.load.audio('equity_audio', TYPE_SCENE_AUDIO_PATH+'equity_audio.mp3');
    game.load.audio('death_benefit', TYPE_SCENE_AUDIO_PATH+'death_benefit.mp3');
    game.load.audio('maturity_benefit', TYPE_SCENE_AUDIO_PATH+'maturity_benefit.mp3');
    game.load.audio('startplan', TYPE_SCENE_AUDIO_PATH+'startplan.mp3');
    game.load.audio('newbenefit', TYPE_SCENE_AUDIO_PATH+'newbenefit.mp3');
   game.load.audio('plantype', TYPE_SCENE_AUDIO_PATH+'plantype.mp3');


    game.load.audio('plan_name', TYPE_SCENE_AUDIO_PATH+'plan_name.mp3');
    game.load.audio('premium_amount', TYPE_SCENE_AUDIO_PATH+'premium_amount.mp3');
    game.load.audio('payment_frequency', TYPE_SCENE_AUDIO_PATH+'payment_frequency.mp3');
    game.load.audio('paying_term', TYPE_SCENE_AUDIO_PATH+'paying_term.mp3');
    game.load.audio('policy_term', TYPE_SCENE_AUDIO_PATH+'policy_term.mp3');
    game.load.audio('sum_assured', TYPE_SCENE_AUDIO_PATH+'sum_assured.mp3');

*/


    // Common

   
    game.load.audio('tradition', TYPE_COMMON_AUDIO_PATH+'hin_tradition.mp3');
    game.load.audio('ulip', TYPE_COMMON_AUDIO_PATH+'hin_ulip.mp3');
    game.load.audio('term', TYPE_COMMON_AUDIO_PATH+'hin_term.mp3');
    game.load.audio('pension', TYPE_COMMON_AUDIO_PATH+'hin_pension.mp3');
    game.load.audio('limited', TYPE_COMMON_AUDIO_PATH+'hin_limited.mp3');

    game.load.audio('single', TYPE_COMMON_AUDIO_PATH+'hin_single.mp3');
    game.load.audio('regular', TYPE_COMMON_AUDIO_PATH+'hin_regular.mp3');

    game.load.audio('annuelle', TYPE_COMMON_AUDIO_PATH+'hin_annually.mp3');
    game.load.audio('years', TYPE_COMMON_AUDIO_PATH+'hin_years.mp3');
    game.load.audio('yearly', TYPE_COMMON_AUDIO_PATH+'hin_yearly.mp3');
    game.load.audio('halfyearly', TYPE_COMMON_AUDIO_PATH+'hin_halfyearly.mp3');
    game.load.audio('quarterly', TYPE_COMMON_AUDIO_PATH+'hin_quarterly.mp3');
    game.load.audio('monthly', TYPE_COMMON_AUDIO_PATH+'hin_monthly.mp3');
    game.load.audio('weekly', TYPE_COMMON_AUDIO_PATH+'hin_weekly.mp3');

    game.load.audio('weeks', TYPE_COMMON_AUDIO_PATH+'hin_weeks.mp3');
    game.load.audio('months', TYPE_COMMON_AUDIO_PATH+'hin_months.mp3');


    game.load.audio('Annuelle', TYPE_COMMON_AUDIO_PATH+'hin_annually.mp3');
    game.load.audio('Years', TYPE_COMMON_AUDIO_PATH+'hin_years.mp3');
    game.load.audio('Yearly', TYPE_COMMON_AUDIO_PATH+'hin_yearly.mp3');
    game.load.audio('Halfyearly', TYPE_COMMON_AUDIO_PATH+'hin_halfyearly.mp3');
    game.load.audio('Quarterly', TYPE_COMMON_AUDIO_PATH+'hin_quarterly.mp3');
    game.load.audio('Monthly', TYPE_COMMON_AUDIO_PATH+'hin_monthly.mp3');
    game.load.audio('Weekly', TYPE_COMMON_AUDIO_PATH+'hin_weekly.mp3');


   // Number
    game.load.audio('and', [COMMON_AUDIO_LANG_PATH+'number/hin_and.mp3']);
    game.load.audio('zero', [COMMON_AUDIO_LANG_PATH+'number/hin_zero.mp3']);
    game.load.audio('one', [COMMON_AUDIO_LANG_PATH+'number/hin_one.mp3']);
    game.load.audio('two', [COMMON_AUDIO_LANG_PATH+'number/hin_two.mp3']);
    game.load.audio('three', [COMMON_AUDIO_LANG_PATH+'number/hin_three.mp3']);
    game.load.audio('four', [COMMON_AUDIO_LANG_PATH+'number/hin_four.mp3']);
    game.load.audio('five', [COMMON_AUDIO_LANG_PATH+'number/hin_five.mp3']);
    game.load.audio('six', [COMMON_AUDIO_LANG_PATH+'number/hin_six.mp3']);
    game.load.audio('seven', [COMMON_AUDIO_LANG_PATH+'number/hin_seven.mp3']);
    game.load.audio('eight', [COMMON_AUDIO_LANG_PATH+'number/hin_eight.mp3']);
    game.load.audio('nine', [COMMON_AUDIO_LANG_PATH+'number/hin_nine.mp3']);
    game.load.audio('ten', [COMMON_AUDIO_LANG_PATH+'number/hin_ten.mp3']);
    game.load.audio('eleven', [COMMON_AUDIO_LANG_PATH+'number/hin_eleven.mp3']);
    game.load.audio('twelve', [COMMON_AUDIO_LANG_PATH+'number/hin_twelve.mp3']);
    game.load.audio('thirteen', [COMMON_AUDIO_LANG_PATH+'number/hin_thirteen.mp3']);
    game.load.audio('fourteen', [COMMON_AUDIO_LANG_PATH+'number/hin_fourteen.mp3']);
    game.load.audio('fifteen', [COMMON_AUDIO_LANG_PATH+'number/hin_fifteen.mp3']);
    game.load.audio('sixteen', [COMMON_AUDIO_LANG_PATH+'number/hin_sixteen.mp3']);
    game.load.audio('seventeen', [COMMON_AUDIO_LANG_PATH+'number/hin_seventeen.mp3']);
    game.load.audio('eighteen', [COMMON_AUDIO_LANG_PATH+'number/hin_eighteen.mp3']);
    game.load.audio('nineteen', [COMMON_AUDIO_LANG_PATH+'number/hin_nineteen.mp3']);
    game.load.audio('twenty', [COMMON_AUDIO_LANG_PATH+'number/hin_twenty.mp3']);
    game.load.audio('thirty', [COMMON_AUDIO_LANG_PATH+'number/hin_thirty.mp3']);
    game.load.audio('forty', [COMMON_AUDIO_LANG_PATH+'number/hin_forty.mp3']);
    game.load.audio('fifty', [COMMON_AUDIO_LANG_PATH+'number/hin_fifty.mp3']);
    game.load.audio('sixty', [COMMON_AUDIO_LANG_PATH+'number/hin_sixty.mp3']);
    game.load.audio('seventy', [COMMON_AUDIO_LANG_PATH+'number/hin_seventy.mp3']);
    game.load.audio('eighty', [COMMON_AUDIO_LANG_PATH+'number/hin_eighty.mp3']);
    game.load.audio('ninety', [COMMON_AUDIO_LANG_PATH+'number/hin_ninety.mp3']);
    game.load.audio('hundred', [COMMON_AUDIO_LANG_PATH+'number/hin_hundred.mp3']);
    game.load.audio('thousand', [COMMON_AUDIO_LANG_PATH+'number/hin_thousand.mp3']);
    game.load.audio('lakh', [COMMON_AUDIO_LANG_PATH+'number/hin_lakh.mp3']);
 //   game.load.audio('million', [COMMON_AUDIO_LANG_PATH+'number/hin_million.mp3']);
    game.load.audio('crore', [COMMON_AUDIO_LANG_PATH+'number/hin_crore.mp3']);
    game.load.audio('billion', [COMMON_AUDIO_LANG_PATH+'number/hin_billion.mp3']);
    game.load.audio('trillion', [COMMON_AUDIO_LANG_PATH+'number/hin_trillion.mp3']);
    // game.load.audio('quadrillion', [COMMON_AUDIO_LANG_PATH+'number/hin_quadrillion.mp3']);
    // game.load.audio('quintillion', [COMMON_AUDIO_LANG_PATH+'number/hin_quintillion.mp3']);
    // game.load.audio('sextillion', [COMMON_AUDIO_LANG_PATH+'number/hin_sextillion.mp3']);
    // game.load.audio('septillion', [COMMON_AUDIO_LANG_PATH+'number/hin_septillion.mp3']);
    // game.load.audio('octillion', [COMMON_AUDIO_LANG_PATH+'number/hin_octillion.mp3']);
    // game.load.audio('nonillion', [COMMON_AUDIO_LANG_PATH+'number/hin_nonillion.mp3']);

    // Currency
    game.load.audio('rupee', [COMMON_AUDIO_LANG_PATH+'currency/hin_rupee.mp3']);
    game.load.audio('rupees', [COMMON_AUDIO_LANG_PATH+'currency/hin_rupees.mp3']);

    game.load.audio('paisa', [COMMON_AUDIO_LANG_PATH+'currency/hin_paisa.mp3']);
    game.load.audio('dollar', [COMMON_AUDIO_LANG_PATH+'currency/hin_dollar.mp3']);
    game.load.audio('cent', [COMMON_AUDIO_LANG_PATH+'currency/hin_cent.mp3']);
    game.load.audio('pound', [COMMON_AUDIO_LANG_PATH+'currency/hin_pound.mp3']);
    game.load.audio('pence', [COMMON_AUDIO_LANG_PATH+'currency/hin_pence.mp3']);
    game.load.audio('yuan', [COMMON_AUDIO_LANG_PATH+'currency/hin_yuan.mp3']);
    game.load.audio('jiao', [COMMON_AUDIO_LANG_PATH+'currency/hin_jiao.mp3']);
    game.load.audio('euro', [COMMON_AUDIO_LANG_PATH+'currency/hin_euro.mp3']);

    // Alphabets
    game.load.audio('a', [COMMON_AUDIO_LANG_PATH+'alphabet/hin_a.mp3']);
    game.load.audio('b', [COMMON_AUDIO_LANG_PATH+'alphabet/hin_b.mp3']);
    game.load.audio('c', [COMMON_AUDIO_LANG_PATH+'alphabet/hin_c.mp3']);
    game.load.audio('d', [COMMON_AUDIO_LANG_PATH+'alphabet/hin_d.mp3']);
    game.load.audio('e', [COMMON_AUDIO_LANG_PATH+'alphabet/hin_e.mp3']);
    game.load.audio('f', [COMMON_AUDIO_LANG_PATH+'alphabet/hin_f.mp3']);
    game.load.audio('g', [COMMON_AUDIO_LANG_PATH+'alphabet/hin_g.mp3']);
    game.load.audio('h', [COMMON_AUDIO_LANG_PATH+'alphabet/hin_h.mp3']);
    game.load.audio('i', [COMMON_AUDIO_LANG_PATH+'alphabet/hin_i.mp3']);
    game.load.audio('j', [COMMON_AUDIO_LANG_PATH+'alphabet/hin_j.mp3']);
    game.load.audio('k', [COMMON_AUDIO_LANG_PATH+'alphabet/hin_k.mp3']);
    game.load.audio('l', [COMMON_AUDIO_LANG_PATH+'alphabet/hin_l.mp3']);
    game.load.audio('m', [COMMON_AUDIO_LANG_PATH+'alphabet/hin_m.mp3']);
    game.load.audio('n', [COMMON_AUDIO_LANG_PATH+'alphabet/hin_n.mp3']);
    game.load.audio('o', [COMMON_AUDIO_LANG_PATH+'alphabet/hin_o.mp3']);
    game.load.audio('p', [COMMON_AUDIO_LANG_PATH+'alphabet/hin_p.mp3']);
    game.load.audio('q', [COMMON_AUDIO_LANG_PATH+'alphabet/hin_q.mp3']);
    game.load.audio('r', [COMMON_AUDIO_LANG_PATH+'alphabet/hin_r.mp3']);
    game.load.audio('s', [COMMON_AUDIO_LANG_PATH+'alphabet/hin_s.mp3']);
    game.load.audio('t', [COMMON_AUDIO_LANG_PATH+'alphabet/hin_t.mp3']);
    game.load.audio('u', [COMMON_AUDIO_LANG_PATH+'alphabet/hin_u.mp3']);
    game.load.audio('v', [COMMON_AUDIO_LANG_PATH+'alphabet/hin_v.mp3']);
    game.load.audio('w', [COMMON_AUDIO_LANG_PATH+'alphabet/hin_w.mp3']);
    game.load.audio('x', [COMMON_AUDIO_LANG_PATH+'alphabet/hin_x.mp3']);
    game.load.audio('y', [COMMON_AUDIO_LANG_PATH+'alphabet/hin_y.mp3']);
    game.load.audio('z', [COMMON_AUDIO_LANG_PATH+'alphabet/hin_z.mp3']);
    game.load.audio('dot', [COMMON_AUDIO_LANG_PATH+'alphabet/hin_dot.mp3']);
    game.load.audio('comma', [COMMON_AUDIO_LANG_PATH+'alphabet/hin_comma.mp3']);
   
    // Months
    game.load.audio('january', [COMMON_AUDIO_LANG_PATH+'date/hin_january.mp3']);
    game.load.audio('february', [COMMON_AUDIO_LANG_PATH+'date/hin_february.mp3']);
    game.load.audio('march', [COMMON_AUDIO_LANG_PATH+'date/hin_march.mp3']);
    game.load.audio('april', [COMMON_AUDIO_LANG_PATH+'date/hin_april.mp3']);
    game.load.audio('may', [COMMON_AUDIO_LANG_PATH+'date/hin_may.mp3']);
    game.load.audio('june', [COMMON_AUDIO_LANG_PATH+'date/hin_june.mp3']);
    game.load.audio('july', [COMMON_AUDIO_LANG_PATH+'date/hin_july.mp3']);
    game.load.audio('august', [COMMON_AUDIO_LANG_PATH+'date/hin_august.mp3']);
    game.load.audio('september', [COMMON_AUDIO_LANG_PATH+'date/hin_september.mp3']);
    game.load.audio('october', [COMMON_AUDIO_LANG_PATH+'date/hin_october.mp3']);
    game.load.audio('november', [COMMON_AUDIO_LANG_PATH+'date/hin_november.mp3']);
    game.load.audio('december', [COMMON_AUDIO_LANG_PATH+'date/hin_december.mp3']);


    //new add

game.load.audio('and', [COMMON_AUDIO_LANG_PATH+'number/hin_and.mp3']);
game.load.audio('billion', [COMMON_AUDIO_LANG_PATH+'number/hin_billion.mp3']);
game.load.audio('crore', [COMMON_AUDIO_LANG_PATH+'number/hin_crore.mp3']);
game.load.audio('eight', [COMMON_AUDIO_LANG_PATH+'number/hin_eight.mp3']);
game.load.audio('eighteen', [COMMON_AUDIO_LANG_PATH+'number/hin_eighteen.mp3']);
game.load.audio('eighty', [COMMON_AUDIO_LANG_PATH+'number/hin_eighty.mp3']);
game.load.audio('eightyeight', [COMMON_AUDIO_LANG_PATH+'number/hin_eightyeight.mp3']);
game.load.audio('eightyfive', [COMMON_AUDIO_LANG_PATH+'number/hin_eightyfive.mp3']);
game.load.audio('eightyfour', [COMMON_AUDIO_LANG_PATH+'number/hin_eightyfour.mp3']);
game.load.audio('eightynine', [COMMON_AUDIO_LANG_PATH+'number/hin_eightynine.mp3']);
game.load.audio('eightyone', [COMMON_AUDIO_LANG_PATH+'number/hin_eightyone.mp3']);
game.load.audio('eightyseven', [COMMON_AUDIO_LANG_PATH+'number/hin_eightyseven.mp3']);
game.load.audio('eightysix', [COMMON_AUDIO_LANG_PATH+'number/hin_eightysix.mp3']);
game.load.audio('eightythree', [COMMON_AUDIO_LANG_PATH+'number/hin_eightythree.mp3']);
game.load.audio('eightytwo', [COMMON_AUDIO_LANG_PATH+'number/hin_eightytwo.mp3']);
game.load.audio('elevevn', [COMMON_AUDIO_LANG_PATH+'number/hin_elevevn.mp3']);
game.load.audio('fifty', [COMMON_AUDIO_LANG_PATH+'number/hin_fifty.mp3']);
game.load.audio('fiftyeight', [COMMON_AUDIO_LANG_PATH+'number/hin_fiftyeight.mp3']);
game.load.audio('fiftyfive', [COMMON_AUDIO_LANG_PATH+'number/hin_fiftyfive.mp3']);
game.load.audio('fiftyfour', [COMMON_AUDIO_LANG_PATH+'number/hin_fiftyfour.mp3']);
game.load.audio('fiftynine', [COMMON_AUDIO_LANG_PATH+'number/hin_fiftynine.mp3']);
game.load.audio('fiftyone', [COMMON_AUDIO_LANG_PATH+'number/hin_fiftyone.mp3']);
game.load.audio('fiftyseven', [COMMON_AUDIO_LANG_PATH+'number/hin_fiftyseven.mp3']);
game.load.audio('fiftysix', [COMMON_AUDIO_LANG_PATH+'number/hin_fiftysix.mp3']);
game.load.audio('fiftythree', [COMMON_AUDIO_LANG_PATH+'number/hin_fiftythree.mp3']);
game.load.audio('fiftytwo', [COMMON_AUDIO_LANG_PATH+'number/hin_fiftytwo.mp3']);
game.load.audio('five', [COMMON_AUDIO_LANG_PATH+'number/hin_five.mp3']);
game.load.audio('fivteen', [COMMON_AUDIO_LANG_PATH+'number/hin_fivteen.mp3']);
game.load.audio('forty', [COMMON_AUDIO_LANG_PATH+'number/hin_forty.mp3']);
game.load.audio('fortyeight', [COMMON_AUDIO_LANG_PATH+'number/hin_fortyeight.mp3']);
game.load.audio('fortyfive', [COMMON_AUDIO_LANG_PATH+'number/hin_fortyfive.mp3']);
game.load.audio('fortyfour', [COMMON_AUDIO_LANG_PATH+'number/hin_fortyfour.mp3']);
game.load.audio('fortynine', [COMMON_AUDIO_LANG_PATH+'number/hin_fortynine.mp3']);
game.load.audio('fortyone', [COMMON_AUDIO_LANG_PATH+'number/hin_fortyone.mp3']);
game.load.audio('fortyseven', [COMMON_AUDIO_LANG_PATH+'number/hin_fortyseven.mp3']);
game.load.audio('fortysix', [COMMON_AUDIO_LANG_PATH+'number/hin_fortysix.mp3']);
game.load.audio('fortythree', [COMMON_AUDIO_LANG_PATH+'number/hin_fortythree.mp3']);
game.load.audio('fortytwo', [COMMON_AUDIO_LANG_PATH+'number/hin_fortytwo.mp3']);
game.load.audio('four', [COMMON_AUDIO_LANG_PATH+'number/hin_four.mp3']);
game.load.audio('fourteen', [COMMON_AUDIO_LANG_PATH+'number/hin_fourteen.mp3']);
game.load.audio('hundred', [COMMON_AUDIO_LANG_PATH+'number/hin_hundred.mp3']);
game.load.audio('lakh', [COMMON_AUDIO_LANG_PATH+'number/hin_lakh.mp3']);
game.load.audio('million', [COMMON_AUDIO_LANG_PATH+'number/hin_million.mp3']);
game.load.audio('nine', [COMMON_AUDIO_LANG_PATH+'number/hin_nine.mp3']);
game.load.audio('nineteen', [COMMON_AUDIO_LANG_PATH+'number/hin_nineteen.mp3']);
game.load.audio('ninety', [COMMON_AUDIO_LANG_PATH+'number/hin_ninety.mp3']);
game.load.audio('ninetyeight', [COMMON_AUDIO_LANG_PATH+'number/hin_ninetyeight.mp3']);
game.load.audio('ninetyfive', [COMMON_AUDIO_LANG_PATH+'number/hin_ninetyfive.mp3']);
game.load.audio('ninetyfour', [COMMON_AUDIO_LANG_PATH+'number/hin_ninetyfour.mp3']);
game.load.audio('ninetynine', [COMMON_AUDIO_LANG_PATH+'number/hin_ninetynine.mp3']);
game.load.audio('ninetyone', [COMMON_AUDIO_LANG_PATH+'number/hin_ninetyone.mp3']);
game.load.audio('ninetyseven', [COMMON_AUDIO_LANG_PATH+'number/hin_ninetyseven.mp3']);
game.load.audio('ninetysix', [COMMON_AUDIO_LANG_PATH+'number/hin_ninetysix.mp3']);
game.load.audio('ninetythree', [COMMON_AUDIO_LANG_PATH+'number/hin_ninetythree.mp3']);
game.load.audio('ninetytwo', [COMMON_AUDIO_LANG_PATH+'number/hin_ninetytwo.mp3']);
// game.load.audio('nonillion', [COMMON_AUDIO_LANG_PATH+'number/hin_nonillion.mp3']);
// game.load.audio('octillion', [COMMON_AUDIO_LANG_PATH+'number/hin_octillion.mp3']);
game.load.audio('one', [COMMON_AUDIO_LANG_PATH+'number/hin_one.mp3']);
// game.load.audio('quadrillion', [COMMON_AUDIO_LANG_PATH+'number/hin_quadrillion.mp3']);
// game.load.audio('quintillion', [COMMON_AUDIO_LANG_PATH+'number/hin_quintillion.mp3']);
// game.load.audio('septillion', [COMMON_AUDIO_LANG_PATH+'number/hin_septillion.mp3']);
game.load.audio('seven', [COMMON_AUDIO_LANG_PATH+'number/hin_seven.mp3']);
game.load.audio('seventeen', [COMMON_AUDIO_LANG_PATH+'number/hin_seventeen.mp3']);
game.load.audio('seventy', [COMMON_AUDIO_LANG_PATH+'number/hin_seventy.mp3']);
game.load.audio('seventyeight', [COMMON_AUDIO_LANG_PATH+'number/hin_seventyeight.mp3']);
game.load.audio('seventyfive', [COMMON_AUDIO_LANG_PATH+'number/hin_seventyfive.mp3']);
game.load.audio('seventyfour', [COMMON_AUDIO_LANG_PATH+'number/hin_seventyfour.mp3']);
game.load.audio('seventynine', [COMMON_AUDIO_LANG_PATH+'number/hin_seventynine.mp3']);
game.load.audio('seventyone', [COMMON_AUDIO_LANG_PATH+'number/hin_seventyone.mp3']);
game.load.audio('seventyseven', [COMMON_AUDIO_LANG_PATH+'number/hin_seventyseven.mp3']);
game.load.audio('seventysix', [COMMON_AUDIO_LANG_PATH+'number/hin_seventysix.mp3']);
game.load.audio('seventythree', [COMMON_AUDIO_LANG_PATH+'number/hin_seventythree.mp3']);
game.load.audio('seventytwo', [COMMON_AUDIO_LANG_PATH+'number/hin_seventytwo.mp3']);
//game.load.audio('sextillion', [COMMON_AUDIO_LANG_PATH+'number/hin_sextillion.mp3']);
game.load.audio('six', [COMMON_AUDIO_LANG_PATH+'number/hin_six.mp3']);
game.load.audio('sixteen', [COMMON_AUDIO_LANG_PATH+'number/hin_sixteen.mp3']);
game.load.audio('sixty', [COMMON_AUDIO_LANG_PATH+'number/hin_sixty.mp3']);
game.load.audio('sixtyeight', [COMMON_AUDIO_LANG_PATH+'number/hin_sixtyeight.mp3']);
game.load.audio('sixtyfive', [COMMON_AUDIO_LANG_PATH+'number/hin_sixtyfive.mp3']);
game.load.audio('sixtyfour', [COMMON_AUDIO_LANG_PATH+'number/hin_sixtyfour.mp3']);
game.load.audio('sixtynine', [COMMON_AUDIO_LANG_PATH+'number/hin_sixtynine.mp3']);
game.load.audio('sixtyone', [COMMON_AUDIO_LANG_PATH+'number/hin_sixtyone.mp3']);
game.load.audio('sixtyseven', [COMMON_AUDIO_LANG_PATH+'number/hin_sixtyseven.mp3']);
game.load.audio('sixtysix', [COMMON_AUDIO_LANG_PATH+'number/hin_sixtysix.mp3']);
game.load.audio('sixtythree', [COMMON_AUDIO_LANG_PATH+'number/hin_sixtythree.mp3']);
game.load.audio('sixtytwo', [COMMON_AUDIO_LANG_PATH+'number/hin_sixtytwo.mp3']);
game.load.audio('ten', [COMMON_AUDIO_LANG_PATH+'number/hin_ten.mp3']);
game.load.audio('thirteen', [COMMON_AUDIO_LANG_PATH+'number/hin_thirteen.mp3']);
game.load.audio('thirty', [COMMON_AUDIO_LANG_PATH+'number/hin_thirty.mp3']);
game.load.audio('thirtyeight', [COMMON_AUDIO_LANG_PATH+'number/hin_thirtyeight.mp3']);
game.load.audio('thirtyfive', [COMMON_AUDIO_LANG_PATH+'number/hin_thirtyfive.mp3']);
game.load.audio('thirtyfour', [COMMON_AUDIO_LANG_PATH+'number/hin_thirtyfour.mp3']);
game.load.audio('thirtynine', [COMMON_AUDIO_LANG_PATH+'number/hin_thirtynine.mp3']);
game.load.audio('thirtyone', [COMMON_AUDIO_LANG_PATH+'number/hin_thirtyone.mp3']);
game.load.audio('thirtyseven', [COMMON_AUDIO_LANG_PATH+'number/hin_thirtyseven.mp3']);
game.load.audio('thirtysix', [COMMON_AUDIO_LANG_PATH+'number/hin_thirtysix.mp3']);
game.load.audio('thirtythree', [COMMON_AUDIO_LANG_PATH+'number/hin_thirtythree.mp3']);
game.load.audio('thirtytwo', [COMMON_AUDIO_LANG_PATH+'number/hin_thirtytwo.mp3']);
game.load.audio('thousand', [COMMON_AUDIO_LANG_PATH+'number/hin_thousand.mp3']);
game.load.audio('three', [COMMON_AUDIO_LANG_PATH+'number/hin_three.mp3']);
game.load.audio('trillion', [COMMON_AUDIO_LANG_PATH+'number/hin_trillion.mp3']);
game.load.audio('twelve', [COMMON_AUDIO_LANG_PATH+'number/hin_twelve.mp3']);
game.load.audio('twenty', [COMMON_AUDIO_LANG_PATH+'number/hin_twenty.mp3']);
game.load.audio('twentyeight', [COMMON_AUDIO_LANG_PATH+'number/hin_twentyeight.mp3']);
game.load.audio('twentyfive', [COMMON_AUDIO_LANG_PATH+'number/hin_twentyfive.mp3']);
game.load.audio('twentyfour', [COMMON_AUDIO_LANG_PATH+'number/hin_twentyfour.mp3']);
game.load.audio('twentynine', [COMMON_AUDIO_LANG_PATH+'number/hin_twentynine.mp3']);
game.load.audio('twentyone', [COMMON_AUDIO_LANG_PATH+'number/hin_twentyone.mp3']);
game.load.audio('twentyseven', [COMMON_AUDIO_LANG_PATH+'number/hin_twentyseven.mp3']);
game.load.audio('twentysix', [COMMON_AUDIO_LANG_PATH+'number/hin_twentysix.mp3']);
game.load.audio('twentythree', [COMMON_AUDIO_LANG_PATH+'number/hin_twentythree.mp3']);
game.load.audio('twentytwo', [COMMON_AUDIO_LANG_PATH+'number/hin_twentytwo.mp3']);
game.load.audio('two', [COMMON_AUDIO_LANG_PATH+'number/hin_two.mp3']);
game.load.audio('zero', [COMMON_AUDIO_LANG_PATH+'number/hin_zero.mp3 ']);



    game.load.start();

    console.log("langAssetsRest : Started");
}
