

/*
 Animation Flow
 PageByPage
 */

window.stage.screens = [];

for(var i=0; i<window.common_screens.length; i++ )
{
    window.stage.screens.push(window.common_screens[i]);
}
var customer,dob,gender,email,mob,id,nom,annul,add;
//window.up_CUSTOMER_NAME='';
if(window.up_CUSTOMER_NAME!='')
{
  customer=window.up_CUSTOMER_NAME;
}
else
{
    customer=window.p_CUSTOMER_NAME;
}
if (window.up_DOB_PH!='')
   {
   dob=window.up_DOB_PH;
}
else {
  dob=window.p_DOB_PH;
}
if (window.up_MA_GENDER!='')
   {
   gender=window.up_MA_GENDER;
}
else {
  gender=window.p_MA_GENDER;
}
if (window.up_EMAIL!='')
   {
   email=window.up_EMAIL;
}
else {
  email=window.p_EMAIL;
}
if (window.up_MOBILE_NUMBER!='')
   {
   mob=window.up_MOBILE_NUMBER;
}
else {
  mob=window.p_MOBILE_NUMBER;
}
if (window.up_IDPROF!='')
   {
   id=window.up_IDPROF;
}
else {
  id=window.p_IDPROF;
}
if (window.up_NOMINEE_NAME!='')
   {
   nom=window.up_NOMINEE_NAME;
}
else {
  nom=window.p_NOMINEE_NAME;
}
if (window.up_ANNUALINCOME!='')
   {
   annul=window.up_ANNUALINCOME;
}
else {
  annul=window.p_ANNUALINCOME;
}
if (window.up_address!='')
   {
   add=window.up_address;
}
else {
  add=window.p_address;
}

var numericType='indian';

//alert(customer);
var screens_eng = [
    {
        "sprite_animations": [
            {"sprite": "bar_01","x": 0,"y": 0,"loop": false,"timing": 0,"delay": 0,"toTopObj":1, "anchor":[0,0]},
            {"sprite": "logo_01","x": 20,"y": 35,"loop": false,"timing": 0,"delay": 0.1,"toTopObj":2, "anchor":[0,0]}
        ],
        "text_animations": [
            {"text": [{"content":"Pre Issuance Verification Call "}], "sx": -1000,"sy": 50,"x": 210 ,"y": 190,"size": 28,"weight":"bold","color":"#FFFFFF","tween_type": "Elastic.easeOut","timing": 200,"delay":0.2, "anchor":[0,0] }
        ],
        "functions": [
            {"fn": "SetBGTile('bg_02')", "delay": 0 },
            {"fn": "langAssetsRest()", "delay": 0 },
            {"fn": "getGeoLocationText()", "delay": 0 },
            {"fn": "webCamCreate()", "delay": 0 },
            {"fn": "initCamOnly()", "delay": 0.5 }
        ],
        "name": "Assets Loading",
        "timing": -1,
        "index": 2
    },
    {
        "sprite_animations": [
            {"sprite": "bar_01","x": 0,"y": 0,"loop": false,"timing": 0,"delay": 0,"toTopObj":1, "anchor":[0,0]},
            {"sprite": "logo_01","x": 10,"y": 33,"loop": false,"timing": 0,"delay": 0.1,"toTopObj":2,"anchor":[0,0]},
            
        ],
         "text_animations": [
            {"text": [{"content":"Pre Issuance Verification Call "}], "sx": -1000,"sy": 50,"x": 190 ,"y": 50,"size": 28,"weight":"bold","color":"#003869","tween_type": "Elastic.easeOut","timing": 200,"delay":0.2, "anchor":[0,0]},
            {"text": [{"content":"પ્રી ઇશ્યુઅન્સ વેરિફિકેશન ક Callલ પહેલાથી જ પૂર્ણ થયેલ છે!"}], "sx": -1000,"sy": 120,"x": 300 ,"y": 400,"size": 32,"weight":"bold","color":"#FF0000","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0.5,0.5],"wordWrap": true, "wordWrapWidth":520,"align":"center" }

        ],
       
        "functions": [

            {"fn": "SetBGTile('bg_02')", "delay": 0 },
          

        ],
        "name": "Invalid",
        "timing": 10,
        "index": 3
    },

  
];



for(var i=0; i<screens_eng.length; i++ )
{
    window.stage.screens.push(screens_eng[i]);
}