/*
 Custom Assets
 */

// Define Paths
var CUSTOM_IMG_PATH = './assets/images/custom/';
var PRODUCT_PATH = './assets/product_assets/'+window.flow_slug+'/';
var PRODUCT_IMG_PATH = PRODUCT_PATH+'images/';
var COMMON_PRODUCT_IMG_PATH = './assets/images/common/product/';
var COMMON_PRODUCT_LANG_IMG_PATH = COMMON_PRODUCT_IMG_PATH+'eng/';
var PRODUCT_SCENE_AUDIO_PATH = PRODUCT_PATH+'audio/eng/scenes/';

var COMMON_JS_PATH = './assets/js/common/';

var TYPE_SCENE_AUDIO_PATH = './assets/audio/product/type/mconnect/eng/scenes/';
var TYPE_COMMON_AUDIO_PATH = './assets/audio/product/type/mconnect/eng/common/';

function customAssets()
{
    // SpriteSheet



    // Images


    // Scripts

    // Camera Plugin
    game.load.script('webcam', COMMON_JS_PATH+'camera/Webcam.js');


    // Audio

    // Scenes
/*     game.load.audio('au_0_1', TYPE_SCENE_AUDIO_PATH+'0_1.mp3');
    game.load.audio('au_1_1', TYPE_SCENE_AUDIO_PATH+'1_1.mp3'); */
	
	// game.load.audio('ab_1', TYPE_SCENE_AUDIO_PATH+'ab_1.mp3');
	// game.load.audio('ab_2', TYPE_SCENE_AUDIO_PATH+'ab_2.mp3');

    game.load.audio('start_audio', TYPE_SCENE_AUDIO_PATH+'start_audio.mp3');
    game.load.audio('pre_lang_audio', TYPE_SCENE_AUDIO_PATH+'pre_lang_audio.mp3');

    // Common



    // Screens

}