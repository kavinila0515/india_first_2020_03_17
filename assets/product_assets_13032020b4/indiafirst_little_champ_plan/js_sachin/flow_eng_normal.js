
/*
 Animation Flow
 PageByPage
 */

window.stage.screens = [];

for(var i=0; i<window.common_screens.length; i++ )
{
    window.stage.screens.push(window.common_screens[i]);
}
var customer,dob,gender,email,mob,id,nom,annul,add;
//window.up_CUSTOMER_NAME='';
if(window.up_CUSTOMER_NAME!='')
{
  customer=window.up_CUSTOMER_NAME;
}
else
{
    customer=window.p_CUSTOMER_NAME;
}
if (window.up_DOB_PH!='')
   {
   dob=window.up_DOB_PH;
}
else {
  dob=window.p_DOB_PH;
}
if (window.up_MA_GENDER!='')
   {
   gender=window.up_MA_GENDER;
}
else {
  gender=window.p_GENDER;
}
if (window.up_EMAIL!='')
   {
   email=window.up_EMAIL;
}
else {
  email=window.p_EMAIL;
}
if (window.up_MOBILE_NUMBER!='')
   {
   mob=window.up_MOBILE_NUMBER;
}
else {
  mob=window.p_MOBILE_NUMBER;
}
if (window.up_IDPROF!='')
   {
   id=window.up_IDPROF;
}
else {
  id=window.p_IDPROF;
}
if (window.up_NOMINEE_NAME!='')
   {
   nom=window.up_NOMINEE_NAME;
}
else {
  nom=window.p_NOMINEE_NAME;
}
if (window.up_ANNUALINCOME!='')
   {
   annul=window.up_ANNUALINCOME;
}
else {
  annul=window.p_ANNUALINCOME;
}
if (window.up_address!='')
   {
   add=window.up_address;
}
else {
  add=window.p_address;
}

var numericType='indian';

//alert(customer);
var screens_eng = [
    {
        "sprite_animations": [
            {"sprite": "bar_01","x": 0,"y": 0,"loop": false,"timing": 0,"delay": 0,"toTopObj":1, "anchor":[0,0]},
            {"sprite": "logo_01","x": 20,"y": 35,"loop": false,"timing": 0,"delay": 0.1,"toTopObj":2, "anchor":[0,0]}
        ],
        "text_animations": [
            {"text": [{"content":"Pre Issuance Verification Call "}], "sx": -1000,"sy": 50,"x": 210 ,"y": 190,"size": 28,"weight":"bold","color":"#FFFFFF","tween_type": "Elastic.easeOut","timing": 200,"delay":0.2, "anchor":[0,0] }
        ],
        "functions": [
             {"fn": "SetBGTile('bg_02')", "delay": 0 },
            {"fn": "langAssetsRest()", "delay": 0 },
            {"fn": "getGeoLocationText()", "delay": 0 },
            {"fn": "webCamCreate()", "delay": 0 },
            {"fn": "initCamOnly()", "delay": 0.5 }
        ],
        "name": "Assets Loading",
        "timing": -1,
        "index": 2
    },
    {
        "sprite_animations": [
            {"sprite": "bar_01","x": 0,"y": 0,"loop": false,"timing": 0,"delay": 0,"toTopObj":1, "anchor":[0,0]},
            {"sprite": "logo_01","x": 10,"y": 33,"loop": false,"timing": 0,"delay": 0.1,"toTopObj":2,"anchor":[0,0]},
            {"sprite": "icn_01","x": 20,"y": 150,"loop": false,"timing": 0,"delay": 0, "toTopObj":1, "anchor":[0,0]},
            {"sprite": "bar_02","x": 300,"y": 250,"loop": false,"timing": 0,"delay": 0, "toTopObj":1, "anchor":[0.5,0]},
            {"sprite": "btn_02","x": 300,"y": 732,"loop": false,"timing": 0,"delay": 3, "toTopObj":1, "anchor":[0.5,0],"onClickFn":"goToPage(4),yesno_update('proceed','Welcome Screen')"},//4
            {"sprite": "btn_sp_1","x": 540,"y": 112,"loop": false,"timing": 0,"delay": 2, "toTopObj":1, "anchor":[0,0], "onClickFn":"audioReplay()"},

            {"sprite": "welcome_anim","x": 210,"y": 500,"loop": false,"timing": 30,"delay": 3, "toTopObj":1, "anchor":[0,0]}

        ],
        "text_animations": [
            {"text": [{"content":"Pre Issuance Verification Call "}], "sx": -1000,"sy": 50,"x": 190 ,"y": 50,"size": 28,"weight":"bold","color":"#003869","tween_type": "Elastic.easeOut","timing": 200,"delay":0.2, "anchor":[0,0]},

            {"text": [
                    {"content":"In Case if you wish to hear again, Click here ---->"}

                ], "sx": -1000,"sy": 120,"x": 290 ,"y": 120,"size": 12,"weight":"bold","color":"#003869","tween_type": "Elastic.easeOut","timing": 200,"delay":2, "anchor":[0,0],"wordWrap": true, "wordWrapWidth":520,"align":"right"}, //,"fontFamily":"Whitney Medium"
           {"text": [
             {"content":"Welcome, "},
             {"content":"$var.window.p_CUSTOMER_NAME"},
               ], "sx": -1000,"sy": 150,"x": 102 ,"y": 160,"size": 20,"weight":"bold","color":"#003869","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },
           {"text": [
             {"content":"Proposal Number : "},
             {"content":"$var.window.p_PROPOSAL_NUMBER"}
           ], "sx": -1000,"sy": 155,"x": 225 ,"y": 195,"size": 20,"weight":"bold","color":"#003869","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0.5,0] },

            {"text": [
            {"content":"Welcome to the pre-issuance verification call of your proposal no. "},
            {"content":"$var.window.p_PROPOSAL_NUMBER"},
            {"content":". This process will assist you in understanding and confirming important details such as Personal and Plan details, Loan questions, Medical questions & Product Benefits, etc for processing your proposal."},
            ], "sx": -1000,"sy": 245,"x": 50 ,"y": 290,"size": 20,"lineSpacing":-1,"color":"#545252","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5,"wordWrap": true, "wordWrapWidth":520,"align":"left", "anchor":[0,0],"weight":"normal" },


            {"text": [{"content":"Proceed"}], "sx": -1000,"sy": 750,"x": 300 ,"y": 750,"size": 26,"weight":"bold","color":"#FFFFFF","tween_type": "Elastic.easeOut","timing": 200,"delay":3, "anchor":[0.5,0] }

        ],
        "sound_list": [
            {
                "sound": ["welcome1_audio","$var.alphanumeric_window.p_PROPOSAL_NUMBER","welcome2_audio"]
            }
        ],
        "functions": [

            {"fn": "SetBGTile('bg_02')", "delay": 0 },
           {"fn": "cameraAccessError()", "delay" : 0.5 },
            {"fn": "captureImage()", "delay": 0 }


        ],
        "name": "Welcome Screen",
        "timing": -1,
        "index": 3
    },

  {
     "sprite_animations": [
            {"sprite": "bar_01","x": 0,"y": 0,"loop": false,"timing": 0,"delay": 0,"toTopObj":1, "anchor":[0,0]},
            {"sprite": "logo_01","x": 10,"y": 33,"loop": false,"timing": 0,"delay": 0.1,"toTopObj":2, "anchor":[0,0]},
            {"sprite": "bar_03","x": 300,"y": 180,"loop": false,"timing": 0,"delay": 0, "toTopObj":1, "anchor":[0.5,0]},
            {"sprite": "box_18","x": 300,"y": 223,"loop": false,"timing": 0,"delay": 0, "toTopObj":1, "anchor":[0.5,0]},
         //   {"sprite": "icn_02","x": 33,"y": 343,"loop": false,"timing": 0,"delay": 0, "toTopObj":1, "anchor":[0,0]},

            {"sprite": "btn_03_2","x": 150,"y": 740,"loop": false,"timing": 0,"delay": 4, "toTopObj":1, "anchor":[0.5,0],"onClickFn":"goToScreen(7,'cPerDet',1),yesno_update('Agree','Personal Details - Show')"},
            {"sprite": "btn_03_3","x": 450,"y": 740,"loop": false,"timing": 0,"delay": 4, "toTopObj":1, "anchor":[0.5,0],"onClickFn":"goToPage(5),yesno_update('Disagree','Personal Details - Show')"},
            {"sprite": "btn_sp_1","x": 540,"y": 112,"loop": false,"timing": 0,"delay": 2, "toTopObj":1, "anchor":[0,0], "onClickFn":"audioReplay()"}
        ],
        "text_animations": [
           {"text": [{"content":"Pre Issuance Verification Call "}], "sx": -1000,"sy": 50,"x": 190 ,"y": 50,"size": 28,"weight":"bold","color":"#003869","tween_type": "Elastic.easeOut","timing": 200,"delay":0.2, "anchor":[0,0] },
            {"text": [
                    {"content":"Proposal Number: "},
                    {"content":"$var.window.p_PROPOSAL_NUMBER"}
                ], "sx": -1000,"sy": 120,"x": 40 ,"y": 150,"size": 22,"weight":"bold","color":"#235291","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },
             {"text": [
                    {"content":"In Case if you wish to hear again, Click here ---->"}
                              ], "sx": -1000,"sy": 120,"x": 290 ,"y": 120,"size": 12,"weight":"bold","color":"#235291","tween_type": "Elastic.easeOut","timing": 200,"delay":2, "anchor":[0,0],"wordWrap": true, "wordWrapWidth":520,"align":"right" },
            {"text": [{"content":"PERSONAL & CONTACT DETAILS"}], "sx": -1000,"sy": 120,"x": 300 ,"y": 184,"size": 26,"weight":"bold","color":"#FFFFFF","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0.5,0] },
            {"text": [{"content":"Your personal details as per  our records are "}], "sx": -1000,"sy": 120,"x": 80 ,"y": 235,"size": 18,"weight":"bold","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },

            {"text": [{"content":"Name"}], "sx": -1000,"sy": 120,"x": 80 ,"y": 270,"size": 18,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },
            {"text": [{"content":":"}], "sx": -1000,"sy": 120,"x": 310 ,"y": 270,"size": 18,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },
            {"text": [{"content":customer}], "sx": -1000,"sy": 120,"x": 325 ,"y": 270,"size": 18,"weight":"bold","color":"#0098DE","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },


            {"text": [{"content":"Date of Birth"}], "sx": -1000,"sy": 120,"x": 80 ,"y": 310,"size": 18,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },
            {"text": [{"content":":"}], "sx": -1000,"sy": 120,"x": 310 ,"y": 310,"size": 18,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },
            {"text": [{"content":dob}], "sx": -1000,"sy": 120,"x": 325 ,"y": 310,"size": 18,"weight":"bold","color":"#0098DE","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },


            {"text": [{"content":"Gender"}], "sx": -1000,"sy": 120,"x": 80 ,"y": 350,"size": 18,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },
            {"text": [{"content":":"}], "sx": -1000,"sy": 120,"x": 310 ,"y": 350,"size": 18,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },
            {"text": [{"content":gender}], "sx": -1000,"sy": 120,"x": 325 ,"y": 350,"size": 18,"weight":"bold","color":"#0098DE","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },


            {"text": [{"content":"Email Id"}], "sx": -1000,"sy": 120,"x": 80 ,"y":390,"size": 18,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },
            {"text": [{"content":":"}], "sx": -1000,"sy": 120,"x": 310 ,"y": 390,"size": 18,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },
            {"text": [{"content":email}], "sx": -1000,"sy": 120,"x": 325 ,"y": 390,"size": 18,"weight":"bold","color":"#0098DE","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },


            {"text": [{"content":"Mobile No."}], "sx": -1000,"sy": 120,"x": 80 ,"y": 430,"size": 18,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },
            {"text": [{"content":":"}], "sx": -1000,"sy": 120,"x": 310 ,"y": 430,"size": 18,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },
            {"text": [{"content":mob}], "sx": -1000,"sy": 120,"x": 325 ,"y": 430,"size": 18,"weight":"bold","color":"#0098DE","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },

            // {"text": [{"content":"Type of ID card Proof"}], "sx": -1000,"sy": 120,"x": 80 ,"y": 470,"size": 18,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },
            // {"text": [{"content":":"}], "sx": -1000,"sy": 120,"x": 310 ,"y": 470,"size": 18,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },
            // {"text": [{"content":id}], "sx": -1000,"sy": 120,"x": 325 ,"y": 470,"size": 18,"weight":"bold","color":"#0098DE","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },

            {"text": [{"content":"Nominee name & relationship"}], "sx": -1000,"sy": 120,"x": 80 ,"y": 470,"size": 18,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },
            {"text": [{"content":":"}], "sx": -1000,"sy": 120,"x": 310 ,"y": 470,"size": 18,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },
            {"text": [{"content":nom}], "sx": -1000,"sy": 120,"x": 325 ,"y": 470,"size": 18,"weight":"bold","color":"#0098DE","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },

            {"text": [{"content":"Annual income"}], "sx": -1000,"sy": 120,"x": 80 ,"y": 510,"size": 18,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },
            {"text": [{"content":":"}], "sx": -1000,"sy": 120,"x": 310 ,"y": 510,"size": 18,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },
            {"text": [{"content":annul}], "sx": -1000,"sy": 120,"x": 325 ,"y": 510,"size": 18,"weight":"bold","color":"#0098DE","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },


            {"text": [{"content":"Address"}], "sx": -1000,"sy": 120,"x": 80 ,"y": 550,"size": 18,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },
            {"text": [{"content":":"}], "sx": -1000,"sy": 120,"x": 310 ,"y": 550,"size": 18,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },
            {"text": [
                       {"content":add}

            ], "sx": -1000,"sy": 120,"x": 325 ,"y": 550,"size": 18,"weight":"bold","color":"#0098DE","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0],"wordWrap":true,"wordWrapWidth":230,"align":"left","lineSpacing":-5},


            {"text": [{"content":"If all information displayed on the screen is correct,"}], "sx": -1000,"sy": 120,"x": 300 ,"y": 676,"size": 18,"weight":"bold","color":"#235291","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0.5,0] },
            {"text": [{"content":'Please click on "Agree", else click "Disagree"'}], "sx": -1000,"sy": 120,"x": 300 ,"y": 698,"size": 18,"weight":"bold","color":"#235291","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0.5,0] },

            {"text": [{"content":'Agree'}], "sx": -1000,"sy": 120,"x": 150 ,"y": 754,"size": 24,"weight":"bold","color":"#FFFFFF","tween_type": "Elastic.easeOut","timing": 200,"delay":4, "anchor":[0.5,0] },
            {"text": [{"content":'Disagree'}], "sx": -1000,"sy": 120,"x": 450 ,"y": 754,"size": 24,"weight":"bold","color":"#FFFFFF","tween_type": "Elastic.easeOut","timing": 200,"delay":4, "anchor":[0.5,0] },
        ],
        "sound_list": [
            {
                "sound": ["personal_details_audio"]
            }
        ],
        "functions": [
            {"fn": "SetBGTile('bg_02')", "delay": 0 },
             {"fn": "cameraAccessError()", "delay" : 0.5 },
             {"fn": "captureImage()", "delay": 1.5 }
        ],
        "name": "Personal Details - Show",
        "timing": -1,
        "index": 4
    },


    {
        "sprite_animations": [
            {"sprite": "bar_01","x": 0,"y": 0,"loop": false,"timing": 0,"delay": 0,"toTopObj":1, "anchor":[0,0]},
            {"sprite": "logo_01","x": 10,"y": 33,"loop": false,"timing": 0,"delay": 0.1,"toTopObj":2, "anchor":[0,0]},
            {"sprite": "bar_03","x": 300,"y": 180,"loop": false,"timing": 0,"delay": 0, "toTopObj":1, "anchor":[0.5,0]},
            {"sprite": "box_02","x": 300,"y": 223,"loop": false,"timing": 0,"delay": 0, "toTopObj":1, "anchor":[0.5,0]},

            {"sprite": "btn_03_2","x": 150,"y": 454,"loop": false,"timing": 0,"delay": 2, "toTopObj":1, "anchor":[0.5,0],"onClickFn":"goToPage(6)"},

            {"sprite": "btn_03_3","x": 450,"y": 454,"loop": false,"timing": 0,"delay": 2, "toTopObj":1, "anchor":[0.5,0],"onClickFn":"goToPageBack(4)"},

            {"sprite": "btn_sp_1","x": 540,"y": 112,"loop": false,"timing": 0,"delay": 2, "toTopObj":1, "anchor":[0,0], "onClickFn":"audioReplay()"}
        ],
          "text_animations": [
           {"text": [{"content":"Pre Issuance Verification Call "}], "sx": -1000,"sy": 50,"x": 190 ,"y": 50,"size": 28,"weight":"bold","color":"#003869","tween_type": "Elastic.easeOut","timing": 200,"delay":0.2, "anchor":[0,0] },
            {"text": [
                    {"content":"Proposal Number: "},
                    {"content":"$var.window.p_PROPOSAL_NUMBER"}
                ], "sx": -1000,"sy": 120,"x": 40 ,"y": 150,"size": 22,"weight":"bold","color":"#235291","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },
             {"text": [
                    {"content":"In Case if you wish to hear again, Click here ---->"}
                              ], "sx": -1000,"sy": 120,"x": 290 ,"y": 120,"size": 12,"weight":"bold","color":"#235291","tween_type": "Elastic.easeOut","timing": 200,"delay":2, "anchor":[0,0],"wordWrap": true, "wordWrapWidth":520,"align":"right" },
            {"text": [{"content":"PERSONAL & CONTACT DETAILS"}], "sx": -1000,"sy": 120,"x": 300 ,"y": 184,"size": 26,"weight":"bold","color":"#FFFFFF","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0.5,0] },

            {"text": [{"content":"Are you sure you want to disagree to the details on this screen."}], "sx": -1000,"sy": 120,"x": 50 ,"y": 280,"size": 35,"weight":"bold","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0],"wordWrap": true, "wordWrapWidth":520,"align":"center" },

            {"text": [{"content":'Yes'}], "sx": -1000,"sy": 120,"x": 150 ,"y": 468,"size": 26,"weight":"bold","color":"#FFFFFF","tween_type": "Elastic.easeOut","timing": 200,"delay":2, "anchor":[0.5,0] },
            {"text": [{"content":'No'}], "sx": -1000,"sy": 120,"x": 450 ,"y": 468,"size": 26,"weight":"bold","color":"#FFFFFF","tween_type": "Elastic.easeOut","timing": 200,"delay":2, "anchor":[0.5,0] },
        ],
        "functions": [

            {"fn": "SetBGTile('bg_02')", "delay": 0 }

        ],

         "sound_list": [
            {
                "sound": ["personal_yesno_audio"]
            }
        ],

        "name": "Personal YesNo - Confirm",
        "timing": -1,
        "index": 5
    },


 {
     "sprite_animations": [
            {"sprite": "bar_01","x": 0,"y": 0,"loop": false,"timing": 0,"delay": 0,"toTopObj":1, "anchor":[0,0]},
            {"sprite": "logo_01","x": 10,"y": 33,"loop": false,"timing": 0,"delay": 0.1,"toTopObj":2, "anchor":[0,0]},
            {"sprite": "bar_03","x": 300,"y": 180,"loop": false,"timing": 0,"delay": 0, "toTopObj":1, "anchor":[0.5,0]},
            {"sprite": "box_18","x": 300,"y": 223,"loop": false,"timing": 0,"delay": 0, "toTopObj":1, "anchor":[0.5,0]},
         //   {"sprite": "icn_02","x": 35,"y": 343,"loop": false,"timing": 0,"delay": 0, "toTopObj":1, "anchor":[0,0]},
            {"sprite": "btn_03_3","x": 300,"y": 748,"loop": false,"timing": 0,"delay": 4.5, "toTopObj":1, "anchor":[0.5,0],"onClickFn":"editGoToPage(17,240,220,'ePerDet','cPerDet'),yesno_update('Update','Personal Details - Show')"},
            {"sprite": "btn_sp_1","x": 540,"y": 112,"loop": false,"timing": 0,"delay": 2, "toTopObj":1, "anchor":[0,0], "onClickFn":"audioReplay()"}
        ],
        "text_animations": [
           {"text": [{"content":"Pre Issuance Verification Call "}], "sx": -1000,"sy": 50,"x": 190 ,"y": 50,"size": 28,"weight":"bold","color":"#003869","tween_type": "Elastic.easeOut","timing": 200,"delay":0.2, "anchor":[0,0] },
            {"text": [
                    {"content":"Proposal Number: "},
                    {"content":"$var.window.p_PROPOSAL_NUMBER"}
                ], "sx": -1000,"sy": 120,"x": 40 ,"y": 150,"size": 22,"weight":"bold","color":"#235291","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },
             {"text": [
                    {"content":"In Case if you wish to hear again, Click here ---->"}
                              ], "sx": -1000,"sy": 120,"x": 290 ,"y": 120,"size": 12,"weight":"bold","color":"#235291","tween_type": "Elastic.easeOut","timing": 200,"delay":2, "anchor":[0,0],"wordWrap": true, "wordWrapWidth":520,"align":"right" },
            {"text": [{"content":"PERSONAL & CONTACT DETAILS"}], "sx": -1000,"sy": 120,"x": 300 ,"y": 184,"size": 26,"weight":"bold","color":"#FFFFFF","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0.5,0] },
            {"text": [{"content":"Your personal details as per  our records are "}], "sx": -1000,"sy": 120,"x": 80 ,"y": 235,"size": 18,"weight":"bold","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },

            {"text": [{"content":"Name"}], "sx": -1000,"sy": 120,"x": 80 ,"y": 270,"size": 18,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },
            {"text": [{"content":":"}], "sx": -1000,"sy": 120,"x": 310 ,"y": 270,"size": 18,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },

            {"text": [{"content":"Date of Birth"}], "sx": -1000,"sy": 120,"x": 80 ,"y": 310,"size": 18,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },
            {"text": [{"content":":"}], "sx": -1000,"sy": 120,"x": 310 ,"y": 310,"size": 18,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },

            {"text": [{"content":"Gender"}], "sx": -1000,"sy": 120,"x": 80 ,"y": 350,"size": 18,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },
            {"text": [{"content":":"}], "sx": -1000,"sy": 120,"x": 310 ,"y": 350,"size": 18,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },

            {"text": [{"content":"Email Id"}], "sx": -1000,"sy": 120,"x": 80 ,"y":390,"size": 18,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },
            {"text": [{"content":":"}], "sx": -1000,"sy": 120,"x": 310 ,"y": 390,"size": 18,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },

            {"text": [{"content":"Mobile No."}], "sx": -1000,"sy": 120,"x": 80 ,"y": 430,"size": 18,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },
            {"text": [{"content":":"}], "sx": -1000,"sy": 120,"x": 310 ,"y": 430,"size": 18,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },

            // {"text": [{"content":"Type of ID card Proof"}], "sx": -1000,"sy": 120,"x": 80 ,"y": 470,"size": 18,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },
            // {"text": [{"content":":"}], "sx": -1000,"sy": 120,"x": 310 ,"y": 470,"size": 18,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },

            {"text": [{"content":"Nominee name & relationship"}], "sx": -1000,"sy": 120,"x": 80 ,"y": 470,"size": 18,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },
            {"text": [{"content":":"}], "sx": -1000,"sy": 120,"x": 310 ,"y": 470,"size": 16,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },

            {"text": [{"content":"Annual income"}], "sx": -1000,"sy": 120,"x": 80 ,"y": 510,"size": 18,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },
            {"text": [{"content":":"}], "sx": -1000,"sy": 120,"x": 310 ,"y": 510,"size": 18,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },


            {"text": [{"content":"Address"}], "sx": -1000,"sy": 120,"x": 80 ,"y": 550,"size": 18,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },
            {"text": [{"content":":"}], "sx": -1000,"sy": 120,"x": 310 ,"y": 550,"size": 18,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },


            {"text": [{"content":"Please do the necessary corrections in boxes marked 'yellow', and then click on 'Save and Proceed' to proceed further."}], "sx": -1000,"sy": 120,"x": 300 ,"y": 676,"size": 18,"weight":"bold","color":"#235291","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0.5,0],"wordWrap":true,"wordWrapWidth":480 },

            {"text": [{"content":'Save & Proceed'}], "sx": -1000,"sy": 120,"x": 300 ,"y": 764,"size": 18,"weight":"bold","color":"#FFFFFF","tween_type": "Elastic.easeOut","timing": 200,"delay":4.5, "anchor":[0.5,0] },
        ],

        "input_animations": [
            {"text": [{"content":customer}], "placeHolder":" ", "key":"in_name", "sx": -1000,"sy": 120,"x": 325 ,"y": 270,"size": 18,"weight":"bold","width":250,"backgroundColor":"#e9e579","fill":"#2961ab","tween_type": "Elastic.easeOut","timing": 200,"delay":1, "anchor":[0,0] },
            {"text": [{"content":dob}], "placeHolder":" ", "key":"in_dob", "sx": -1000,"sy": 120,"x": 325 ,"y": 310,"size": 18,"weight":"bold","width":250,"backgroundColor":"#e9e579","fill":"#2961ab","tween_type": "Elastic.easeOut","timing": 200,"delay":1, "anchor":[0,0] },
            {"text": [{"content":gender}], "placeHolder":" ", "key":"in_gender", "sx": -1000,"sy": 120,"x": 325 ,"y": 350,"size": 18,"weight":"bold","width":250,"backgroundColor":"#e9e579","fill":"#2961ab","tween_type": "Elastic.easeOut","timing": 200,"delay":1, "anchor":[0,0] },
            {"text": [{"content":email}], "placeHolder":" ", "key":"in_email", "sx": -1000,"sy": 120,"x": 325 ,"y": 390,"size": 18,"weight":"bold","width":250,"backgroundColor":"#e9e579","fill":"#2961ab","tween_type": "Elastic.easeOut","timing": 200,"delay":1, "anchor":[0,0] },
            {"text": [{"content":mob}], "placeHolder":" ", "key":"in_mob","sx": -1000,"sy": 120,"x": 325 ,"y": 430,"size": 18,"weight":"bold","width":250,"backgroundColor":"#e9e579","fill":"#2961ab","tween_type": "Elastic.easeOut","timing": 200,"delay":1, "anchor":[0,0] },
            // {"text": [{"content":id}], "placeHolder":" ", "key":"in_id", "sx": -1000,"sy": 120,"x": 325 ,"y": 470,"size": 18,"weight":"bold","width":250,"backgroundColor":"#e9e579","fill":"#2961ab","tween_type": "Elastic.easeOut","timing": 200,"delay":1, "anchor":[0,0] },
            {"text": [{"content":nom}], "placeHolder":" ", "key":"in_nom", "sx": -1000,"sy": 120,"x": 325 ,"y": 470,"size": 18,"weight":"bold","width":250,"backgroundColor":"#e9e579","fill":"#2961ab","tween_type": "Elastic.easeOut","timing": 200,"delay":1, "anchor":[0,0] },
            {"text": [{"content":annul}], "placeHolder":" ", "key":"in_annul", "sx": -1000,"sy": 120,"x": 325 ,"y": 510,"size": 18,"weight":"bold","width":250,"backgroundColor":"#e9e579","fill":"#2961ab","tween_type": "Elastic.easeOut","timing": 200,"delay":1, "anchor":[0,0] },
            {"text": [
                       {"content":add}

             ], "placeHolder":" ", "key":"in_add", "sx": -1000,"sy": 120,"x": 325 ,"y": 550,"size": 18,"weight":"bold","width":250,"backgroundColor":"#e9e579","fill":"#2961ab","tween_type": "Elastic.easeOut","timing": 200,"delay":1, "anchor":[0,0],"wordWrap":true,"wordWrapWidth":100 }

        ],

        "sound_list": [
            {
                "sound": ["personal_disagree_audio"]
            }
        ],
        "functions": [
            {"fn": "SetBGTile('bg_02')", "delay": 0 },
             {"fn": "cameraAccessError()", "delay" : 0.5 },
             {"fn": "captureImage()", "delay": 1.5 }
        ],
        "name": "Personal Details - edit",
        "timing": -1,
        "index": 6
    },

{
          "sprite_animations": [
             {"sprite": "bar_01","x": 0,"y": 0,"loop": false,"timing": 0,"delay": 0,"toTopObj":1, "anchor":[0,0]},
            {"sprite": "logo_01","x": 10,"y": 33,"loop": false,"timing": 0,"delay": 0.1,"toTopObj":2, "anchor":[0,0]},
            {"sprite": "bar_03","x": 300,"y": 180,"loop": false,"timing": 0,"delay": 0, "toTopObj":1, "anchor":[0.5,0]},
            {"sprite": "box_18","x": 300,"y": 223,"loop": false,"timing": 0,"delay": 0, "toTopObj":1, "anchor":[0.5,0]},
           {"sprite": "loan_icon","x": 300,"y": 240,"loop": false,"timing": 0,"delay": 0.5, "toTopObj":1, "anchor":[0.5,0]},


              {"sprite": "btn_03_2","x": 150,"y": 674,"loop": false,"timing": 0,"delay": 2, "toTopObj":1, "anchor":[0.5,0],"onClickFn":"goToPage(8),yesno_update('Yes','Loan Details - Confirm')"},

            {"sprite": "btn_03_3","x": 450,"y": 674,"loop": false,"timing": 0,"delay": 2, "toTopObj":1, "anchor":[0.5,0],"onClickFn":"goToPageBack(8),yesno_update('No','Loan Details - Confirm')"},

            {"sprite": "btn_sp_1","x": 540,"y": 112,"loop": false,"timing": 0,"delay": 2, "toTopObj":1, "anchor":[0,0], "onClickFn":"audioReplay()"}
        ],
          "text_animations": [
           {"text": [{"content":"Pre Issuance Verification Call "}], "sx": -1000,"sy": 50,"x": 190 ,"y": 50,"size": 28,"weight":"bold","color":"#003869","tween_type": "Elastic.easeOut","timing": 200,"delay":0.2, "anchor":[0,0] },
            {"text": [
                    {"content":"Proposal Number: "},
                    {"content":"$var.window.p_PROPOSAL_NUMBER"}
                ], "sx": -1000,"sy": 120,"x": 40 ,"y": 150,"size": 22,"weight":"bold","color":"#235291","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },
             {"text": [
                    {"content":"In Case if you wish to hear again, Click here ---->"}
                              ], "sx": -1000,"sy": 120,"x": 290 ,"y": 120,"size": 12,"weight":"bold","color":"#235291","tween_type": "Elastic.easeOut","timing": 200,"delay":2, "anchor":[0,0],"wordWrap": true, "wordWrapWidth":520,"align":"right" },
            {"text": [{"content":"LOAN QUESTIONS"}], "sx": -1000,"sy": 120,"x": 300 ,"y": 184,"size": 26,"weight":"bold","color":"#FFFFFF","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0.5,0] },

            {"text": [
            {"content":"We believe that all the policy features have been explained to you correctly. The amount paid by you is only towards the premium of the Policy and you have not been promised with any kind of bonus, loan, Mobile tower installation or refund against any other policy. Please do not believe in any such false promises and highlight any such concern to us immediately. So can we confirm that no such promise has been made to you.\nPlease note that IndiaFirst may not consider any such complaint in the matter on a later date."}], "sx": -1000,"sy": 120,"x": 45 ,"y": 360,"size": 20,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0],"wordWrap": true, "wordWrapWidth":520,"align":"center" },

//{"text": [{"content":"Please note that IndiaFirst may not consider any such complaint in the matter on a later date."}], "sx": -1000,"sy": 120,"x": 45 ,"y": 570,"size": 20,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0],"wordWrap": true, "wordWrapWidth":520,"align":"center" },


            {"text": [{"content":'Yes'}], "sx": -1000,"sy": 120,"x": 150 ,"y": 688,"size": 26,"weight":"bold","color":"#FFFFFF","tween_type": "Elastic.easeOut","timing": 200,"delay":2, "anchor":[0.5,0] },
            {"text": [{"content":'No'}], "sx": -1000,"sy": 120,"x": 450 ,"y": 688,"size": 26,"weight":"bold","color":"#FFFFFF","tween_type": "Elastic.easeOut","timing": 200,"delay":2, "anchor":[0.5,0] },
        ],
        "functions": [

            {"fn": "SetBGTile('bg_02')", "delay": 0 },
           //  {"fn": "actionOnClickDelay2(5)", "delay": 0 },
            {"fn": "cameraAccessError()", "delay" : 0.5 },
            {"fn": "captureImage()", "delay": 1.5 }


        ],
         "sound_list": [
            {
                "sound": ["loan_question_audio"]
            }
        ],
        "name": "Loan Details - Confirm",
        "timing": -1,
        "index": 7
      },

   {
               "sprite_animations": [
            {"sprite": "bar_01","x": 0,"y": 0,"loop": false,"timing": 0,"delay": 0,"toTopObj":1, "anchor":[0,0]},
            {"sprite": "logo_01","x": 10,"y": 33,"loop": false,"timing": 0,"delay": 0.1,"toTopObj":2, "anchor":[0,0]},
            {"sprite": "bar_03","x": 300,"y": 180,"loop": false,"timing": 0,"delay": 0, "toTopObj":1, "anchor":[0.5,0]},
            {"sprite": "box_22","x": 300,"y": 223,"loop": false,"timing": 0,"delay": 0, "toTopObj":1, "anchor":[0.5,0]},
           {"sprite": "box_073","x": 30,"y": 640,"loop": false,"timing": 0,"delay": 0, "toTopObj":1, "anchor":[0,0]},
            {"sprite": "btn_03_2","x": 150,"y": 740,"loop": false,"timing": 0,"delay": 4, "toTopObj":1, "anchor":[0.5,0],"onClickFn":"goToScreen(13,'cPerDet',1),yesno_update('Agree','Plan Details')"},
            {"sprite": "btn_03_3","x": 450,"y": 740,"loop": false,"timing": 0,"delay": 4, "toTopObj":1, "anchor":[0.5,0],"onClickFn":"goToPage(9),yesno_update('Disagree','Plan Details')"},
            {"sprite": "btn_sp_1","x": 540,"y": 112,"loop": false,"timing": 0,"delay": 2, "toTopObj":1, "anchor":[0,0], "onClickFn":"audioReplay()"}
        ],
        "text_animations": [
           {"text": [{"content":"Pre Issuance Verification Call "}], "sx": -1000,"sy": 50,"x": 190 ,"y": 50,"size": 28,"weight":"bold","color":"#003869","tween_type": "Elastic.easeOut","timing": 200,"delay":0.2, "anchor":[0,0] },
            {"text": [
                    {"content":"Proposal Number: "},
                    {"content":"$var.window.p_PROPOSAL_NUMBER"}
                ], "sx": -1000,"sy": 120,"x": 40 ,"y": 150,"size": 22,"weight":"bold","color":"#235291","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },
             {"text": [
                    {"content":"In Case if you wish to hear again, Click here ---->"}
                              ], "sx": -1000,"sy": 120,"x": 290 ,"y": 120,"size": 12,"weight":"bold","color":"#235291","tween_type": "Elastic.easeOut","timing": 200,"delay":2, "anchor":[0,0],"wordWrap": true, "wordWrapWidth":520,"align":"right" },
            {"text": [{"content":"PLAN DETAILS"}], "sx": -1000,"sy": 120,"x": 300 ,"y": 186,"size": 26,"weight":"bold","color":"#FFFFFF","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0.5,0] },
            {"text": [{"content":"Your plan details as per  our records are "}], "sx": -1000,"sy": 120,"x": 100 ,"y": 230,"size": 18,"weight":"bold","color":"#3b3a3a ","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },

            {"text": [{"content":"Plan Name"}], "sx": -1000,"sy": 120,"x": 104 ,"y": 270,"size": 18,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },
            {"text": [{"content":":"}], "sx": -1000,"sy": 120,"x": 296 ,"y": 270,"size": 18,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },
            
            {"text": [{"content":"$var.window.p_planname"}], "sx": -1000,"sy": 120,"x": 310 ,"y": 270,"size": 18,"weight":"bold","color":"#0098DE","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },
           
             {"text": [{"content":"Plan Type"}], "sx": -1000,"sy": 120,"x": 104 ,"y": 320,"size": 18,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },
            {"text": [{"content":":"}], "sx": -1000,"sy": 120,"x": 296 ,"y": 320,"size": 18,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },

            {"text": [{"content":"Non Linked Participating Endowment Pension Plan"}], "sx": -1000,"sy": 120,"x": 310 ,"y": 306,"size": 18,"weight":"bold","color":"#0098DE","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0],"wordWrap":true,"wordWrapWidth":250,"lineSpacing":-5,"align":"center" },


            {"text": [{"content":"Premium Amount"}], "sx": -1000,"sy": 120,"x": 104 ,"y": 374,"size": 18,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },
            {"text": [{"content":":"}], "sx": -1000,"sy": 120,"x": 296 ,"y": 374,"size": 18,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },
            {"text": [
            {"content":"Rs. "},
            {"content":"$var.window.p_premiumamount","enableNumericType":true}
            ], "sx": -1000,"sy": 120,"x": 310 ,"y": 374,"size": 18,"weight":"bold","color":"#0098DE","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },


            {"text": [{"content":"Payment Frequency"}], "sx": -1000,"sy": 120,"x": 104 ,"y":434,"size": 18,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },
            {"text": [{"content":":"}], "sx": -1000,"sy": 120,"x": 296 ,"y": 434,"size": 18,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },
            {"text": [{"content":"$var.window.p_paymentFre"}], "sx": -1000,"sy": 120,"x": 310 ,"y": 434,"size": 18,"weight":"bold","color":"#0098DE","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },


            {"text": [{"content":"Premium Paying Term"}], "sx": -1000,"sy": 120,"x": 104 ,"y": 494,"size": 18,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },
            {"text": [{"content":":"}], "sx": -1000,"sy": 120,"x": 296 ,"y": 494,"size": 18,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },
            {"text": [
            {"content":"$var.window.p_payingTerm"},
            {"content":" years"}
            ], "sx": -1000,"sy": 120,"x": 310 ,"y": 494,"size": 18,"weight":"bold","color":"#0098DE","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },


            {"text": [{"content":"Policy Term"}], "sx": -1000,"sy": 120,"x": 104 ,"y": 554,"size": 18,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },
            {"text": [{"content":":"}], "sx": -1000,"sy": 120,"x": 296 ,"y": 554,"size": 18,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },
            {"text": [
            {"content":"$var.window.p_policyTerm"},
            {"content":" years"}
            ], "sx": -1000,"sy": 120,"x": 310 ,"y": 554,"size": 18,"weight":"bold","color":"#0098DE","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },

            {"text": [{"content":"Sum Assured"}], "sx": -1000,"sy": 120,"x": 104 ,"y": 605,"size": 18,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },
            {"text": [{"content":":"}], "sx": -1000,"sy": 120,"x": 296 ,"y": 605,"size": 18,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },
            {"text": [
            {"content":"Rs. "},
            {"content":"$var.window.p_Assuredsum","enableNumericType":true}
            ], "sx": -1000,"sy": 120,"x": 310 ,"y": 605,"size": 18,"weight":"bold","color":"#0098DE","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },
      
            
            {"text": [{"content":"IndiaFirst Little Champ Plan is a long term plan where you can get maximum benefits only if you pay till the end of term "},
                      {"content":"$var.window.p_payingTerm"},
                      {"content":" years."},

            ], "sx": -1000,"sy": 120,"x": 300 ,"y": 645,"size": 16,"weight":"bold","color":"#0098DE","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0.5,0],"wordWrap":true,"wordWrapWidth":500,"lineSpacing":-5 },


            {"text": [{"content":'If all information displayed on the screen is correct, Please click on "Agree", else click "Disagree"'}], "sx": -1000,"sy": 120,"x": 300 ,"y": 695,"size": 17,"weight":"bold","color":"#4c7dbf","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0.5,0],"wordWrap":true,"wordWrapWidth":500 },
         //  {"text": [{"content":'Please click on "Agree", else click "Disagree"'}], "sx": -1000,"sy": 120,"x": 300 ,"y": 698,"size": 18,"weight":"bold","color":"#4c7dbf","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0.5,0] },

            {"text": [{"content":'Agree'}], "sx": -1000,"sy": 120,"x": 150 ,"y": 754,"size": 24,"weight":"bold","color":"#FFFFFF","tween_type": "Elastic.easeOut","timing": 200,"delay":4, "anchor":[0.5,0] },
            {"text": [{"content":'Disagree'}], "sx": -1000,"sy": 120,"x": 450 ,"y": 754,"size": 24,"weight":"bold","color":"#FFFFFF","tween_type": "Elastic.easeOut","timing": 200,"delay":4, "anchor":[0.5,0] },
        ],
        "sound_list": [
            {
                //"sound": ["plan_details_audio","startplan","plan_name","Money_plan","premium_amount",numToWordIndian(2),"thousand","rupees","payment_frequency","monthly","paying_term",numToWordIndian(5),"years","policy_term",numToWordIndian(10),"years","sum_assured",numToWordIndian(6),"lakh","rupees"]
                 "sound":["plan_details_audio","startplan","plan_name","little_champ_plan","plantype","premium_amount","$var.currency_window.p_premiumamount","payment_frequency","$var.window.p_paymentFre","paying_term","$var.number_window.p_payingTerm","years","policy_term","$var.number_window.p_policyTerm","years","sum_assured","$var.currency_window.p_Assuredsum"]
        
            }
        ],
        "functions": [
           //  {"fn": "actionOnClickDelay2(13)", "delay": 0 }, 
            {"fn": "SetBGTile('bg_02')", "delay": 0 },       // actionOnClickDelay2
            {"fn": "cameraAccessError()", "delay" : 0.5 },
            {"fn": "captureImage()", "delay": 1.5 }

        ],
        "name": "Plan Details",
        "timing": -1,
        "index": 8
    },


    {
       "sprite_animations": [
             {"sprite": "bar_01","x": 0,"y": 0,"loop": false,"timing": 0,"delay": 0,"toTopObj":1, "anchor":[0,0]},
            {"sprite": "logo_01","x": 10,"y": 33,"loop": false,"timing": 0,"delay": 0.1,"toTopObj":2, "anchor":[0,0]},
            {"sprite": "bar_03","x": 300,"y": 180,"loop": false,"timing": 0,"delay": 0, "toTopObj":1, "anchor":[0.5,0]},
            {"sprite": "box_02","x": 300,"y": 223,"loop": false,"timing": 0,"delay": 0, "toTopObj":1, "anchor":[0.5,0]},

            {"sprite": "btn_03_2","x": 150,"y": 454,"loop": false,"timing": 0,"delay": 2, "toTopObj":1, "anchor":[0.5,0],"onClickFn":"goToPage(10)"},

            {"sprite": "btn_03_3","x": 450,"y": 454,"loop": false,"timing": 0,"delay": 2, "toTopObj":1, "anchor":[0.5,0],"onClickFn":"goToPageBack(8)"},

            {"sprite": "btn_sp_1","x": 540,"y": 112,"loop": false,"timing": 0,"delay": 2, "toTopObj":1, "anchor":[0,0], "onClickFn":"audioReplay()"}
        ],
          "text_animations": [
           {"text": [{"content":"Pre Issuance Verification Call "}], "sx": -1000,"sy": 50,"x": 190 ,"y": 50,"size": 28,"weight":"bold","color":"#003869","tween_type": "Elastic.easeOut","timing": 200,"delay":0.2, "anchor":[0,0] },
            {"text": [
                    {"content":"Proposal Number: "},
                    {"content":"$var.window.p_PROPOSAL_NUMBER"}
                ], "sx": -1000,"sy": 120,"x": 40 ,"y": 150,"size": 22,"weight":"bold","color":"#235291","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },
             {"text": [
                    {"content":"In Case if you wish to hear again, Click here ---->"}
                              ], "sx": -1000,"sy": 120,"x": 290 ,"y": 120,"size": 12,"weight":"bold","color":"#235291","tween_type": "Elastic.easeOut","timing": 200,"delay":2, "anchor":[0,0],"wordWrap": true, "wordWrapWidth":520,"align":"right" },
            {"text": [{"content":"PLAN DETAILS"}], "sx": -1000,"sy": 120,"x": 300 ,"y": 184,"size": 26,"weight":"bold","color":"#FFFFFF","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0.5,0] },
            {"text": [{"content":"Are you sure you want to disagree to the details on this screen."}], "sx": -1000,"sy": 120,"x": 50 ,"y": 280,"size": 35,"weight":"bold","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0],"wordWrap": true, "wordWrapWidth":520,"align":"center" },

            {"text": [{"content":'Yes'}], "sx": -1000,"sy": 120,"x": 150 ,"y": 468,"size": 26,"weight":"bold","color":"#FFFFFF","tween_type": "Elastic.easeOut","timing": 200,"delay":2, "anchor":[0.5,0] },
            {"text": [{"content":'No'}], "sx": -1000,"sy": 120,"x": 450 ,"y": 468,"size": 26,"weight":"bold","color":"#FFFFFF","tween_type": "Elastic.easeOut","timing": 200,"delay":2, "anchor":[0.5,0] },
        ],
        "functions": [

            {"fn": "SetBGTile('bg_02')", "delay": 0 }

        ],

          "sound_list": [
            {
                "sound": ["plan_yesno_audio"]
            }
        ],

        "name": "Plan Yesno - Confirm",
        "timing": -1,
        "index": 9
    },

     {
         "sprite_animations": [
             {"sprite": "bar_01","x": 0,"y": 0,"loop": false,"timing": 0,"delay": 0,"toTopObj":1, "anchor":[0,0]},
            {"sprite": "logo_01","x": 10,"y": 33,"loop": false,"timing": 0,"delay": 0.1,"toTopObj":2, "anchor":[0,0]},
            {"sprite": "bar_03","x": 300,"y": 180,"loop": false,"timing": 0,"delay": 0, "toTopObj":1, "anchor":[0.5,0]},
            {"sprite": "box_18","x": 300,"y": 223,"loop": false,"timing": 0,"delay": 0, "toTopObj":1, "anchor":[0.5,0]},
            {"sprite": "btn_03_3","x": 300,"y": 748,"loop": false,"timing": 0,"delay": 4.5, "toTopObj":1, "anchor":[0.5,0],"onClickFn":"editGoToPage(13,240,660,'ePerDet','cPerDet')"},
            {"sprite": "btn_sp_1","x": 540,"y": 112,"loop": false,"timing": 0,"delay": 2, "toTopObj":1, "anchor":[0,0], "onClickFn":"audioReplay()"}
        ],
        "text_animations": [
           {"text": [{"content":"Pre Issuance Verification Call "}], "sx": -1000,"sy": 50,"x": 190 ,"y": 50,"size": 28,"weight":"bold","color":"#003869","tween_type": "Elastic.easeOut","timing": 200,"delay":0.2, "anchor":[0,0] },
            {"text": [
                    {"content":"Proposal Number: "},
                    {"content":"$var.window.p_PROPOSAL_NUMBER"}
                ], "sx": -1000,"sy": 120,"x": 40 ,"y": 150,"size": 22,"weight":"bold","color":"#235291","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },
             {"text": [
                    {"content":"In Case if you wish to hear again, Click here ---->"}
                              ], "sx": -1000,"sy": 120,"x": 290 ,"y": 120,"size": 12,"weight":"bold","color":"#235291","tween_type": "Elastic.easeOut","timing": 200,"delay":2, "anchor":[0,0],"wordWrap": true, "wordWrapWidth":520,"align":"right" },
            {"text": [{"content":"PLAN DETAILS"}], "sx": -1000,"sy": 120,"x": 300 ,"y": 184,"size": 26,"weight":"bold","color":"#FFFFFF","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0.5,0] },
          //  {"text": [{"content":"Your personal details as per  our records are "}], "sx": -1000,"sy": 120,"x": 100 ,"y": 240,"size": 18,"weight":"bold","color":"#a3a5a6","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },

            {"text": [{"content":"Plan Name"}], "sx": -1000,"sy": 120,"x": 104 ,"y": 250,"size": 18,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },
            {"text": [{"content":":"}], "sx": -1000,"sy": 120,"x": 296 ,"y": 250,"size": 18,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },
           {"text": [{"content":"$var.window.p_planname"}], "sx": -1000,"sy": 120,"x": 310 ,"y": 250,"size": 18,"weight":"bold","color":"#0098DE","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },


            {"text": [{"content":"Plan Type"}], "sx": -1000,"sy": 120,"x": 104 ,"y": 310,"size": 18,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },
            {"text": [{"content":":"}], "sx": -1000,"sy": 120,"x": 296 ,"y": 310,"size": 18,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },

            {"text": [{"content":"Non Linked Participating Endowment Pension Plan"}], "sx": -1000,"sy": 120,"x": 310 ,"y": 305,"size": 18,"weight":"bold","color":"#0098DE","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0],"wordWrap":true,"wordWrapWidth":250,"lineSpacing":-5,"align":"center" },


            {"text": [{"content":"Premium Amount"}], "sx": -1000,"sy": 120,"x": 104 ,"y": 370,"size": 18,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },
            {"text": [{"content":":"}], "sx": -1000,"sy": 120,"x": 296 ,"y": 370,"size": 18,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },
            {"text": [
            {"content":"Rs. "},
            {"content":"$var.window.p_premiumamount","enableNumericType":true}
            ], "sx": -1000,"sy": 120,"x": 310 ,"y": 370,"size": 18,"weight":"bold","color":"#0098DE","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },


            {"text": [{"content":"Payment Frequency"}], "sx": -1000,"sy": 120,"x": 104 ,"y": 430,"size": 18,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },
            {"text": [{"content":":"}], "sx": -1000,"sy": 120,"x": 296 ,"y": 430,"size": 18,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },
            {"text": [{"content":"$var.window.p_paymentFre"}], "sx": -1000,"sy": 120,"x": 310 ,"y": 430,"size": 18,"weight":"bold","color":"#0098DE","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },


            {"text": [{"content":"Premium Paying Term"}], "sx": -1000,"sy": 120,"x": 104 ,"y": 490,"size": 18,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },
            {"text": [{"content":":"}], "sx": -1000,"sy": 120,"x": 296 ,"y": 490,"size": 18,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },
            {"text": [
            {"content":"$var.window.p_payingTerm"},
            {"content":" years"}
            ], "sx": -1000,"sy": 120,"x": 310 ,"y": 490,"size": 18,"weight":"bold","color":"#0098DE","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },


            {"text": [{"content":"Policy Term"}], "sx": -1000,"sy": 120,"x": 104 ,"y": 550,"size": 18,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },
            {"text": [{"content":":"}], "sx": -1000,"sy": 120,"x": 296 ,"y": 550,"size": 18,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },
            {"text": [
            {"content":"$var.window.p_policyTerm"},
            {"content":" years"}
            ], "sx": -1000,"sy": 120,"x": 310 ,"y": 550,"size": 18,"weight":"bold","color":"#0098DE","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },

            {"text": [{"content":"Sum Assured"}], "sx": -1000,"sy": 120,"x": 104 ,"y": 610,"size": 18,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },
            {"text": [{"content":":"}], "sx": -1000,"sy": 120,"x": 296 ,"y": 610,"size": 18,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },
            {"text": [
            {"content":"Rs. "},
            {"content":"$var.window.p_Assuredsum","enableNumericType":true}
            ], "sx": -1000,"sy": 120,"x": 310 ,"y": 610,"size": 18,"weight":"bold","color":"#0098DE","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },

         //   {"text": [{"content":"Equity "}], "sx": -1000,"sy": 120,"x": 104 ,"y": 488,"size": 16,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },
         //   {"text": [{"content":":"}], "sx": -1000,"sy": 120,"x": 296 ,"y": 488,"size": 16,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },
           {"text": [
                    {"content":"Enter your disagreement on the box provided above and click 'Save and Proceed'"}
                     ], "sx": -1000,"sy": 120,"x": 300 ,"y": 700,"size": 18,"weight":"bold","color":"#235291","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0.5,0],"wordWrap":true,"wordWrapWidth":500 },
            {"text": [{"content":'Save & Proceed'}], "sx": -1000,"sy": 120,"x": 300 ,"y": 764,"size": 18,"weight":"bold","color":"#FFFFFF","tween_type": "Elastic.easeOut","timing": 200,"delay":4.5, "anchor":[0.5,0] },
        ],
         "input_animations": [
          {"text": [{"content":""}], "key":"in_disagreement", "sx": -1000,"sy": 120,"x": 20 ,"y": 670,"size": 20,"weight":"bold","width":558,"tween_type": "Elastic.easeOut","timing": 200,"delay":1,"backgroundColor":"#e9e9e9","placeHolder":"Enter your disagreement ...", "anchor":[0,0] }
        ],
        "sound_list": [
            {
                "sound": ["plan_disagree_audio"]
            }
        ],
        "functions": [

            {"fn": "SetBGTile('bg_02')", "delay": 0 },

        ],
       "name": "Plan Details - edit",
        "timing": -1,
        "index": 10
    },


    {
         "sprite_animations": [
            {"sprite": "bar_01","x": 0,"y": 0,"loop": false,"timing": 0,"delay": 0,"toTopObj":1, "anchor":[0,0]},
            {"sprite": "logo_01","x": 10,"y": 33,"loop": false,"timing": 0,"delay": 0.1,"toTopObj":2, "anchor":[0,0]},
            {"sprite": "bar_03","x": 300,"y": 180,"loop": false,"timing": 0,"delay": 0, "toTopObj":1, "anchor":[0.5,0]},
            {"sprite": "box_23","x": 300,"y": 223,"loop": false,"timing": 0,"delay": 0, "toTopObj":1, "anchor":[0.5,0]},
             {"sprite": "medicon","x": 300,"y": 250,"loop": false,"timing": 0,"delay": 0.5, "toTopObj":1, "anchor":[0.5,0]},

               {"sprite": "btn_03_2","x": 150,"y": 604,"loop": false,"timing": 0,"delay": 2, "toTopObj":1, "anchor":[0.5,0],"onClickFn":"goToPage(14),yesno_update('Agree','Medical Questions')"},

            {"sprite": "btn_03_3","x": 450,"y": 604,"loop": false,"timing": 0,"delay": 2, "toTopObj":1, "anchor":[0.5,0],"onClickFn":"goToPageBack(12),yesno_update('Disgree','Medical Questions')"},

            {"sprite": "btn_sp_1","x": 540,"y": 112,"loop": false,"timing": 0,"delay": 2, "toTopObj":1, "anchor":[0,0], "onClickFn":"audioReplay()"}

        ],
          "text_animations": [
           {"text": [{"content":"Pre Issuance Verification Call "}], "sx": -1000,"sy": 50,"x": 190 ,"y": 50,"size": 28,"weight":"bold","color":"#003869","tween_type": "Elastic.easeOut","timing": 200,"delay":0.2, "anchor":[0,0] },
            {"text": [
                    {"content":"Proposal Number: "},
                    {"content":"$var.window.p_PROPOSAL_NUMBER"}
                ], "sx": -1000,"sy": 120,"x": 40 ,"y": 150,"size": 22,"weight":"bold","color":"#235291","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },
             {"text": [
                    {"content":"In Case if you wish to hear again, Click here ---->"}
                              ], "sx": -1000,"sy": 120,"x": 290 ,"y": 120,"size": 12,"weight":"bold","color":"#235291","tween_type": "Elastic.easeOut","timing": 200,"delay":2, "anchor":[0,0],"wordWrap": true, "wordWrapWidth":520,"align":"right" },
            {"text": [{"content":"MEDICAL QUESTIONS"}], "sx": -1000,"sy": 120,"x": 300 ,"y": 184,"size": 26,"weight":"bold","color":"#FFFFFF","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0.5,0] },

            {"text": [{"content":"We would like you to confirm that you have read and answered all the medical questions in the Proposal correctly and disclosed all details of medical/treatment history (if any)"}], "sx": -1000,"sy": 215,"x": 45 ,"y": 390,"size": 20,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5,"wordWrap": true, "wordWrapWidth":520,"align":"center", "anchor":[0,0] },
            {"text": [{"content":"[Non-disclosure of any adverse medical history may lead to rejection of claim in future]."}], "sx": -1000,"sy": 295,"x": 45 ,"y": 500,"size": 20,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5,"wordWrap": true, "wordWrapWidth":520,"align":"center", "anchor":[0,0] },

          //  {"text": [{"content":"There were a set of medical questions in the application form, which you have answered as “No” which means that you are not under any medication and you are in good health."}], "sx": -1000,"sy": 120,"x": 50 ,"y": 390,"size": 22,"weight":"bold","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0],"wordWrap": true, "wordWrapWidth":520,"align":"center" },

            {"text": [{"content":'Agree'}], "sx": -1000,"sy": 120,"x": 150 ,"y": 618,"size": 26,"weight":"bold","color":"#FFFFFF","tween_type": "Elastic.easeOut","timing": 200,"delay":2, "anchor":[0.5,0] },
            {"text": [{"content":'Disagree'}], "sx": -1000,"sy": 120,"x": 450 ,"y": 618,"size": 26,"weight":"bold","color":"#FFFFFF","tween_type": "Elastic.easeOut","timing": 200,"delay":2, "anchor":[0.5,0] },
        ],
        "functions": [

            {"fn": "SetBGTile('bg_02')", "delay": 0 },
            {"fn": "cameraAccessError()", "delay" : 0.5 },
            {"fn": "captureImage()", "delay": 1.5 }

        ],
         "sound_list": [
            {
                "sound": ["medical_audio"]
            }
        ],
        "name": "Medical Questions - Confirm",
        "timing": -1,
        "index": 11
    },


 {

          "sprite_animations": [
           {"sprite": "bar_01","x": 0,"y": 0,"loop": false,"timing": 0,"delay": 0,"toTopObj":1, "anchor":[0,0]},
            {"sprite": "logo_01","x": 10,"y": 33,"loop": false,"timing": 0,"delay": 0.1,"toTopObj":2, "anchor":[0,0]},
            {"sprite": "bar_03","x": 300,"y": 180,"loop": false,"timing": 0,"delay": 0, "toTopObj":1, "anchor":[0.5,0]},
            {"sprite": "box_06","x": 300,"y": 223,"loop": false,"timing": 0,"delay": 0, "toTopObj":1, "anchor":[0.5,0]},
            {"sprite": "illness_icon","x": 300,"y": 240,"loop": false,"timing": 0,"delay": 0.5, "toTopObj":1, "anchor":[0.5,0]},
            {"sprite": "btn_03_3","x": 300,"y": 748,"loop": false,"timing": 0,"delay": 4.5, "toTopObj":1, "anchor":[0.5,0],"onClickFn":"editGoToPage(14,240,197,'ePerDet','cPerDet')"},
            {"sprite": "btn_sp_1","x": 540,"y": 112,"loop": false,"timing": 0,"delay": 2, "toTopObj":1, "anchor":[0,0], "onClickFn":"audioReplay()"}
        ],

           "text_animations": [
           {"text": [{"content":"Pre Issuance Verification Call "}], "sx": -1000,"sy": 50,"x": 190 ,"y": 50,"size": 28,"weight":"bold","color":"#003869","tween_type": "Elastic.easeOut","timing": 200,"delay":0.2, "anchor":[0,0] },
            {"text": [
                    {"content":"Proposal Number: "},
                    {"content":"$var.window.p_PROPOSAL_NUMBER"}
                ], "sx": -1000,"sy": 120,"x": 40 ,"y": 150,"size": 22,"weight":"bold","color":"#235291","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },
             {"text": [
                    {"content":"In Case if you wish to hear again, Click here ---->"}
                              ], "sx": -1000,"sy": 120,"x": 290 ,"y": 120,"size": 12,"weight":"bold","color":"#235291","tween_type": "Elastic.easeOut","timing": 200,"delay":2, "anchor":[0,0],"wordWrap": true, "wordWrapWidth":520,"align":"right" },
            {"text": [{"content":"ILLNESS DETAILS"}], "sx": -1000,"sy": 120,"x": 300 ,"y": 184,"size": 26,"weight":"bold","color":"#FFFFFF","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0.5,0] },

            {"text": [{"content":"Name of the illness"}], "sx": -1000,"sy": 120,"x": 74 ,"y": 392,"size": 18,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },
            {"text": [{"content":":"}], "sx": -1000,"sy": 120,"x": 296 ,"y": 392,"size": 18,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },

            {"text": [{"content":"Period of diagnose"}], "sx": -1000,"sy": 120,"x": 74 ,"y": 462,"size": 18,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },
            {"text": [{"content":":"}], "sx": -1000,"sy": 120,"x": 296 ,"y": 462,"size": 18,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },


            {"text": [{"content":"Medicine consumption details"}], "sx": -1000,"sy": 120,"x": 74 ,"y": 532,"size": 18,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },
            {"text": [{"content":":"}], "sx": -1000,"sy": 120,"x": 296 ,"y": 532,"size": 18,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },


            {"text": [
                    {"content":"Capture the name of the illness, period of diagnose and medicine consumption details and click 'Save & Proceed'."}
                     ], "sx": -1000,"sy": 120,"x": 300 ,"y": 660,"size": 18,"weight":"bold","color":"#235291","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0.5,0],"wordWrap":true,"wordWrapWidth":450,"align":"left" },
            {"text": [{"content":'Save & Proceed'}], "sx": -1000,"sy": 120,"x": 300 ,"y": 764,"size": 18,"weight":"bold","color":"#FFFFFF","tween_type": "Elastic.easeOut","timing": 200,"delay":4.5, "anchor":[0.5,0] },
        ],
        "input_animations": [
            {"text": [{"content":" "}], "placeHolder":" ", "key":"in_name", "sx": -1000,"sy": 120,"x": 310 ,"y": 392,"size": 18,"weight":"bold","width":255,"backgroundColor":"#e9e579","fill":"#2961ab","tween_type": "Elastic.easeOut","timing": 200,"delay":1, "anchor":[0,0] },
            {"text": [{"content":" "}], "placeHolder":" ", "key":"in_dob", "sx": -1000,"sy": 120,"x": 310 ,"y": 462,"size": 18,"weight":"bold","width":255,"backgroundColor":"#e9e579","fill":"#2961ab","tween_type": "Elastic.easeOut","timing": 200,"delay":1, "anchor":[0,0] },
            {"text": [{"content":" "}], "placeHolder":" ", "key":"in_gender", "sx": -1000,"sy": 120,"x": 310 ,"y": 532,"size": 18,"weight":"bold","width":255,"backgroundColor":"#e9e579","fill":"#2961ab","tween_type": "Elastic.easeOut","timing": 200,"delay":1, "anchor":[0,0] },

        ],
        "sound_list": [
            {
                "sound": ["illness_details_audio"]
            }
        ],
        "functions": [

            {"fn": "SetBGTile('bg_02')", "delay": 0 }

        ],
        "name": "Illness Details - Capture",
        "timing": -1,
        "index":12

    },

//little champ

 {
            "sprite_animations": [
            {"sprite": "bar_01","x": 0,"y": 0,"loop": false,"timing": 0,"delay": 0,"toTopObj":1, "anchor":[0,0]},
            {"sprite": "logo_01","x": 10,"y": 33,"loop": false,"timing": 0,"delay": 0.1,"toTopObj":2, "anchor":[0,0]},
            {"sprite": "bar_03","x": 300,"y": 180,"loop": false,"timing": 0,"delay": 0, "toTopObj":1, "anchor":[0.5,0]},
            {"sprite": "box_02","x": 300,"y": 230,"loop": false,"timing": 0,"delay": 0, "toTopObj":1, "anchor":[0.5,0]},
            {"sprite": "box_02","x": 300,"y": 450,"loop": false,"timing": 0,"delay": 0, "toTopObj":1, "anchor":[0.5,0]},
            {"sprite": "maturity_icon","x": 300,"y": 280,"loop": false,"timing": 0,"delay": 0.5, "toTopObj":1, "anchor":[0.5,0]},
            {"sprite": "dea_icon","x": 300,"y": 500,"loop": false,"timing": 0,"delay": 0.5, "toTopObj":1, "anchor":[0.5,0]},

             {"sprite": "btn_03_3","x": 300,"y": 748,"loop": false,"timing": 0,"delay":1.5 , "toTopObj":1, "anchor":[0.5,0],"onClickFn":"goToPage(11)"},
            {"sprite": "btn_sp_1","x": 540,"y": 112,"loop": false,"timing": 0,"delay": 2, "toTopObj":1, "anchor":[0,0], "onClickFn":"audioReplay()"}
        ],

         "text_animations": [
        {"text": [{"content":"Pre Issuance Verification Call "}], "sx": -1000,"sy": 50,"x": 190 ,"y": 50,"size": 28,"weight":"bold","color":"#003869","tween_type": "Elastic.easeOut","timing": 200,"delay":0.2, "anchor":[0,0] },

        {"text": [
                    {"content":"Proposal Number: "},
                    {"content":"$var.window.p_PROPOSAL_NUMBER"}
        ], "sx": -1000,"sy": 120,"x": 40 ,"y": 150,"size": 22,"weight":"bold","color":"#235291","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },

        {"text": [
                    {"content":"In Case if you wish to hear again, Click here ---->"}
         ], "sx": -1000,"sy": 120,"x": 290 ,"y": 120,"size": 12,"weight":"bold","color":"#235291","tween_type": "Elastic.easeOut","timing": 200,"delay":2, "anchor":[0,0],"wordWrap": true, "wordWrapWidth":520,"align":"right" },

        {"text": [{"content":"PRODUCT BENEFITS"}], "sx": -1000,"sy": 120,"x": 300 ,"y": 184,"size": 26,"weight":"bold","color":"#FFFFFF","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0.5,0] },

        {"text": [{"content":"Maturity Benefit"}], "sx": -1000,"sy": 120,"x": 300 ,"y": 240,"size": 18,"weight":"bold","color":"","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0.5,0] },

        {"text": [{"content":"You will receive guaranteed payout As per option chosen by you 101% to 125% Of your Sum Assured additionally you will get Simple Reversionary Bonus till date of maturity + may get terminal Bonus."}], "sx": -1000,"sy": 120,"x": 50 ,"y": 350,"size": 17,"weight":"bold","color":"#003869","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5,"wordWrap": true, "wordWrapWidth":500,"align":"center", "anchor":[0,0] },


        {"text": [{"content":"Death Benefit"}], "sx": -1000,"sy": 120,"x": 300 ,"y": 460,"size": 18,"weight":"bold","color":"","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0.5,0] },

        {"text": [{"content":"In case of death of the life assured nominee shall receive the higher of sum assured or 10 times Of Annual premium or 105% of total premium paid till death + All guaranteed payout & maturity benefit are paid as scheduled + Policy continue to accrue Bonuses."}], "sx": -1000,"sy": 120,"x": 50 ,"y": 560,"size": 17,"weight":"bold","color":"#003869","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5,"wordWrap": true, "wordWrapWidth":500,"align":"center", "anchor":[0,0] },


        {"text": [{"content":"Hope you have fully understood all the features of this policy."}], "sx": -1000,"sy": 120,"x": 300 ,"y": 710,"size": 18,"weight":"bold","color":"#235291","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0.5,0] },

        {"text": [{"content":"Proceed"}], "sx": -1000,"sy": 750,"x": 300 ,"y": 762,"size": 22,"weight":"bold","color":"#FFFFFF","tween_type": "Elastic.easeOut","timing": 200,"delay":1, "anchor":[0.5,0] }



        ],
        "sound_list": [
            {
               // "sound": ["newbenefit","little_maturity_benefit","little_death_benefit"]

                 "sound": ["newbenefit","little_overall","hope"]
            }
        ],
        "functions": [
            {"fn": "SetBGTile('bg_02')", "delay": 0 }

        ],
        "name": "Benefits Details - Show",
        "timing": -1,
        "index":13
      },


    {
                 "sprite_animations": [
           {"sprite": "bar_01","x": 0,"y": 0,"loop": false,"timing": 0,"delay": 0,"toTopObj":1, "anchor":[0,0]},
            {"sprite": "logo_01","x": 10,"y": 33,"loop": false,"timing": 0,"delay": 0.1,"toTopObj":2, "anchor":[0,0]},
            {"sprite": "bar_03","x": 300,"y": 180,"loop": false,"timing": 0,"delay": 0, "toTopObj":1, "anchor":[0.5,0]},
            {"sprite": "box_072","x": 300,"y": 220,"loop": false,"timing": 0,"delay": 0, "toTopObj":1, "anchor":[0.5,0]},
            {"sprite": "btn_03_3","x": 300,"y": 742,"loop": false,"timing": 0,"delay": 0, "toTopObj":1, "anchor":[0.5,0],"onClickFn":"onVideoRecord('btn_03_3',15,13,220,715,15)"},
            {"sprite": "btn_sp_1","x": 540,"y": 112,"loop": false,"timing": 0,"delay": 2, "toTopObj":1, "anchor":[0,0], "onClickFn":"audioreplay2()"}
        ],
          "text_animations": [
            {"text": [{"content":"Pre Issuance Verification Call "}], "sx": -1000,"sy": 50,"x": 190 ,"y": 50,"size": 28,"weight":"bold","color":"#003869","tween_type": "Elastic.easeOut","timing": 200,"delay":0.2, "anchor":[0,0] },
            {"text": [
                    {"content":"Proposal Number: "},
                    {"content":"$var.window.p_PROPOSAL_NUMBER"}
                ], "sx": -1000,"sy": 120,"x": 40 ,"y": 150,"size": 22,"weight":"bold","color":"#235291","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },
            {"text": [
                    {"content":"In Case if you wish to hear again, Click here ---->"}
                              ], "sx": -1000,"sy": 120,"x": 290 ,"y": 120,"size": 12,"weight":"bold","color":"#235291","tween_type": "Elastic.easeOut","timing": 200,"delay":2, "anchor":[0,0],"wordWrap": true, "wordWrapWidth":520,"align":"right" },
            {"text": [{"content":"VIDEO CONSENT"}], "sx": -1000,"sy": 120,"x": 300 ,"y": 184,"size": 26,"weight":"bold","color":"#FFFFFF","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0.5,0] },

             {"text": [

            // {"content":'Please ensure that you are able to view yourself within the square box clearly till the screen displays as "Face Detected" and then click on "Record" to proceed further.'}
            {"content":"I "},
            {"content":"$var.window.p_CUSTOMER_NAME"},
            {"content":" , hereby confirm  the above details of my proposal and give my consent for the Insurance Policy to be proceed further. I also confirm that the Application form and Benefit Illustration has been signed by me personally."}

             ], "sx": -1000,"sy": 205,"x": 45 ,"y": 225,"size": 18,"weight":"normal","color":"#545252","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5,"wordWrap": true, "wordWrapWidth":520,"align":"center", "anchor":[0,0] },

             {"text": [{"content":'Record'}], "sx": -1000,"sy": 120,"x": 300 ,"y": 757,"size": 26,"weight":"bold","color":"#FFFFFF","tween_type": "Elastic.easeOut","timing": 200,"delay":0, "anchor":[0.5,0] },

        ],
        "functions": [
             {"fn": "SetBGTile('bg_02')", "delay": 0 },
              {"fn": "actionOnClickDelay2(18)", "delay": 0 },
              {"fn": "cameraAccessError()", "delay" : 0.5 },
              {"fn": "initWebCamPos(300,350,500,350)", "delay" : 1  },
              {"fn": "faceDetectStart(60,360)", "delay" : 2  },


            ],
         "sound_list": [
            {
                "sound": ["video_audio"]
            }
        ],
        "name": "Video consent - Confirm",
        "timing": -1,
        "index": 14

    },

    {
           "sprite_animations": [
           {"sprite": "bar_01","x": 0,"y": 0,"loop": false,"timing": 0,"delay": 0,"toTopObj":1, "anchor":[0,0]},
            {"sprite": "logo_01","x": 10,"y": 33,"loop": false,"timing": 0,"delay": 0.1,"toTopObj":2, "anchor":[0,0]},
            {"sprite": "bar_03","x": 300,"y": 180,"loop": false,"timing": 0,"delay": 0, "toTopObj":1, "anchor":[0.5,0]},
            {"sprite": "box_10","x": 300,"y": 223,"loop": false,"timing": 0,"delay": 0, "toTopObj":1, "anchor":[0.5,0]},
            {"sprite":"hand","x":300,"y":220,"loop":false,"timing":0,"delay":26,"toTopObj":1, "anchor":[0.5,0]},

            {"sprite":"icn_10","x":300,"y":273,"loop":false,"timing":0,"delay":0,"toTopObj":1, "anchor":[0.5,0],"disappear":25},
           {"sprite": "phno","x":120,"y": 400,"loop":false,"timing":1000,"delay": 29,"toTopObj":1,"anchor":[0.5,0]},
            {"sprite": "Email","x":120,"y": 486,"loop":false,"timing":1000,"delay": 37,"toTopObj":1,"anchor":[0.5,0]},
           {"sprite": "web_icon","x":120,"y": 560,"loop":false,"timing":1000,"delay": 42,"toTopObj":1,"anchor":[0.5,0]},

            {"sprite": "btn_sp_1","x": 540,"y": 112,"loop": false,"timing": 0,"delay": 2, "toTopObj":1, "anchor":[0,0], "onClickFn":"audioReplay()"}
        ],
          "text_animations": [
           {"text": [{"content":"Pre Issuance Verification Call "}], "sx": -1000,"sy": 50,"x": 190 ,"y": 50,"size": 28,"weight":"bold","color":"#003869","tween_type": "Elastic.easeOut","timing": 200,"delay":0.2, "anchor":[0,0] },
           {"text": [
                    {"content":"Proposal Number: "},
                    {"content":"$var.window.p_PROPOSAL_NUMBER"}
                ], "sx": -1000,"sy": 120,"x": 40 ,"y": 150,"size": 22,"weight":"bold","color":"#235291","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },
            {"text": [
                    {"content":"In Case if you wish to hear again, Click here ---->"}
                              ], "sx": -1000,"sy": 120,"x": 290 ,"y": 120,"size": 12,"weight":"bold","color":"#235291","tween_type": "Elastic.easeOut","timing": 200,"delay":2, "anchor":[0,0],"wordWrap": true, "wordWrapWidth":520,"align":"right" },
            {"text": [{"content":"THANK YOU"}], "sx": -1000,"sy": 120,"x": 300 ,"y": 184,"size": 26,"weight":"bold","color":"#FFFFFF","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0.5,0] },

            {"text": [
            {"content":"Dear "},
            {"content":"$var.window.p_CUSTOMER_NAME"},
            {"content":", we would also like to thank you for choosing ECS/Direct debit as your mode of payment. This shall help you making your payments in a hassle free manner. Just ensure to keep sufficient funds in your account on the due date."}
            ], "sx": -1000,"sy": 120,"x": 50 ,"y": 451,"size": 20,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0],"wordWrap": true, "wordWrapWidth":520,"align":"center","disappear":25 },

             {"text": [{"content":"Our underwriting team is going through the application and shall provide their decision soon. In case, there is any requirement from your side, IndiaFirst shall soon get in touch with you."}], "sx": -1000,"sy": 120,"x": 50 ,"y": 580,"size": 20,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0],"wordWrap": true, "wordWrapWidth":520,"align":"center","disappear":25},

           {"text": [{"content":"In case of further queries, please feel free to contact us"}], "sx": -1000,"sy": 120,"x": 60 ,"y": 320,"size": 22,"weight":"normal","color":"#003869","tween_type": "Elastic.easeOut","timing": 200,"delay":26, "anchor":[0,0],"wordWrap": true, "wordWrapWidth":520,"align":"center" },


           {"text": [{"content":"1800-209-8700"}], "sx": -1000,"sy": 120,"x": 180 ,"y": 420,"size": 22,"weight":"normal","color":"#0098DE","tween_type": "Elastic.easeOut","timing": 200,"delay":30, "anchor":[0,0],"wordWrap": true, "wordWrapWidth":520,"align":"center" },

          {"text": [{"content":"[9am - 7pm (Mon - Sat) ]"}], "sx": -1000,"sy": 120,"x": 340 ,"y": 420,"size": 22,"weight":"normal","color":"#0098DE","tween_type": "Elastic.easeOut","timing": 200,"delay":34, "anchor":[0,0],"wordWrap": true, "wordWrapWidth":520,"align":"center" },


          {"text": [{"content":"customer.first@indiafirstlife.com"}], "sx": -1000,"sy": 120,"x": 180 ,"y": 500,"size": 22,"weight":"normal","color":"#0098DE","tween_type": "Elastic.easeOut","timing": 200,"delay":38, "anchor":[0,0],"wordWrap": true, "wordWrapWidth":520,"align":"center" },

          {"text": [{"content":"www.indiafirstlife.com"}], "sx": -1000,"sy": 120,"x": 180 ,"y": 580,"size": 22,"weight":"normal","color":"#0098DE","tween_type": "Elastic.easeOut","timing": 200,"delay":43, "anchor":[0,0],"wordWrap": true, "wordWrapWidth":520,"align":"center" },


           {"text": [{"content":"!!! Have a pleasant day !!!"}], "sx": -1000,"sy": 120,"x": 140 ,"y": 700,"size": 30,"weight":"normal","color":"#003869","tween_type": "Elastic.easeOut","timing": 200,"delay":56, "anchor":[0,0],"wordWrap": true, "wordWrapWidth":520,"align":"center" },

        ],
        "functions": [
            {"fn": "SetBGTile('bg_02')", "delay": 0 }
        ],
         "sound_list": [
            {
                "sound": ["tq_audio"]
            }
        ],
        "name": "Thankyou - Confirm",
        "timing": -1,
        "index": 15
    },

   {
          "sprite_animations": [
             {"sprite": "bar_01","x": 0,"y": 0,"loop": false,"timing": 0,"delay": 0,"toTopObj":1, "anchor":[0,0]},
            {"sprite": "logo_01","x": 10,"y": 33,"loop": false,"timing": 0,"delay": 0.1,"toTopObj":2, "anchor":[0,0]},
            {"sprite": "bar_03","x": 300,"y": 180,"loop": false,"timing": 0,"delay": 0, "toTopObj":1, "anchor":[0.5,0]},
            {"sprite": "box_22","x": 300,"y": 223,"loop": false,"timing": 0,"delay": 0, "toTopObj":1, "anchor":[0.5,0]},
           {"sprite": "equity_icon","x": 300,"y": 240,"loop": false,"timing": 0,"delay": 0.5, "toTopObj":1, "anchor":[0.5,0]},


            {"sprite": "btn_03_2","x": 300,"y": 704,"loop": false,"timing": 0,"delay": 2, "toTopObj":1, "anchor":[0.5,0],"onClickFn":"goToPage(13)"},

           // {"sprite": "btn_03_3","x": 450,"y": 674,"loop": false,"timing": 0,"delay": 2, "toTopObj":1, "anchor":[0.5,0],"onClickFn":"goToPageBack(8)"},

            {"sprite": "btn_sp_1","x": 540,"y": 112,"loop": false,"timing": 0,"delay": 2, "toTopObj":1, "anchor":[0,0], "onClickFn":"audioReplay()"}
        ],
          "text_animations": [
           {"text": [{"content":"Pre Issuance Verification Call "}], "sx": -1000,"sy": 50,"x": 190 ,"y": 50,"size": 28,"weight":"bold","color":"#003869","tween_type": "Elastic.easeOut","timing": 200,"delay":0.2, "anchor":[0,0] },
            {"text": [
                    {"content":"Proposal Number: "},
                    {"content":"$var.window.p_PROPOSAL_NUMBER"}
                ], "sx": -1000,"sy": 120,"x": 40 ,"y": 150,"size": 22,"weight":"bold","color":"#235291","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },
             {"text": [
                    {"content":"In Case if you wish to hear again, Click here ---->"}
                              ], "sx": -1000,"sy": 120,"x": 290 ,"y": 120,"size": 12,"weight":"bold","color":"#235291","tween_type": "Elastic.easeOut","timing": 200,"delay":2, "anchor":[0,0],"wordWrap": true, "wordWrapWidth":520,"align":"right" },
            {"text": [{"content":"EQUITY"}], "sx": -1000,"sy": 120,"x": 300 ,"y": 184,"size": 26,"weight":"bold","color":"#FFFFFF","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0.5,0] },

            {"text": [{"content":"Equity is a high risk investment option. As an investor, if you have a long term horizon, equity investments will work the best for you."}], "sx": -1000,"sy": 120,"x": 50 ,"y": 350,"size": 20.5,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0],"wordWrap":true,"wordWrapWidth":510,"align":"center"},//,"lineSpacing": },

            {"text": [{"content":"There is an Auto Trigger Based Feature in your policy. In case the return is 10% or higher in Equity I fund, the amount equal to the appreciation will be shifted to the Debt I fund."}], "sx": -1000,"sy": 120,"x": 50 ,"y": 455,"size": 20.5,"weight":"normal","color":"#3b3a3a ","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0],"wordWrap":true,"wordWrapWidth":510,"align":"center"},//"lineSpacing":},

            {"text": [{"content":"For e.g If the fund value at the time of taking the policy was Rs. 100/- & it increases by Rs. 10/- (i.e. 110% of Fund Value) then the increased amount of Rs. 10 /- will be shifted to Debt I fund."}], "sx": -1000,"sy": 120,"x": 50 ,"y": 557,"size": 20.5,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0],"wordWrap":true,"wordWrapWidth":510,"align":"center"},//,"lineSpacing":},


           // {"text": [{"content":"We believe that all the policy features have been explained to you correctly. The amount paid by you is only towards the premium of the Policy and you have not been promised with any kind of bonus, loan, Mobile tower installation or refund against any other policy. Please do not believe in any such false promises and highlight any such concern to us immediately. So can we confirm that no such promise has been made to you."}], "sx": -1000,"sy": 120,"x": 50 ,"y": 360,"size": 22,"weight":"bold","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0],"wordWrap": true, "wordWrapWidth":520,"align":"center" },

            {"text": [{"content":'Proceed'}], "sx": -1000,"sy": 120,"x": 300 ,"y": 718,"size": 26,"weight":"bold","color":"#FFFFFF","tween_type": "Elastic.easeOut","timing": 200,"delay":2, "anchor":[0.5,0] },
           // {"text": [{"content":'NO'}], "sx": -1000,"sy": 120,"x": 450 ,"y": 688,"size": 26,"weight":"bold","color":"#FFFFFF","tween_type": "Elastic.easeOut","timing": 200,"delay":2, "anchor":[0.5,0] },
        ],
        "functions": [

            {"fn": "SetBGTile('bg_02')", "delay": 0 }

        ],
         "sound_list": [
            {
                "sound": ["equity_audio"]
            }
        ],
         "name": "equity - Confirm",
        "timing": -1,
        "index": 16
      },

        {
           "sprite_animations": [
                  {"sprite": "bar_01","x": 0,"y": 0,"loop": false,"timing": 0,"delay": 0,"toTopObj":1, "anchor":[0,0]},
                  {"sprite": "logo_01","x": 10,"y": 33,"loop": false,"timing": 0,"delay": 0.1,"toTopObj":2, "anchor":[0,0]},
                  {"sprite": "bar_03","x": 300,"y": 180,"loop": false,"timing": 0,"delay": 0, "toTopObj":1, "anchor":[0.5,0]},
                  {"sprite": "box_18","x": 300,"y": 223,"loop": false,"timing": 0,"delay": 0, "toTopObj":1, "anchor":[0.5,0]},
               //   {"sprite": "icn_02","x": 33,"y": 343,"loop": false,"timing": 0,"delay": 0, "toTopObj":1, "anchor":[0,0]},

            {"sprite": "btn_03_2","x": 150,"y": 740,"loop": false,"timing": 0,"delay": 4, "toTopObj":1, "anchor":[0.5,0],"onClickFn":"goToScreen(7,'cPerDet',1),yesno_update('Agree','Personal Details - Show')"},
            {"sprite": "btn_03_3","x": 450,"y": 740,"loop": false,"timing": 0,"delay": 4, "toTopObj":1, "anchor":[0.5,0],"onClickFn":"goToPage(5),yesno_update('Disagree','Personal Details - Show')"},
                  {"sprite": "btn_sp_1","x": 540,"y": 112,"loop": false,"timing": 0,"delay": 2, "toTopObj":1, "anchor":[0,0], "onClickFn":"audioReplay()"}
              ],
              "text_animations": [
                 {"text": [{"content":"Pre Issuance Verification Call "}], "sx": -1000,"sy": 50,"x": 190 ,"y": 50,"size": 28,"weight":"bold","color":"#003869","tween_type": "Elastic.easeOut","timing": 200,"delay":0.2, "anchor":[0,0] },
                  {"text": [
                          {"content":"Proposal Number: "},
                          {"content":"$var.window.p_PROPOSAL_NUMBER"}
                      ], "sx": -1000,"sy": 120,"x": 40 ,"y": 150,"size": 22,"weight":"bold","color":"#235291","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },
                   {"text": [
                          {"content":"In Case if you wish to hear again, Click here ---->"}
                                    ], "sx": -1000,"sy": 120,"x": 290 ,"y": 120,"size": 12,"weight":"bold","color":"#235291","tween_type": "Elastic.easeOut","timing": 200,"delay":2, "anchor":[0,0],"wordWrap": true, "wordWrapWidth":520,"align":"right" },
                  {"text": [{"content":"PERSONAL & CONTACT DETAILS"}], "sx": -1000,"sy": 120,"x": 300 ,"y": 184,"size": 26,"weight":"bold","color":"#FFFFFF","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0.5,0] },
                  {"text": [{"content":"Your personal details as per  our records are "}], "sx": -1000,"sy": 120,"x": 80 ,"y": 235,"size": 18,"weight":"bold","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },

                  {"text": [{"content":"Name"}], "sx": -1000,"sy": 120,"x": 80 ,"y": 270,"size": 18,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },
                  {"text": [{"content":":"}], "sx": -1000,"sy": 120,"x": 310 ,"y": 270,"size": 18,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },
                  {"text": [{"content":"$var.window.up_CUSTOMER_NAME"}], "sx": -1000,"sy": 120,"x": 325 ,"y": 270,"size": 18,"weight":"bold","color":"#0098DE","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },


                  {"text": [{"content":"Date of Birth"}], "sx": -1000,"sy": 120,"x": 80 ,"y": 310,"size": 18,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },
                  {"text": [{"content":":"}], "sx": -1000,"sy": 120,"x": 310 ,"y": 310,"size": 18,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },
                  {"text": [{"content":"$var.window.up_DOB_PH"}], "sx": -1000,"sy": 120,"x": 325 ,"y": 310,"size": 18,"weight":"bold","color":"#0098DE","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },


                  {"text": [{"content":"Gender"}], "sx": -1000,"sy": 120,"x": 80 ,"y": 350,"size": 18,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },
                  {"text": [{"content":":"}], "sx": -1000,"sy": 120,"x": 310 ,"y": 350,"size": 18,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },
                  {"text": [{"content":"$var.window.up_MA_GENDER"}], "sx": -1000,"sy": 120,"x": 325 ,"y": 350,"size": 18,"weight":"bold","color":"#0098DE","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },


                  {"text": [{"content":"Email Id"}], "sx": -1000,"sy": 120,"x": 80 ,"y":390,"size": 18,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },
                  {"text": [{"content":":"}], "sx": -1000,"sy": 120,"x": 310 ,"y": 390,"size": 18,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },
                  {"text": [{"content":"$var.window.up_EMAIL"}], "sx": -1000,"sy": 120,"x": 325 ,"y": 390,"size": 18,"weight":"bold","color":"#0098DE","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },


                  {"text": [{"content":"Mobile No."}], "sx": -1000,"sy": 120,"x": 80 ,"y": 430,"size": 18,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },
                  {"text": [{"content":":"}], "sx": -1000,"sy": 120,"x": 310 ,"y": 430,"size": 18,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },
                  {"text": [{"content":"$var.window.up_MOBILE_NUMBER"}], "sx": -1000,"sy": 120,"x": 325 ,"y": 430,"size": 18,"weight":"bold","color":"#0098DE","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },

                  // {"text": [{"content":"Type of ID card Proof"}], "sx": -1000,"sy": 120,"x": 80 ,"y": 470,"size": 18,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },
                  // {"text": [{"content":":"}], "sx": -1000,"sy": 120,"x": 310 ,"y": 470,"size": 18,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },
                  // {"text": [{"content":"$var.window.up_IDPROF"}], "sx": -1000,"sy": 120,"x": 325 ,"y": 470,"size": 18,"weight":"bold","color":"#0098DE","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },

                  {"text": [{"content":"Nominee name & relationship"}], "sx": -1000,"sy": 120,"x": 80 ,"y": 470,"size": 18,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },
                  {"text": [{"content":":"}], "sx": -1000,"sy": 120,"x": 310 ,"y": 470,"size": 18,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },
                  {"text": [{"content":"$var.window.up_NOMINEE_NAME"}], "sx": -1000,"sy": 120,"x": 325 ,"y": 470,"size": 18,"weight":"bold","color":"#0098DE","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },

                  {"text": [{"content":"Annual income"}], "sx": -1000,"sy": 120,"x": 80 ,"y": 510,"size": 18,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },
                  {"text": [{"content":":"}], "sx": -1000,"sy": 120,"x": 310 ,"y": 510,"size": 18,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },
                  {"text": [{"content":"$var.window.up_ANNUALINCOME"}], "sx": -1000,"sy": 120,"x": 325 ,"y": 510,"size": 18,"weight":"bold","color":"#0098DE","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },


                  {"text": [{"content":"Address"}], "sx": -1000,"sy": 120,"x": 80 ,"y": 550,"size": 18,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },
                  {"text": [{"content":":"}], "sx": -1000,"sy": 120,"x": 310 ,"y": 550,"size": 18,"weight":"normal","color":"#3b3a3a","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0] },
                  {"text": [
                             {"content":"$var.window.up_address"}
                             //  {"content":", "},
                             // {"content": "$var.window.p_MAILINGADDRESS2"},
                             //  {"content":", "},
                             // {"content": "$var.window.p_MAILINGCITY"}
                  ], "sx": -1000,"sy": 120,"x": 325 ,"y": 550,"size": 18,"weight":"bold","color":"#0098DE","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0,0],"wordWrap":true,"wordWrapWidth":230,"align":"left","lineSpacing":-5},


                  {"text": [{"content":"If all information displayed on the screen is correct,"}], "sx": -1000,"sy": 120,"x": 300 ,"y": 676,"size": 18,"weight":"bold","color":"#235291","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0.5,0] },
                  {"text": [{"content":'Please click on "Agree", else click "Disagree"'}], "sx": -1000,"sy": 120,"x": 300 ,"y": 698,"size": 18,"weight":"bold","color":"#235291","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0.5,0] },

                  {"text": [{"content":'Agree'}], "sx": -1000,"sy": 120,"x": 150 ,"y": 754,"size": 24,"weight":"bold","color":"#FFFFFF","tween_type": "Elastic.easeOut","timing": 200,"delay":4, "anchor":[0.5,0] },
                  {"text": [{"content":'Disagree'}], "sx": -1000,"sy": 120,"x": 450 ,"y": 754,"size": 24,"weight":"bold","color":"#FFFFFF","tween_type": "Elastic.easeOut","timing": 200,"delay":4, "anchor":[0.5,0] },
              ],
              "sound_list": [
                  {
                      "sound": ["personal_details_audio"]
                  }
              ],
              "functions": [
                  {"fn": "SetBGTile('bg_02')", "delay": 0 },
                   {"fn": "cameraAccessError()", "delay" : 0.5 },
                   {"fn": "captureImage()", "delay": 1.5 }
              ],
              "name": "Personal Details - Show",
              "timing": -1,
              "index": 17
          },

{
        "sprite_animations": [
            {"sprite": "bar_01","x": 0,"y": 0,"loop": false,"timing": 0,"delay": 0,"toTopObj":1, "anchor":[0,0]},
            {"sprite": "logo_01","x": 20,"y": 35,"loop": false,"timing": 0,"delay": 0.1,"toTopObj":2, "anchor":[0,0]},
            {"sprite": "no-pictures","x": 300,"y": 200,"loop": false,"timing": 0,"delay": 0,"toTopObj":1, "anchor":[0.5,0],"scale":0.5},
          //  {"sprite": "btn_sp_1","x": 540,"y": 112,"loop": false,"timing": 0,"delay": 2, "toTopObj":1, "anchor":[0,0], "onClickFn":"audioReplay()"}
              ],
        "text_animations": [
            {"text": [{"content":"Pre Issuance Verification Call"}], "sx": -1000,"sy": 50,"x": 190 ,"y": 50,"size": 28,"weight":"bold","color":"#003869","tween_type": "Elastic.easeOut","timing": 200,"delay":0.2, "anchor":[0,0] },
          
             // {"text": [
             //              {"content":"In Case if you wish to hear again, Click here ---->"}
             //                        ], "sx": -1000,"sy": 120,"x": 290 ,"y": 120,"size": 12,"weight":"bold","color":"#235291","tween_type": "Elastic.easeOut","timing": 200,"delay":2, "anchor":[0,0],"wordWrap": true, "wordWrapWidth":520,"align":"right" },


            {"text": [
                    {"content":"Camera is not accessible. Please enable the camera and try again!"}
                ], "sx": -1000,"sy": 340,"x": 300 ,"y": 340,"size": 22,"weight":"bold","color":"#575755","tween_type": "Elastic.easeOut","timing": 200,"delay":0.5, "anchor":[0.5,0], "wordWrap": true, "wordWrapWidth":500,"align":"center" }
        ],
        "functions": [
            {"fn": "SetBGTile('bg_02')", "delay": 0},
            {"fn": "reloadPage()", "delay" : 25 }
        ],
        "name": "Camera Error Page",
        "timing": -1,
        "index": 18
    },




];





for(var i=0; i<screens_eng.length; i++ )
{
    if(i==5)
    {
        if(window.pa_PREMIUM_POLICY_TYPE!==false)
        {
            if(window.pa_PREMIUM_POLICY_TYPE==='regular')
            {
                console.log("Enter : regular");
                screens_eng[i]['sound_list'] = [{"sound": ["au_5_1", "$var.window.pa_product", "au_5_2", "$var.window.pa_category", "au_5_3", "$var.window.pa_PREMIUM_POLICY_TYPE", "au_5_4", "$var.currency_window.p_PREMIUM_AMOUNT", "$var.window.pa_FREQUENCY", "au_5_5", "$var.number_window.pa_PAYMENT_TERM", "years", "au_5_6", "$var.number_window.pa_BENEFIT_TERM", "au_5_7", "$var.currency_window.p_SUM_ASSURED", "au_5_8"]}];
            }
            else if(window.pa_PREMIUM_POLICY_TYPE==='single')
            {
                console.log("Enter : single");
                screens_eng[i]['sound_list'] = [{"sound": ["au_5_1", "$var.window.pa_product", "au_5_2", "$var.window.pa_category", "au_5_3", "$var.window.pa_PREMIUM_POLICY_TYPE", "au_5_4_1", "$var.currency_window.p_PREMIUM_AMOUNT", "au_5_6", "$var.number_window.pa_BENEFIT_TERM", "au_5_7", "$var.currency_window.p_SUM_ASSURED", "au_5_8"]}];
            }
        }
    }
    window.stage.screens.push(screens_eng[i]);
}
